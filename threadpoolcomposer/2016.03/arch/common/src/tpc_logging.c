//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
//! @file	tpc_logging.c
//! @brief	Logging helper implementation. Initialization for debug output.
//! @authors	J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
//!
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>
#include <sys/syscall.h>
#include <sys/types.h>

#include "tpc_logging.h"
#include "gen_queue.h"
#include "gen_stack.h"

#ifndef NDEBUG
static unsigned long int libtpc_logging_level = ULONG_MAX;
static FILE *libtpc_logging_file = NULL;

static struct gq_t log_q;
static volatile bool exiting = false;
static pthread_t log_thread;
static struct gs_t log_s = { NULL};

#define LOG_MSG_S_SZ						4096
struct log_msg_t {
	char msg[256];
	tpc_ll_t lvl;
	struct timespec tv;
	pid_t tid;
};

static inline struct log_msg_t *get_msg()
{
	struct log_msg_t *msg = (struct log_msg_t *) gs_pop(&log_s);
	if (! msg) msg = malloc(sizeof(*msg));
	assert(msg);
	return msg;
}

void tpc_log(tpc_ll_t const level, char *fmt, ...)
{
	if (!level || (level & libtpc_logging_level)) {
		struct log_msg_t *lm = get_msg();
		clock_gettime(CLOCK_MONOTONIC, &lm->tv);
		va_list ap;
		va_start(ap, fmt);
		vsnprintf(lm->msg, sizeof(lm->msg) - 1, fmt, ap);
		va_end(ap);
		lm->lvl = level;
		lm->tid = syscall(SYS_gettid);
		gq_enqueue(&log_q, lm);
	}
}

static inline void handle_msg(struct log_msg_t *m)
{
	fprintf(libtpc_logging_file ? libtpc_logging_file : stderr, 
		m->lvl ? (m->lvl ^ 1 ?
			"%lld\t%d\t[libtpc - info]\t%s" :
			"%lld\t%d\t[libtpc - warning]\t%s") :
			"%lld\t%d\t [libtpc - error]\t%s",
		m->tv.tv_sec * 1000000000LL + m->tv.tv_nsec, m->tid, m->msg);
	gs_push(&log_s, m);
}

static void *log_thread_main(void *p)
{
	struct gq_t *q = (struct gq_t *)p;
	void *lm;
	while (! exiting) {
		unsigned long n = 10;
		while (n-- && (lm = gq_dequeue(q)))
			handle_msg((struct log_msg_t *) lm);
		usleep(1000000);
	}
	// flush the queue
	while ((lm = gq_dequeue(q)))
		handle_msg((struct log_msg_t *) lm);
	return NULL;
}

int tpc_logging_init(void)
{
	static int is_initialized = 0;
	if (! is_initialized) {
		char const *dbg = getenv("LIBTPC_DEBUG");
		char const *lgf = getenv("LIBTPC_LOGFILE");
		libtpc_logging_level = dbg ? (strtoul(dbg, NULL, 0) | 0x1) : ULONG_MAX;
		libtpc_logging_file = lgf ? fopen(lgf, "w+") : stderr;
		if (! libtpc_logging_file) {
			libtpc_logging_file = stderr;
			WRN("could not open logfile '%s'!\n", lgf);
		}

		gq_init(&log_q);
		for (int i = 0; i < LOG_MSG_S_SZ; ++i) {
			void *msg = malloc(sizeof(struct log_msg_t));
			assert (msg);
			gs_push(&log_s, msg);
		}
		if (pthread_create(&log_thread, NULL, log_thread_main, &log_q))
			ERR("could not create the logging thread");
	}
	return 1;
}

void tpc_logging_exit(void)
{
	struct log_msg_t *lm;
	exiting = true;
	if (log_thread) pthread_join(log_thread, NULL);
	gq_destroy(&log_q);
	while ((lm = (struct log_msg_t *) gs_pop(&log_s)))
		free(lm);

	if (libtpc_logging_file != stderr) {
		fflush(libtpc_logging_file);
		fclose(libtpc_logging_file);
	}
	libtpc_logging_file = NULL;
}
#else  // NDEBUG
int tpc_logging_init(void) { return 1; }
void tpc_logging_exit(void) {}
void tpc_log(tpc_ll_t const level, char *fmt, ...) {}
#endif // NDEBUG
