# Architecture: baseline
Proof-of-concept implementation of the most simple conceivable hardware
threadpool implementation based on the AXI4-bus. Useful as baseline for future,
optimized architectures as well as for demonstration of the basic implementation
of an Architecture for *ThreadpoolComposer*.

## Building the TPC API library
Simply type `make` in this directory, and a directory `lib` should be created
which will contain the `.so` files.

