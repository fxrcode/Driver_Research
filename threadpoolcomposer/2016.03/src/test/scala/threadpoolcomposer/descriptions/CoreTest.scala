//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     CoreTest.scala
 * @brief    Unit tests for Core description file.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import org.scalatest._
import java.nio.file._

class CoreSpec extends FlatSpec with Matchers {
  val jsonPath = Paths.get("json-examples").toAbsolutePath

  "A missing Core file" should "throw an exception" in {
    a [Exception] should be thrownBy {
      CoreBuilder.fromFile(jsonPath.resolve("missing.json"))
    }
  }

  "A correct Core file" should "be parsed to Some(Core)" in {
    CoreBuilder.fromFile(jsonPath.resolve("correct-core.json")) should not be empty
  }
  
  "A correct Core file" should "be parsed correctly" in {
    val oc = CoreBuilder.fromFile(jsonPath.resolve("correct-core.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("TestCore")
    c.version should equal ("0.1")
    c.id should equal (42)
    c.description should equal ("A correct core description.")
    c.zipPath.toFile.exists should be (true)
    c.averageClockCycles should equal (Some(1234567890))
  }
  
  "A Core file with unknown entries" should "be parsed correctly" in {
    val oc = CoreBuilder.fromFile(jsonPath.resolve("unknown-core.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("TestCore")
    c.version should equal ("0.1")
    c.id should equal (42)
    c.description should equal ("A correct core description.")
    c.zipPath.toFile.exists should be (true)
  }

  "An invalid Core file" should "not be parsed" in {
    val oc1 = CoreBuilder.fromFile(jsonPath.resolve("invalid-core1.json"))
    val oc2 = CoreBuilder.fromFile(jsonPath.resolve("invalid-core2.json"))
    val oc3 = CoreBuilder.fromFile(jsonPath.resolve("invalid-core3.json"))
    val oc4 = CoreBuilder.fromFile(jsonPath.resolve("invalid-core4.json"))
    oc1 shouldBe empty
    oc2 shouldBe empty
    oc3 shouldBe empty
    oc4 shouldBe empty
  }
}
