//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     CompositionTest.scala
 * @brief    Unit tests for Composition description file.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import org.scalatest._
import java.nio.file._

class CompositionSpec extends FlatSpec with Matchers {
  val jsonPath = Paths.get("json-examples").toAbsolutePath

  "A missing Composition file" should "throw an exception" in {
    a [Exception] should be thrownBy {
      CompositionBuilder.fromFile(jsonPath.resolve("missing.json"))
    }
  }

  "A correct Composition file" should "be parsed to Some(Composition)" in {
    CompositionBuilder.fromFile(jsonPath.resolve("correct-composition.json")) should not be empty
  }

  "A correct Composition file" should "be parsed correctly" in {
    val oc = CompositionBuilder.fromFile(jsonPath.resolve("correct-composition.json"))
    lazy val c: Composition = oc.get
    oc should not be empty
    c.description should equal ("Test")
    c.composition.length should equal (2)
    c.composition(0).kernel should equal ("sudoku")
    c.composition(0).count should be (1)
    c.composition(1).kernel should be ("warraw")
    c.composition(1).count should be (2)
  }

  "A Composition file with unknown entries" should "be parsed correctly" in {
    val oc = CompositionBuilder.fromFile(jsonPath.resolve("unknown-composition.json"))
    lazy val c: Composition = oc.get
    oc should not be empty
    c.description should equal ("Test")
    c.composition.length should equal (2)
    c.composition(0).kernel should equal ("sudoku")
    c.composition(0).count should be (1)
    c.composition(1).kernel should be ("warraw")
    c.composition(1).count should be (2)
  }

  "A Composition file with invalid entries" should "not be parsed" in {
    val oc1 = CompositionBuilder.fromFile(jsonPath.resolve("invalid-count1-composition.json"))
    val oc2 = CompositionBuilder.fromFile(jsonPath.resolve("invalid-count2-composition.json"))
    val oc3 = CompositionBuilder.fromFile(jsonPath.resolve("invalid-count3-composition.json"))
    oc1 shouldBe empty
    oc2 shouldBe empty
    oc3 shouldBe empty
  }
}
