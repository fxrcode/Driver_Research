//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     PlatformTest.scala
 * @brief    Unit tests for Platform description file.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import org.scalatest._
import java.nio.file._

class PlatformSpec extends FlatSpec with Matchers {
  val jsonPath = Paths.get("json-examples").toAbsolutePath

  "A missing Platform file" should "throw an exception" in {
    a [Exception] should be thrownBy {
      PlatformBuilder.fromFile(jsonPath.resolve("missing.json"))
    }
  }

  "A correct Platform file" should "be parsed to Some(Platform)" in {
    PlatformBuilder.fromFile(jsonPath.resolve("correct-platform.json")) should not be empty
  }
  
  "A correct Platform file" should "be parsed correctly" in {
    val oc = PlatformBuilder.fromFile(jsonPath.resolve("correct-platform.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("zynq")
    c.tclLibrary should equal (jsonPath.resolve("zynq.tcl"))
    c.part should equal ("xc7z045ffg900-2")
    c.boardPart should equal ("xilinx.com:zc706:part0:1.1")
    c.boardPreset should equal ("ZC706")
    c.targetUtilization should equal (55)
    c.supportedFrequencies should contain inOrderOnly (250, 200, 150, 100, 42)
  }
  
  "An Platform file with unknown entries" should "be parsed correctly" in {
    val oc = PlatformBuilder.fromFile(jsonPath.resolve("unknown-platform.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("zynq")
    c.tclLibrary should equal (jsonPath.resolve("zynq.tcl"))
    c.part should equal ("xc7z045ffg900-2")
    c.boardPart should equal ("xilinx.com:zc706:part0:1.1")
    c.boardPreset should equal ("ZC706")
    c.targetUtilization should equal (55)
    c.supportedFrequencies should contain inOrderOnly (250, 200, 150, 100, 42)
  }

  "An Platform file without a name" should "not be parsed" in {
    val oc1 = PlatformBuilder.fromFile(jsonPath.resolve("invalid-platform1.json"))
    val oc2 = PlatformBuilder.fromFile(jsonPath.resolve("invalid-platform2.json"))
    oc1 shouldBe empty
    oc2 shouldBe empty
  }
}
