//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     KernelTest.scala
 * @brief    Unit tests for Kernel description file.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import org.scalatest._
import java.nio.file._

class KernelSpec extends FlatSpec with Matchers {
  val jsonPath = Paths.get("json-examples").toAbsolutePath

  "A missing Kernel file" should "throw an exception" in {
    a [Exception] should be thrownBy {
      KernelBuilder.fromFile(jsonPath.resolve("missing.json"))
    }
  }

  "A correct Kernel file" should "be parsed to Some(Kernel)" in {
    KernelBuilder.fromFile(jsonPath.resolve("correct-kernel.json")) should not be empty
  }
  
  "A correct Kernel file" should "be parsed correctly" in {
    val oc = KernelBuilder.fromFile(jsonPath.resolve("correct-kernel.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("sudoku")
    c.topFunction should equal ("sudoku_solve")
    c.files should equal (Seq(jsonPath.resolve("src/Sudoku.cpp"), jsonPath.resolve("src/Sudoku_HLS.cpp")))
    c.testbenchFiles should equal (Seq(jsonPath.resolve("src/main.cpp"), jsonPath.resolve("hard_sudoku.txt"), jsonPath.resolve("hard_sudoku_solution.txt")))
    c.compilerFlags should equal (Seq())
    c.testbenchCompilerFlags should equal (Seq("-lrt"))
    c.args.length should equal (1)
    c.args(0).name should equal ("grid")
    c.args(0).passingConvention should equal (ByReference)
    c.otherDirectives should equal (Some(jsonPath.resolve("sudoku.dir")))
  }
  
  "A Kernel file with unknown entries" should "be parsed correctly" in {
    val oc = KernelBuilder.fromFile(jsonPath.resolve("correct-kernel.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("sudoku")
    c.topFunction should equal ("sudoku_solve")
    c.files should equal (Seq(jsonPath.resolve("src/Sudoku.cpp"), jsonPath.resolve("src/Sudoku_HLS.cpp")))
    c.testbenchFiles should equal (Seq(jsonPath.resolve("src/main.cpp"), jsonPath.resolve("hard_sudoku.txt"), jsonPath.resolve("hard_sudoku_solution.txt")))
    c.compilerFlags should equal (Seq())
    c.testbenchCompilerFlags should equal (Seq("-lrt"))
    c.args.length should equal (1)
    c.args(0).name should equal ("grid")
    c.args(0).passingConvention should equal (ByReference)
    c.otherDirectives should equal (Some(jsonPath.resolve("sudoku.dir")))
  }

  "An invalid Kernel file" should "not be parsed" in {
    val oc1 = KernelBuilder.fromFile(jsonPath.resolve("invalid-kernel1.json"))
    val oc2 = KernelBuilder.fromFile(jsonPath.resolve("invalid-kernel2.json"))
    val oc3 = KernelBuilder.fromFile(jsonPath.resolve("invalid-kernel3.json"))
    oc1 shouldBe empty
    oc2 shouldBe empty
    oc3 shouldBe empty
  }
}
