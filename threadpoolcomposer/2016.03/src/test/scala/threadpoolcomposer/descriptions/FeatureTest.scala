//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     FeatureTest.scala
 * @brief    Unit tests for Features.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import org.scalatest._
import java.nio.file._

class ConfigurationFeatureSpec extends FlatSpec with Matchers {
  val jsonPath = Paths.get("json-examples").toAbsolutePath.resolve("configTest")

  "LED Feature" should "be parsed correctly" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-led.json"))
    lazy val c = oc.get
    oc should not be empty
    c.platformFeatures.length shouldBe (1)
    c.platformFeatures(0) shouldEqual (LEDFeature(true))
  }

  "OLED Feature" should "be parsed correctly" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-oled.json"))
    lazy val c = oc.get
    oc should not be empty
    c.platformFeatures.length shouldBe (1)
    c.platformFeatures(0) shouldEqual (OLEDFeature(true))
  }

  "Cache Feature" should "be parsed correctly" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-cache.json"))
    lazy val c = oc.get
    oc should not be empty
    c.platformFeatures.length shouldBe (1)
    c.platformFeatures(0) shouldEqual (CacheFeature(true, 262144, 4))
  }

  "Invalid cache configurations" should "be ignored" in {
    lazy val oc1 = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-invalid-cache1.json"))
    lazy val oc2 = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-invalid-cache2.json"))
    lazy val c1 = oc1.get
    lazy val c2 = oc2.get
    oc1 should not be empty
    oc2 should not be empty
    c1.platformFeatures.length shouldBe (0)
    c2.platformFeatures.length shouldBe (0)
  }

  "Debug Feature" should "be parsed correctly" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-debug.json"))
    val oc2 = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-debug2.json"))
    lazy val c = oc.get
    lazy val c2 = oc2.get
    oc should not be empty
    oc2 should not be empty
    c.platformFeatures.length shouldBe (1)
    c.platformFeatures(0) shouldEqual (DebugFeature(true, Some(4096), Some(1), Some(false), Some(Seq("*interrupt", "*HP0*", "*GP*"))))
    c2.platformFeatures.length shouldBe (1)
    c2.platformFeatures(0) shouldEqual (DebugFeature(true, None, None, None, None))
  }

  "Invalid debug configurations" should "be ignored" in {
    lazy val oc1 = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-invalid-debug1.json"))
    lazy val oc2 = ConfigurationBuilder.fromFile(jsonPath.resolve("platform-invalid-debug2.json"))
    lazy val c1 = oc1.get
    lazy val c2 = oc2.get
    oc1 should not be empty
    oc2 should not be empty
    c1.platformFeatures.length shouldBe (0)
    c2.platformFeatures.length shouldBe (0)
  }
}
