//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     ConfigurationTest.scala
 * @brief    Unit tests for Configuration description file.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import org.scalatest._
import java.nio.file._

class ConfigurationSpec extends FlatSpec with Matchers {
  val jsonPath = Paths.get("json-examples").toAbsolutePath

  "A missing Configuration file" should "throw an exception" in {
    a [Exception] should be thrownBy {
      ConfigurationBuilder.fromFile(jsonPath.resolve("missing.json"))
    }
  }

  "A correct Configuration file" should "be parsed to Some(Configuration)" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("configTest/config.json"))
    oc should not be empty
  }
  
  "A correct Configuration file" should "be parsed correctly" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("configTest/config.json"))
    lazy val c = oc.get
    oc should not be empty
    c.architectures.map(_.name).toSet should contain only ("Arch1", "Arch3")
    c.platforms.map(_.name).toSet should contain only ("Plat1", "Plat2")
    c.kernels.map(_.name).toSet should contain only ("Kern2", "Kern3")
    println("Compositions = " + c.compositions)
    c.compositions.map(_.id).filter(_.endsWith("test.bd")).length should be (1)
  }
  
  "An invalid Configuration file" should "not be parsed" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("invalid-config.json"))
    oc shouldBe empty
  }

  "A Configuration file" should "be overriden by direct args" in {
    val oc = ConfigurationBuilder.fromArgs(Seq("configFile",
      jsonPath.resolve("configTest/config.json").toString,
      "architecture", "Arch2",
      "platform", "Plat3",
      "kernel", "Kern1",
      "composition", jsonPath.resolve("configTest/test2.bd").toString))
    lazy val c = oc.get
    oc should not be empty
    c.architectures.map(_.name) should contain only ("Arch2")
    c.platforms.map(_.name) should contain only ("Plat3")
    c.kernels.map(_.name) should contain only ("Kern1")
    c.compositions.map(_.id).filter(_.endsWith("test2.bd")).length should be (0)
  }

  "Inline composition" should "be parsed correctly" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("configTest/config-inline-compo.json"))
    lazy val c = oc.get
    oc should not be empty
    c.architectures.map(_.name).toSet should contain only ("Arch1", "Arch3")
    c.platforms.map(_.name).toSet should contain only ("Plat1", "Plat2")
    println("Compositions = " + c.compositions)
    c.compositions.length should be (1)
    c.compositions.head.composition.map(ce => ce.kernel) should equal (Seq("Kern1", "Kern2"))
    c.compositions.head.composition.map(ce => ce.count) should equal (Seq(12, 3))
  }

  "Inline composition" should "be added to list of compositions" in {
    val oc = ConfigurationBuilder.fromFile(jsonPath.resolve("configTest/config-inline-compo2.json"))
    lazy val c = oc.get
    oc should not be empty
    c.architectures.map(_.name).toSet should contain only ("Arch1", "Arch3")
    c.platforms.map(_.name).toSet should contain only ("Plat1", "Plat2")
    println("Compositions = " + c.compositions)
    c.compositions.length should be (3)
  }
}
