//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     ArchitectureTest.scala
 * @brief    Unit tests for Architecture description file.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import org.scalatest._
import java.nio.file._

class ArchitectureSpec extends FlatSpec with Matchers {
  val jsonPath = Paths.get("json-examples").toAbsolutePath

  "A missing Architecture file" should "throw an exception" in {
    a [Exception] should be thrownBy {
      ArchitectureBuilder.fromFile(jsonPath.resolve("missing.json"))
    }
  }

  "A correct Architecture file" should "be parsed to Some(Architecture)" in {
    ArchitectureBuilder.fromFile(jsonPath.resolve("correct-arch.json")) should not be empty
  }
  
  "A correct Architecture file" should "be parsed correctly" in {
    val oc = ArchitectureBuilder.fromFile(jsonPath.resolve("correct-arch.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("baseline")
    c.tclLibrary should equal (jsonPath.resolve("baseline.tcl"))
    c.valueArgTemplate should equal (jsonPath.resolve("valuearg.directives.template"))
    c.referenceArgTemplate should equal (jsonPath.resolve("referencearg.directives.template"))
  }
  
  "An Composition file with unknown entries" should "be parsed correctly" in {
    val oc = ArchitectureBuilder.fromFile(jsonPath.resolve("unknown-arch.json"))
    lazy val c = oc.get
    oc should not be empty
    c.name should equal ("baseline")
    c.tclLibrary should equal (jsonPath.resolve("baseline.tcl"))
    c.valueArgTemplate should equal (jsonPath.resolve("valuearg.directives.template"))
    c.referenceArgTemplate should equal (jsonPath.resolve("referencearg.directives.template"))
  }

  "An Architecture file without a name" should "not be parsed" in {
    val oc = ArchitectureBuilder.fromFile(jsonPath.resolve("invalid-arch.json"))
    oc shouldBe empty
  }

  "An Architecture file with missing files" should "be parsed as None" in {
    val oc = ArchitectureBuilder.fromFile(jsonPath.resolve("missing-file-arch.json"))
    oc shouldBe empty
  }
}
