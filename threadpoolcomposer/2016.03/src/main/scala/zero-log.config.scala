//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import com.dongxiguo.zeroLog._
import com.dongxiguo.zeroLog.formatters.SimpleFormatter
import com.dongxiguo.zeroLog.context._
import com.dongxiguo.fastring.Fastring
import com.dongxiguo.fastring.Fastring.Implicits._
import com.dongxiguo.zeroLog.appenders.ConsoleAppender
import java.util.Calendar
import java.io.FileWriter
import scala.util.Properties.{lineSeparator => NL}

final object ColorConsoleAppender extends Appender {
  final private val colorReplacements = List(
    ("""SEVERE:""".r, Console.RED),
    ("""WARNING:""".r, Console.MAGENTA),
    ("""CONFIG:""".r, Console.BLUE),
    ("""INFO:""".r, Console.GREEN),
    ("""FINE:|FINER:|FINEST:""".r, Console.CYAN)
  )

  final private def colorize(l: String): String =
    colorReplacements.foldLeft(l){ (ln, x) => x._1.replaceAllIn(ln,
      m => "[" + x._2 + m.toString().dropRight(1) + Console.RESET + "]") }

  override final def append(message: Fastring) {
    val ml = message.toString.lines.toList
    println(Console.UNDERLINED + ml.head + Console.RESET)
    ml.tail.map { l =>
      println(colorize(l))
      _fileWriter.map(_.append(l).append(NL).flush)
    }
  }

  private var _logFile: Option[String] = None
  private var _fileWriter: Option[FileWriter] = None

  def logFile = _logFile

  def logFile_=(p: Option[String]) = {
    _logFile = p
    if (!p.isEmpty) {
      if (! _fileWriter.isEmpty)
        _fileWriter.get.close
      _fileWriter = Some(new FileWriter(p.get, false))
    }
  }
}

object ZeroLoggerFactory {
  final def newLogger(singleton: Singleton): (Logger, Formatter, Appender) =
    (Filter.Info, SimpleFormatter, ColorConsoleAppender)
}
