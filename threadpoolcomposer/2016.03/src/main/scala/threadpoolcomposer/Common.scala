//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Common.scala
 * @brief   Common classes and tools for the GenerateX classes.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import descriptions._
import util.Properties
import scala.io.Source
import java.nio.file._
import scala.sys.process._

/**
 * Contains the most basic common methods, e.g., to parse description files.
 **/
object Common {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  lazy val homeDir: Path                      = Paths.get(sys.env("TPC_HOME")).toAbsolutePath
  lazy val commonDir: Path                    = Paths.get(sys.env("TPC_HOME")).resolve("common").toAbsolutePath
  lazy val targetDesignFrequency: Option[Int] = sys.env.get("TPC_FREQ") map (_.toInt)

  def getVivadoVersion: String = try {
      Seq("vivado", "-version", "-nolog", "-nojournal").!!
    } catch { case ex: Exception =>
      throw new Exception("Could not find 'vivado' in path, please make sure you sourced the scripts.")
    }

  def checkEnvironment(needVivado: Boolean = false) = {
    if (sys.env.get("TPC_HOME").isEmpty)
      throw new Exception("Environment variable 'TPC_HOME' not set - please set to TPC base directory.")
    if (needVivado) logger.info("Vivado version: " + getVivadoVersion)
  }

  def getFiles(dir: java.io.File): Array[java.io.File] =
    if (dir != null && dir.exists && dir.listFiles != null)
      dir.listFiles ++ dir.listFiles.filter(_.isDirectory).flatMap(getFiles)
    else
      Array()

  def getSubDirs(path: Path): Array[Path] =
    path.toFile.listFiles.filter(_.isDirectory) map (d => Paths.get(d.toString))

  def vlnvFromZip(path: Path): VLNV = {
    assert(path.toFile.exists)
    import java.io._
    import java.util.zip._
    try {
      val zip = new ZipInputStream(new BufferedInputStream(new FileInputStream(path.toFile)))
      var zipEntry = zip.getNextEntry()
      var vlnv: String = ""

      while (zipEntry != null && !zipEntry.toString().endsWith("component.xml"))
        zipEntry = zip.getNextEntry()

      if (zipEntry != null) {
        val xml = scala.xml.XML.load(zip)
        val vendor: String = ((xml \ "vendor") map (e => e.text)).head
        val library: String = ((xml \ "library") map (e => e.text)).head
        val name: String = ((xml \ "name") map (e => e.text)).head
        val ver: String = ((xml \ "version") map (e => e.text)).head
        vlnv = List(vendor, library, name, ver).mkString(":")
      } else throw new Exception("Could not find component.xml in " + path)
      zip.close()
      VLNV(vlnv)
    } catch {
      case x: Exception => throw new Exception("Could not read VLNV from " + path.toString + ": " + x.toString)
    }
  }

  def findKernelDescForKernel(name: String)(implicit cfg: Configuration): Kernel = {
    val kds = cfg.kernels.filter (k => k.name.equals(name))
    if (kds.length != 1)
      throw new Exception("Found " + kds.length + " kernel descriptions for '" + name + "'.")
    kds.head
  }

  def findCoreDesc(name: String, target: Target)(implicit cfg:Configuration): Core = {
    val searchee = "'" + name + "^" + target + "'"
    val coreDir = cfg.outputDir(name, target)
    logger.finest("scanning directory " + coreDir + " for " + searchee)
    def cores = getFiles(coreDir.toFile)
      .filter (_.getName.toString equals "core.description")
      .flatMap (f => CoreBuilder.fromFile(Paths.get(f.toString)))
    if (! coreDir.toFile.exists || cores.length == 0) {
      val gob = ConfigurationBuilder.fromConfiguration(cfg)
      gob.architectures = Some(List(target.ad.name))
      gob.platforms = Some(List(target.pd.name))
      gob.kernels = Some(List(name))
      new VivadoHighLevelSynthesis()(gob.build(cfg.descPath)).execute
    }
    if (! coreDir.toFile.exists)
      throw new Exception("Cannot find core.description for " + searchee + " and could not build one via HLS, aborting")
    if (cores.length != 1)
      logger.warning("Found " + cores.length + " matching core.descriptions: " + cores + " using only the first: " + cores.head)
    cores.head
  }
}
