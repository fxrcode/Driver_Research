//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     PowerReport.scala
 * @brief    Model for parsing and evaluating power reports in Vivado format.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.reports
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.{Common => TpcCommon, Target}
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions._
import  java.nio.file.{Path, Paths}
import  scala.io.Source

/** Co-Simulation Report model. **/
final case class PowerReport(
    file: Path,
    totalOnChipPower: Option[Double],
    dynamicPower: Option[Double],
    staticPower: Option[Double],
    confidenceLevel: Option[String]
  )

object PowerReport {
  import util.Properties.{lineSeparator => NL}
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)
  private val STR_TOCP = "Total On-Chip Power (W)"
  private val STR_DYNP = "Dynamic (W)"
  private val STR_STAP = "Device Static (W)"
  private val STR_CONF = "Confidence Level"
  private val STR_FILTERS = Seq(STR_TOCP, STR_DYNP, STR_STAP, STR_CONF)

  /** Returns a list of all power reports for the given Target.
    * @param target Target Architecture and Platform
    * @return list of file paths **/
  def findAll(target: Target)(implicit cfg: Configuration): Seq[Path] =
    TpcCommon.getFiles(cfg.coreDir.toFile)
      .filter (_.toString.endsWith(".*power.rpt"))
      .filter (_.toString.contains(target.ad.name))
      .filter (_.toString.contains(target.pd.name))
      .sortBy (_.toString)
      .map    (f => Paths.get(f.toString).toAbsolutePath)

  /** Finds a power report file for the given Kernel and Target.
    * @param kd Kernel description
    * @param target Architecture and Platform target
    * @return Path to file (if found, first if multiple), None otherwise **/
  def find(kd: Kernel, target: Target)(implicit cfg: Configuration): Option[PowerReport] = {
    val fs = findAll(target) filter (f => f.toString.endsWith("power.rpt")) map (_.toString)
    logger.finest("Looking for " + ("power.rpt") + " in " +
        cfg.coreDir.toString + ": " + fs.length + " results: " + NL + fs.mkString(NL))
    if (fs.length == 0)
      logger.finer("Could not find power report for " + kd.name + " and " + target)
    fs.headOption.flatMap(p => PowerReport(Paths.get(p)))
  }


  /** Finds a power report file for the given Core and Target.
    * @param cd Core description
    * @param target Architecture and Platform target
    * @return Path to file (if found, first if multiple), None otherwise **/
  def find(cd: Core, target: Target)(implicit cfg: Configuration): Option[PowerReport] = {
    val fs = findAll(target) filter (f => f.toString.endsWith("power.rpt")) map (_.toString)
    logger.finest("Looking for " + "power.rpt" + " in " +
        cfg.coreDir.toString + ": " + fs.length + " results: " + NL + fs.mkString(NL))
    if (fs.length == 0)
      logger.finer("Could not find power report for " + cd.name + " and " + target)
    fs.headOption.flatMap(p => PowerReport(Paths.get(p)))
  }

  /** Extract min, max and average clock cycles from the power report (if available). **/
  private def extract(sr: Path): Option[PowerReport] = try {
      val matches =
          Source.fromFile(sr.toString)
            .getLines
            .map (_.split("\\|") map (_.trim))
            .filter (l => l.length > 2 && STR_FILTERS.contains(l(1)))
            .map (l => (l(1), l(2)))
      Some(PowerReport(sr,
        (matches filter (l => l._1.equals(STR_TOCP)) toSeq).headOption map(_._2.toDouble),
        (matches filter (l => l._1.equals(STR_DYNP)) toSeq).headOption map(_._2.toDouble),
        (matches filter (l => l._1.equals(STR_STAP)) toSeq).headOption map(_._2.toDouble),
        (matches filter (l => l._1.equals(STR_CONF)) toSeq).headOption map(_._2)
      ))
    } catch { case e: Exception => { 
      logger.warning(Seq("Could not extract power data from ", sr, ": ", e) mkString)
      None
    }}

  /** Produce PowerReport instance from file. **/
  def apply(sr: Path): Option[PowerReport] = extract(sr)
}
