//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     SynthesisReport.scala
 * @brief    Model for parsing and evaluating synthesis reports in XML format
 *           (see common/ip_report.xml.template for an example).
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.reports
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.{Common => TpcCommon, AreaEstimate, ResourcesEstimate,
                                                   TimingEstimate, Target}
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions._
import  java.nio.file.{Path, Paths}

/** Synthesis Report model. **/
final case class SynthesisReport(file: Path, area: Option[AreaEstimate], timing: Option[TimingEstimate])

object SynthesisReport {
  import util.Properties.{lineSeparator => NL}
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  /** Returns a list of all synthesis reports for the given Target.
    * @param target Target Architecture and Platform
    * @return list of file paths **/
  def findAll(target: Target)(implicit cfg: Configuration): Seq[Path] =
    TpcCommon.getFiles(cfg.coreDir.toFile)
      .filter (_.toString.endsWith("_export.xml"))
      .filter (_.toString.contains(target.ad.name))
      .filter (_.toString.contains(target.pd.name))
      .sortBy (_.toString)
      .map    (f => Paths.get(f.toString).toAbsolutePath)

  /** Finds a synthesis report file for the given Kernel and Target.
    * @param kd Kernel description
    * @param target Architecture and Platform target
    * @return Path to file (if found, first if multiple), None otherwise **/
  def find(kd: Kernel, target: Target)(implicit cfg: Configuration): Option[SynthesisReport] = {
    val fs = findAll(target) filter (f => f.toString.endsWith(kd.topFunction + "_export.xml")) map (_.toString)
    logger.finest("Looking for " + (kd.topFunction + "_export.xml") + " in " +
        cfg.coreDir.toString + ": " + fs.length + " results: " + NL + fs.mkString(NL))
    if (fs.length == 0)
      logger.finer("Could not find synthesis report for " + kd.name + " and " + target)
    fs.headOption.map(p => SynthesisReport(Paths.get(p)))
  }

  /** Finds a synthesis report file for the given Core and Target.
    * @param cd Core description
    * @param target Architecture and Platform target
    * @return Path to file (if found, first if multiple), None otherwise **/
  def find(cd: Core, target: Target)(implicit cfg: Configuration): Option[SynthesisReport] = {
    val fs = findAll(target) filter (f => f.toString.endsWith(cd.name + "_export.xml")) map (_.toString)
    logger.finest("Looking for " + (cd.name + "_export.xml") + " in " +
        cfg.coreDir.toString + ": " + fs.length + " results: " + NL + fs.mkString(NL))
    if (fs.length == 0)
      logger.finer("Could not find synthesis report for " + cd.name + " and " + target)
    fs.headOption.map(p => SynthesisReport(Paths.get(p)))
  }

  /** Extracts the area estimation from the given synthesis report file.
    * @param sr Path to file
    * @return AreaEstimate if successful, None otherwise **/
  def extractArea(sr: Path): Option[AreaEstimate] = try {
      val xml = scala.xml.XML.loadFile(sr.toAbsolutePath.toString)
      (for (e <- List("Resources", "AvailableResources")) yield {
        val slice: Integer = ((xml \\ "AreaReport" \\ e \\ "SLICE") text).toInt
        val lut: Integer = ((xml \\ "AreaReport" \\ e \\ "LUT") text).toInt
        val ff: Integer = ((xml \\ "AreaReport" \\ e \\ "FF") text).toInt
        val dsp: Integer = ((xml \\ "AreaReport" \\ e \\ "DSP") text).toInt
        val bram: Integer = ((xml \\ "AreaReport" \\ e \\ "BRAM") text).toInt
        new ResourcesEstimate(slice, lut, ff, dsp, bram)
      }).grouped(2).map(x => new AreaEstimate(x.head, x.tail.head)).toList.headOption
    } catch { case e: Exception => None }

  /** Extracts the timing estimation from the given synthesis report file.
    * @param sr Path to file
    * @return TimingEstimate if successful, None otherwise **/
  def extractTiming(sr: Path): Option[TimingEstimate] = try {
      val xml = scala.xml.XML.loadFile(sr.toAbsolutePath.toString)
      Some(TimingEstimate(
        ((xml \\ "TimingReport" \\ "AchievedClockPeriod") text).toDouble,
        ((xml \\ "TimingReport" \\ "TargetClockPeriod") text).toDouble))
    } catch { case e: Exception => None }

  /** Produce SynthesisReport instance from file. **/
  def apply(sr: Path): SynthesisReport = SynthesisReport(sr, extractArea(sr), extractTiming(sr))
}
