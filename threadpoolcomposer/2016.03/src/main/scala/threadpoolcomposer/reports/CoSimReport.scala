//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     CoSimReport.scala
 * @brief    Model for parsing and evaluating co-simulation reports in Vivado HLS format.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.reports
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.{Common => TpcCommon, Target}
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions._
import  java.nio.file.{Path, Paths}
import  scala.io.Source

final case class ClockCycles(min: Int, avg: Int, max: Int)

/** Co-Simulation Report model. **/
final case class CoSimReport(file: Path, latency: ClockCycles, interval: ClockCycles)

object CoSimReport {
  import util.Properties.{lineSeparator => NL}
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  /** Returns a list of all co-simulation reports for the given Target.
    * @param target Target Architecture and Platform
    * @return list of file paths **/
  def findAll(target: Target)(implicit cfg: Configuration): Seq[Path] =
    TpcCommon.getFiles(cfg.coreDir.toFile)
      .filter (_.toString.endsWith("_cosim.rpt"))
      .filter (_.toString.contains(target.ad.name))
      .filter (_.toString.contains(target.pd.name))
      .sortBy (_.toString)
      .map    (f => Paths.get(f.toString).toAbsolutePath)

  /** Finds a co-simulation report file for the given Kernel and Target.
    * @param kd Kernel description
    * @param target Architecture and Platform target
    * @return Path to file (if found, first if multiple), None otherwise **/
  def find(kd: Kernel, target: Target)(implicit cfg: Configuration): Option[CoSimReport] = {
    val fs = findAll(target) filter (f => f.toString.endsWith(kd.topFunction + "_cosim.rpt")) map (_.toString)
    logger.finest("Looking for " + (kd.topFunction + "_cosim.rpt") + " in " +
        cfg.coreDir.toString + ": " + fs.length + " results: " + NL + fs.mkString(NL))
    if (fs.length == 0)
      logger.finer("Could not find co-simulation report for " + kd.name + " and " + target)
    fs.headOption.flatMap(p => CoSimReport(Paths.get(p)))
  }


  /** Finds a co-simulation report file for the given Core and Target.
    * @param cd Core description
    * @param target Architecture and Platform target
    * @return Path to file (if found, first if multiple), None otherwise **/
  def find(cd: Core, target: Target)(implicit cfg: Configuration): Option[CoSimReport] = {
    val fs = findAll(target) filter (f => f.toString.endsWith(cd.name + "_cosim.rpt")) map (_.toString)
    logger.finest("Looking for " + (cd.name + "_cosim.rpt") + " in " +
        cfg.coreDir.toString + ": " + fs.length + " results: " + NL + fs.mkString(NL))
    if (fs.length == 0)
      logger.finer("Could not find co-simulation report for " + cd.name + " and " + target)
    fs.headOption.flatMap(p => CoSimReport(Paths.get(p)))
  }

  /** Extract min, max and average clock cycles from the co-simulation report (if available). **/
  private def extractClockCycles(sr: Path): Option[(ClockCycles, ClockCycles)] = try {
      Source.fromFile(sr.toString)
            .getLines
            .map (_.split("\\|").map(_.trim))
            .filter (l => l.length > 2 && "Pass".equals(l(2)))
            .map (l => (ClockCycles(l(3).toInt, l(4).toInt, l(5).toInt),
                        ClockCycles(l(6).toInt, l(7).toInt, l(8).toInt)))
            .toSeq.headOption
    } catch { case e: Exception => { 
      logger.warning(Seq("Could not extract clock cycles from ", sr, ": ", e) mkString)
      None
    }}

  /** Produce CoSimReport instance from file. **/
  def apply(sr: Path): Option[CoSimReport] = {
    val cc = extractClockCycles(sr)
    if (cc.isEmpty) logger.warning(Seq("Failed to read co-sim report ", sr) mkString)
    cc map (c => CoSimReport(sr, c._1, c._2))
  }
}
