//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import descriptions._

object ThreadPoolComposer {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  def main(args: Array[String]) {
    val ok = try {
        if (args.length == 0)
          throw new Exception("Missing arguments, usage: <command> <args>")
        Common.checkEnvironment(! Seq("corestats", "itpc").contains(args.head))
  
        implicit val cfg = ConfigurationBuilder.fromArgs(args.tail).get
        ColorConsoleAppender.logFile = cfg.logFile.map(_.toString)
        Command(args.head).execute
      } catch { case ex: Exception =>
        logger.severe(ex)
        false
      }
    if (! ok) {
      logger.severe("TPC finished with errors")
      sys.exit(1)
    }
  }
}
