//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import descriptions._
import itpc._

trait Command {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)
  def execute: Boolean
}

object Command {
  def apply(command: String)(implicit cfg: Configuration): Command = {
   command.toLowerCase match {
      case "addcore" => new AddCore
      case "compose" => new Compose
      case "corestats" => new CoreStatistics
      case "hls" => new VivadoHighLevelSynthesis
      case "itpc" => new InteractiveThreadPoolComposer
      case "import" => new Import
      case cmd => throw new Exception("Unknown command '" + cmd + "'")
    }
  }
}
