//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     Compose.scala
 * @brief    Threadpool composition task.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import descriptions._
import dse._
import reports._
import java.io.FileWriter
import java.nio.file._
import util.Properties
import scala.concurrent.{Future, future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.io._
import scala.sys.process._
import scala.util.{Success, Failure}
import scala.util.matching.Regex

/**
 * The Compose class orchestrates the generation of a hardware design for a given
 * threadpool composition. It will generate a Vivado project in the configured output directory, 
 * create the entire hardware design automatically and launch simulation or bitstream generation
 * (depending on TPC_MODE env var). If design space exploration (DSE) is activated, it will
 * generate list of feasible composition and design frequency combinations (ordered by a
 * throughput-oriented metric) and iterate over it until bitstream generation succeeded.
 *
 * Note that the cross-product of selected Platforms and Architectures in the Configuration will
 * be run in parallel, i.e., there will be one thread per Platform/Architecture combination.
 **/
final protected class Compose()(implicit cfg: Configuration) extends Command {
  import util.Properties.{lineSeparator => NL}
  private val WNS_RE          = new Regex("""\s*WNS[^\n]*\n[^\n]*\n\s*(-?\d+\.\d+)""", "wns")
  val SLACK_THRESHOLD: Double = -0.3

  /**
   * @defgroup publicinterface Public interface
   * @{
   **/

  def execute: Boolean = {
    val summary = "Run Summary:" + NL + cfg
    logger.info(cfg.toString)

    // first, collect all kernels and trigger HLS if not built yet
    val kds = cfg.compositions map (b => b.composition) reduce (_++_) map (_.kernel) toSet

    val generateHLS: Seq[Future[java.io.File]] =
      for (k <- kds.toSeq; pd <- cfg.platforms; ad <- cfg.architectures; target = Target(ad, pd))
        yield future(Common.findCoreDesc(k, target).zipPath.toFile)

    generateHLS map (fr => Await.result(fr, Duration.Inf))

    // then start actual bitstream generation
    val generateResults: Seq[Future[Boolean]] =
      for (bd <- cfg.compositions; pd <- cfg.platforms; ad <- cfg.architectures; t = Target(ad, pd))
        yield future(try { generate(bd, t) } catch { case ex: Exception => logger.severe(ex.toString); false })

    generateResults
      .map { fr => Await.result(fr, Duration.Inf) }
      .reduce (_&&_)
  }
  /** @} **/

  /**
   * @defgroup privateinterface Private methods
   * @{
   **/

  /** Instantiates design space and executes Vivado. **/
  private def generate(bd: Composition, target: Target)(implicit cfg: Configuration): Boolean = {
    val dseMode = cfg.designSpaceExploration match {
      case DesignSpaceExplorationModeFrequency => DesignSpaceDimensions(true, false, false)
      case DesignSpaceExplorationModeAreaFrequency => DesignSpaceDimensions(true, true, true)
      case DesignSpaceExplorationModeNone => DesignSpaceDimensions(false, false, false)
      case _ => DesignSpaceDimensions(true, true, true)
    }

    // instantiate design space
    val ds = new DesignSpace(bd, target, new Heuristics.ThroughputHeuristic, dseMode)

    if (! cfg.dryRun.isEmpty) {
      val csv = ds.dump(",", true)
      println(csv)
      try { new FileWriter(cfg.dryRun.get + "_" + target + ".csv").append(csv).close(); true }
      catch { case e: Exception => false }
    } else {
      val dsSize = ds.enumerate.length
      // log progress during DSE
      def logProgress(ds: Seq[Any], composition: Composition, f: Double) = logger.info(Seq(
          Seq("DSE progress: ", dsSize - ds.length, "/", dsSize, ", current composition:").mkString,
          composition,
          Seq("Frequency = ", f).mkString
        ).mkString(NL))
      // recursive run execution
      def run(dse: Seq[(Composition, Heuristics.Frequency, Double)]): Boolean = dse.headOption match {
        case Some((bd, f, _)) => {
            logProgress(dse.tail, bd, f)
            val wns = perform(bd, target, f)
            // compute new design space element, iff frequency may be varied
            if (ds.dim.frequency && wns < SLACK_THRESHOLD && wns != Double.NegativeInfinity) {
              val newFreq = 1000.0 / ((1000.0 / f) - wns)
              val newElem = (bd, newFreq, ds.heuristic(bd, newFreq, target))
              logger.info("Adding new element to design space: " + newElem)
              // check if we're only varying the frequency
              if (!ds.dim.utilization && !ds.dim.alternatives)
                // in this case filter all higher frequencies (unlikely to succeed)
                run ((newElem +: (dse.tail filter (_._2 <= newFreq))) sortBy (_._3) reverse)
              else
                // continue in design space, alternative cores may succeed
                run((newElem +: dse.tail) sortBy (_._3) reverse)
            } else wns >= SLACK_THRESHOLD || run (dse.tail)
          }
        case _ => false
      }
      val result = run(ds.enumerate)
      if (! result) logger.severe("No working design found, DSE exhausted search space.")
      result
    }
  }

  /** Produces the Tcl dictionary for the given composition and the IP catalog setup code for Vivado. **/
  private def composition(bd: Composition, target: Target): String = {
    val elems =
    for (ce <- bd.composition;
         cd = Common.findCoreDesc(ce.kernel, target);
         vl = Common.vlnvFromZip(Paths.get(cd.zipPath.toString)))
      yield (ce.kernel, ce.count, cd, cd.zipPath, vl)
    val zipPaths = elems map (_._4.getParent.toString) toSet
    val repoPaths = 
      "set_property IP_REPO_PATHS \"[pwd]/user_ip " + Common.commonDir + "\" [current_project]" + NL +
      "file delete -force [pwd]/user_ip" + NL +
      "file mkdir [pwd]/user_ip" + NL +
      "update_ip_catalog" + NL +
      elems.map(_._4.toString).map(zp => "update_ip_catalog -add_ip " + zp + " -repo_path ./user_ip").mkString(NL) + NL +
      "update_ip_catalog" + NL

    repoPaths + (for (i <- 0 until elems.length) yield
      List(
        //"update_ip_catalog -add_ip {" + elems(i)._4.toString + "} -repo_path [pwd]/[tpc::get_generate_mode]",
        "dict set kernels {" + i + "} vlnv {" + elems(i)._5 + "}",
        "dict set kernels {" + i + "} count {" + elems(i)._2 + "}",
        "dict set kernels {" + i + "} id {" + elems(i)._3.id + "}",
	""
     ).mkString(NL)).mkString(NL)
  }

  /** Produces the header section of the main Tcl file, containing several global vars. **/
  private def makeHeader(bd: Composition, target: Target): String =
    "set TPC_PLATFORM_HEADER {" + target.pd.harness.getOrElse("missing") + " " +
    target. pd.api.getOrElse("missing") + "}" + NL +
    "set TPC_SIM_MODULE " + target.pd.testbenchTemplate.getOrElse("missing") + NL +
    "set TPC_BOARD_PRESET " + target.pd.boardPreset + NL +
    (cfg.platformFeatures.map { f => new FeatureTclPrinter("platform").toTcl(f) } mkString NL) +
    (cfg.architectureFeatures.map { f => new FeatureTclPrinter("architecture").toTcl(f) } mkString NL)

  /**
   * Creates the main Tcl script and runs Vivado.
   **/
  private def perform(bd: Composition, target: Target, f: Double = 0): Double = {
    // create output directory
    val outdir = cfg.outputDir(bd, target)
    outdir.toFile.mkdirs
    val runName = "'" + bd.id + "' with " + target + "[F=" + f + "]"
    // needles for template
    val needles: scala.collection.mutable.Map[String, String] = scala.collection.mutable.Map(
      "PROJECT_NAME" -> "[tpc::get_generate_mode]",
      "BITSTREAM_NAME" -> bd.id,
      "HEADER" -> (makeHeader(bd, target) + NL + "set tpc_freq " + f + NL),
      "TESTBENCH_MODULE" -> target.pd.testbenchTemplate.toString,
      "PRELOAD_FILES" -> "",
      "PART" -> target.pd.part,
      "BOARD_PART" -> target.pd.boardPart,
      "BOARD_PRESET" -> target.pd.boardPreset,
      "PLATFORM_TCL" -> target.pd.tclLibrary.toString,
      "ARCHITECTURE_TCL" -> target.ad.tclLibrary.toString,
      "COMPOSITION" -> composition(bd, target)
    )

    // write Tcl script
    Template.interpolateFile(
      Template.DEFAULT_NEEDLE,
      Common.commonDir.resolve("design.master.tcl.template").toString,
      outdir.resolve(target.pd.name + ".tcl").toString,
      needles)

    val logFile = outdir.resolve(bd.id + ".log")
    logger.info("Starting run " + runName + ": output in " + logFile)

    // ignore everything
    val io = new ProcessIO(stdin => {stdin.close()}, stdout => {stdout.close()}, stderr => {stderr.close()})

    // execute Vivado (max runtime: 1 day)
    val p = Process(Seq("timeout", "1d", "vivado", "-mode", "batch", "-source",
        outdir.resolve(target.pd.name + ".tcl").toString, "-notrace",
	"-nojournal", "-log", logFile.toString),
	outdir.toFile).run(io)
    var r = p.exitValue()

    if (r == 124) {
      logger.severe("Vivado timeout for " + runName)
      Double.NegativeInfinity
    } else if (r != 0) {
      logger.severe("Vivado finished with non-zero exit code: " + r + " for " + runName)
      Double.NegativeInfinity
    } else {
      // check for timing failure
      val timingReport = logFile.resolveSibling("timing.txt").toString
      val wns: Double = WNS_RE.findFirstMatchIn(Source.fromFile(timingReport).getLines.mkString("\n")) match {
        case Some(m) => m.group(1).toDouble
        case _ => throw new Exception("Could not parse timing report '" + timingReport + "'!")
      }
      if (wns < SLACK_THRESHOLD)
        logger.severe("Vivado finished, but did not achieve timing closure for " + runName + ", WNS: " + wns)
      else
        logger.info("Vivado finished successfully for " + runName + ", WNS: " + wns)
      wns
    }
  }
  /** @} **/
}

