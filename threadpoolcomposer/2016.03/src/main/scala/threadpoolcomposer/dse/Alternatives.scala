//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     Alternatives.scala
 * @brief    One dimension for design space exploration are variants or alternatives of
 *           the same kernel, i.e., IP cores that compute the same function using the
 *           same interface, but with different implementations. This can be useful to
 *           evaluate area/performance trade-offs.
 *           This class provides helpers to identify alternatives for cores using their
 *           id; every core with the same id is assumed to implement the same function.
 *           Methods in this class can compute sets of alternatives for cores as well
 *           as all alternatives for an entire composition.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.dse
import de.tu_darmstadt.cs.esa.threadpoolcomposer.{Common => TpcCommon, Target}
import de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions._
import de.tu_darmstadt.cs.esa.threadpoolcomposer.reports.SynthesisReport
import java.nio.file.Paths

object Alternatives {
  implicit val (logger, appender, formatter) = ZeroLoggerFactory.newLogger(this)

  /** Returns the ID of a given kernel name.
    * @param name Name of the kernel
    * @return ID if found, None otherwise **/
  def idForName(name: String)(implicit cfg: Configuration): Option[Int] = {
    val ks = cfg.kernels.filter(_.name == name).headOption.map(_.id)
    lazy val cs = cfg.cores.filter(_.name == name).headOption.map(_.id)
    if (ks.orElse(cs).isEmpty) logger.warning("Could not find id for '" + name + "'")
    ks.orElse(cs)
  }

  /** Returns all kernel and core names for a given id.
    * @param id Id of the kernel
    * @return set of all names of kernels and cores with id **/
  def namesForId(id: Int)(implicit cfg: Configuration): Set[String] =
    (cfg.kernels.filter(_.id == id).map(_.name) ++ cfg.cores.filter(_.id == id).map(_.name)).toSet

  /** Returns the names of all kernels synthesized for the given Target.
    * @param id Id of the kernel
    * @param target Filter cores/kernels not available on this Target
    * @return set of names of kernels and cores which exist on Target **/
  def namesForId(id: Int, target: Target)(implicit cfg: Configuration): Set[String] = namesForId(id) filter { n =>
      try { ! SynthesisReport.find(TpcCommon.findCoreDesc(n, target), target).isEmpty }
      catch { case e: Exception => false }
    }

  /** Returns alternative kernel names for given kernel on Target.
    * @param kernel Name of the kernel
    * @param target Target Architecture and Platform
    * @return Set of alternative names for kernel on Target **/
  def alternatives(kernel: String, target: Target)(implicit cfg: Configuration): Set[String] = 
    idForName(kernel) map (namesForId(_, target)) getOrElse(Set())

 /** Returns alternative compositions on given Target.
   * @param bd Original Composition 
   * @param target Target for the Composition
   * @return List of alternative compositions with same counts and all alternative
             kernel combinations (beware of combinatorial explosion!) **/
 def alternatives(bd: Composition, target: Target)(implicit cfg: Configuration): Seq[Composition] = {
    def combine[A](xs: Traversable[Traversable[A]]): Seq[Seq[A]] =
      xs.foldLeft(Seq(Seq.empty[A])) { (x,y) => for (a <- x.view; b <- y) yield a :+ b }

    for (ces <- combine(bd.composition map (ce => alternatives(ce.kernel, target) map (CompositionEntry(_, ce.count)))))
      yield Composition(Paths.get(bd.id), ces, "generated alternative composition")
  }
}
