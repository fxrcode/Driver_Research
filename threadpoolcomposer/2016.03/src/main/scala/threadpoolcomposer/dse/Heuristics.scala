//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     Heuristics.scala
 * @brief    Heuristic functions for the automated design space exploration.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.dse
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.{Common => TpcCommon, Target}
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions._
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.reports._

object Heuristics {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)
  type Frequency = Double
  type Heuristic = (Composition, Frequency, Target) => Double

  final class ThroughputHeuristic(implicit cfg: Configuration) extends Heuristic {
    private def findAverageClockCycles(kernel: String, target: Target): Int = {
      val cd = TpcCommon.findCoreDesc(kernel, target)
      cd.averageClockCycles.getOrElse {
        val report = CoSimReport.find(cd, target)
        if (report.isEmpty)
          logger.warning("Core description does not contain 'averageClockCycles' and " +
              "co-simulation report could not be found, assuming one-cycle execution: " +
              kernel + " [" + cd.descPath + "]")
        report map (_.latency.avg) getOrElse (1)
      }
    }

    def apply(bd: Composition, freq: Double, target: Target): Double = {
      val maxClockCycles = bd.composition map (ce => findAverageClockCycles(ce.kernel, target))
      val t = 1.0 / (freq * 1000000.0)
      val t_irq = target.pd.interruptLatency.getOrElse(0) / 1000000000.0
      val t_setup = target.pd.minimalStartLatency.getOrElse(0) / 1000000000.0
      val jobsla = maxClockCycles map (_ * t + t_irq + t_setup)
      bd.composition.map(_.count) zip jobsla map (x => x._1 / x._2) reduce (_+_)
    }
  }
}
