//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     DesignSpace.scala
 * @brief    The DesignSpace class models the discrete design space for TPC hardware
 *           designs. Essentially, it provides a list of configurations consisting of
 *           a composition and target frequency, which can then be passed to the 
 *           Compose task to generate a bitstream. Compose can automatically iterate
 *           over the design space in case of failure, thus performing a simple design
 *           space exploration (DSE). DesignSpace offers helper methods to define and
 *           span the design space one is interested in. At the moment there are three
 *           basic dimensions: Frequency, Utilization (i.e., number of instances) and
 *           Alternatives. Variation of the frequency is straightforward; variation of
 *           the utilization modifies the number of instances proportionally (keeping
 *           at least one instance of each core) to optimize the area utilization.
 *           Alternatives considers all kernels with the same ID as alternative
 *           implementations of the same computation and generates all compositions
 *           with all available alternatives. Finally, a heuristic is applied to order
 *           the design space; the heuristic function should encode the optimization
 *           goal in a ordering.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.dse
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.{Common => TpcCommon, Target}
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions._
import  de.tu_darmstadt.cs.esa.threadpoolcomposer.reports.{SynthesisReport}
import  java.nio.file.{Paths}
import  Alternatives._
import  Heuristics._

final case class DesignSpaceDimensions(
    frequency: Boolean,
    utilization: Boolean,
    alternatives: Boolean
  )

object DesignSpace {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)
  val defaultDim = DesignSpaceDimensions(true, true, true)
}

class DesignSpace(
    bd: Composition,
    target: Target,
    val heuristic: Heuristic,
    val dim: DesignSpaceDimensions = DesignSpace.defaultDim
  )(implicit cfg: Configuration) {
  import DesignSpace._
  import util.Properties.{lineSeparator => NL}
  logger.finer(Seq("DesignSpace(", dim, ")") mkString)

  private def feasibleFreqs(bd: Composition): Seq[Double] = if (dim.frequency) {
    val cores = bd.composition map (ce => TpcCommon.findCoreDesc(ce.kernel, target));
    val srs   = cores flatMap (SynthesisReport.find(_, target))
    val cps   = srs flatMap (_.timing) map (_.clockPeriod)
    val fmax  = 1000.0 / cps.max
    target.pd.supportedFrequencies map (_.toDouble) filter (_ <= fmax) sortWith (_>_)
  } else TpcCommon.targetDesignFrequency.map(_.toDouble).map(Seq(_)).getOrElse {
    throw new Exception("Fixed frequency mode, but environment variable TPC_FREQ is not set.")
  }

  /**
   * Computes the set of feasible compositions for a given base composition.
   * The given composition defines the ratios of the kernels, each kernel will be
   * instantiated at least once.
   **/
  private def feasibleCompositions(bd: Composition): Seq[Composition] = if (dim.utilization) {
    var counts        = bd.composition.map(_.count)
    val totalCount    = counts.reduce(_+_)
    val minCounts     = counts.map(n => (n / counts.min).toInt)
    val weights       = counts.map(_ / totalCount)
    val cores         = bd.composition map (ce => TpcCommon.findCoreDesc(ce.kernel, target));
    val srs           = cores flatMap (SynthesisReport.find(_, target))
    val areaEstimates = srs flatMap (_.area)
    val targetUtil    = target.pd.targetUtilization.toDouble
    logger.finer("target util = " + targetUtil)

    // check if there is any feasible composition
    if (minCounts.reduce(_+_) > target.pd.slotCount)
      throw new Exception("Composition infeasible! Exceeds maximal slot count of " + target.pd.slotCount
          + " for " + target + "." + NL + bd)

    def areaUtil(counts: Seq[Int]) = (areaEstimates zip counts) map (a => a._1 * a._2) reduce (_+_)

    val currUtil = areaUtil(counts).utilization
    val df = (targetUtil - currUtil) / currUtil
    counts = counts map (n => n + n * df) map (_.toInt)
    
    logger.finer("counts = " + counts + " minCounts = " + minCounts + " currUtil = " + currUtil)

    // iterate over counts: reduce largest count stepwise 
    val seqs = for {
        i <- 0 to (counts.max - minCounts.max);
        f = (counts.max - i) / counts.max.toDouble;
	currCounts = counts map (n => f * n) map (_.toInt);
	if currCounts.min > 0 && currCounts.reduce(_+_) <= target.pd.slotCount && areaUtil(currCounts).isFeasible
      } yield currCounts

    logger.finest("number of feasible counts: " + seqs.length)
    if (seqs.length == 0)
      logger.warning("No feasible composition found; please check starting composition ratios: " + NL + bd)

    // make sequence of CompositionEntries
    val ces = for {
        s <- seqs filter (_.reduce(_+_) <= target.pd.slotCount)
      } yield bd.composition.map(_.kernel) zip s map (x => CompositionEntry(x._1, x._2))

    // make full composition
    ces map (Composition(Paths.get(bd.id), _, "Generated composition."))
  } else Seq(bd)

  private def feasibleAlternatives(bd: Composition): Seq[Composition] =
    if (dim.alternatives) alternatives(bd, target) else Seq(bd)

  private def compositions(bd: Composition): Seq[Composition] =
    feasibleAlternatives(bd) map (feasibleCompositions(_)) reduce (_++_)

  lazy val enumerate: Seq[(Composition, Frequency, Double)] = (for {
      bd <- compositions(bd);
      f  <- feasibleFreqs(bd)
    } yield (bd, f, heuristic(bd, f, target))) sortBy (_._3) reverse

  // TODO remove this and move to proper dumping helper class
  def dumpC(bd: Composition, sep: String = "; "): String = bd.composition.map(ce => Seq(ce.kernel, " x ", ce.count).mkString).mkString(sep)
  def dump(sep: String = ",", header: Boolean = true): String = 
      (if (header) Seq("Composition", "Frequency", "H").mkString(sep) + NL else "") +
      (enumerate map (p => Seq(dumpC(p._1), p._2, p._3) mkString sep)).mkString(NL)
}
