//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     AreaEstimate.scala
 * @brief    Model of FPGA area estimate.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.e)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import util.Properties.{lineSeparator => NL}

class ResourcesEstimate(
    val SLICE: Integer,
    val LUT: Integer,
    val FF: Integer,
    val DSP: Integer,
    val BRAM: Integer
  ) {
  override def toString: String = List(
    "AreaEstimate: ",
    "  SLICE: " + BRAM,
    "  LUT  : " + LUT,
    "  FF   : " + FF,
    "  DSP  : " + DSP,
    "  BRAM : " + BRAM
  ).mkString(NL)

  def *(n: Integer) = new ResourcesEstimate(
      SLICE * n,
      LUT * n,
      FF * n,
      DSP * n,
      BRAM * n
    )

  def +(r: ResourcesEstimate) = new ResourcesEstimate(
      SLICE + r.SLICE,
      LUT + r.LUT,
      FF + r.FF,
      DSP + r.DSP,
      BRAM + r.BRAM
    )
}

class AreaEstimate(val resources: ResourcesEstimate, val available: ResourcesEstimate) {
  private val formatter = new java.text.DecimalFormat("#.#")

  val slice = resources.SLICE * 100.0 / available.SLICE
  val lut = resources.LUT * 100.0 / available.LUT
  val ff = resources.FF * 100.0 / available.FF
  val dsp = resources.DSP * 100.0 / available.DSP
  val bram = resources.BRAM  * 100.0 / available.BRAM
  val utilization = slice

  override def toString: String = List(
    "AreaEstimate: ",
    "  SLICE: " + resources.SLICE + " / " + available.SLICE + " (" + formatter.format(slice) + "%)",
    "  LUT: " + resources.LUT + " / " + available.LUT + " (" + formatter.format(lut) + "%)",
    "  FF: " + resources.FF + " / " + available.FF + " (" + formatter.format(ff) + "%)",
    "  DSP: " + resources.DSP + " / " + available.DSP + " (" + formatter.format(dsp) + "%)",
    "  BRAM: " + resources.BRAM + " / " + available.BRAM + " (" + formatter.format(bram) + "%)",
    "  Utilization: " + formatter.format(utilization) + "%"
  ).mkString(NL)

  def *(n: Integer) = new AreaEstimate(resources * n, available)
  def +(a: AreaEstimate) = new AreaEstimate(resources + a.resources, available)
  def isFeasible: Boolean = List(slice, lut, ff, dsp, bram).map(x => x <= 100.0).reduce(_&&_)
}
