//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     VLNV.scala
 * @brief    Model for Version-Library-Vendor-Version string identifier.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import scala.util.matching.Regex

sealed case class Version(major: Int, minor: Int, revision: Option[Int]) {
  override def toString =
    if (revision.isEmpty) List(major, minor).mkString(".")
    else                  List(major, minor, revision).mkString(".")
}

sealed case class VLNV(vendor: String, library: String, name: String, version: Version) {
  override def toString = List(vendor, library, name, version).mkString(":")
}

object Version {
  private val VERSION_REGEX = new Regex("""(\d+).(\d+)(.(\d+))?""")
  def apply(version: String): Version = version match {
    case VERSION_REGEX(major, minor, _, revision) => Version(major.toInt, minor.toInt, Option(revision).map(_.toInt))
    case invalid => throw new Exception("Invalid version string: " + invalid)
  }
}

object VLNV {
  val VLNV_REGEX = new Regex("([^:]+):([^:]+):([^:]+):([^:]+)", "vendor", "library", "name", "version")
  def apply(vlnv: String): VLNV = vlnv match {
    case VLNV_REGEX(vendor, library, name, version) => VLNV(vendor, library, name, Version(version))
    case invalid => throw new Exception("Invalid VLNV string: " + invalid)
  }
}
