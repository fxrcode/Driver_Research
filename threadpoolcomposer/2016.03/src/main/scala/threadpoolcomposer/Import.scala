//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     Import.scala
 * @brief    Task to bulk-import IP cores given in a comma-separated values file.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import  descriptions._
import  reports._
import  scala.io.Source
import  java.nio.file.{Path, Paths}
import  scala.concurrent.{Future, future, Await}
import  scala.concurrent.ExecutionContext.Implicits.global
import  scala.concurrent.duration._


final protected class Import(implicit cfg: Configuration) extends Command {
  import util.Properties.{lineSeparator => NL}
  private case class ImportCoreTask(zip: Path, id: Int, Description: Option[String], target: Target, runtime: Option[Int])
  private val headerLine = Seq("Zip", "ID", "Description", "Architecture", "Platform", "Avg Runtime (clock cycles)")

  def execute: Boolean = {
    val summary = "Run Summary:" + NL + cfg
    logger.info(summary)

    val csvFile = cfg.zipFile.getOrElse { throw new Exception("Missing parameter: Use 'zipFile' to pass CSV import list.") }

    if (! checkCsv(csvFile.toString))
      throw new Exception ("File '" + csvFile + "' is not in the expected format; use CSV with this header: " +
        NL + headerLine.mkString(", "))

    val imports: Seq[Future[Boolean]] = for { task <- readCsv(csvFile.toString) } yield future(importCore(task))
    if (imports.length == 0) throw new Exception("Found no valid import tasks in file.")
    imports map (fr => Await.result(fr, Duration.Inf)) reduce (_&&_)
  }

  private def checkCsv(fn: String): Boolean = {
    ((Source.fromFile(fn).getLines.toSeq)(0).split("""\s*,\s*""") zip headerLine) map (p => p._1.equals(p._2)) reduce (_&&_)
  }

  private def readCsv(fn: String): Seq[ImportCoreTask] = for {
      line <- Source.fromFile(fn).getLines.toSeq.tail;
      fields = line.split("""\s*,\s*""");
      target <- Target.fromString(fields(3), fields(4))
    } yield ImportCoreTask(Paths.get(fields(0)), fields(1).toInt, if (fields(2).isEmpty) None else Some(fields(2)),
        target, if (fields(5).isEmpty) None else Some(fields(5).toInt))

  private def importCore(task: ImportCoreTask) = {
    // get VLNV from the file
    val vlnv = Common.vlnvFromZip(task.zip)
    logger.finest("found VLNV in zip " + task.zip + ": " + vlnv)
    // extract version and name from VLNV, create Core
    val cd = Core(
        Paths.get("."),
        Paths.get("ipcore").resolve(vlnv.name + "_" + task.target.ad.name + ".zip"),
        vlnv.name,
        task.id,
        vlnv.version.toString,
        "",
        task.runtime
      )

    // write core.description to output directory (as per config)
    val p = cfg.outputDir(cd, task.target).resolve("core.description")
    if (! p.toFile.exists) {
      p.getParent.toFile.mkdirs
      CoreBuilder.toFile(cd, p)

      // add link to original .zip in the 'ipcore' subdir
      val linkp = cfg.outputDir(cd, task.target).resolve(cd.zipPath.toString)
      if (! linkp.toFile.equals(task.zip.toAbsolutePath.toFile)) {
        linkp.getParent.toFile.mkdirs
        if (linkp.toFile.exists)
          linkp.toFile.delete()
        try {
          java.nio.file.Files.createSymbolicLink(linkp, task.zip.toAbsolutePath)
        } catch { case ex: java.nio.file.FileSystemException => {
          logger.warning("cannot create link " + linkp + " -> " + task.zip + ", copying data")
          java.nio.file.Files.copy(task.zip, linkp, java.nio.file.StandardCopyOption.REPLACE_EXISTING)
        }}
      }

      // finally, evaluate the ip core and store the report with the link
      val period = 1000.0 / task.target.pd.supportedFrequencies.sortWith(_>_).head
      val report = cfg.outputDir(cd, task.target).resolve("ipcore").resolve(cd.name + "_export.xml")
      try {
        val hls_report = SynthesisReport.find(cd, task.target)(cfg)
        java.nio.file.Files.createSymbolicLink(report, hls_report.get.file.toAbsolutePath)
        true
      } catch { case _: Exception => // report not found: evaluate
        new EvaluateIP(task.zip, period, task.target.pd.part, report).evaluate
      }
    } else logger.info("File '" + p + "' already exists, skipping task " + task)
    true
  }
}

