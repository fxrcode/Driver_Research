//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     AddCore.scala
 * @brief    Task to add an existing IP core to the TPC catalog. Will perform
 *           evaluation of the core with the current configuration parameters
 *           (i.e., it will perform evaluation for all configured Architectures
 *           and Platforms).
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import  descriptions._
import  reports._
import  scala.concurrent.{Future, future, Await}
import  scala.concurrent.ExecutionContext.Implicits.global
import  scala.concurrent.duration._
import  java.nio.file.{Path, Paths}

final protected class AddCore(implicit cfg: Configuration) extends Command {
  import util.Properties.{lineSeparator => NL}

  def execute: Boolean = {
    val summary = "Run Summary:" + NL + cfg
    logger.info(summary)

    if (cfg.zipFile.isEmpty || ! cfg.zipFile.get.toFile.exists)
      throw new Exception(Seq("Missing .zip file, or file does not exist (", cfg.zipFile.getOrElse(""), ")") mkString)

    val addCores: Seq[Future[Boolean]] =
      for (pd <- cfg.platforms; ad <- cfg.architectures; t = Target(ad, pd))
        yield future(try { addCore(cfg.zipFile.get, t) } catch { case ex: Exception => logger.severe(ex); false })

    addCores map(fr => Await.result(fr, Duration.Inf)) reduce (_&&_)
  }

  private def addCore(zip: Path, target: Target): Boolean = {
    // get VLNV from the file
    val vlnv = Common.vlnvFromZip(zip)
    logger.finest("found VLNV in zip " + zip + ": " + vlnv)
    // extract version and name from VLNV, create Core
    val cd = Core(
        Paths.get("."),
        Paths.get("ipcore").resolve(vlnv.name + "_" + target.ad.name + ".zip"),
        vlnv.name,
        cfg.id,
        vlnv.version.toString,
        "",
        cfg.averageClockCycles
    )

    // write core.description to output directory (as per config)
    val p = cfg.outputDir(cd, target).resolve("core.description")
    p.getParent.toFile.mkdirs
    CoreBuilder.toFile(cd, p)

    // add link to original .zip in the 'ipcore' subdir
    val linkp = cfg.outputDir(cd, target).resolve(cd.zipPath.toString)
    if (! linkp.toFile.equals(zip.toAbsolutePath.toFile)) {
      linkp.getParent.toFile.mkdirs
      if (linkp.toFile.exists)
        linkp.toFile.delete()
      try {
        java.nio.file.Files.createSymbolicLink(linkp, zip.toAbsolutePath)
      } catch { case ex: java.nio.file.FileSystemException => {
        logger.warning("cannot create link " + linkp + " -> " + zip + ", copying data")
        java.nio.file.Files.copy(zip, linkp, java.nio.file.StandardCopyOption.REPLACE_EXISTING)
      }}
    }

    // finally, evaluate the ip core and store the report with the link
    val period = 1000.0 / target.pd.supportedFrequencies.sortWith(_>_).head
    val report = cfg.outputDir(cd, target).resolve("ipcore").resolve(cd.name + "_export.xml")
    try {
      val hls_report = SynthesisReport.find(cd, target)(cfg)
      java.nio.file.Files.createSymbolicLink(report, hls_report.get.file.toAbsolutePath)
      true
    } catch { case _: Exception => // report not found: evaluate
      new EvaluateIP(zip, period, target.pd.part, report).evaluate
    }
  }
}
