//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Feature.scala
 * @brief   TPC Architecture / Platform features.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import de.tu_darmstadt.cs.esa.threadpoolcomposer.itpc.query._

sealed abstract class Feature(enabled: Boolean)

case class CacheFeature(enabled: Boolean, size: Int, associativity: Int) extends Feature(enabled)
case class LEDFeature(enabled: Boolean) extends Feature(enabled)
case class OLEDFeature(enabled: Boolean) extends Feature(enabled)
case class DebugFeature(enabled: Boolean, depth: Option[Int], stages: Option[Int], useDefaults: Option[Boolean], nets: Option[Seq[String]]) extends Feature(enabled)

object CacheFeature {
  val supportedSizes = Seq(32768, 65536, 131072, 262144, 524288)
  val sizeQE: QueryExecutor[Int] = new MenuOneQueryExecutor(Some("Cache: Size"), supportedSizes)
  val assocQE: QueryExecutor[Int] = new MenuOneQueryExecutor(Some("Cache: Associativity"), Seq(2, 4))
  def query = {
    val en = Query[Boolean]("Cache: Enabled").query()
    if (en) CacheFeature(en,
        Query[Int]("Cache: Size")(sizeQE).query(),
        Query[Int]("Cache: Associativity")(assocQE).query()
      )
    else CacheFeature(en, 32768, 2)
  }
}

object LEDFeature {
  def query = LEDFeature(Query[Boolean]("LED: Enabled").query())
}

object OLEDFeature {
  def query = OLEDFeature(Query[Boolean]("OLED: Enabled").query())
}

object DebugFeature {
  val supportedDepths = List(1024, 2048, 4096, 8192, 16384)
  val ddQE: QueryExecutor[Int] = new MenuOneQueryExecutor(Some("Debug: Data Depth"), supportedDepths)
  val oddQE: QueryExecutor[Option[Int]] = new MenuOptionQueryExecutor("Debug: Data Depth")(ddQE)
  val psQE: QueryExecutor[Int] = new MenuOneQueryExecutor(Some("Debug: Pipeline Stages"), 0 until 6)
  val opsQE: QueryExecutor[Option[Int]] = new MenuOptionQueryExecutor("Debug: Pipeline Stages")(psQE)
  def query = {
    val enabled = Query[Boolean]("Debug: Enabled").query()
    if (enabled) DebugFeature(
        enabled,
        Query[Option[Int]]("Debug: Data Depth")(oddQE).query(),
        Query[Option[Int]]("Debug: Pipeline Stages")(opsQE).query(),
        None, // no value for use defaults, defined in Tcl
        None // no custom nets; add manually
      )
   else DebugFeature(enabled, None, None, None, None)
  }
}
