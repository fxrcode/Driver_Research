//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Builder.scala
 * @brief   Generic Builder class: build object of given type.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._

abstract class Builder[T <: Description] {
  def build(descPath: Path): T
}

object DescriptionBuilder {
  def fromFile[T <: Description, B <: Builder[T]](descPath: Path)
    (implicit r: Reads[B]): Option[T] = 
    Description.fromFile[B](descPath).map(_.build(descPath))

  def toFile[T <: Description](t: T, descPath: Path)
    (implicit w: Writes[T]) = Description.toFile[T](t, descPath)
}
