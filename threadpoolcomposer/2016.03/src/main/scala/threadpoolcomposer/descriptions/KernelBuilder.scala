//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    KernelBuilder.scala
 * @brief   Builder object for Kernel instances.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

object KernelBuilder {
  import Common._

  def fromFile(descPath: Path): Option[Kernel] = {
    implicit val pr = new PathReads(descPath.toAbsolutePath.getParent)
    implicit val ka: Reads[KernelArgument] = (
        (__ \ "Name").read[String] (minLength[String](1)) ~
        (__ \ "Passing").readNullable[String]
      ) (KernelArgument.fromParse _)
    logger.finest("building Kernel from " + descPath)
    implicit val kr: Reads[KernelBuilder] = (
      (__ \ "Name").read[String] (minLength[String](1)) ~
      (__ \ "TopFunction").read[String] (minLength[String](1)) ~
      (__ \ "Id").read[Int] (min(1)) ~
      (__ \ "Version").read[String] (minLength[String](1)) ~
      (__ \ "Files").read[Seq[Path]] (minLength[Seq[Path]](1) keepAnd verifying[Seq[Path]] (mustAllExist)) ~
      (__ \ "TestbenchFiles").readNullable[Seq[Path]] (verifying[Seq[Path]](mustAllExist)) ~
      (__ \ "Description").readNullable[String] ~
      (__ \ "Arguments").read[Seq[KernelArgument]] ~
      (__ \ "CompilerFlags").readNullable[Seq[String]] ~
      (__ \ "TestbenchCompilerFlags").readNullable[Seq[String]] ~
      (__ \ "OtherDirectives").readNullable[Path] (verifying[Path](mustExist))
    ) (KernelBuilder.apply _)
    try {
      DescriptionBuilder.fromFile[Kernel, KernelBuilder](descPath)
    } catch {
      case e: MissingFileException => {
        logger.warning("Exception during parse of '" + descPath + "': " + e)
        None
      }
    }
  }
}

case class KernelBuilder (
    name: String,
    topFunction: String,
    id: Int,
    version: String,
    files: Seq[Path],
    testbenchFiles: Option[Seq[Path]],
    description: Option[String],
    args: Seq[KernelArgument],
    compilerFlags: Option[Seq[String]],
    testbenchCompilerFlags: Option[Seq[String]],
    otherDirectives: Option[Path]
  ) extends Builder[Kernel] {
  def build(descPath: Path): Kernel = {
    Kernel (
      descPath,
      name,
      topFunction,
      id,
      version,
      files,
      testbenchFiles.getOrElse(Seq()),
      description.getOrElse(""),
      compilerFlags.getOrElse(Seq()),
      testbenchCompilerFlags.getOrElse(Seq()),
      args,
      otherDirectives
    )
  }
}

