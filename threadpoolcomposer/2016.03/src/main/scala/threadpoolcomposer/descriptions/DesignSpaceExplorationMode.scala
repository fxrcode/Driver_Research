//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    DesignSpaceExplorationMode.scala
 * @brief   Model: TPC Design Space Exploration Mode.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import play.api.libs.json._
import play.api.libs.functional.syntax._
import de.tu_darmstadt.cs.esa.threadpoolcomposer.itpc.query._

object DesignSpaceExplorationMode {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  def apply(mode: String) = mode.toLowerCase match {
    case "freq" => DesignSpaceExplorationModeFrequency
    case "full" => DesignSpaceExplorationModeAreaFrequency
    case "none" => DesignSpaceExplorationModeNone
    case unknownMode => {
      logger.warning("unknown DSE mode '" + unknownMode + "', switching to 'None'")
      DesignSpaceExplorationModeNone
    }
  }

  def unapply(dse: DesignSpaceExplorationMode) = dse match {
    case DesignSpaceExplorationModeFrequency => "freq"
    case DesignSpaceExplorationModeAreaFrequency => "full"
    case _ => "none"
  }

  implicit val readsDSE: Reads[DesignSpaceExplorationMode] =
    (__ \ "DesignSpaceExplorationMode").read[String].map(DesignSpaceExplorationMode.apply _)

  implicit val writesDSE: Writes[DesignSpaceExplorationMode] = new Writes[DesignSpaceExplorationMode] {
    def writes(dse: DesignSpaceExplorationMode): JsValue = JsString(DesignSpaceExplorationMode.unapply(dse))
  }

  implicit val queriesDSE: QueryExecutor[DesignSpaceExplorationMode] = new MenuOneQueryExecutor(Some(""), 
    Seq(DesignSpaceExplorationModeNone, DesignSpaceExplorationModeFrequency, DesignSpaceExplorationModeAreaFrequency))
}

sealed abstract class DesignSpaceExplorationMode
final case object DesignSpaceExplorationModeNone extends DesignSpaceExplorationMode
final case object DesignSpaceExplorationModeFrequency extends DesignSpaceExplorationMode
final case object DesignSpaceExplorationModeAreaFrequency extends DesignSpaceExplorationMode
