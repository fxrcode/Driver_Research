//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    PrettyPrinter.scala
 * @brief   Implements pretty printing for Description subclasses.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import util.Properties.{lineSeparator => NL}

object PrettyPrinter {
  def printArchitecture(a: Architecture): String = List(
      "[Architecture @" + a.descPath + "]",
      "Name = " + a.name,
      "TclLibrary = " + a.tclLibrary,
      "Description = " + a.description,
      "testbenchTemplate = " + a.testbenchTemplate,
      "testAPI = " + a.testAPI,
      "testHarness = " + a.testHarness,
      "valueArgTemplate = " + a.valueArgTemplate,
      "referenceArgTemplate = " + a.referenceArgTemplate,
      "verilogIncludeAPI = " + a.verilogIncludeAPI,
      "verilogIncludeHarness = " + a.verilogIncludeHarness,
      "additionalSteps = " + a.additionalSteps.mkString(" ")
    ) mkString NL

  def printPlatform(p: Platform): String = List(
      "[Platform @" + p.descPath + "]",
      "Name = " + p.name,
      "Description = " + p.description,
      "TclLibrary = " + p.tclLibrary,
      "Part = " + p.part,
      "BoardPart = " + p.boardPart,
      "BoardPreset = " + p.boardPreset,
      "TargetUtilization = " + p.targetUtilization + "%",
      "SlotCount = " + p.slotCount,
      "Harness = " + p.harness,
      "API = " + p.api,
      "TestbenchTemplate = " + p.testbenchTemplate
    ) mkString NL

  def printKernelArg(ka: KernelArgument) = ka.name + " " + ka.passingConvention

  def printKernel(k: Kernel): String = List(
      "[Kernel @" + k.descPath + "]",
      "Name = " + k.name,
      "TopFunction = " + k.topFunction,
      "Version = " + k.version,
      "Files = " + k.files.mkString(" "),
      "TestbenchFiles = " + k.testbenchFiles.mkString(" "),
      "CompilerFlags = " + k.compilerFlags.mkString(" "),
      "TestbenchCompilerFlags = " + k.testbenchCompilerFlags.mkString(" "),
      "Args = " + k.args.map(printKernelArg).mkString(" "),
      "OtherDirectives = " + k.otherDirectives
    ) mkString NL

  def printCompositionEntry(ce: CompositionEntry) =
    List(ce.kernel, " x ", ce.count) mkString NL
    
  def printComposition(c: Composition): String = List(
      "[Composition @ " + c.id + "]",
      "Description = " + c.description,
      "Composition = " + (c.composition map { ce => ce.kernel + " x " + ce.count } mkString ", ")
    ) mkString NL

  def printConfiguration(c: Configuration): String = List(
      "[Configuration @" + c.descPath + "]",
      "KernelDir = " + c.kernelDir,
      "CoreDir = " + c.coreDir,
      "ArchDir = " + c.archDir,
      "PlatformDir = " + c.platformDir,
      "CompositionDir = " + c.compositionDir,
      "Architectures = " + c.architectures.map(_.name).mkString(" "),
      "Platforms = " + c.platforms.map(_.name).mkString(" "),
      "Kernels = " + c.kernels.map(_.name).mkString(" "),
      "Compositions = " + c.compositions.map(_.id).mkString(" "),
      "Cores = " + c.cores.map(cd => cd.name + "@" + cd.descPath.getParent.getFileName).mkString(" "),
      "PlatformFeatures = " + (c.platformFeatures map (_.toString)),
      "ArchitectureFeatures = " + (c.architectureFeatures map (_.toString)),
      "DesignSpaceExploration = " + c.designSpaceExploration,
      "LogFile = " + c.logFile,
      "ZipFile = " + c.zipFile,
      "DryRun = " + c.dryRun
    ) mkString NL

  def print(d: Description): String = d match {
    case a: Architecture => printArchitecture(a)
    case p: Platform => printPlatform(p)
    case k: Kernel => printKernel(k)
    case c: Composition => printComposition(c)
    case c: Configuration => printConfiguration(c)
    case _ => "Unknown description:"
  }
}
