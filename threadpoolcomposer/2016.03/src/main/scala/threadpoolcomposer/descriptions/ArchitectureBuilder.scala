//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    ArchitectureBuilder.scala
 * @brief   Builder object for Architecture instances.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

object ArchitectureBuilder {
  import Common._

  def fromFile(descPath: Path): Option[Architecture] = {
    implicit val pr = new PathReads(descPath.toAbsolutePath.getParent)
    implicit val ar: Reads[ArchitectureBuilder] = (
      (__ \ "Name").read[String] (minLength[String](1)) ~
      (__ \ "TclLibrary").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "Description").readNullable[String] ~
      (__ \ "TestbenchTemplate").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "TestAPI").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "TestHarness").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "ValueArgTemplate").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "ReferenceArgTemplate").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "VerilogIncludeAPI").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "VerilogIncludeHarness").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "AdditionalSteps").readNullable[Seq[String]]
    ) (ArchitectureBuilder.apply _)
    try {
      DescriptionBuilder.fromFile[Architecture, ArchitectureBuilder](descPath)
    } catch {
      case e: MissingFileException => {
        logger.warning("Exception during parse of '" + descPath + "': " + e)
        None
      }
    }
  }
}

case class ArchitectureBuilder (
    name: String,
    tclLibrary: Option[Path],
    description: Option[String],
    testbenchTemplate: Option[Path],
    testAPI: Option[Path],
    testHarness: Option[Path],
    valueArgTemplate: Option[Path],
    referenceArgTemplate: Option[Path],
    verilogIncludeAPI: Option[Path],
    verilogIncludeHarness: Option[Path],
    additionalSteps: Option[Seq[String]]
  ) extends Builder[Architecture] {
  import Common.{findFile, tpcDir}

  def build(descPath: Path): Architecture = {
    val bp = descPath.toAbsolutePath.getParent
    Architecture (
      descPath,
      name,
      tclLibrary.getOrElse(findFile(bp, name + ".tcl")),
      description.getOrElse(""),
      testbenchTemplate.getOrElse(findFile(bp, "tb.v.template")),
      testAPI.getOrElse(findFile(bp, "test-api.vh")),
      testHarness.getOrElse(findFile(bp, "test-harness.vh")),
      valueArgTemplate.getOrElse(findFile(bp, "valuearg.directives.template")),
      referenceArgTemplate.getOrElse(findFile(bp, "referencearg.directives.template")),
      verilogIncludeAPI.getOrElse(findFile(bp, "test-api.vh")),
      verilogIncludeHarness.getOrElse(findFile(bp, "test-harness.vh")),
      additionalSteps.getOrElse(Seq())
    )
  }
}
