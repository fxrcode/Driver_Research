//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Core.scala
 * @brief   Model: TPC IP Core.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._

object Core {
  def unapplyJson(c: Core) = Some(
      c.zipPath,
      c.name,
      c.id,
      c.version,
      if ("".equals(c.description)) None else Some(c.description),
      c.averageClockCycles
    )
}
case class Core (
    descPath: Path,
    zipPath: Path,
    name: String,
    id: Int,
    version: String,
    description: String,
    averageClockCycles: Option[Int]
  ) extends Description(descPath)
