//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    PlatformBuilder.scala
 * @brief   Builder object for Platform instances.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

object PlatformBuilder {
  import Common._

  def fromFile(descPath: Path): Option[Platform] = {
    implicit val pr = new PathReads(descPath.toAbsolutePath.getParent)
    implicit val pfr: Reads[PlatformBuilder] = (
      (__ \ "Name").read[String] (minLength[String](1)) ~
      (__ \ "TclLibrary").read[Path] (verifying[Path](mustExist)) ~
      (__ \ "Part").read[String] (minLength[String](1)) ~
      (__ \ "BoardPart").read[String] (minLength[String](4)) ~
      (__ \ "BoardPreset").read[String] (minLength[String](4)) ~
      (__ \ "TargetUtilization").read[Int] (min(5) keepAnd max(100)) ~
      (__ \ "SupportedFrequencies").readNullable[Seq[Int]] (minLength[Seq[Int]](1)) ~
      (__ \ "SlotCount").read[Int] (min(1) keepAnd max(255)) ~
      (__ \ "Description").readNullable[String] ~
      (__ \ "Harness").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "API").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "TestbenchTemplate").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "InterruptLatency").readNullable[Int] (min(1)) ~
      (__ \ "MinimalStartLatency").readNullable[Int] (min(1))
    ) (PlatformBuilder.apply _)
    try {
      DescriptionBuilder.fromFile[Platform, PlatformBuilder](descPath)
    } catch {
      case e: MissingFileException => {
        logger.warning("Exception during parse of '" + descPath + "': " + e)
        None
      }
    }
  }
}

case class PlatformBuilder (
    name: String,
    tclLibrary: Path,
    part: String,
    boardPart: String,
    boardPreset: String,
    targetUtilization: Int,
    supportedFrequencies: Option[Seq[Int]],
    slotCount: Int,
    description: Option[String],
    harness: Option[Path],
    api: Option[Path],
    testbenchTemplate: Option[Path],
    interruptLatency: Option[Int],
    minimalStartLatency: Option[Int]
  ) extends Builder[Platform] {
  private def locate(basePath: Path, fn: String): Option[Path] = {
    val ap = Paths.get(fn)
    val bp = basePath.resolve(fn).toAbsolutePath
    if (ap.isAbsolute && ap.toFile.exists) Some(ap)
    else if (bp.toFile.exists) Some(bp)
    else None
  }

  def build(descPath: Path): Platform = {
    val bp = descPath.toAbsolutePath.getParent
    Platform (
      descPath,
      name,
      tclLibrary,
      part,
      boardPart,
      boardPreset,
      targetUtilization,
      supportedFrequencies.getOrElse(50 to 450 by 5),
      slotCount,
      description.getOrElse(""),
      harness,
      api,
      testbenchTemplate,
      interruptLatency,
      minimalStartLatency
    )
  }
}  
