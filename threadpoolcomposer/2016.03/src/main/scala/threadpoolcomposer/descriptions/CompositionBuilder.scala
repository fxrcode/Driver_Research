//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    CompositionBuilder.scala
 * @brief   Builder object for Composition instances.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

object CompositionBuilder {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  private def totalCountOk(c: Seq[CompositionEntry]): Boolean =
    c.map(_.count).reduce(_+_) <= 128

  implicit object writesCEs extends Writes[CompositionEntry] {
    def writes(ce: CompositionEntry): JsValue = Json.obj("Kernel" -> ce.kernel, "Count" -> ce.count)
  }

  implicit val ce: Reads[CompositionEntry] = (
    (__ \ "Kernel").read[String] (minLength[String](1)) ~
    (__ \ "Count").read[Int] (min(1) keepAnd max(128))
  ) (CompositionEntry.apply _)
  implicit val cdb: Reads[CompositionBuilder] = (
    (__ \ "Composition").read[Seq[CompositionEntry]]
      (minLength[Seq[CompositionEntry]](1) keepAnd
       verifying[Seq[CompositionEntry]](totalCountOk)) ~
    (__ \ "Description").readNullable[String]
  ) (CompositionBuilder.apply _)

  def fromFile(descPath: Path): Option[Composition] = {
    logger.finer("Reading composition from " + descPath);
    DescriptionBuilder.fromFile[Composition, CompositionBuilder](descPath)
  }

  def fromString(str: String): Option[Composition] = {
    logger.finer("Reading composition from string '" + str + "'")
    try {
      val json = Json.parse(str)
      json.validate[CompositionBuilder] match {
        case s: JsSuccess[CompositionBuilder] => Some(s.get.build(Paths.get("N/A")))
	case e: JsError => {
	  logger.warning("Could not parse string '" + str + "': " + e)
	  None
	}
      }
    } catch {
      case e: Exception => {
        logger.severe("Exception during parse of string '" + str + "': " + e)
	throw e
      }
    }
  }
}

case class CompositionBuilder (
    composition: Seq[CompositionEntry],
    description: Option[String]
  ) extends Builder[Composition] {
  def build(descPath: Path): Composition =
    Composition(descPath, composition, description.getOrElse(""))
}

