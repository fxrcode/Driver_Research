//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    FeatureReads.scala
 * @brief   Json parsers for Architecture / Platform features.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.data.validation.Constraints._
import play.api.data.validation.ValidationError

object FeatureReads {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  private def cacheSizeSupported(n: Int): Boolean = {
    val supportedSizes = List(32768, 65536, 131072, 262144, 524288)
    val ok = supportedSizes.contains(n)
    if (! ok)
      logger.warning("Cache size " + n + " is not supported, " +
        "ignoring cache configuration. Supported sizes: " + supportedSizes)
    ok
  }

  private def dataDepthSupported(n: Int): Boolean = {
    val supportedDepths = List(1024, 2048, 4096, 8192, 16384)
    val ok = supportedDepths.contains(n)
    if (! ok)
      logger.warning("Debug core data depth " + n + " is not supported, " +
        "ignoring debug configuration. Supported sizes: " + supportedDepths)
    ok
  }

  implicit val readsLEDFeature: Reads[LEDFeature] =
    (__ \ "Enabled").read[Boolean].map(LEDFeature.apply _)

  implicit val readsOLEDFeature: Reads[OLEDFeature] =
    (__ \ "Enabled").read[Boolean].map(OLEDFeature.apply _)

  implicit val readsCacheFeature: Reads[CacheFeature] = (
      (__ \ "Enabled").read[Boolean] ~
      (__ \ "Size").read[Int] (verifying[Int](cacheSizeSupported)) ~
      (__ \ "Associativity").read[Int] (verifying[Int](n => n == 2 || n == 4))
    ) (CacheFeature.apply _)
  
  implicit val readsDebugFeature: Reads[DebugFeature] = (
      (__ \ "Enabled").read[Boolean] ~
      (__ \ "Depth").readNullable[Int] (verifying[Int](dataDepthSupported)) ~
      (__ \ "Stages").readNullable[Int] (verifying[Int](n => n >= 0 && n <= 6)) ~
      (__ \ "Use Defaults").readNullable[Boolean] ~
      (__ \ "Nets").readNullable[Seq[String]]
    ) (DebugFeature.apply _)

  implicit object ReadsFeature extends Reads[Seq[Feature]] {
    def reads(json: JsValue): JsResult[Seq[Feature]] = json match {
      case JsObject(fields) => JsSuccess(fields.map { x => x match {
        case ("Cache", value: JsValue) => readsCacheFeature.reads(value)
        case ("LED", value: JsValue) => readsLEDFeature.reads(value)
        case ("OLED", value: JsValue) => readsOLEDFeature.reads(value)
        case ("Debug", value: JsValue) => readsDebugFeature.reads(value)
        case _ => JsError(Seq(JsPath() ->
          Seq(ValidationError("validation.error.expected.valid.feature.name"))))
      }}.map(_.asOpt).filterNot(_.isEmpty).map(_.get))

      case _ => JsError(Seq(JsPath() ->
        Seq(ValidationError("validation.error.expected.features.object"))))
    }
  }
}
