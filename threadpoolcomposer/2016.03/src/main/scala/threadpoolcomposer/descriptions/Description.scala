//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Description.scala
 * @brief   Abstract base class for configuration files in TPC.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import util.Properties.{lineSeparator => NL}
import play.api.libs.json._
import scala.io.Source

object Description {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)

  def fromFile[T](descPath: Path)(implicit r: Reads[T]): Option[T] = {
    try {
      val json = Json.parse(Source.fromFile(descPath.toString).getLines.mkString(NL))
      json.validate[T] match {
        case s: JsSuccess[T] => Some(s.get)
        case e: JsError => {
          logger.warning("Could not parse file '" + descPath + "': " + e)
	  // throw new Exception(descPath + ": ParseException: " + e)
          None
        }
      }
    } catch {
      case e: Exception => {
        logger.severe("Exception during read of file '" + descPath + "': " + e)
        throw e
      }
    }
  }

  def toFile[T](t: T, descPath: Path)(implicit w: Writes[T]) = {
    try {
      val json = Json.toJson(t).toString
      val fs = new java.io.FileOutputStream(descPath.toFile)
      fs.write(json.getBytes)
      fs.flush()
      fs.close()
    } catch {
      case e: Exception => {
        logger.severe("Exception during write of file '" + descPath + "': " + e)
        throw e
      }
    }
  }
}

abstract class Description(descPath: Path) {
  override def toString: String = PrettyPrinter.print(this)
}
