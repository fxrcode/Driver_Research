//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Kernel.scala
 * @brief   Model: TPC Kernel.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._

sealed trait PassingConvention
case object ByValue extends PassingConvention
case object ByReference extends PassingConvention

object PassingConvention {
  def apply(passingConvention: Option[String]) =
    if (passingConvention.isEmpty) ByValue else passingConvention.get match {
      case "by reference" => ByReference
      case "by value"     => ByValue
      case x              => throw new Exception("invalid passing convention: " + x)
    }
}

sealed case class KernelArgument(
    name: String,
    passingConvention: PassingConvention
  )

object KernelArgument {
  def fromParse(name: String, passingConvention: Option[String]): KernelArgument =
    KernelArgument(name, PassingConvention(passingConvention))
}

case class Kernel (
    descPath: Path,
    name: String,
    topFunction: String,
    id: Int,
    version: String,
    files: Seq[Path],
    testbenchFiles: Seq[Path],
    description: String,
    compilerFlags: Seq[String],
    testbenchCompilerFlags: Seq[String],
    args: Seq[KernelArgument],
    otherDirectives: Option[Path]
  ) extends Description(descPath)
