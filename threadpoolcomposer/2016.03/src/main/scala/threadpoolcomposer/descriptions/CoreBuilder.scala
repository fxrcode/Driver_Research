//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    CoreBuilder.scala
 * @brief   Builder object for Core instances.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

object CoreBuilder {
  import Common._
  implicit val pw = new PathWrites

  def fromFile(descPath: Path): Option[Core] = {
    implicit val pr = new PathReads(descPath.toAbsolutePath.getParent)
    logger.finest("building Core from " + descPath)
    implicit val kr: Reads[CoreBuilder] = (
      (__ \ "ZipFile").read[Path] (verifying[Path](mustExist)) ~
      (__ \ "Name").read[String] (minLength[String](1)) ~
      (__ \ "Id").read[Int] (min(1)) ~
      (__ \ "Version").read[String] (minLength[String](1)) ~
      (__ \ "Description").readNullable[String] ~
      (__ \ "AverageClockCycles").readNullable[Int]
    ) (CoreBuilder.apply _)
    try {
      DescriptionBuilder.fromFile[Core, CoreBuilder](descPath)
    } catch {
      case e: MissingFileException => {
        logger.warning("Exception during parse of '" + descPath + "': " + e)
        None
      }
    }
  }

  def toFile(c: Core, descPath: Path) = {
    logger.finest("writing Core to file " + descPath)
    Description.toFile(c, descPath)
  }

  implicit val coreWrites: Writes[Core] = (
    (__ \ "ZipFile").write[Path] ~
    (__ \ "Name").write[String] ~
    (__ \ "Id").write[Int] ~
    (__ \ "Version").write[String] ~
    (__ \ "Description").writeNullable[String] ~
    (__ \ "AverageClockCycles").writeNullable[Int]
  ) (unlift(Core.unapplyJson))
}

case class CoreBuilder (
    zip: Path,
    name: String,
    id: Int,
    version: String,
    description: Option[String],
    averageClockCycles: Option[Int]
  ) extends Builder[Core] {
  def build(descPath: Path): Core = {
    Core (
      descPath,
      zip,
      name,
      id,
      version,
      description.getOrElse(""),
      averageClockCycles
    )
  }
}
