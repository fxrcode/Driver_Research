//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Configuration.scala
 * @brief   Model: TPC Configuration.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
 package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
 import  de.tu_darmstadt.cs.esa.threadpoolcomposer.Target
 import  java.nio.file._

 case class Configuration (
     descPath: Path,
     kernelDir: Path,
     coreDir: Path,
     archDir: Path,
     platformDir: Path,
     compositionDir: Path,
     architectures: Seq[Architecture],
     platforms: Seq[Platform],
     kernels: Seq[Kernel],
     compositions: Seq[Composition],
     cores: Seq[Core],
     platformFeatures:  Seq[Feature],
     architectureFeatures: Seq[Feature],
     designSpaceExploration: DesignSpaceExplorationMode,
     logFile: Option[Path],
     zipFile: Option[Path],
     id: Int,
     averageClockCycles: Option[Int],
     dryRun: Option[Path]
   ) extends Description(descPath: Path) {
  /**
   * Returns a path to the appropriate output directory for the given combination
   * of platform, architecture and kernel descriptions.
   * @todo fix this definition
   * @param arch architecture description
   * @param kernel kernel description
   **/
  def outputDir(kernel: Kernel, target: Target): Path =
    coreDir.toAbsolutePath.resolve(kernel.name.toString).resolve(target.ad.name).resolve(target.pd.name)

  def outputDir(composition: Composition, target: Target): Path =
    compositionDir.toAbsolutePath.resolve(composition.id).resolve(target.ad.name).resolve(target.pd.name)

  def outputDir(core: Core, target: Target): Path =
    coreDir.toAbsolutePath.resolve(core.name.toString).resolve(target.ad.name).resolve(target.pd.name)

  def outputDir(name: String, target: Target): Path =
    coreDir.toAbsolutePath.resolve(name).resolve(target.ad.name).resolve(target.pd.name)
 }

