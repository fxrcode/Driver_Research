//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    ConfigurationBuilder.scala
 * @brief   Builder object for Configuration instances.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import de.tu_darmstadt.cs.esa.threadpoolcomposer.{Common => TpcCommon}
import java.nio.file._
import scala.io.Source
import util.Properties.{lineSeparator => NL}
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

object ConfigurationBuilder {
  import Common._
  def fromConfiguration(cfg: Configuration): ConfigurationBuilder = {
    logger.finest("building configuration from configuration: " + cfg)
    val compos = cfg.compositions.map(_.id).filterNot(_.startsWith("0x"))
    ConfigurationBuilder (
      Some(cfg.kernelDir),
      Some(cfg.coreDir),
      Some(cfg.archDir),
      Some(cfg.platformDir),
      Some(cfg.compositionDir),
      if (cfg.architectures.length == 0) None else Some(cfg.architectures.map(_.name)),
      if (cfg.platforms.length == 0) None else Some(cfg.platforms.map(_.name)),
      if (cfg.kernels.length == 0) None else Some(cfg.kernels.map(_.name)),
      if (compos.length == 0) None else Some(compos),
      None,
      None,
      None,
      None,
      cfg.logFile,
      cfg.zipFile,
      Some(cfg.id),
      cfg.averageClockCycles,
      cfg.dryRun
    )
  }

  def fromArgs(args: Seq[String]): Option[Configuration] = {
    logger.finest("building configuration from args: " + args)
    if (args.size % 2 > 0) throw new Exception("Expected key value pairs, but found " +
        args.size + " arguments!")
    val am = args.grouped(2).map(x => x(0).toLowerCase -> x(1)).toMap
    if (am.contains("configfile")) {
      val descPath = Paths.get(am("configfile"))
      val cb = builderFromFile(descPath)
      cb.map(_.apply(args).build(descPath))
    } else {
      val cb = ConfigurationBuilder (
        am.get("kerneldir").map(s => Paths.get(s)),
        am.get("coredir").map(s => Paths.get(s)),
        am.get("archdir").map(s => Paths.get(s)),
	am.get("platformdir").map(s => Paths.get(s)),
	am.get("compositiondir").map(s => Paths.get(s)),
        am.get("architecture").map(s => Seq(s)),
        am.get("platform").map(s => Seq(s)),
        am.get("kernel").map(s => Seq(s)),
        am.get("composition").map(s => Seq(s)),
	None,
	None,
	None,
	am.get("designspaceexploration"),
	am.get("logfile").map(s => Paths.get(s)),
	am.get("zipfile").map(s => Paths.get(s)),
	am.get("id").map(_.toInt),
        am.get("averageClockCycles").map(_.toInt),
        am.get("dryrun").map(s => Paths.get(s))
      )
      Some(cb.build(Paths.get("none")))
    }
  }
    

  private def builderFromFile(descPath: Path): Option[ConfigurationBuilder] = {
    logger.finest("building ConfigurationBuilder from file: " + descPath)
    implicit val pr = new PathReads(descPath.toAbsolutePath.getParent)
    implicit val cr = new CompositionReads
    import FeatureReads._
    implicit val configReads: Reads[ConfigurationBuilder] = (
      (__ \ "KernelDir").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "CoreDir").readNullable[Path] ~
      (__ \ "ArchDir").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "PlatformDir").readNullable[Path] (verifying[Path](mustExist)) ~
      (__ \ "CompositionDir").readNullable[Path] ~
      (__ \ "Architectures").readNullable[Seq[String]] ~
      (__ \ "Platforms").readNullable[Seq[String]] ~
      (__ \ "Kernels").readNullable[Seq[String]] ~
      (__ \ "Compositions").readNullable[Seq[String]] ~
      (__ \ "Composition").readNullable[Composition] ~
      (__ \ "PlatformFeatures").readNullable[Seq[Feature]] ~
      (__ \ "ArchitectureFeatures").readNullable[Seq[Feature]] ~
      (__ \ "DesignSpaceExploration").readNullable[String] ~
      (__ \ "LogFile").readNullable[Path] ~
      (__ \ "ZipFile").readNullable[Path] ~
      (__ \ "Id").readNullable[Int] ~
      (__ \ "AverageClockCycles").readNullable[Int] ~
      (__ \ "DryRun").readNullable[Path]
    ) (ConfigurationBuilder.apply _)
    val json = Json.parse(Source.fromFile(descPath.toString).getLines.mkString(NL))
    json.validate[ConfigurationBuilder] match {
      case s: JsSuccess[ConfigurationBuilder] => Some(s.get)
      case e => {
        logger.warning("Could not parse '" + descPath + "': " + e)
        None
      }
    }
  }

  def fromFile(descPath: Path): Option[Configuration] = {
    logger.finest("building Configuration from file: " + descPath)
    try {
      builderFromFile(descPath) map (_.build(descPath))
    } catch {
      case e: MissingFileException => {
        logger.warning("Path does not exist: " + e)
        None
      }
    }
  }
}

case class ConfigurationBuilder (
    var kernelDir: Option[Path],
    var coreDir: Option[Path],
    var archDir: Option[Path],
    var platformDir: Option[Path],
    var compositionDir: Option[Path],
    var architectures: Option[Seq[String]],
    var platforms: Option[Seq[String]],
    var kernels: Option[Seq[String]],
    var compositions: Option[Seq[String]],
    var composition: Option[Composition],
    var platformFeatures: Option[Seq[Feature]],
    var architectureFeatures: Option[Seq[Feature]],
    var designSpaceExploration: Option[String],
    var logFile: Option[Path],
    var zipFile: Option[Path],
    var id: Option[Int],
    var averageClockCycles: Option[Int],
    var dryRun: Option[Path]
  ) extends Builder[Configuration] {
  import Common._

  private def getFiles(dir: java.io.File): Seq[java.io.File] =
    if (dir.exists && dir.listFiles != null) 
      dir.listFiles ++ dir.listFiles.filter(_.isDirectory).flatMap(getFiles)
    else
      Seq()

  private def findArchs(p: Path, archs: Seq[String]) = {
    logger.finest(List("searching for archs '", archs, "' in ", p).mkString)
    getFiles(p.toFile)
      .filter(_.getName.equals("architecture.description"))
      .map(f => Paths.get(f.toString))
      .map(ArchitectureBuilder.fromFile _)
      .filter(a => !a.isEmpty && (archs.length == 0 || archs.contains(a.get.name)))
      .map(_.get)
  }

  private def findPlatforms(p: Path, platforms: Seq[String]) = {
    logger.finest(List("searching for plats '", platforms, "' in ", p).mkString)
    getFiles(p.toFile)
      .filter(_.getName.equals("platform.description"))
      .map(f => Paths.get(f.toString))
      .map(PlatformBuilder.fromFile _)
      .filter(p => !p.isEmpty && (platforms.length == 0 || platforms.contains(p.get.name)))
      .map(_.get)
  }

  private def findKernels(p: Path, kernels: Seq[String]) = {
    logger.finest(List("searching for kernels '", kernels, "' in ", p).mkString)
    getFiles(p.toFile)
      .filter(_.getName.equals("kernel.description"))
      .map(f => Paths.get(f.toString))
      .map(KernelBuilder.fromFile _)
      .filter(k => !k.isEmpty && (kernels.length == 0 || kernels.contains(k.get.name)))
      .map(_.get)
  }

  private def findCompositions(p: Path, compositions: Seq[String]) = {
    logger.finest(List("searching for compos '", compositions, "' in ", p).mkString)
    compositions.map(f => if (Paths.get(f).isAbsolute) Paths.get(f)
                          else p.resolve(f))
      .filter(_.toFile.exists)
      .map(CompositionBuilder.fromFile _)
      .map(_.get)
  }

  private def findCores(p: Path) = {
    logger.finest(List("searching for cores in ", p).mkString)
    getFiles(p.toFile)
      .filter(_.getName.equals("core.description"))
      .map(f => Paths.get(f.toString))
      .map(CoreBuilder.fromFile _)
      .filter(c => !c.isEmpty)
      .map(_.get)
  }

  def apply(args: Seq[String]) = {
    val am = args.grouped(2).map(x => x(0).toLowerCase -> x(1)).toMap
    if (am.keySet.contains("kerneldir"))
      kernelDir = Some(Paths.get(am("kerneldir")).toAbsolutePath)
    if (am.keySet.contains("coredir"))
      coreDir = Some(Paths.get(am("coredir")).toAbsolutePath)
    if (am.keySet.contains("archdir"))
      archDir = Some(Paths.get(am("archdir")).toAbsolutePath)
    if (am.keySet.contains("platformdir"))
      platformDir = Some(Paths.get(am("platformdir")).toAbsolutePath)
    if (am.keySet.contains("compositiondir"))
      compositionDir = Some(Paths.get(am("compositiondir")).toAbsolutePath)

    if (am.keySet.contains("architecture"))
      architectures = Some(Seq(am("architecture")))
    if (am.keySet.contains("platform"))
      platforms = Some(Seq(am("platform")))
    if (am.keySet.contains("composition"))
      compositions = Some(Seq(am("composition")))
    if (am.keySet.contains("kernel"))
      kernels = Some(Seq(am("kernel")))

    if (am.keySet.contains("designspaceexploration"))
      designSpaceExploration = Some(am("designspaceexploration"))
    if (am.keySet.contains("logfile"))
      logFile = Some(Paths.get(am("logfile")))
    if (am.keySet.contains("zipfile"))
      zipFile = Some(Paths.get(am("zipfile")))
    if (am.keySet.contains("id"))
      id = Some(am("id").toInt)
    if (am.keySet.contains("averageClockCycles"))
      averageClockCycles = Some(am("averageClockCycles").toInt)
    if (am.keySet.contains("dryrun"))
      dryRun = Some(Paths.get(am("dryrun")))
    this
  }

  def build(descPath: Path): Configuration = {
    val bp = descPath.toAbsolutePath.getParent
    val ad = archDir.getOrElse(TpcCommon.homeDir.resolve("arch"))
    val pd = platformDir.getOrElse(TpcCommon.homeDir.resolve("platform"))
    val kd = kernelDir.getOrElse(TpcCommon.homeDir.resolve("kernel"))
    val cd = coreDir.getOrElse(TpcCommon.homeDir.resolve("core"))
    val cod = compositionDir.getOrElse(TpcCommon.homeDir.resolve("bd"))
    Configuration (
      descPath,
      kd,
      cd,
      ad,
      pd,
      cod,
      findArchs(ad, architectures.getOrElse(Seq())),
      findPlatforms(pd, platforms.getOrElse(Seq())),
      findKernels(kd, kernels.getOrElse(Seq())),
      findCompositions(bp, compositions.getOrElse(Seq())) ++ composition,
      findCores(cd),
      platformFeatures.getOrElse(Seq()),
      architectureFeatures.getOrElse(Seq()),
      DesignSpaceExplorationMode(designSpaceExploration.getOrElse("None")),
      logFile,
      zipFile,
      id.getOrElse(new scala.util.Random().nextInt(1234567890)),
      averageClockCycles,
      dryRun
    )
  }
}
