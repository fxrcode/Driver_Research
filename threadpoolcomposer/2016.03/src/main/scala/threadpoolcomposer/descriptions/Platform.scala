//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Platform.scala
 * @brief   Model: TPC Platform.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._

case class Platform (
    descPath: Path,
    name: String,
    tclLibrary: Path,
    part: String,
    boardPart: String,
    boardPreset: String,
    targetUtilization: Int,
    supportedFrequencies: Seq[Int],
    slotCount: Int,
    description: String,
    harness: Option[Path],
    api: Option[Path],
    testbenchTemplate: Option[Path],
    interruptLatency: Option[Int],
    minimalStartLatency: Option[Int]
  ) extends Description(descPath: Path)
