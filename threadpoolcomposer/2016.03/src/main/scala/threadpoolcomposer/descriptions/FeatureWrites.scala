//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    FeatureWrites.scala
 * @brief   Json writers for Architecture / Platform features.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import play.api.libs.json._
import play.api.libs.functional.syntax._

object FeatureWrites {
  implicit val writesLEDFeature: Writes[LEDFeature] = new Writes[LEDFeature] {
    def writes(f: LEDFeature): JsValue = Json.obj("Enabled" -> f.enabled)
  }

  implicit val writesOLEDFeature: Writes[OLEDFeature] = new Writes[OLEDFeature] {
    def writes(f: OLEDFeature): JsValue = Json.obj("Enabled" -> f.enabled)
  }

  implicit val writesCacheFeature: Writes[CacheFeature] = (
    (__ \ "Enabled").write[Boolean] ~
    (__ \ "Size").write[Int] ~
    (__ \ "Associativity").write[Int]
  ) (unlift(CacheFeature.unapply))

  implicit val writesDebugFeature: Writes[DebugFeature] = (
    (__ \ "Enabled").write[Boolean] ~
    (__ \ "Depth").writeNullable[Int] ~
    (__ \ "Stages").writeNullable[Int] ~
    (__ \ "Use Defaults").writeNullable[Boolean] ~
    (__ \ "Nets").writeNullable[Seq[String]]
  ) (unlift(DebugFeature.unapply))
}
