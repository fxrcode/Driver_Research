//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    Common.scala
 * @brief   Shared code for Description implementations.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions
import java.nio.file._
import play.api.libs.json._
import play.api.data.validation.ValidationError

object Common {
  implicit val (logger, appender, formatter) = ZeroLoggerFactory.newLogger(this)
  final class MissingFileException(msg: String) extends Exception(msg)

  class PathReads(basePath: Path) extends Reads[Path] {
    def reads(json: JsValue): JsResult[Path] = json match {
      case JsString(s) => {
        if (Paths.get(s).isAbsolute) JsSuccess(Paths.get(s))
        else JsSuccess(basePath.resolve(s))
      }
      case _ => JsError(Seq(JsPath() -> Seq(ValidationError("validation.error.expected.jsstring"))))
    }
  }

  class PathWrites extends Writes[Path] {
    def writes(p: Path): JsValue = JsString(p.toString)
  }

  class CompositionReads extends Reads[Composition] {
    def reads(json: JsValue): JsResult[Composition] = CompositionBuilder.cdb.reads(json) match {
      case s: JsSuccess[CompositionBuilder] => JsSuccess(s.get.build(Paths.get("N/A")))
      case x: JsError => x
    }
  }

  def mustExist(p: Path): Boolean = p.toFile.exists

  def mustAllExist(ps: Seq[Path]) = ps.length == 0 || ps.map(mustExist).reduce(_&&_)

  val tpcDir: Path = Paths.get(sys.env("TPC_HOME")).toAbsolutePath

  def findFile(basePath: Path, fn: String): Path = {
    val ap = Paths.get(fn)
    val bp = basePath.resolve(fn).toAbsolutePath
    if (ap.isAbsolute && ap.toFile.exists) ap
    else if (bp.toFile.exists) bp
    else throw new MissingFileException("File '" + fn + "' could not be found in " + basePath)
  }
}
