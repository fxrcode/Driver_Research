//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     EvaluateIP.scala
 * @brief    Task to evaluate an existing IP core:
 *           Must be given a .zip file containing an IPXACT IP core, a target
 *           part (i.e., device id) and clock period to produce a report with
 *           estimates for area usage (in different categories, e.g., LUTs,
 *           Slices) and timing. This data is used in design space exploration.
 *           Conventions: There must be a main clock port in the top-level 
 *           module that contains either 'clk' or 'CLK' in its name.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import java.nio.file._
import scala.sys.process._
import scala.io._
import scala.util.matching.Regex

object EvaluateIP {
  implicit val (logger, formatter, appender) = ZeroLoggerFactory.newLogger(this)
  val LUTS_REGEX   = new Regex("""\| Slice LUTs\s+\|\s+(\d+)\s+\|\s+\d+\s+\|\s+(\d+)""", "used", "available")
  val FF_REGEX     = new Regex("""\| Slice Registers\s+\|\s+(\d+)\s+\|\s+\d+\s+\|\s+(\d+)""", "used", "available")
  val SLICE_REGEX  = new Regex("""\| Slice\s+\|\s+(\d+)\s+\|\s+\d+\s+\|\s+(\d+)""", "used", "available")
  val DSP_REGEX    = new Regex("""\| DSPs\s+\|\s+(\d+)\s+\|\s+\d+\s+\|\s+(\d+)""", "used", "available")
  val BRAM_REGEX   = new Regex("""\| Block RAM Tile\s+\|\s+([^ \t]+)\s+\|\s+\d+\s+\|\s+(\d+)""", "used", "available")
  val PERIOD_REGEX = new Regex("""Data Path Delay:\s+([^ \t]+)ns""", "period")

  def main(args: Array[String]) {
    try {
      if (sys.env.get("TPC_HOME").isEmpty)
        throw new Exception("Please set 'TPC_HOME' environment variable to TPC root dir.")
      logger.info("Vivado version: " + Common.getVivadoVersion)
      if (args.length < 4)
        throw new Exception("Missing arguments, expected: <ZIP> <PERIOD> <PART> <REPORT>")

      val e = new EvaluateIP(Paths.get(args(0)).toAbsolutePath, args(1).toInt, args(2), Paths.get(args(3)).toAbsolutePath)
      if (! e.evaluate()) { throw new Exception("Evaluation of IP failed!") }
    } catch { case ex: Throwable =>
      logger.severe(ex)
      logger.severe("TPC finished with errors")
      sys.exit(1)
    }
  }

  /** 
   *  Extracts an AreaEstimate from a given Vivado utilization report.
   *  @param reportFile Path to report file.
   *  @return AreaEstimate
   **/
  def extractAreaEstimate(reportFile: Path) : AreaEstimate = {
    val rpt = Source.fromFile(reportFile.toString).mkString("")
    new AreaEstimate(
      new ResourcesEstimate(
        SLICE_REGEX.findFirstIn(rpt) match {
          case Some(SLICE_REGEX(used, available)) => used.toInt
          case None => 0
        },
        LUTS_REGEX.findFirstIn(rpt) match {
          case Some(LUTS_REGEX(used, available)) => used.toInt
          case None => 0
        },
        FF_REGEX.findFirstIn(rpt) match {
          case Some(FF_REGEX(used, available)) => used.toInt
          case None => 0
        },
        DSP_REGEX.findFirstIn(rpt) match {
          case Some(DSP_REGEX(used, available)) => used.toInt
          case None => 0
        },
        BRAM_REGEX.findFirstIn(rpt) match {
          case Some(BRAM_REGEX(used, available)) => used.toFloat.ceil.toInt
          case None => 0
        }
      ),
      new ResourcesEstimate(
        SLICE_REGEX.findFirstIn(rpt) match {
          case Some(SLICE_REGEX(used, available)) => available.toInt
          case None => 0
        },
        LUTS_REGEX.findFirstIn(rpt) match {
          case Some(LUTS_REGEX(used, available)) => available.toInt
          case None => 0
        },
        FF_REGEX.findFirstIn(rpt) match {
          case Some(FF_REGEX(used, available)) => available.toInt
          case None => 0
        },
        DSP_REGEX.findFirstIn(rpt) match {
          case Some(DSP_REGEX(used, available)) => available.toInt
          case None => 0
        },
        BRAM_REGEX.findFirstIn(rpt) match {
          case Some(BRAM_REGEX(used, available)) => available.toInt
          case None => 0
        }
      )
    )
  }

  /**
   *  Extracts the minimum clock period from a Vivado timing report.
   *  @param reportFile Path to report.
   *  @return Period in ns.
   **/
  def extractMinPeriod(reportFile: Path) : Double = {
    val rpt = Source.fromFile(reportFile.toString).mkString("")
    PERIOD_REGEX.findFirstIn(rpt) match {
      case Some(PERIOD_REGEX(period)) => period.toDouble
      case None => 0.0
    }
  }
}

/** Evaluation class. **/
private class EvaluateIP(zipFile: Path, targetPeriod: Double, targetPart: String, reportFile: Path) {
  import EvaluateIP._

  /** Perform the evaluation.
    * @return true if successful **/
  def evaluate(): Boolean = {
    def deleteOnExit(f: java.io.File) = f.deleteOnExit
    //def deleteOnExit(f: java.io.File) = f        // keep files?

    // logging prefix
    val runPrefix  = "evaluation of " + zipFile + " for " + targetPart + "@" + (1000 / targetPeriod) + "MHz"
    // unzip relevant files into temporary directory
    val (baseDir, files) = unzipFile(zipFile, List(".v", ".vhd", ".xdc", ".vh", ".xci", ".saif"))
    // define report filenames
    val rpt_timing = reportFile.resolveSibling("timing.rpt")
    val rpt_util   = reportFile.resolveSibling("utilization.rpt")
    val rpt_power  = reportFile.resolveSibling("power.rpt")
    val dcp        = reportFile.resolveSibling("out-of-context_synth.dcp")

    val needles: scala.collection.mutable.Map[String, String] = scala.collection.mutable.Map(
      "SRC_FILES" -> files.mkString(" "),
      "PART" -> targetPart,
      "PERIOD" -> targetPeriod.toString,
      "REPORT_TIMING" -> rpt_timing.toString,
      "REPORT_UTILIZATION" -> rpt_util.toString,
      "REPORT_POWER" -> rpt_power.toString,
      "SYNTH_CHECKPOINT" -> dcp.toString
    )

    // write Tcl script
    Template.interpolateFile(
        Template.DEFAULT_NEEDLE,
        Common.commonDir.resolve("evaluate_ip.tcl.template").toString,
        baseDir.resolve("evaluate.tcl").toString,
        needles)

    val logFile = baseDir.resolve("evaluate.log")
    logger.info("starting " + runPrefix + ", output in " + logFile)

    // ignore everything
    val io = new ProcessIO(stdin => {stdin.close()}, stdout => {stdout.close()}, stderr => {stderr.close()})

    // execute Vivado (max runtime: 5h)
    val p = Process(Seq("timeout", "5h", "vivado", "-mode", "batch", "-source",
        baseDir.resolve("evaluate.tcl").toString, "-notrace", "-nojournal",
        "-log", logFile.toString), baseDir.toFile).run(io)
    var r = p.exitValue()

    if (r == 124) {
      logger.severe(runPrefix + ": Vivado timeout error")
    } else {
      if (r == 0) {
        logger.finer(runPrefix + ": Vivado finished successfully")
        val ae = EvaluateIP.extractAreaEstimate(rpt_util)
        val pm = EvaluateIP.extractMinPeriod(rpt_timing)
        val needles = scala.collection.mutable.Map[String, String](
          "SLICE" -> ae.resources.SLICE.toString,
          "SLICES" -> ae.available.SLICE.toString,
          "LUT" -> ae.resources.LUT.toString,
          "LUTS" -> ae.available.LUT.toString,
          "FF" -> ae.resources.FF.toString,
          "FFS" -> ae.available.FF.toString,
          "BRAM" -> ae.resources.BRAM.toString,
          "BRAMS" -> ae.available.BRAM.toString,
          "DSP" -> ae.resources.DSP.toString,
          "DSPS" -> ae.available.DSP.toString,
          "PERIOD" -> targetPeriod.toString,
          "MIN_PERIOD" -> pm.toString
        )

        // write final report
        Template.interpolateFile(
            Template.DEFAULT_NEEDLE,
            Common.commonDir.resolve("ip_report.xml.template").toString,
            reportFile.toString,
            needles
        )
        logger.info(runPrefix + " finished successfully, report in " + reportFile)
        // clean up files on exit
        files.map(f => deleteOnExit(f.toFile))
        deleteOnExit(baseDir.resolve("evaluate.tcl").toFile)
        deleteOnExit(baseDir.toFile)
        deleteOnExit(logFile.toFile)
        deleteOnExit(baseDir.resolve(".Xil").toFile) // also remove Xilinx's crap
      } else {
        logger.severe(runPrefix + ": Vivado finished with error (" + r + ")")
      }
    }
    r == 0
  }

  /**
   *  Unpacks all files with the given extensions into a temporary directory.
   *  @param zipFile Path to .zip file.
   *  @param extensions List of suffixes; all matching files will be extracted.
   *  @return tuple (temporary directory, list of extracted files).
   **/
  private def unzipFile(zipFile: Path, extensions: List[String]): (Path, List[Path]) = {
    import java.util.zip._
    import java.io.{BufferedInputStream, BufferedOutputStream, FileInputStream, FileOutputStream}
    var extracted: List[Path] = List()
    val zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile.toFile)))
    val tempdir = Files.createTempDirectory(null)
    try {
      var zipEntry = zis.getNextEntry()
      while (zipEntry != null) {
        if (extensions.map(ext => zipEntry.toString().endsWith(ext)).reduce(_||_)) {
          val buffer = new Array[Byte](1024)
          val outname = tempdir.resolve(Paths.get(zipEntry.getName()).getFileName()).toString()
          val dest = new BufferedOutputStream(new FileOutputStream(outname), 1024)
          extracted = Paths.get(outname) :: extracted
          var count = 0
          while ({count = zis.read(buffer, 0, 1024); count != -1})
            dest.write(buffer, 0, count);
          dest.flush()
          dest.close()
        }
        zipEntry = zis.getNextEntry()
      }
    } finally {
      zis.close()
    }
    (tempdir, extracted)
  }
}
