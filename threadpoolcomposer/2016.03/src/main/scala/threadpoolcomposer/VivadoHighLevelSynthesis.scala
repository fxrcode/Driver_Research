//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file    VivadoHighLevelSynthesis.scala
 * @brief   High-level synthesis task.
 * @authors J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import descriptions._
import reports._
import java.io.ByteArrayInputStream
import java.io.FileWriter
import java.nio.file._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{future, Future, Await}
import scala.concurrent.duration._
import scala.io._
import scala.reflect.runtime._
import scala.reflect.runtime.universe._
import scala.sys.process._
import scala.tools.reflect.ToolBox

protected final class VivadoHighLevelSynthesis(implicit cfg: Configuration) extends Command {
  import util.Properties.{lineSeparator => NL}

  def execute: Boolean = {
    val summary = "Run Summary:" + NL + cfg
    logger.info(summary)

    if (cfg.architectures.isEmpty || cfg.platforms.isEmpty || cfg.kernels.isEmpty)
      throw new Exception("Either found no kernels, no platforms or no architectures, check params!")

    val generateResults: Seq[Future[Boolean]] =
      for (kd <- cfg.kernels; ad <- cfg.architectures; pd <- cfg.platforms; t = Target(ad, pd))
        yield future { try { generate(kd, t) } catch { case ex: Exception => logger.severe(ex); false } }

    generateResults
      .map { fr => Await.result(fr, Duration.Inf) }
      .reduce (_&&_)
  }

  private def generate(kd: Kernel, target: Target): Boolean = {
    val fullOutputPath = cfg.outputDir(kd, target).resolve("hls")
    val f = fullOutputPath.resolve(target.ad.name + ".tcl")
    fullOutputPath.toFile.mkdirs // make output dirs

    logger.finest(kd.toString)

    new FileWriter(f.toString).append(makeScript(kd, target)).close() // write Tcl file

    val runName = "'" + kd.name + "' for " + target
    val logFile = fullOutputPath.resolve(target.ad.name + ".log").toString
    logger.info("Starting run " + runName + ": output in " + logFile)

    // execute Vivado HLS (max. runtime: 1 day)
    val vivadoRet = Process(Seq("timeout", "1d", "vivado_hls",
        "-f", f.toString,
        "-l", logFile
      ), fullOutputPath.toFile).!(ProcessLogger(line => (), line => logger.warning(line)))

    if (vivadoRet == 124) {
      logger.severe("Vivado HLS timeout for " + runName)
    } else if (vivadoRet != 0) {
      logger.severe("Vivado HLS finished with non-zero exit code: " + vivadoRet + " for " + runName)
    } else {
      logger.info("Vivado HLS finished successfully for " + runName)
    }

    vivadoRet == 0 && performAdditionalSteps(kd, target) && copyZip(kd, target)
  }

  private def performAdditionalSteps(kd: Kernel, target: Target): Boolean = {
    if (target.ad.additionalSteps.length > 0) {
      val tb = universe.runtimeMirror(getClass.getClassLoader).mkToolBox()
      target.ad.additionalSteps map { step =>
        try {
          // call singleton method 'apply' via reflection
          val stepInst = tb.eval(tb.parse(step))
          val stepInstMirror = universe.runtimeMirror(getClass.getClassLoader).reflect(stepInst)
          val meth = stepInstMirror.symbol.typeSignature.member(newTermName("apply")).asMethod
          stepInstMirror.reflectMethod(meth)(cfg, target.ad, kd)
        } catch {
          case tie: java.lang.reflect.InvocationTargetException =>
            logger.severe("Executing additional step '" + step + "' failed: " + tie.getTargetException)
            return false // FIXME
          case ex: Throwable =>
            logger.severe("Executing additional step '" + step + "' failed: " + ex)
            return false // FIXME
        }
      }
    }
    true
  }

  private def copyZip(kd: Kernel, target: Target): Boolean = {
    val prefix = kd.name + "_"  + target.ad.name
    val zips = Common.getFiles(cfg.outputDir(kd, target).toFile).filter(_.toString.endsWith(".zip"))

    if (zips.length > 0) {
      val outzip = cfg.outputDir(kd, target).resolve("ipcore").resolve(prefix + ".zip")
      outzip.toFile.mkdirs
      logger.info("Found .zip: " + zips(0).getAbsolutePath().toString + " copying to " + outzip.toString)

      try {
        java.nio.file.Files.copy(Paths.get(zips(0).getAbsolutePath), outzip,
          java.nio.file.StandardCopyOption.REPLACE_EXISTING)
      } catch {
        case ex: java.io.IOException => logger.severe("Copy failed: " + ex)
        return false
      }
      val ncfg = ConfigurationBuilder.fromConfiguration(cfg)
      ncfg.zipFile = Some(outzip)
      ncfg.platforms = Some(Seq(target.pd.name))
      ncfg.architectures = Some(Seq(target.ad.name))
      ncfg.id = Some(kd.id)
      ncfg.averageClockCycles = CoSimReport.find(kd, target).map(_.latency.avg)
      new AddCore()(ncfg.build(cfg.descPath)).execute
    } else {
      logger.severe("No .zip file with IP core found!")
      return false
    }
    true
  }

  private def kernelArgs(kd: Kernel, target: Target): String = {
    val base = 0x20
    val offs = 0x10
    var i = 0
    kd.args map { ka => {
      val tmpl = new Template
      tmpl("TOP") = kd.topFunction
      tmpl("ARG") = ka.name
      tmpl("OFFSET") = "0x" + (base + i * offs).toHexString
      i += 1
      ka.passingConvention match {
        case ByValue     => tmpl.interpolateFile(target.ad.valueArgTemplate.toString)
	case ByReference => tmpl.interpolateFile(target.ad.referenceArgTemplate.toString)
      }
    }} mkString NL
  }

  def makeScript(kd: Kernel, target: Target): String = {
    val tmpl = new Template
    val dirs = if (kd.otherDirectives.isEmpty) ""
               else Source.fromFile(kd.otherDirectives.get.toString).getLines.mkString(NL)
    tmpl("PROJECT") = target.ad.name
    tmpl("HEADER") = Seq(
        "set tpc_freq " + scala.util.Sorting.stableSort(target.pd.supportedFrequencies).reverse.head,
        "source " + Common.commonDir.resolve("common.tcl").toString
      ).mkString(NL)
    tmpl("COSIMULATION_FLAG") = if (kd.testbenchFiles.length > 0) "1" else "0"
    tmpl("SOLUTION") = "solution"
    tmpl("TOP") = kd.topFunction
    tmpl("PART") = target.pd.part
    tmpl("PERIOD") =  "[tpc::get_design_period]"
    tmpl("VENDOR") = "esa.cs.tu-darmstadt.de"
    tmpl("VERSION") = kd.version
    tmpl("SOURCES") = kd.files.mkString(" ")
    tmpl("SRCSCFLAGS") = kd.compilerFlags.mkString(" ")
    tmpl("TBSRCS") = kd.testbenchFiles.map(_.toString).mkString(" ")
    tmpl("TBCFLAGS") = kd.testbenchCompilerFlags.mkString(" ")
    tmpl("DIRECTIVES") = List(
      kernelArgs(kd, target),
      dirs
    ).mkString(NL)
    tmpl.interpolateFile(Common.commonDir.resolve("hls.tcl.template").toString)
  }
}

