//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     Menu.scala
 * @brief    Simple command line menus.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.itpc
import scala.util.Properties.{lineSeparator => NL}
import java.io.{InputStream, PrintStream}

/**
 * Models an entry in a menu.
 * @param title object to display (via toString)
 * @param execute action to execute if selected (optional)
 **/
case class MenuOption[T](title: T, execute: () => Unit = ()=>{})

/**
 * A simple, interactive command line menu.
 * @param title underlined large title
 * @param caption smaller caption for the menu (e.g., question)
 * @param menu options to display
 * @param out (implicit) output PrintStream
 * @param in  (implicit) input InputStream (for user input)
 **/
case class Menu[T](title: Option[String], caption: Option[String], menu: Seq[MenuOption[T]])(implicit out: PrintStream, in: InputStream) {
  /**
   * Shows menu and asks user to select one option.
   * @return selected option (will repeat query until valid)
   **/
  def showAndChooseOne: MenuOption[T] = {
    title map showTitle
    chooseOne
  }

  /**
   * Shows menu and asks user to select multiple options (currently selected are indicated).
   * @return set of selected options
   **/
  def showAndChooseMultiple: Set[MenuOption[T]] = {
    title map showTitle
    chooseMultiple(Set())
  }

  private val m = menu.zipWithIndex.map(o => (o._1, ('a' + o._2).toChar))
  private val Choice = new scala.util.matching.Regex("([a-" + ('a' + (menu.length-1)).toChar +"])")
  private val Nl = new scala.util.matching.Regex("([" + NL + "])")
  private def toIndex(s: String): Int = s(0) - 'a'
  private def clearInput = while (in.available > 0) in.read
  private def showCaption(caption: String) = out.println(caption)
  private def showTitle(title: String) = {
    out.println(title)
    title.foreach(_ => out.print("*"))
    out.println(NL)
  }

  private def chooseOne: MenuOption[T] = {
    def showOption(o: (MenuOption[T], Char)) = out.println("\t" + o._2 + ": " + o._1.title)

    clearInput
    caption map showCaption
    m.foreach(o => showOption(o))
    out.print("Your choice: ")

    in.read.toChar.toString match {
      case Choice(c) => menu(toIndex(c))
      case x => out.println("Invalid choice (" + x + ")"); chooseOne
    }
  }

  private def chooseMultiple(set: Set[MenuOption[T]]): Set[MenuOption[T]] = {
    def showOption(o: (MenuOption[T], Char)) = out.println("\t" + o._2 + " (" +
      {if (set.contains(o._1)) "x" else " "} + "): " + o._1.title)

    clearInput
    caption map showCaption
    m.foreach(o => showOption(o))
    out.print("Your choice (press return to finish selection): ")

    in.read.toChar.toString match {
      case Choice(c) => chooseMultiple(if (set.contains(menu(toIndex(c)))) set - menu(toIndex(c)) else set + menu(toIndex(c)))
      case Nl(s) => set
      case x => out.println("Invalid choice (" + x + ")"); chooseMultiple(set)
    }
  }
}
