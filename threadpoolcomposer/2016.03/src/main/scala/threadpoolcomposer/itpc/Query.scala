//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     Query.scala
 * @brief    Command line based user queries.
 *           Uniform way to query the user for input. Query class can be used
 *           uniformely to retrieve well-typed input from the user interactively.
 *           Queries are performed by QueryExecutor instances, which can be \
 *           easily extended by defining new implicits.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.itpc
// a package object to contain the implicit QueryExecutors
package object query {
  import Validator._
  import java.nio.file.{Path, Paths}
  import java.io.PrintStream

  /** QueryExecutor instances perform the user interaction to get validated input. **/
  trait QueryExecutor[T] {
    /**
     * Query the user for input. Repeat until validation passes.
     * @param validator Validation object, e.g., Validator.NonEmpty
     * @return validated user input of type T
     **/
    def query(validator: Validator[T]): T
  }

  /**
   * Query is the central point of the query API: uses implicit executors to perform
   * user dialog(s) and retrieve input.
   **/
  case class Query[T](text: String)(implicit exe: QueryExecutor[T]) {
    val out = System.out  // TODO fix this with useful implicit
  
    def query(validator: Validator[T] = AllValid()): T = {
      // output text and a textual representation of the constraints from the Validator
      out.print(Seq(text, validator.constraintToString,  ": ").mkString)
      exe.query(validator)
    }
  }

  /**
   * Base class for simple user interactions: Query text input from user and apply 
   * a conversion method to retrieve the correct type.
   * @param cf conversion function
   **/
  class StrConvQueryExecutor[T](cf: String => T) extends QueryExecutor[T] {
    def query(validator: Validator[T]): T = {
      // apply conversion, on exception repeat
      val v = try { cf(QueryExecutor.baseQuery) } catch { case ex: Exception => query(validator) }
      if (validator(v)) v else query(validator) // validate
    }
  }

  /**
   * Base class for discrete choices with single selection.
   * @param text question displayed to user
   * @param options discrete choices
   **/
  class MenuOneQueryExecutor[T](text: Option[String], options: Seq[T]) extends QueryExecutor[T] {
    def query(validator: Validator[T]): T = {
      implicit val (in, out) = (System.in, System.out)
      val v = Menu[T](None, text, options.toList.map(i => MenuOption(i))).showAndChooseOne.title
      if (validator(v)) v else query(validator)
    }
  }
  
  /**
   * Base class for discrete choices with multiple selections.
   * @param text question displayed to user
   * @param options discrete choices
   **/
  class MenuMultipleQueryExecutor[T](text: Option[String], options: Seq[T]) extends QueryExecutor[Set[T]] {
    def query(validator: Validator[Set[T]]): Set[T] = {
      implicit val (in, out) = (System.in, System.out)
      val v = Menu[T](None, text, options.toList.map(MenuOption(_))).showAndChooseMultiple.map(_.title)
      if (validator(v)) v else query(validator)
    }
  }

  /**
   * Lifts existing QueryExecutors of type T to Option[T]; shows an initial dialog to determine
   * whether or not the user wants to supply a value, then executes the original query (if so).
   * @param text question displayed to user (if value will be supplied)
   * @param exe (implicit) implicit QueryExecutor for the base type
   **/
  class MenuOptionQueryExecutor[T](text: String)(implicit exe: QueryExecutor[T]) extends QueryExecutor[Option[T]] {
    def query(validator: Validator[Option[T]]): Option[T] = {
     val oQE = new MenuOneQueryExecutor[String](Some(text), Seq("Some", "None"))
     val v = Query[String](text)(oQE).query() match {
       case "Some" => Some(Query[T]("").query(unliftV(validator)))
       case _      => None
     }
     if (validator(v)) v else query(validator)
    }
  }

  // implicit QueryExecutors: can be used for most primitive types
  implicit val intQueryExecutor: QueryExecutor[Int] = new StrConvQueryExecutor[Int](_.toInt)
  implicit val chrQueryExecutor: QueryExecutor[Char] = new StrConvQueryExecutor[Char](_(0))
  implicit val strQueryExecutor: QueryExecutor[String] = new StrConvQueryExecutor[String](_.toString)
  implicit val dblQueryExecutor: QueryExecutor[Double] = new StrConvQueryExecutor[Double](_.toDouble)
  implicit val fltQueryExecutor: QueryExecutor[Float] = new StrConvQueryExecutor[Float](_.toFloat)
  implicit val pthQueryExecutor: QueryExecutor[Path] = new StrConvQueryExecutor[Path](Paths.get(_))
  implicit val boolQE: QueryExecutor[Boolean] = new MenuOneQueryExecutor[Boolean](Some(""), Seq(true, false))


  // this is the innermost implementation of the user interaction
  private object QueryExecutor {
    implicit val (in, out, err) = (System.in, System.out, System.err)
  
    final def baseQuery: String = {
      import java.io._
      while (in.available > 0) in.read
      (new BufferedReader(new InputStreamReader(in))).readLine
    }
  }
}

/* Examples how to use:
(
  Query[String]("Name").query(nonEmpty),
  Query[Int]("Size").query(lessThan(12))
)
*/
