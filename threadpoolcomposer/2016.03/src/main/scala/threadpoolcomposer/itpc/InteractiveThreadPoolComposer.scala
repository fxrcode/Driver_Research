//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     InteractiveThreadPoolComposer.scala
 * @brief    Command line based dialog system for TPC.
 *           Enables the user to perform several TPC related activities via
 *           the command line, e.g., adding custom IP cores, or to generate
 *           (and execute) configuration files.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.itpc
import de.tu_darmstadt.cs.esa.threadpoolcomposer._
import de.tu_darmstadt.cs.esa.threadpoolcomposer.descriptions._
import FeatureWrites._
import scala.util.Properties.{lineSeparator => NL}
import play.api.libs.json._
import play.api.libs.functional.syntax._
import query._
import query.Validator._
import scala.sys.process._

/**
 * Implements user dialog for TPC (interactive).
 **/
final protected class InteractiveThreadPoolComposer(implicit cfg: Configuration) extends Command {
  implicit val in  = System.in
  implicit val out = System.out
  import java.io.{BufferedReader, InputStreamReader}
  import java.nio.file.{Path, Paths}

  def execute: Boolean = { run(true); true }

  /**
   * Shows the main menu and executes user's chosen action.
   * @param first if true, will display title line.
   **/
  def run(first: Boolean = false): Unit = {
    Menu(
      if (first) Some("Welcome to interactive ThreadPoolComposer") else None,
      Some("What would you like to do?"),
      mainMenu
    ).showAndChooseOne.execute()
  }

  /** main menu **/
  private val mainMenu = Seq(
    MenuOption("Setup TPC", setupTpc _),
    MenuOption("Add an existing IPXACT core .zip file to the library", addCore _),
    MenuOption("List known kernels", listKernels _),
    MenuOption("List existing cores in library", listCores _),
    MenuOption("Build a bitstream configuration file", buildConfig _),
    MenuOption("Exit", () => sys.exit(0))
  )

  /** Performs user dialog to add existing IPXACT test. **/
  private def addCore = {
    val newCfg = ConfigurationBuilder.fromConfiguration(cfg)
    newCfg.zipFile = Some(Query[Path]("Path to .zip file").query(PathMustExist()))
    val pQE: QueryExecutor[Set[String]] = new MenuMultipleQueryExecutor(Some("Select Platforms"), cfg.platforms.map(_.name))
    val platforms: Set[String] = Query[Set[String]]("Select Platforms")(pQE).query(MinLength(1))
    out.println("Chosen platforms: " + platforms)
    newCfg.platforms = Some(platforms.toList)
    newCfg.id = Some(Query[Int]("Kernel ID").query(GreaterThan(0)))
    if (! new AddCore()(newCfg.build(Paths.get("."))).execute) { throw new Exception("Adding core failed") }
  }

  /** Lists existing kernels. **/
  private def listKernels = {
    out.println(Seq(
      "",
      "Known kernels: ",
      cfg.kernels.map(k => k.name + " (" + k.descPath +  ")").sortWith(_<_).mkString(NL),
      "").mkString(NL))
    run()
  }

  /** Lists existing cores. **/
  private def listCores = {
    out.println(Seq(
      "",
      "Existing cores: ",
      cfg.cores.map(k => k.name + " (" + k.descPath +  ")").sortWith(_<_).mkString(NL),
      "").mkString(NL))
    run()
  }

  /** Performs user dialog to construct a valid Composition. **/
  private def buildComposition: Seq[CompositionEntry] = {
    val kQE: QueryExecutor[String] = new MenuOneQueryExecutor(Some("Select a kernel"),
      (cfg.cores.map(_.name) ++ cfg.kernels.map(_.name)).toSet.toList.sortWith(_.toLowerCase < _.toLowerCase))
    val ce = CompositionEntry(Query[String]("Select a kernel")(kQE).query(),
      Query[Int]("Number of instances").query(GreaterThan(0)))
    if (Query[Boolean]("Add more kernels to composition?").query())
      Seq(ce) ++ buildComposition
    else
      Seq(ce)
  }

  // TODO can't we use reflection to infer them?
  /** Queries all currently implemented PlatformFeatures. **/
  private def buildPlatformFeatures: JsValue = Json.obj(
    "LED"   -> Json.toJson(LEDFeature.query),
    "OLED"  -> Json.toJson(OLEDFeature.query),
    "Cache" -> Json.toJson(CacheFeature.query),
    "Debug" -> Json.toJson(DebugFeature.query)
  )

  /** Performs user dialog to generate a TPC configuration file. **/
  private def buildConfig = {
    import descriptions.CompositionBuilder.{ce, writesCEs}
    // construct Menu from known / filtered Platforms in current configuration and query user (skip if just one)
    val pQE: QueryExecutor[Set[String]] = new MenuMultipleQueryExecutor(Some(""), cfg.platforms.map(_.name))
    val platforms: Set[String] =
      if (cfg.platforms.length > 1) Query[Set[String]]("Select Platform(s)")(pQE).query(MinLength(1))
      else cfg.platforms.map(_.name).toSet

    // construct Menu from known / filtered Architectures in current configuration and query user (skip if just one)
    val aQE: QueryExecutor[Set[String]] = new MenuMultipleQueryExecutor(Some(""), cfg.architectures.map(_.name))
    val architectures: Set[String] =
      if (cfg.architectures.length > 1) Query[Set[String]]("Select Architecture(s)")(aQE).query(MinLength(1))
      else cfg.architectures.map(_.name).toSet
    
    // query user for DSE mode
    val dse: DesignSpaceExplorationMode = Query[DesignSpaceExplorationMode]("Design Space Exploration Mode").query()

    // construct final Json object
    val json = Json.obj(
      "Platforms" -> Json.toJson(platforms),
      "Architectures" -> Json.toJson(architectures),
      "DesignSpaceExploration" -> Json.toJson(dse),
      "Composition" -> Json.obj("Composition" -> Json.toJson(buildComposition)),
      "PlatformFeatures" -> buildPlatformFeatures   // query user for PlatformFeatures
    )

    // query user for file name for configuration
    var configFile = Paths.get(".")
    var valid = false
    do {
      configFile = Query[Path]("Enter filename for configuration").query()
      try {
        val fw = new java.io.FileWriter(configFile.toFile)
        fw.write(json.toString)
        fw.flush
        fw.close
        valid = true
      } catch { case ex: Exception =>
        logger.severe(Seq("could not write to file '", configFile.toString, "', reason: ", ex).mkString(""))
      }
    } while (! valid)

    // ask user if TPC should be run immediately
    if (Query[Boolean]("Run ThreadPoolComposer with this configuration now?").query()) {
      val ncfg = new Compose()(ConfigurationBuilder.fromFile(configFile).get).execute
    } else run()
  }

  private def setupTpc = {
    val aQE: QueryExecutor[String] = new MenuOneQueryExecutor(Some("Select Architecture"), cfg.architectures.map(_.name))
    val architecture: String = Query[String]("Select Architecture")(aQE).query()
    val pQE: QueryExecutor[String] = new MenuOneQueryExecutor(Some("Select Platform"), cfg.platforms.map(_.name))
    val platform: String = Query[String]("Select Platform")(pQE).query()
    //val mQE: QueryExecutor[Boolean] = new MenuOneQueryExecutor(Some("Build libraries in debug mode?"), Seq(true, false))
    val debug: Boolean = Query[Boolean]("Build libraries in debug mode?").query()
    val relstr = Seq(if (debug) "" else "-DCMAKE_BUILD_TYPE=Release", if (debug) "" else "release")

    // build kernel module
    val m = if (platform.equals("zedboard") || platform.equals("zc706")) "zynq" else platform
    logger.info("Building kernel module for " + m + " ...")
    val mRet = "cd " + sys.env("TPC_HOME") + "/platform/" + m + "/module" #&& "make " + relstr(1) !!

    if (mRet == 0)
      logger.info("Successfully built kernel module for " + platform)
    else
      logger.severe("Failed to build kernel module for " + platform + ", check log!")

    // build libs
    logger.info("Building libplatform for " + platform + " ...")
    val pRet = ("cd " + sys.env("TPC_HOME") + "/platform/" + platform #&& "mkdir -p build" #&& "cd build" #&&
        "cmake " + relstr(0) + " .." #&& "make" #&& "make install").!
    if (pRet == 0)
      logger.info("Successfully built libplatform for " + platform)
    else
      logger.severe("Failed to build libplatform for " + platform + ", check log!")

    logger.info("Building libtpc for " + architecture + " ...")
    val aRet = ("cd " + sys.env("TPC_HOME") + "/arch/" + architecture #&& "mkdir -p build" #&& "cd build" #&&
        "cmake " + relstr(0) + " .." #&& "make" #&& "make install").!
    if (aRet == 0)
      logger.info("Successfully built libtpc for " + architecture)
    else
      logger.severe("Failed to build libtpc for " + architecture + ", check log!")

    pRet == 0 && aRet == 0 && mRet == 0
  }
}
