//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     Validators.scala
 * @brief    Common validation routines for Query.
 *           Provides some common validations for user input, e.g., Int must be less than X.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer.itpc.query
import java.io.File
import java.nio.file.Path

/** Base class for Validators. */
abstract class Validator[-T] {
  /**
   * Performs validation.
   * @param v object to validate
   * @return true, if object is valid, false otherwise
   **/
  def apply(v: T) : Boolean
  /**
   * Returns a textual representation of the constraint that is being
   * checked by this validator to support the user dialog (e.g., [< 12]
   * would be a helpful representation).
   **/
  def constraintToString: String
}

/** All values are valid. **/
case class AllValid[T]() extends Validator[T] {
  def apply(v: T) = Validator.allValid(v)
  def constraintToString = "[]"
}

/** Only values below a limit N are valid. **/
case class LessThan(N: Int) extends Validator[Int] {
  def apply(v: Int) = Validator.lessThan(N)(v)
  def constraintToString = "[< " + N + "]"
}

/** Only values above a limit N are valid. **/
case class GreaterThan(N: Int) extends Validator[Int] {
  def apply(v: Int) = Validator.greaterThan(N)(v)
  def constraintToString = "[> " + N + "]"
}

/** Only non-empty string are valid. **/
case class NonEmpty() extends Validator[String] {
  def apply(v: String) = Validator.nonEmpty(v)
  def constraintToString = "[!= \"\"]"
}

/** Only inputs of minimal given length are valid. **/
case class MinLength[T <: Traversable[_]](N: Int) extends Validator[T] {
  def apply(v: T) = Validator.minLength(N)(v)
  def constraintToString = "[|x| >= " + N + "]"
}

/** Only inputs of maximal given length are valid. **/
case class MaxLength[T <: Traversable[_]](N: Int) extends Validator[T] {
  def apply(v: T) = Validator.maxLength(N)(v)
  def constraintToString = "[|x| >= " + N + "]"
}

/** Only inputs of length between range bounds are valid. **/
case class RangeLength[T <% Traversable[_]](min: Int, max: Int) extends Validator[Traversable[_]] {
  def apply(v: Traversable[_]) = Validator.minLength(min)(v) && Validator.maxLength(max)(v)
  def constraintToString = "[" + min + " <= x <= " + max + "]"
}

/** Only existing paths are valid. **/
case class PathMustExist() extends Validator[Path] {
  def apply(v: Path) = Validator.pathMustExist(v)
  def constraintToString = "[must exist]"
}

/** Only existing files are valid. **/
case class FileMustExist() extends Validator[File] {
  def apply(v: File) = Validator.fileMustExist(v)
  def constraintToString = "[must exist]"
}

/** Companion object containing simple implementations. **/
object Validator {
  /**
   * Lifts an existing validator for T to Option[T], where None is valid.
   * @param validator Validator object to lift
   * @return lifted Validator[Option[T]]
   **/
  def liftV[T](validator: Validator[T]): Validator[Option[T]] = new Validator[Option[T]] {
    def apply(v: Option[T]) = v match {
      case Some(x) => validator(x)
      case _       => true
    }
    def constraintToString = validator.constraintToString
  }
  
  /**
   * Unlifts an existing validator for Option[T] to T.
   * @param validator Validator object to lift
   * @return lifted Validator[Option[T]]
   **/
  def unliftV[T](validator: Validator[Option[T]]): Validator[T] = new Validator[T] {
    def apply(v: T) = validator(Some(v))
    def constraintToString = validator.constraintToString
  }
  
  def lessThan(N: Int): Int => Boolean = _ < N
  def greaterThan(N: Int): Int => Boolean = _ > N
  def nonEmpty: String => Boolean = !_.isEmpty
  def allValid[T]: T => Boolean = _ => true
  def minLength[T <: Traversable[_]](N: Int): T => Boolean = _.size >= N
  def maxLength[T <: Traversable[_]](N: Int): T => Boolean = _.size <= N
  def pathMustExist: Path => Boolean = _.toFile.exists
  def fileMustExist: File => Boolean = _.exists
}
