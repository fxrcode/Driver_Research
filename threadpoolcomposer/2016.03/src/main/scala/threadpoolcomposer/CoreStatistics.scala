//
// Copyright (C) 2014 Jens Korinth, TU Darmstadt
//
// This file is part of ThreadPoolComposer (TPC).
//
// ThreadPoolComposer is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ThreadPoolComposer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with ThreadPoolComposer.  If not, see <http://www.gnu.org/licenses/>.
//
/**
 * @file     CoreStatistics.scala
 * @brief    Task to scan the cores directory and produce spreadsheets for each
 *           platform and architecture containing the evaluation results of all
 *           currently available cores.
 * @authors  J. Korinth, TU Darmstadt (jk@esa.cs.tu-darmstadt.de)
 **/
package de.tu_darmstadt.cs.esa.threadpoolcomposer
import  descriptions._
import  reports._
import  java.io.FileWriter
import  java.nio.file.{Path, Paths}
import  scala.concurrent.{Future, future, Await}
import  scala.concurrent.ExecutionContext.Implicits.global
import  scala.concurrent.duration._

final protected class CoreStatistics(implicit cfg: Configuration) extends Command {
  import util.Properties.{lineSeparator => NL}
  private val HEADER = Seq(
      "Core", "Slices", "Slices (%)", "LUT", "LUT (%)", "FF", "FF (%)",
      "DSP", "DSP (%)", "BRAM", "BRAM (%)", "AchievedClockPeriod", "TargetClockPeriod",
      "Total On-Chip Power (W)", "Dynamic Power (W)", "Device Static Power (W)", "Power Confidence Level"
    ) mkString ","

  def execute: Boolean = {
    val summary = "Run Summary:" + NL + cfg
    logger.info(summary)

    val coreStats: Seq[Future[Boolean]] =
      for (pd <- cfg.platforms; ad <- cfg.architectures; t = Target(ad, pd))
        yield future(coreStatistics(t))

    coreStats map (fr => Await.result(fr, Duration.Inf)) reduce (_&&_)
  }

  private def dumpArea(r: SynthesisReport): String =
    Seq(r.area.map(_.resources.SLICE), r.area.map("%2.1f" format _.slice),
        r.area.map(_.resources.LUT), r.area.map("%2.1f" format _.lut),
        r.area.map(_.resources.FF), r.area.map("%2.1f" format _.ff),
        r.area.map(_.resources.DSP), r.area.map("%2.1f" format _.dsp),
        r.area.map(_.resources.BRAM), r.area.map("%2.1f" format _.bram)
       ) map (_.getOrElse("N/A")) mkString ","

  private def dumpClocks(r: SynthesisReport): String =
    Seq(r.timing.map(_.clockPeriod).getOrElse(""),
        r.timing.map(_.targetPeriod).getOrElse("")
       ) mkString ","

  private def dumpPower(pr: Option[PowerReport]): String = pr match {
    case Some(r) => Seq(r.totalOnChipPower.getOrElse(""),
                        r.dynamicPower.getOrElse(""),
                        r.staticPower.getOrElse(""),
                        r.confidenceLevel.getOrElse("")) mkString ","
    case _ => Seq("","","","") mkString ","
  }

  private def dumpCSV(target: Target, es: Seq[(SynthesisReport, Option[PowerReport])]): Boolean = try {
      val fw = new java.io.FileWriter(target + ".csv")
      fw.append(HEADER + NL)
      fw.append(es map (e => Seq(e._1.file, dumpArea(e._1), dumpClocks(e._1), dumpPower(e._2)) mkString ",") mkString NL)
      fw.flush
      fw.close
      true
    } catch { case e: Exception => false }

  private def coreStatistics(target: Target): Boolean =
    dumpCSV(target, SynthesisReport.findAll(target).map(path => (SynthesisReport(path), PowerReport(path.resolveSibling("power.rpt")))))
}

