scalaVersion := "2.10.4"

libraryDependencies += "com.dongxiguo" %% "zero-log" % "0.3.6"

libraryDependencies += "org.scala-lang" % "scala-compiler" % scalaVersion.value

libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value

scalacOptions ++= Seq("-feature", "-language:postfixOps", "-language:reflectiveCalls", "-deprecation" )

org.scalastyle.sbt.ScalastylePlugin.Settings

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "play" % "play_2.10" % "2.1.1"

lazy val scalaTest = "org.scalatest" %% "scalatest" % "2.2.4"

libraryDependencies += scalaTest % Test

val tpc = inputKey[Unit]("Run ThreadPoolComposer command.")

tpc := (runMain in Compile).partialInput (" de.tu_darmstadt.cs.esa.threadpoolcomposer.ThreadPoolComposer ").evaluated

