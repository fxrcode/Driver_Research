ThreadPoolComposer (TPC)!
=========================

System Requirements
-------------------
TPC is known to work in this environment:

* Intel x86_64 arch
* Fedora 20/22 / Ubuntu 14.04
* Bash Shell 4.2.x+

Other setups likely work as well, but are untested.

Prerequisites
-------------
To use TPC, you'll need working installations of

* Vivado Design Suite 2015.2
* Java SDK 7+
* sbt 0.13.x
* git

If you want to use the High-Level Synthesis flow for generating custom IP
cores, you'll also need:

* Vivado HLS 2015.2+

If you also want to use full-system simulation:

* QuestaSim / ModelSim 10.3+

Check that at least the following are in your `$PATH`:

* `sbt`
* `vivado`
* `git`
* `bash`
* [`vivado_hls`]
* [`vsim`, `vlog`, `vlib`]

Read on in [Getting Started](GETTINGSTARTED.md).
