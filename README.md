# Research on PCIe driver design

## Open source PCIe driver 
* [FPGA_driver: by HUST](https://github.com/zjlearn/FPGA_driver.git), this is a fpga driver for Machine Learning Training. Take a careful look. 
* [fflink/ThreadPoolComposer](https://www.esa.informatik.tu-darmstadt.de/twiki/bin/view/Downloads/ThreadPoolComposer.html)
* [Jetstream](https://maltevesper.github.io/JetStream/)
* EPEE
* Riffa
* connectal
* pci_template.c: a very simple simple pci template.
* pci-driver.c: a better simple pci driver.

## Useful Info
* [FPGA, PCIe example from research gate](https://www.researchgate.net/post/In_terms_of_using_PCIE_for_transfering_data_on_FPGA_is_there_any_simple_example_for_using_Xilinx_PCIE)
	* introduced riffa, EPEE.
	
* [PCI_template.c](http://true-random.com/homepage/projects/pci_template/pci_template.c)

* [connectal/drivers/pcieportal/pcieportal.c](https://github.com/cambridgehackers/connectal/blob/master/drivers/pcieportal/pcieportal.c)
