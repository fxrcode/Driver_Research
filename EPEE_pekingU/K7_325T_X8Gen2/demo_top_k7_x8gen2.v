`timescale 1ns / 1ps
/////////////////////////////////////
//
// demo_top_x8gen2.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// This file is the top file of the demo. It demos how to use the UCR, DMA and UDI
// interfaces. The details of the demo is illustrated in the user guide.
// This demo top uses PCIe Gen2 X8 mode, which requires the PCIe X8Gen2 IP in CORE_WRAPPER.
// It is for 128 bit interface version of EPEE.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module demo_top_x8gen2 #(
	parameter PCIE_LINK_WIDTH = 8, // PCIe lane number
	parameter PCIE_LINK_GEN = 2 // PCIe generation, not used in virtex-6 version
)(
	// System interface for PCIe IP core
	input sys_clk_n,
	input sys_clk_p,
	input sys_reset_n,
	// PCIe interface
	output [PCIE_LINK_WIDTH-1:0]pci_exp_txp,
	output [PCIE_LINK_WIDTH-1:0]pci_exp_txn,
	input  [PCIE_LINK_WIDTH-1:0]pci_exp_rxp,
	input  [PCIE_LINK_WIDTH-1:0]pci_exp_rxn//,
	// user clock
	//input usr_clk_i
);
	/*user clock*/
	wire usr_clk;

	/*PCIe system clock and reset*/
	wire trn_clk;
	wire trn_reset_n;
	/*reset, in PCIe clock domain*/
	wire RST_USR;
	wire RST_USR_done;
	wire RST_HOST2BOARD;
	wire RST_BOARD2HOST;
	
	/** connect PCIE_TOP and EXTEN_LIB **/
	/*PIO interface, in PCIe clock domain*/
	wire [16:0] usr_pio_wr_addr;
	wire [31:0] usr_pio_wr_data;
	wire usr_pio_wr_req;
	wire usr_pio_wr_ack;
	wire [16:0] usr_pio_rd_addr;
	wire [31:0] usr_pio_rd_data;
	wire usr_pio_rd_req;
	wire usr_pio_rd_ack;
	/*DMA interface, in PCIe system clock domain*/
	wire [129:0] FIFO_host2board_dout;
	wire FIFO_host2board_empty;
	wire  FIFO_host2board_rd_en;
	wire  [129:0] FIFO_board2host_din;
	wire FIFO_board2host_prog_full;
	wire  FIFO_board2host_wr_en;
	/*user interrupt interface, in system/trn_clk clock domain*/
	wire  usr_int_req;
	wire  [2:0] usr_int_vector;
	wire  [7:0] usr_int_sw_waiting;
	wire  usr_int_clr;
	wire  usr_int_enable;
	
	/** connect EXTEN_LIB and USER_HW **/
	/* reset signal */
	wire usr_rst;
	/* PIO interface */
	// to user: channel 0, addr[16:15] = 2'b00
	wire [14:0] usr_pio_ch0_wr_addr;
	wire [31:0] usr_pio_ch0_wr_data;
	wire usr_pio_ch0_wr_req;
	wire usr_pio_ch0_wr_ack;
	wire [14:0] usr_pio_ch0_rd_addr;
	wire [31:0] usr_pio_ch0_rd_data;
	wire usr_pio_ch0_rd_req;
	wire usr_pio_ch0_rd_ack;
	/* DMA host2board interface */
	wire [129:0] usr_host2board_dout;
	wire usr_host2board_empty;
	wire usr_host2board_almost_empty;
	wire usr_host2board_rd_en;
	/* DMA board2host interface */
	wire [129:0] usr_board2host_din;
	wire usr_board2host_prog_full;
	wire usr_board2host_wr_en;
	/* user interrupt */
		// user interrupt 0 (vector = 0)
	wire usr0_int_req;
	wire usr0_int_clr;
	wire usr0_int_enable;
		// user interrupt 1 (vector = 1)
	wire  usr1_int_req;
	wire usr1_int_clr;
	wire usr1_int_enable;
	
	//IBUF   usr_clk_ibuf (.O(usr_clk), .I(usr_clk_i));

	assign usr_clk = trn_clk;

	PCIE_TOP # (
		.PCIE_LINK_WIDTH(PCIE_LINK_WIDTH), // PCIe lane
		.PCIE_LINK_GEN(PCIE_LINK_GEN) // PCIe gen
	)
	PCIE_TOP(
		.sys_clk_n(sys_clk_n), 
		.sys_clk_p(sys_clk_p), 
		.sys_reset_n(sys_reset_n), 
		.pci_exp_txp(pci_exp_txp), 
		.pci_exp_txn(pci_exp_txn), 
		.pci_exp_rxp(pci_exp_rxp), 
		.pci_exp_rxn(pci_exp_rxn), 
		.trn_clk(trn_clk), 
		.trn_reset_n(trn_reset_n), 
		.RST_USR(RST_USR), 
		.RST_USR_done(RST_USR_done),	
		.RST_HOST2BOARD(RST_HOST2BOARD),
		.RST_BOARD2HOST(RST_BOARD2HOST),
		.usr_pio_wr_addr(usr_pio_wr_addr), 
		.usr_pio_wr_data(usr_pio_wr_data), 
		.usr_pio_wr_req(usr_pio_wr_req),
		.usr_pio_wr_ack(usr_pio_wr_ack),
		.usr_pio_rd_addr(usr_pio_rd_addr), 
		.usr_pio_rd_data(usr_pio_rd_data),
		.usr_pio_rd_req(usr_pio_rd_req),
		.usr_pio_rd_ack(usr_pio_rd_ack),
		.usr_int_req(usr_int_req),
		.usr_int_vector(usr_int_vector),
		.usr_int_sw_waiting(usr_int_sw_waiting),
		.usr_int_clr(usr_int_clr),
		.usr_int_enable(usr_int_enable),
		.FIFO_host2board_dout(FIFO_host2board_dout), 
		.FIFO_host2board_empty(FIFO_host2board_empty), 
		.FIFO_host2board_rd_en(FIFO_host2board_rd_en), 
		.FIFO_board2host_din(FIFO_board2host_din), 
		.FIFO_board2host_prog_full(FIFO_board2host_prog_full), 
		.FIFO_board2host_wr_en(FIFO_board2host_wr_en)
	);
	
	EXTEN_LIB EXTEN_LIB(
		// clock
		.trn_clk(trn_clk), 
		// reset
		.usr_rst_clk(usr_clk),
		.trn_reset_n(trn_reset_n),
		.RST_USR(RST_USR), 
		.RST_USR_done(RST_USR_done),	
		.usr_rst(usr_rst),
		// PIO
		.usr_pio_clk(usr_clk),
		.usr_pio_wr_addr(usr_pio_wr_addr), 
		.usr_pio_wr_data(usr_pio_wr_data), 
		.usr_pio_wr_req(usr_pio_wr_req),
		.usr_pio_wr_ack(usr_pio_wr_ack),
		.usr_pio_rd_addr(usr_pio_rd_addr), 
		.usr_pio_rd_data(usr_pio_rd_data),
		.usr_pio_rd_req(usr_pio_rd_req),
		.usr_pio_rd_ack(usr_pio_rd_ack),
		/*PIO fifo, user clock domain*/
		// channel 0, addr[16:15] = 2'b00
		.usr_pio_ch0_wr_addr(usr_pio_ch0_wr_addr),
		.usr_pio_ch0_wr_data(usr_pio_ch0_wr_data),
		.usr_pio_ch0_wr_req(usr_pio_ch0_wr_req),
		.usr_pio_ch0_wr_ack(usr_pio_ch0_wr_ack),
		.usr_pio_ch0_rd_addr(usr_pio_ch0_rd_addr),
		.usr_pio_ch0_rd_data(usr_pio_ch0_rd_data),
		.usr_pio_ch0_rd_req(usr_pio_ch0_rd_req),
		.usr_pio_ch0_rd_ack(usr_pio_ch0_rd_ack),
		// DMA
		// DMA host2board
		.usr_host2board_clk(usr_clk),
		.usr_host2board_dout(usr_host2board_dout),
		.usr_host2board_empty(usr_host2board_empty),
		.usr_host2board_almost_empty(usr_host2board_almost_empty),
		.usr_host2board_rd_en(usr_host2board_rd_en),
		.RST_HOST2BOARD(RST_HOST2BOARD),
		.FIFO_host2board_dout(FIFO_host2board_dout), 
		.FIFO_host2board_empty(FIFO_host2board_empty), 
		.FIFO_host2board_rd_en(FIFO_host2board_rd_en), 
		// DMA board2host
		.usr_board2host_clk(usr_clk),
		.usr_board2host_din(usr_board2host_din),
		.usr_board2host_prog_full(usr_board2host_prog_full),
		.usr_board2host_wr_en(usr_board2host_wr_en),
		.RST_BOARD2HOST(RST_BOARD2HOST),
		.FIFO_board2host_din(FIFO_board2host_din), 
		.FIFO_board2host_prog_full(FIFO_board2host_prog_full), 
		.FIFO_board2host_wr_en(FIFO_board2host_wr_en),
		// user interrupt
		.usr_interrupt_clk(usr_clk),
		// from PCIe system
		.usr_int_req(usr_int_req),
		.usr_int_vector(usr_int_vector),
		.usr_int_sw_waiting(usr_int_sw_waiting),
		.usr_int_clr(usr_int_clr),
		.usr_int_enable(usr_int_enable),
		// user interrupt 0 (vector = 0)
		.usr0_int_req(usr0_int_req),
		.usr0_int_clr(usr0_int_clr),
		.usr0_int_enable(usr0_int_enable),
		// user interrupt 1 (vector = 1)
		.usr1_int_req(usr1_int_req),
		.usr1_int_clr(usr1_int_clr),
		.usr1_int_enable(usr1_int_enable)
	);
	
	USER_HW USER_HW(
		.usr_clk(usr_clk),
		.usr_rst(usr_rst),
		// PCIe PIO
		.usr_pio_ch0_wr_addr(usr_pio_ch0_wr_addr),
		.usr_pio_ch0_wr_data(usr_pio_ch0_wr_data),
		.usr_pio_ch0_wr_req(usr_pio_ch0_wr_req),
		.usr_pio_ch0_wr_ack(usr_pio_ch0_wr_ack),
		.usr_pio_ch0_rd_addr(usr_pio_ch0_rd_addr),
		.usr_pio_ch0_rd_data(usr_pio_ch0_rd_data),
		.usr_pio_ch0_rd_req(usr_pio_ch0_rd_req),
		.usr_pio_ch0_rd_ack(usr_pio_ch0_rd_ack),
		// PCIe interrupt
		.usr0_int_req(usr0_int_req),
		.usr0_int_clr(usr0_int_clr),
		.usr0_int_enable(usr0_int_enable),
		.usr1_int_req(usr1_int_req),
		.usr1_int_clr(usr1_int_clr),
		.usr1_int_enable(usr1_int_enable),
		// PCIe DMA
		.usr_host2board_dout(usr_host2board_dout),
		.usr_host2board_empty(usr_host2board_empty),
		.usr_host2board_almost_empty(usr_host2board_almost_empty),
		.usr_host2board_rd_en(usr_host2board_rd_en),
		.usr_board2host_din(usr_board2host_din),
		.usr_board2host_prog_full(usr_board2host_prog_full),
		.usr_board2host_wr_en(usr_board2host_wr_en)
	);
endmodule
