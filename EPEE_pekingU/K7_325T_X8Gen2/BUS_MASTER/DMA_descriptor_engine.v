`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_descriptor_engine.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The descriptor engine. It DMA the descriptors from host to FPGA board so that EPEE
// will know what to DMA next. This module's DMA operation is controlled by meta 
// descriptors.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_descriptor_engine #
(
	parameter TAG_DEFAULT = 8'd0
)
(
	input sys_clk,
	input sys_rst_n,
	input dma_channel_rst,
	output dma_channel_rst_done,
	// to DMA_MRD_channel_collector
	output [71:0] DMA_descriptor_MRD_TLP_dout,
	output DMA_descriptor_MRD_TLP_empty,
	input  DMA_descriptor_MRD_TLP_rd_en,
	// from DMA_CPLD_channel_dispatcher
	input  [71:0] DMA_descriptor_CPLD_TLP_din,
	output DMA_descriptor_CPLD_TLP_prog_full,
	input  DMA_descriptor_CPLD_TLP_wr_en,
	// to DMA_engine's DMA_descriptor_buf
	output [95:0] dma_descriptor_buf_din,
	input dma_descriptor_buf_prog_full,
	output dma_descriptor_buf_wr_en,
	// from DMA_INIT_CTRL
	input dma_init_descriptor_mux_ctrl,
	input [95:0] dma_init_descriptor_din,
	input dma_init_descriptor_wr_en,
	output dma_init_descriptor_prog_full,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	input CFG_EXT_TAG_EN,
	input [2:0] CFG_MAX_RD_REQ_SIZE,
	input CFG_BUS_MSTR_EN
);

	// connect DMA_descriptor_FSM and tlp_head_gen
	wire [127:0] descriptor_mrd_buf_dout;
	wire descriptor_mrd_buf_empty;
	wire descriptor_mrd_buf_rd_en;
	// connect tlp_head_gen and dma_meta_descriptor_buf
	wire [95:0] dma_meta_descriptor_buf_dout;
	wire dma_meta_descriptor_buf_empty;
	wire dma_meta_descriptor_buf_rd_en;
	// connect DMA_descriptor_FSM and DMA_descriptor_dispatcher
	wire [71:0] descriptor_cpld_buf_din;
	wire descriptor_cpld_buf_prog_full;
	wire descriptor_cpld_buf_wr_en;
	// connect DMA_descriptor_dispatcher and dma_meta_descriptor_buf
	wire [95:0] dma_meta_descriptor_buf_din_pre;
	wire dma_meta_descriptor_buf_prog_full;
	wire dma_meta_descriptor_buf_wr_en_pre;
	
	assign dma_init_descriptor_prog_full = dma_meta_descriptor_buf_prog_full;

	DMA_descriptor_FSM DMA_descriptor_FSM(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_channel_rst(dma_channel_rst),
		.dma_channel_rst_done(dma_channel_rst_done),
		// to DMA_MRD_channel_collector
		.DMA_descriptor_MRD_TLP_dout(DMA_descriptor_MRD_TLP_dout),
		.DMA_descriptor_MRD_TLP_empty(DMA_descriptor_MRD_TLP_empty),
		.DMA_descriptor_MRD_TLP_rd_en(DMA_descriptor_MRD_TLP_rd_en),
		// from DMA_CPLD_channel_dispatcher
		.DMA_descriptor_CPLD_TLP_din(DMA_descriptor_CPLD_TLP_din),
		.DMA_descriptor_CPLD_TLP_prog_full(DMA_descriptor_CPLD_TLP_prog_full),
		.DMA_descriptor_CPLD_TLP_wr_en(DMA_descriptor_CPLD_TLP_wr_en),
		// to DMA_descriptor_dispatcher
		.descriptor_cpld_buf_din(descriptor_cpld_buf_din), // [63:0] is data(), din[64] means [31:0] is valid(), din[65] means [63:32] is valid
		.descriptor_cpld_buf_prog_full(descriptor_cpld_buf_prog_full),
		.descriptor_cpld_buf_wr_en(descriptor_cpld_buf_wr_en),
		// from TLP_head_gen
		.descriptor_mrd_buf_dout(descriptor_mrd_buf_dout),
		.descriptor_mrd_buf_empty(descriptor_mrd_buf_empty),
		.descriptor_mrd_buf_rd_en(descriptor_mrd_buf_rd_en)
	);
	
	TLP_head_gen tlp_head_gen(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_channel_rst(dma_channel_rst),
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID),
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN),
		.CFG_MAX_TLP_SIZE(CFG_MAX_RD_REQ_SIZE),
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN),
		// from descriptor FIFO
		.descriptor_buf_dout(dma_meta_descriptor_buf_dout),
		.descriptor_buf_empty(dma_meta_descriptor_buf_empty),
		.descriptor_buf_rd_en(dma_meta_descriptor_buf_rd_en),
		// from tag FIFO
		.tag_buf_dout(TAG_DEFAULT),
		.tag_buf_empty(1'b0),
		.tag_buf_rd_en(),
		// to TLP head FIFO
		.tlp_head_buf_dout(descriptor_mrd_buf_dout),
		.tlp_head_buf_empty(descriptor_mrd_buf_empty),
		.tlp_head_buf_rd_en(descriptor_mrd_buf_rd_en)
	);
	
	DMA_descriptor_dispatcher DMA_descriptor_dispatcher(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_channel_rst(dma_channel_rst), 
		// from DMA_descriptor_FSM
		.descriptor_cpld_buf_din(descriptor_cpld_buf_din), 
		.descriptor_cpld_buf_prog_full(descriptor_cpld_buf_prog_full), 
		.descriptor_cpld_buf_wr_en(descriptor_cpld_buf_wr_en), 
		// to DMA_engine's DMA_descriptor_buf
		.dma_descriptor_buf_din(dma_descriptor_buf_din), 
		.dma_descriptor_buf_prog_full(dma_descriptor_buf_prog_full), 
		.dma_descriptor_buf_wr_en(dma_descriptor_buf_wr_en),
		// to DMA_meta_descriptor_buf 
		.dma_meta_descriptor_buf_din_pre(dma_meta_descriptor_buf_din_pre), 
		.dma_meta_descriptor_buf_prog_full(dma_meta_descriptor_buf_prog_full), 
		.dma_meta_descriptor_buf_wr_en_pre(dma_meta_descriptor_buf_wr_en_pre)
	);
	
	FIFO_sync_width96_depth16_FWFT dma_meta_descriptor_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din((dma_init_descriptor_mux_ctrl == 1'b1)?dma_init_descriptor_din:dma_meta_descriptor_buf_din_pre),
		.wr_en((dma_init_descriptor_mux_ctrl == 1'b1)?dma_init_descriptor_wr_en:dma_meta_descriptor_buf_wr_en_pre),
		.rd_en(dma_meta_descriptor_buf_rd_en),
		.dout(dma_meta_descriptor_buf_dout),
		.full(),
		.empty(dma_meta_descriptor_buf_empty),
		.prog_full(dma_meta_descriptor_buf_prog_full)
	);

endmodule
