`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_host2board_engine.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The DMA host to board engine. It controls the host to board side DMA process.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_host2board_engine(
	input sys_clk,
	input sys_rst_n,
	input dma_host2board_rst,
	output dma_host2board_rst_done,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	input CFG_EXT_TAG_EN,
	input [2:0]  CFG_MAX_RD_REQ_SIZE,
	input CFG_BUS_MSTR_EN,
	// from DMA_host2board_descriptor_engine
	input [95:0] DMA_r_descriptor_buf_din,
	input DMA_r_descriptor_buf_wr_en,
	output DMA_r_descriptor_buf_prog_full,
	// to DMA_MRD_channel_collector
	output [71:0] DMA_r_MRD_TLP_dout,
	input DMA_r_MRD_TLP_rd_en,
	output DMA_r_MRD_TLP_empty,
	// from DMA_CPLD_channel_dispatcher
	input [133:0] DMA_r_CPLD_TLP_din,
	input DMA_r_CPLD_TLP_wr_en,
	output DMA_r_CPLD_TLP_prog_full,
	// to host2board_buf
	output [131:0] host2board_buf_din,
	output host2board_buf_wr_en,
	input host2board_buf_prog_full,
	// connect with SYS_CSR
	input [63:0] DMA_r_timeout_threshold,
	input DMA_r_start_state,
	output DMA_r_done_state,
	output DMA_r_error_state
);

	// connect RX_CPLD_FSM and RX_sequencing
	wire [139:0] RX_buf_dout;
	wire RX_buf_empty;
	wire RX_buf_rd_en;
	
	// connect RX_sequencing and DMA_r_req_buf
	wire [17:0] dma_r_req_buf_dout;
	wire dma_r_req_buf_empty;
	wire dma_r_req_buf_rd_en;
	
	// connect RX_sequencing and RX_data_packer
	wire [129:0] RX_buf_seq_dout;
	wire RX_buf_seq_empty;
	wire RX_buf_seq_rd_en;
	
	// connect RX_sequencing and valid_tag_buf
	wire [7:0] valid_tag_buf_din;
	wire valid_tag_buf_prog_full;
	wire valid_tag_buf_wr_en;
	
	// connect DMA_r_descriptor_buf and RX_data_packer
	wire [17:0] DMA_r_descirptor_req_len_buf_dout;
	wire DMA_r_descirptor_req_len_buf_rd_en;
	wire DMA_r_descirptor_req_len_buf_empty;
	
	// connect DMA_r_rst_gen and DMA_r_req_buf, Valid_tag_buf
	wire [7:0] latest_req_tag;
	wire [7:0] latest_free_tag;
	wire dma_r_rst;
	
	// connect DMA_r_TLP_head_gen and Valid_tag_buf
	wire [7:0] valid_tag_buf_dout;
	wire valid_tag_buf_rd_en;
	wire valid_tag_buf_empty;
	
	// connect DMA_r_descriptor_buf and DMA_r_TLP_head_gen
	wire [95:0] DMA_r_descriptor_buf_dout;
	wire DMA_r_descriptor_buf_rd_en;
	wire DMA_r_descriptor_buf_empty;
	
	// connect DMA_r_TLP_head_gen and DMA_r_TLP_committer
	wire [127:0] dma_r_tlp_head_buf_dout;
	wire dma_r_tlp_head_buf_rd_en;
	wire dma_r_tlp_head_buf_empty;
	
	// connect DMA_r_TLP_committer and DMA_r_req_buf
	wire [17:0] dma_r_req_buf_din; // {tag[7:0], length[9:0]}
	wire dma_r_req_buf_wr_en;
	wire dma_r_req_buf_prog_full;

	RX_CPLD_FSM RX_CPLD_FSM(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_r_rst(dma_r_rst),
		// from DMA_CPLD_channel_dispatcher
		.DMA_r_CPLD_TLP_din(DMA_r_CPLD_TLP_din),
		.DMA_r_CPLD_TLP_prog_full(DMA_r_CPLD_TLP_prog_full),
		.DMA_r_CPLD_TLP_wr_en(DMA_r_CPLD_TLP_wr_en),
		// to RX_sequencing
		.RX_buf_dout(RX_buf_dout), // {DW_valid[1:0](), tag[7:0](), data[63:0]}
		.RX_buf_empty(RX_buf_empty),
		.RX_buf_rd_en(RX_buf_rd_en)
	);

	RX_sequencing RX_sequencing(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n),
		.dma_r_rst(dma_r_rst),	
		// from RX_CPLD_FSM
		.RX_buf_dout(RX_buf_dout), 
		.RX_buf_empty(RX_buf_empty), 
		.RX_buf_rd_en(RX_buf_rd_en), 
		// from DMA_r_req_buf
		.dma_r_req_buf_dout(dma_r_req_buf_dout), 
		.dma_r_req_buf_empty(dma_r_req_buf_empty), 
		.dma_r_req_buf_rd_en(dma_r_req_buf_rd_en), 
		// to RX_buf_seq
		.RX_buf_seq_dout(RX_buf_seq_dout), // {6'd0, DW_valid[1:0], data[63:0]}
		.RX_buf_seq_empty(RX_buf_seq_empty),
		.RX_buf_seq_rd_en(RX_buf_seq_rd_en),
		// to valid_tag_buf
		.valid_tag_buf_din(valid_tag_buf_din), 
		.valid_tag_buf_prog_full(valid_tag_buf_prog_full), 
		.valid_tag_buf_wr_en(valid_tag_buf_wr_en) 
	);
	
	RX_data_packer rx_data_packer(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_r_rst(dma_r_rst),
		// connect with SYS_CSR
		.DMA_r_timeout_threshold(DMA_r_timeout_threshold),
		.DMA_r_start_state(DMA_r_start_state),
		.DMA_r_done_state(DMA_r_done_state),
		.DMA_r_error_state(DMA_r_error_state),
		// from DMA_r_descriptor_buf
		.DMA_r_descirptor_req_len_buf_dout(DMA_r_descirptor_req_len_buf_dout), 
		.DMA_r_descirptor_req_len_buf_rd_en(DMA_r_descirptor_req_len_buf_rd_en),
		.DMA_r_descirptor_req_len_buf_empty(DMA_r_descirptor_req_len_buf_empty),
		// from RX_sequencing
		.RX_buf_seq_dout(RX_buf_seq_dout), 
		.RX_buf_seq_rd_en(RX_buf_seq_rd_en),
		.RX_buf_seq_empty(RX_buf_seq_empty),
		// to host2board_buf
		.host2board_buf_din(host2board_buf_din),
		.host2board_buf_wr_en(host2board_buf_wr_en),
		.host2board_buf_prog_full(host2board_buf_prog_full)
	);

	DMA_r_req_buf DMA_r_req_buf(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_r_rst(dma_r_rst), 
		// from DMA_r_TLP_committer
		.dma_r_req_buf_din(dma_r_req_buf_din), 
		.dma_r_req_buf_wr_en(dma_r_req_buf_wr_en), 
		.dma_r_req_buf_prog_full(dma_r_req_buf_prog_full), 
		// to RX_sequencing
		.dma_r_req_buf_dout(dma_r_req_buf_dout), 
		.dma_r_req_buf_rd_en(dma_r_req_buf_rd_en), 
		.dma_r_req_buf_empty(dma_r_req_buf_empty), 
		// to DMA_r_rst_gen
		.latest_req_tag(latest_req_tag)
	);
	
	valid_tag_buf valid_tag_buf(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_r_rst(dma_r_rst), 
		// from RX_sequencing
		.valid_tag_buf_din(valid_tag_buf_din), 
		.valid_tag_buf_wr_en(valid_tag_buf_wr_en), 
		.valid_tag_buf_prog_full(valid_tag_buf_prog_full), 
		// to DMA_r_TLP_head_gen
		.valid_tag_buf_dout(valid_tag_buf_dout), 
		.valid_tag_buf_rd_en(valid_tag_buf_rd_en), 
		.valid_tag_buf_empty(valid_tag_buf_empty), 
		// to DMA_r_rst_gen
		.latest_free_tag(latest_free_tag)
	);
	
	DMA_r_rst_gen DMA_r_rst_gen(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_host2board_rst(dma_host2board_rst), 
		.dma_host2board_rst_done(dma_host2board_rst_done), 
		// for request tag
		.latest_req_tag(latest_req_tag), 
		.latest_free_tag(latest_free_tag), 
		// reset signal
		.dma_r_rst(dma_r_rst)
	);
	
	DMA_r_TLP_committer DMA_r_TLP_committer(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_host2board_rst(dma_host2board_rst), // when this reset assert, the module will reset when current TLP is sent
		// from DMA_r_TLP_head_gen
		.dma_r_tlp_head_buf_dout(dma_r_tlp_head_buf_dout),
		.dma_r_tlp_head_buf_rd_en(dma_r_tlp_head_buf_rd_en),
		.dma_r_tlp_head_buf_empty(dma_r_tlp_head_buf_empty),
		// to FIFO DMA_r_MRD_TLP_dout
		.DMA_r_MRD_TLP_dout(DMA_r_MRD_TLP_dout),
		.DMA_r_MRD_TLP_rd_en(DMA_r_MRD_TLP_rd_en),
		.DMA_r_MRD_TLP_empty(DMA_r_MRD_TLP_empty),
		// to DMA_r_req_buf
		.dma_r_req_buf_din(dma_r_req_buf_din), // {tag[7:0], length[9:0]}
		.dma_r_req_buf_wr_en(dma_r_req_buf_wr_en),
		.dma_r_req_buf_prog_full(dma_r_req_buf_prog_full)
	);
	
	TLP_head_gen DMA_r_TLP_head_gen(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_channel_rst(dma_r_rst),
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID),
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN),
		.CFG_MAX_TLP_SIZE(CFG_MAX_RD_REQ_SIZE),
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN),
		// from descriptor FIFO
		.descriptor_buf_dout(DMA_r_descriptor_buf_dout),
		.descriptor_buf_empty(DMA_r_descriptor_buf_empty),
		.descriptor_buf_rd_en(DMA_r_descriptor_buf_rd_en),
		// from tag FIFO
		.tag_buf_dout(valid_tag_buf_dout),
		.tag_buf_empty(valid_tag_buf_empty),
		.tag_buf_rd_en(valid_tag_buf_rd_en),
		// to TLP head FIFO
		.tlp_head_buf_dout(dma_r_tlp_head_buf_dout),
		.tlp_head_buf_empty(dma_r_tlp_head_buf_empty),
		.tlp_head_buf_rd_en(dma_r_tlp_head_buf_rd_en)
	);
	
	DMA_r_descriptor_buf DMA_r_descriptor_buf(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_r_rst(dma_r_rst), 
		// from DMA_host2board_descriptor_engine
		.DMA_r_descriptor_buf_din(DMA_r_descriptor_buf_din), 
		.DMA_r_descriptor_buf_wr_en(DMA_r_descriptor_buf_wr_en), 
		.DMA_r_descriptor_buf_prog_full(DMA_r_descriptor_buf_prog_full), 
		// to DMA_r_TLP_head_gen
		.DMA_r_descriptor_buf_dout(DMA_r_descriptor_buf_dout), 
		.DMA_r_descriptor_buf_rd_en(DMA_r_descriptor_buf_rd_en), 
		.DMA_r_descriptor_buf_empty(DMA_r_descriptor_buf_empty), 
		// to RX_data_packer
		.DMA_r_descirptor_req_len_buf_dout(DMA_r_descirptor_req_len_buf_dout), 
		.DMA_r_descirptor_req_len_buf_rd_en(DMA_r_descirptor_req_len_buf_rd_en), 
		.DMA_r_descirptor_req_len_buf_empty(DMA_r_descirptor_req_len_buf_empty)
	);

endmodule
