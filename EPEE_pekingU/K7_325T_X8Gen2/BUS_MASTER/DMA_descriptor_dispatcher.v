`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_description_dispatcher.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Dispatch the descriptors into meta descriptor buf and (normal) descriptor buf.
// The meta descriptor buf contains those descriptors that describe other descriptor's 
// DMA operations.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define DW_RDY_0 4'b0001
`define DW_RDY_1 4'b0010
`define DW_RDY_2 4'b0100
`define DW_RDY_3 4'b1000

module DMA_descriptor_dispatcher(
	input sys_clk,
	input sys_rst_n,
	input dma_channel_rst,
	// from DMA_board2host_descriptor_FSM
	input [71:0] descriptor_cpld_buf_din,
	output descriptor_cpld_buf_prog_full,
	input descriptor_cpld_buf_wr_en,
	// to DMA_board2host_engine's DMA_descriptor_buf
	output reg [95:0] dma_descriptor_buf_din,
	input dma_descriptor_buf_prog_full,
	output reg dma_descriptor_buf_wr_en,
	// to DMA_meta_descriptor_buf
	output reg [95:0] dma_meta_descriptor_buf_din_pre,
	input dma_meta_descriptor_buf_prog_full,
	output reg dma_meta_descriptor_buf_wr_en_pre
);

	wire [71:0] descriptor_cpld_buf_dout;
	wire descriptor_cpld_buf_empty;
	reg descriptor_cpld_buf_rd_en;
	
	// buffer the descriptor
	reg [31:0] descriptor_dw0;
	reg [31:0] descriptor_dw1;
	reg [31:0] descriptor_dw2;

	(*KEEP="TRUE"*)reg [3:0] state;
	reg reset;
	always @(posedge sys_clk)begin
		reset <= (~sys_rst_n) || dma_channel_rst;
	end
	
	always @(posedge sys_clk)begin
		if(reset)begin
			state <= `DW_RDY_0;
			dma_descriptor_buf_din <= 0;
			dma_descriptor_buf_wr_en <= 1'b0;
			dma_meta_descriptor_buf_din_pre <= 0;
			dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
			descriptor_cpld_buf_rd_en <= 1'b0;
			descriptor_dw0 <= 0;
			descriptor_dw1 <= 0;
			descriptor_dw2 <= 0;
		end
		else begin
			if(dma_descriptor_buf_prog_full == 1'b0 && dma_meta_descriptor_buf_prog_full == 1'b0 &&
				descriptor_cpld_buf_empty == 1'b0)begin
				descriptor_cpld_buf_rd_en <= 1'b1;
			end
			else begin
				descriptor_cpld_buf_rd_en <= 1'b0;
			end
			if(descriptor_cpld_buf_rd_en == 1'b1 && descriptor_cpld_buf_empty == 1'b0)begin
				case(state)
					`DW_RDY_0:begin // no data is in descriptor_dw0 or descriptor_dw1 or descriptor_dw2
						dma_descriptor_buf_wr_en <= 1'b0;
						dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
						case(descriptor_cpld_buf_dout[65:64])
							2'b01:begin
								descriptor_dw0 <= descriptor_cpld_buf_dout[31:0];
								state <= `DW_RDY_1;
							end
							2'b10:begin
								descriptor_dw0 <= descriptor_cpld_buf_dout[63:32];
								state <= `DW_RDY_1;
							end
							2'b11:begin
								descriptor_dw0 <= descriptor_cpld_buf_dout[63:32];
								descriptor_dw1 <= descriptor_cpld_buf_dout[31:0];
								state <= `DW_RDY_2;
							end
							default:begin
								state <= state;
							end
						endcase
					end
					`DW_RDY_1:begin // no data is in descriptor_dw1 or descriptor_dw2
						dma_descriptor_buf_wr_en <= 1'b0;
						dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
						case(descriptor_cpld_buf_dout[65:64])
							2'b01:begin
								descriptor_dw1 <= descriptor_cpld_buf_dout[31:0];
								state <= `DW_RDY_2;
							end
							2'b10:begin
								descriptor_dw1 <= descriptor_cpld_buf_dout[63:32];
								state <= `DW_RDY_2;
							end
							2'b11:begin
								descriptor_dw1 <= descriptor_cpld_buf_dout[63:32];
								descriptor_dw2 <= descriptor_cpld_buf_dout[31:0];
								state <= `DW_RDY_3;
							end
							default:begin
								state <= state;
							end
						endcase
					end
					`DW_RDY_2:begin  // no data is in descriptor_dw2
						case(descriptor_cpld_buf_dout[65:64])
							2'b01:begin
								descriptor_dw2 <= descriptor_cpld_buf_dout[31:0];
								state <= `DW_RDY_3;
								dma_descriptor_buf_wr_en <= 1'b0;
								dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
							end
							2'b10:begin
								descriptor_dw2 <= descriptor_cpld_buf_dout[63:32];
								state <= `DW_RDY_3;
								dma_descriptor_buf_wr_en <= 1'b0;
								dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
							end
							2'b11:begin
								if(descriptor_dw0[31:29] == 3'b001)begin // this is a meta descriptor
									dma_meta_descriptor_buf_din_pre <= {descriptor_dw0, descriptor_dw1, descriptor_cpld_buf_dout[63:32]};
									dma_meta_descriptor_buf_wr_en_pre <= 1'b1;
								end
								else begin // this is a normal descriptor
									dma_descriptor_buf_din <= {descriptor_dw0, descriptor_dw1, descriptor_cpld_buf_dout[63:32]};
									dma_descriptor_buf_wr_en <= 1'b1;
								end
								state <= `DW_RDY_0;
							end
							default:begin
								dma_descriptor_buf_wr_en <= 1'b0;
								dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
								state <= state;
							end
						endcase
					end
					`DW_RDY_3:begin
						case(descriptor_cpld_buf_dout[65:64])
							2'b01:begin
								if(descriptor_dw0[31:29] == 3'b001)begin // this is a meta descriptor
									dma_meta_descriptor_buf_din_pre <= {descriptor_dw0, descriptor_dw1, descriptor_dw2};
									dma_meta_descriptor_buf_wr_en_pre <= 1'b1;
								end
								else begin // this is a normal descriptor
									dma_descriptor_buf_din <= {descriptor_dw0, descriptor_dw1, descriptor_dw2};
									dma_descriptor_buf_wr_en <= 1'b1;
								end
								state <= `DW_RDY_0;
							end
							2'b10:begin
								if(descriptor_dw0[31:29] == 3'b001)begin // this is a meta descriptor
									dma_meta_descriptor_buf_din_pre <= {descriptor_dw0, descriptor_dw1, descriptor_dw2};
									dma_meta_descriptor_buf_wr_en_pre <= 1'b1;
								end
								else begin // this is a normal descriptor
									dma_descriptor_buf_din <= {descriptor_dw0, descriptor_dw1, descriptor_dw2};
									dma_descriptor_buf_wr_en <= 1'b1;
								end
								state <= `DW_RDY_0;
							end
							2'b11:begin
								if(descriptor_dw0[31:29] == 3'b001)begin // this is a meta descriptor
									dma_meta_descriptor_buf_din_pre <= {descriptor_dw0, descriptor_dw1, descriptor_dw2};
									dma_meta_descriptor_buf_wr_en_pre <= 1'b1;
								end
								else begin // this is a normal descriptor
									dma_descriptor_buf_din <= {descriptor_dw0, descriptor_dw1, descriptor_dw2};
									dma_descriptor_buf_wr_en <= 1'b1;
								end
								descriptor_dw0 <= descriptor_cpld_buf_dout[31:0];
								state <= `DW_RDY_1;
							end
							default:begin
								dma_descriptor_buf_wr_en <= 1'b0;
								dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
								state <= state;
							end
						endcase
					end
					default:begin
						state <= `DW_RDY_0;
						dma_descriptor_buf_wr_en <= 1'b0;
						dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
					end
				endcase
			end
			else begin
				dma_descriptor_buf_wr_en <= 1'b0;
				dma_meta_descriptor_buf_wr_en_pre <= 1'b0;
			end
		end
	end

	FIFO_sync_width72_depth512_FWFT descriptor_cpld_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(descriptor_cpld_buf_din),
		.wr_en(descriptor_cpld_buf_wr_en),
		.rd_en(descriptor_cpld_buf_rd_en),
		.prog_full_thresh(9'd260), // for enough space for next request
		.dout(descriptor_cpld_buf_dout),
		.full(),
		.empty(descriptor_cpld_buf_empty),
		.prog_full(descriptor_cpld_buf_prog_full)
	);

endmodule
