`timescale 1ns / 1ps
/////////////////////////////////////
//
// RX_sequencing.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The CplD TLP can interleave with each other. The MRd0's CplD TLPs can arrive after 
// MRd1's CplD TLPs. This module will deal with the sequence of that TLPs.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module RX_sequencing(
	input sys_clk,
	input sys_rst_n,
	input dma_r_rst,
	// from RX_CPLD_FSM
	input [139:0] RX_buf_dout, // {DW_valid[3:0], tag[7:0], data[127:0]}
	input RX_buf_empty,
	output RX_buf_rd_en,
	// from DMA_r_req_buf
	input [17:0] dma_r_req_buf_dout, // {tag[7:0], length[9:0]}, tag[7:4] is fixed to 4'b0001, length is in DW
	input dma_r_req_buf_empty,
	output reg dma_r_req_buf_rd_en,
	// to RX_buf_seq
	output [129:0] RX_buf_seq_dout, // {DW_count[1:0], data[127:0]}, valid data should begin at MSB
	output RX_buf_seq_empty,
	input RX_buf_seq_rd_en,
	// to valid_tag_buf
	output reg [7:0] valid_tag_buf_din,
	output reg valid_tag_buf_wr_en,
	input valid_tag_buf_prog_full
);

	assign RX_buf_rd_en = 1'b1;
	
	// better timing of reset
	reg reset;
	always @(posedge sys_clk)begin
		reset <= ~sys_rst_n || dma_r_rst;
	end
	
	// regs & wires for RX_buf_seq
	reg  [129:0] RX_buf_seq_din;
	reg  RX_buf_seq_wr_en;
	wire RX_buf_seq_prog_full;
	
	// regs & wires for RX_seq_ram
	reg  RX_seq_ram_wr_en;
	reg  [10:0] RX_seq_ram_wr_addr;
	reg  [131:0] RX_seq_ram_din;
	reg  [10:0] RX_seq_ram_rd_addr;
	wire [131:0] RX_seq_ram_dout;
	
	// local regs & wires for write into ram
	reg  [7:0] wr_pointers[15:0]; // write pointers, point to each line's write position, the 7th bit is to prevent the pointer overflow
	integer _i;
	
	// write to the RAM
	always @(posedge sys_clk)begin
		if(reset == 1'b1)begin
			RX_seq_ram_wr_en <= 1'b0;
			RX_seq_ram_wr_addr <= 11'd0;
			RX_seq_ram_din <= 132'd0;
			for(_i = 0; _i < 16; _i = _i + 1)begin
				wr_pointers[_i] <= 8'd0;
			end
		end
		else begin
			if(RX_buf_empty == 1'b0 && wr_pointers_clr[RX_buf_dout[131:128]] == 1'b0)begin
				wr_pointers[RX_buf_dout[131:128]] <= wr_pointers[RX_buf_dout[131:128]] + 1'b1;
				RX_seq_ram_wr_en <= 1'b1;
				RX_seq_ram_wr_addr <= {RX_buf_dout[131:128], wr_pointers[RX_buf_dout[131:128]][6:0]};
				RX_seq_ram_din <= {RX_buf_dout[139:136], RX_buf_dout[127:0]};
			end
			else begin
				RX_seq_ram_wr_en <= 1'b0;
				RX_seq_ram_wr_addr <= 11'd0;
				RX_seq_ram_din <= 132'd0;
			end
			for(_i = 0; _i < 16; _i = _i + 1)begin
				if(wr_pointers_clr[_i] == 1'b1)begin
					wr_pointers[_i] <= 8'd0;
				end
			end
		end
	end

	// read from the RAM
	`define RX_SEQ_RD_INIT    2'b01
	`define RX_SEQ_RD_DATA    2'b10
	
	// local regs & wires for read from ram
	reg [15:0] wr_pointers_clr; // bit asserted to clear reg wr_pointers
	reg valid_tag_buf_init_done;
	reg [3:0] valid_tag_buf_init_counter;
	(*KEEP = "true"*)reg [1:0] rx_seq_state;
	reg [7:0] wr_pointer_dly; // delay current item of wr_pointers for 1 cycle to get rid of RAM read/write conflict
	reg [3:0] cur_tag; // record tag of current request
	reg [9:0] cur_len; // record length of current request, in DW
	reg [9:0] has_recv_len; // how many DWs has been received
	reg has_recv_len_clr; // clear register has_recv_len
	reg RX_buf_seq_wr_en_pre; // if this is 1'b1, RX_buf_seq_wr_en should be asserted and data in RX_seq_ram_dout should be inserted into fifo RX_buf_seq
	
	// write to FIFO RX_buf_seq
	always @(posedge sys_clk)begin
		if(reset == 1'b1)begin
			RX_buf_seq_din <= 130'd0;
			RX_buf_seq_wr_en <= 1'b0;
			has_recv_len <= 10'd0;
		end
		else begin
			if(RX_buf_seq_wr_en_pre == 1'b1)begin
				RX_buf_seq_wr_en <= 1'b1;
				case(RX_seq_ram_dout[131:128])
					4'b0001: RX_buf_seq_din <= {2'd1, RX_seq_ram_dout[31: 0], 96'd0}; // 1st data in CPLD
					4'b0100: RX_buf_seq_din <= {2'd1, RX_seq_ram_dout[95:64], 96'd0}; // 1st data in CPLD
					4'b0110: RX_buf_seq_din <= {2'd2, RX_seq_ram_dout[95:32], 64'd0}; // 1st data in CPLD
					4'b0111: RX_buf_seq_din <= {2'd3, RX_seq_ram_dout[95: 0], 32'd0}; // 1st data in CPLD
					4'b1000: RX_buf_seq_din <= {2'd1, RX_seq_ram_dout[127:0]}; // last data in CPLD
					4'b1100: RX_buf_seq_din <= {2'd2, RX_seq_ram_dout[127:0]}; // last data in CPLD
					4'b1110: RX_buf_seq_din <= {2'd3, RX_seq_ram_dout[127:0]}; // last data in CPLD
					4'b1111: RX_buf_seq_din <= {2'd0, RX_seq_ram_dout[127:0]}; // 1ast data in CPLD / in the middle of a CPLD
					default: RX_buf_seq_din <= {2'd0, RX_seq_ram_dout[127:0]}; // This shouldn't happen
				endcase
				if(has_recv_len_clr == 1'b1)
					has_recv_len <= 10'd0;
				else
					has_recv_len <= has_recv_len + RX_seq_ram_dout[131] + RX_seq_ram_dout[130] + RX_seq_ram_dout[129] + RX_seq_ram_dout[128];
			end
			else begin
				RX_buf_seq_din <= 130'd0;
				RX_buf_seq_wr_en <= 1'b0;
				if(has_recv_len_clr == 1'b1)
					has_recv_len <= 10'd0;
				else
					has_recv_len <= has_recv_len;
			end
		end
	end
	
	// read from RAM
	always @(posedge sys_clk)begin
		if(reset == 1'b1)begin
			dma_r_req_buf_rd_en <= 1'b0;
			valid_tag_buf_din <= 8'd0;
			valid_tag_buf_wr_en <= 1'b0;

			RX_seq_ram_rd_addr <= 11'd0;
			
			wr_pointers_clr <= 16'd0;
			valid_tag_buf_init_done <= 1'b0;
			valid_tag_buf_init_counter <= 4'd0;
			
			rx_seq_state <= `RX_SEQ_RD_INIT;
			wr_pointer_dly <= 8'd0;
			cur_tag <= 4'd0;
			cur_len <= 10'd0;
			has_recv_len_clr <= 1'b0;
			
			RX_buf_seq_wr_en_pre <= 1'b0;
		end
		else begin
			if(valid_tag_buf_init_done == 1'b0)begin
				// initiate valid_tag_buf
				if(valid_tag_buf_prog_full == 1'b0)begin
					valid_tag_buf_din <= {4'd1, valid_tag_buf_init_counter};
					valid_tag_buf_wr_en <= 1'b1;
					valid_tag_buf_init_counter <= valid_tag_buf_init_counter + 1'd1;
					valid_tag_buf_init_done <= ((valid_tag_buf_init_counter == 4'd15) ? 1'b1 : 1'b0);
				end
				else begin
					valid_tag_buf_din <= 8'd0;
					valid_tag_buf_wr_en <= 1'b0;
					valid_tag_buf_init_counter <= valid_tag_buf_init_counter;
					valid_tag_buf_init_done <= valid_tag_buf_init_done;
				end
			end
			else begin
				valid_tag_buf_init_done <= valid_tag_buf_init_done;
				valid_tag_buf_init_counter <= 4'd0;
				case(rx_seq_state)
					`RX_SEQ_RD_INIT:begin // wait for request
						valid_tag_buf_wr_en <= 1'b0;
						RX_buf_seq_wr_en_pre <= 1'b0;
						wr_pointers_clr <= 16'd0;
						has_recv_len_clr <= 1'b0;
						if(dma_r_req_buf_empty == 1'b0)begin
							wr_pointer_dly <= wr_pointers[dma_r_req_buf_dout[13:10]];
							cur_tag <= dma_r_req_buf_dout[13:10];
							cur_len <= dma_r_req_buf_dout[9:0];
							dma_r_req_buf_rd_en <= 1'b1;
							RX_seq_ram_rd_addr <= {dma_r_req_buf_dout[13:10], 7'd0};
							rx_seq_state <= `RX_SEQ_RD_DATA;
						end
						else begin
							dma_r_req_buf_rd_en <= 1'b0;
							rx_seq_state <= `RX_SEQ_RD_INIT;
						end
					end
					`RX_SEQ_RD_DATA:begin // move data into RX_buf_seq
						dma_r_req_buf_rd_en <= 1'b0;
						wr_pointer_dly <= wr_pointers[cur_tag];
						if(has_recv_len < cur_len)begin
							rx_seq_state <= `RX_SEQ_RD_DATA;
							has_recv_len_clr <= 1'b0;
							if(RX_buf_seq_prog_full == 1'b0 && RX_seq_ram_rd_addr[6:0] < wr_pointer_dly)begin
								RX_seq_ram_rd_addr <= RX_seq_ram_rd_addr + 1'b1;
								RX_buf_seq_wr_en_pre <= 1'b1;
							end
							else begin
								RX_buf_seq_wr_en_pre <= 1'b0;
							end
						end
						else begin
							wr_pointers_clr[cur_tag] <= 1'b1;
							has_recv_len_clr <= 1'b1;
							rx_seq_state <= `RX_SEQ_RD_INIT;
							valid_tag_buf_din <= {4'b0001, cur_tag};
							valid_tag_buf_wr_en <= 1'b1;
						end
					end
					default:begin
						dma_r_req_buf_rd_en <= 1'b0;
						valid_tag_buf_din <= 8'd0;
						valid_tag_buf_wr_en <= 1'b0;
						
						RX_seq_ram_rd_addr <= 11'd0;
						
						wr_pointers_clr <= 16'd0;
						valid_tag_buf_init_done <= 1'b0;
						valid_tag_buf_init_counter <= 4'd0;
						
						rx_seq_state <= `RX_SEQ_RD_INIT;
					end
				endcase
			end
		end
	end
	
	BRAM_dualport_width132_depth2048 RX_seq_ram(
		.clka(sys_clk),
		.wea(RX_seq_ram_wr_en),
		.addra(RX_seq_ram_wr_addr),
		.dina(RX_seq_ram_din),
		.clkb(sys_clk),
		.addrb(RX_seq_ram_rd_addr),
		.doutb(RX_seq_ram_dout)
	);
	
	FIFO_sync_width130_depth16_FWFT RX_buf_seq(
		.clk(sys_clk),
		.rst(reset),
		.din(RX_buf_seq_din),
		.wr_en(RX_buf_seq_wr_en),
		.rd_en(RX_buf_seq_rd_en),
		.dout(RX_buf_seq_dout),
		.full(),
		.empty(RX_buf_seq_empty),
		.prog_full(RX_buf_seq_prog_full)
	);
	

endmodule
