`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_board2host_INIT_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Control the initailization of DMA board to host.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define DMA_W_INIT_IDLE 3'b001
`define DMA_W_INIT_INIT 3'b010
`define DMA_W_INIT_DCP  3'b100

module DMA_board2host_INIT_CTRL(
	input sys_clk,
	input sys_rst_n,
	input RST_BOARD2HOST,
	// from SYS_CSR
	input dma_w_start,
	input dma_w_init_descriptor_64addr_en,
	input [31:0] dma_w_descriptor_addr_l,
	input [31:0] dma_w_descriptor_addr_h,
	input [13:0] dma_w_descriptor_num,
	output reg dma_w_start_state, // start state is 0 only when reset occurred
	// for DMA_board2host_descriptor_engine
	output reg dma_w_init_descriptor_mux_ctrl,
	output reg [95:0] dma_w_init_descriptor_din,
	output reg dma_w_init_descriptor_wr_en,
	input      dma_w_init_descriptor_prog_full,
	// to DMA_board2host_RST_CTRL
	output reg dma_w_init_req,
	input      dma_w_init_done
);

	(*KEEP="TRUE"*)reg [2:0] dma_w_init_state;
	reg reset;
	
	// for better timing of reset
	always @(posedge sys_clk)begin
		reset <= (~sys_rst_n) || RST_BOARD2HOST;
	end
	
	always @(posedge sys_clk)begin
		if(reset)begin
			dma_w_start_state <= 1'b0;
			dma_w_init_descriptor_mux_ctrl <= 1'b0;
			dma_w_init_descriptor_din <= 96'd0;
			dma_w_init_descriptor_wr_en <= 1'b0;
			dma_w_init_req <= 1'b0;
			dma_w_init_state <= `DMA_W_INIT_IDLE;
		end
		else begin
			case(dma_w_init_state)
				`DMA_W_INIT_IDLE:begin
					dma_w_init_descriptor_mux_ctrl <= 1'b0;
					dma_w_init_descriptor_wr_en <= 1'b0;
					if(dma_w_start)begin
						dma_w_start_state <= 1'b1;
						dma_w_init_req <= 1'b1;
						dma_w_init_state <= `DMA_W_INIT_INIT;
					end
					else begin
						dma_w_init_req <= 1'b0;
						dma_w_init_state <= `DMA_W_INIT_IDLE;
					end
				end
				`DMA_W_INIT_INIT:begin
					dma_w_init_descriptor_mux_ctrl <= 1'b1;
					dma_w_init_descriptor_wr_en <= 1'b0;
					if(dma_w_init_done == 1'b1)begin
						dma_w_init_req <= 1'b0;
						dma_w_init_state <= `DMA_W_INIT_DCP;
					end
					else begin
						dma_w_init_req <= 1'b1;
						dma_w_init_state <= `DMA_W_INIT_INIT;
					end
				end
				`DMA_W_INIT_DCP:begin
					if(dma_w_init_descriptor_prog_full == 1'b0)begin
						dma_w_init_descriptor_mux_ctrl <= 1'b1;
						dma_w_init_descriptor_din <= {3'b001, // this descriptor is for other descriptors
																1'b0, // this is not the last descriptor of a DMA
																1'b0, // this initial descriptor is to DMA read other descriptors
																dma_w_init_descriptor_64addr_en,
																10'd0, // RESERVED,
																{dma_w_descriptor_num, 2'b00},
																dma_w_descriptor_addr_h,
																dma_w_descriptor_addr_l
																};
						dma_w_init_descriptor_wr_en <= 1'b1;
						dma_w_init_state <= `DMA_W_INIT_IDLE;
					end
					else begin
						dma_w_init_descriptor_mux_ctrl <= 1'b1;
						dma_w_init_descriptor_wr_en <= 1'b0;
						dma_w_init_state <= `DMA_W_INIT_DCP;
					end
				end
				default:begin
					dma_w_init_descriptor_mux_ctrl <= 1'b0;
					dma_w_init_descriptor_wr_en <= 1'b0;
					dma_w_init_state <= `DMA_W_INIT_INIT;
				end
			endcase
		end
	end

endmodule
