`timescale 1ns / 1ps
/////////////////////////////////////
//
// INT_GEN.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The interrupt generator. It generate interrupt for EPEE system. There'er three kinds
// of interrupt, that is, the DMA read interrupt, the DMA write interrupt and the user
// defined interrupt.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define INT_GEN_INIT   4'b0001
`define INT_GEN_REQ    4'b0010
`define INT_GEN_WAIT   4'b0100
`define INT_GEN_SERVED 4'b1000

// timeout threshold is about 10 seconds when using 250MHz clk
`define INT_TIMEOUT_THRESHOLD 64'd2500000000

module INT_GEN(
	input sys_clk,
	input sys_rst_n,
	
	// from DMA_ENGINE
	input      IG_DMA_r_int_req,
	output reg IG_DMA_r_int_req_ack,
	input      IG_DMA_r_int_served,
	output reg IG_DMA_r_int_served_ack,
	input      IG_DMA_w_int_req,
	output reg IG_DMA_w_int_req_ack,
	input      IG_DMA_w_int_served,
	output reg IG_DMA_w_int_served_ack,
	
	// from USR_INT_CTRL
	input      IG_usr_int_req,
	output reg IG_usr_int_req_ack,
	input      IG_usr_int_served,
	output reg IG_usr_int_served_ack,
	
	// to INT_CTRL
	output reg INT_REQ,
	input      INT_REQ_ACK,
	output reg INT_SERVED,
	input      INT_SERVED_ACK
);

	// local regs
	(*KEEP="TRUE"*)reg [3:0] int_gen_state;
	(*KEEP="TRUE"*)reg [1:0] int_gen_token; // 0 is for DMA_write, 1 is for DMA_read, 2 is for user interrupt, 3 is invalid
	
	// for timeout logic
	reg [3:0] int_gen_state_dly;
	reg [63:0]timeout_counter;
	reg timeout_flag;
	
	// timeout logic , for recovering from deadlock
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			timeout_counter <= 0;
			timeout_flag <= 0;
			int_gen_state_dly <= `INT_GEN_INIT;
		end else begin
			int_gen_state_dly <= int_gen_state;
			if(int_gen_state == int_gen_state_dly && int_gen_state_dly != `INT_GEN_INIT)begin
				if(timeout_counter >= `INT_TIMEOUT_THRESHOLD)begin
					timeout_flag <= 1'b1;
				end else begin
					timeout_counter <= timeout_counter + 1'b1;
					timeout_flag <= 1'b0;
				end
			end else begin
				timeout_flag <= 0;
				timeout_counter <= 0;
			end
		end
	end
	
	// logic for interrupt
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
		// system reset
			int_gen_state <= `INT_GEN_INIT;
			int_gen_token <= 2'b00;	
			IG_DMA_r_int_req_ack <= 1'b0;
			IG_DMA_r_int_served_ack <= 1'b0;
			IG_DMA_w_int_req_ack <= 1'b0;
			IG_DMA_w_int_served_ack <= 1'b0;
			IG_usr_int_req_ack <= 1'b0;
			IG_usr_int_served_ack <= 1'b0;
			INT_REQ <= 1'b0;
			INT_SERVED <= 1'b0;
		end
		else if(timeout_flag)begin
		// deadlock detected as timeout flag is asserted
			int_gen_state <= `INT_GEN_INIT;
			int_gen_token <= 2'b00;	
			IG_DMA_r_int_req_ack <= 1'b0;
			IG_DMA_r_int_served_ack <= 1'b0;
			IG_DMA_w_int_req_ack <= 1'b0;
			IG_DMA_w_int_served_ack <= 1'b0;
			IG_usr_int_req_ack <= 1'b0;
			IG_usr_int_served_ack <= 1'b0;
			INT_REQ <= 1'b0;
			INT_SERVED <= 1'b0;
		end
		else begin
		// logic for requiring interrupt
			case(int_gen_state)
				`INT_GEN_INIT:begin
					IG_DMA_r_int_req_ack <= 1'b0;
					IG_DMA_r_int_served_ack <= 1'b0;
					IG_DMA_w_int_req_ack <= 1'b0;
					IG_DMA_w_int_served_ack <= 1'b0;
					IG_usr_int_req_ack <= 1'b0;
					IG_usr_int_served_ack <= 1'b0;
					case(int_gen_token)
						2'b00:begin // for DMA read interrupt
							if(IG_DMA_r_int_req == 1'b1)begin
								int_gen_state <= `INT_GEN_REQ;
								int_gen_token <= int_gen_token;
								INT_REQ <= 1'b1;
							end
							else begin
								int_gen_token <= 2'b01;
							end
						end
						2'b01:begin // for DMA write interrupt
							if(IG_DMA_w_int_req == 1'b1)begin
								int_gen_state <= `INT_GEN_REQ;
								int_gen_token <= int_gen_token;
								INT_REQ <= 1'b1;
							end
							else begin
								int_gen_token <= 2'b10;
							end
						end
						2'b10:begin // for user interrupt
							if(IG_usr_int_req == 1'b1)begin
								int_gen_state <= `INT_GEN_REQ;
								int_gen_token <= int_gen_token;
								INT_REQ <= 1'b1;
							end
							else begin
								int_gen_state <= `INT_GEN_INIT;
								int_gen_token <= 2'b00;
							end
						end
						default:begin
							int_gen_state <= `INT_GEN_INIT;
							int_gen_token <= 2'b00;
						end
					endcase
				end
				`INT_GEN_REQ:begin
					if(INT_REQ_ACK == 1'b1)begin
						INT_REQ <= 1'b0;
						int_gen_token <= int_gen_token;
						case(int_gen_token)
							2'b00:begin
								IG_DMA_r_int_req_ack <= 1'b1;
								IG_DMA_w_int_req_ack <= 1'b0;
								IG_usr_int_req_ack <= 1'b0;
								int_gen_state <= `INT_GEN_WAIT;
							end
							2'b01:begin
								IG_DMA_w_int_req_ack <= 1'b1;
								IG_DMA_r_int_req_ack <= 1'b0;
								IG_usr_int_req_ack <= 1'b0;
								int_gen_state <= `INT_GEN_WAIT;
							end
							2'b10:begin
								IG_usr_int_req_ack <= 1'b1;
								IG_DMA_w_int_req_ack <= 1'b0;
								IG_DMA_r_int_req_ack <= 1'b0;
								int_gen_state <= `INT_GEN_WAIT;
							end
							default:begin // token error
								IG_usr_int_req_ack <= 1'b0;
								IG_DMA_w_int_req_ack <= 1'b0;
								IG_DMA_r_int_req_ack <= 1'b0;
								int_gen_state <= `INT_GEN_INIT;
							end
						endcase
					end
					else begin
						int_gen_state <= `INT_GEN_REQ;
						int_gen_token <= int_gen_token;
						INT_REQ <= 1'b1;
					end
				end
				`INT_GEN_WAIT:begin
					IG_usr_int_req_ack <= 1'b0;
					IG_DMA_w_int_req_ack <= 1'b0;
					IG_DMA_r_int_req_ack <= 1'b0;
					if(IG_DMA_r_int_served == 1'b1 || IG_DMA_w_int_served == 1'b1 || IG_usr_int_served == 1'b1)begin
						int_gen_state <= `INT_GEN_SERVED;
						INT_SERVED <= 1'b1;
						int_gen_token <= int_gen_token;
					end
					else begin
						int_gen_state <= `INT_GEN_WAIT;
						int_gen_token <= int_gen_token;
					end
				end
				`INT_GEN_SERVED:begin
					if(INT_SERVED_ACK == 1'b1)begin
						INT_SERVED <= 1'b0;
						case(int_gen_token)
							2'b00:begin
								IG_DMA_r_int_served_ack <= 1'b1;
								IG_DMA_w_int_served_ack <= 1'b0;
								IG_usr_int_served_ack <= 1'b0;
								int_gen_state <= `INT_GEN_INIT;
								int_gen_token <= 2'b01;
							end
							2'b01:begin
								IG_DMA_w_int_served_ack <= 1'b1;
								IG_DMA_r_int_served_ack <= 1'b0;
								IG_usr_int_served_ack <= 1'b0;
								int_gen_state <= `INT_GEN_INIT;
								int_gen_token <= 2'b10;
							end
							2'b10:begin
								IG_usr_int_served_ack <= 1'b1;
								IG_DMA_w_int_served_ack <= 1'b0;
								IG_DMA_r_int_served_ack <= 1'b0;
								int_gen_state <= `INT_GEN_INIT;
								int_gen_token <= 2'b00;
							end
							default:begin // token error
								IG_usr_int_served_ack <= 1'b0;
								IG_DMA_w_int_served_ack <= 1'b0;
								IG_DMA_r_int_served_ack <= 1'b0;
								int_gen_state <= `INT_GEN_INIT;
								int_gen_token <= 2'b00;
							end
						endcase
					end
					else begin
						int_gen_state <= `INT_GEN_SERVED;
						int_gen_token <= int_gen_token;
					end
				end
				default:begin
					int_gen_state <= `INT_GEN_INIT;
				end
			endcase
		end
	end

endmodule
