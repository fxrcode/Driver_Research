`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_r_TLP_committer.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Commit the DMA read TLPs (MRd TLPs in DMA host2board engine).
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

`define DMA_R_TLP_COM_INIT 2'b01
`define DMA_R_TLP_COM_H2   2'b10

module DMA_r_TLP_committer(
	input sys_clk,
	input sys_rst_n,
	input dma_host2board_rst, // when this reset assert, the module will reset when current TLP is sent
	// from DMA_r_TLP_head_gen
	input [127:0] dma_r_tlp_head_buf_dout,
	output reg dma_r_tlp_head_buf_rd_en,
	input dma_r_tlp_head_buf_empty,
	// to FIFO DMA_r_MRD_TLP_dout
	output [71:0] DMA_r_MRD_TLP_dout,
	input DMA_r_MRD_TLP_rd_en,
	output DMA_r_MRD_TLP_empty,
	// to DMA_r_req_buf
	output reg [17:0] dma_r_req_buf_din, // {tag[7:0], length[9:0]}
	output reg dma_r_req_buf_wr_en,
	input dma_r_req_buf_prog_full // the req_buf will never full, so this is not used currently
);

	reg [71:0] DMA_r_MRD_TLP_din;
	reg DMA_r_MRD_TLP_wr_en;
	wire DMA_r_MRD_TLP_prog_full;
	
	reg [1:0] dma_r_tlp_com_state;
	reg tlp_is_64addr; // record whether the current TLP is 64bit address
	reg [63:0] tlp_h2_dly; // delay current's 2nd QW
	
	reg fifo_rst;
	always @(posedge sys_clk)begin
		fifo_rst = ~sys_rst_n || (dma_host2board_rst == 1'b1 && dma_r_tlp_com_state == `DMA_R_TLP_COM_INIT);
	end
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin // system reset
			dma_r_tlp_head_buf_rd_en <= 1'b0;
			dma_r_req_buf_din <= 0;
			dma_r_req_buf_wr_en <= 1'b0;
			DMA_r_MRD_TLP_din <= 0;
			DMA_r_MRD_TLP_wr_en <= 1'b0;
			tlp_is_64addr <= 1'b0;
			tlp_h2_dly <= 0;
			dma_r_tlp_com_state <= `DMA_R_TLP_COM_INIT;
		end
		else if(dma_host2board_rst == 1'b1 && dma_r_tlp_com_state == `DMA_R_TLP_COM_INIT) begin // dma reset or initiate
			dma_r_tlp_head_buf_rd_en <= 1'b0;
			dma_r_req_buf_din <= 0;
			dma_r_req_buf_wr_en <= 1'b0;
			DMA_r_MRD_TLP_din <= 0;
			DMA_r_MRD_TLP_wr_en <= 1'b0;
			tlp_is_64addr <= 1'b0;
		end
		else begin
			case(dma_r_tlp_com_state)
				`DMA_R_TLP_COM_INIT:begin
					if(dma_r_tlp_head_buf_empty == 1'b0 && DMA_r_MRD_TLP_prog_full == 1'b0 &&
						dma_r_req_buf_prog_full == 1'b0)begin
						dma_r_tlp_com_state <= `DMA_R_TLP_COM_H2;
						dma_r_tlp_head_buf_rd_en <= 1'b1;
						dma_r_req_buf_din <= {dma_r_tlp_head_buf_dout[79:72], dma_r_tlp_head_buf_dout[105:96]};
						dma_r_req_buf_wr_en <= 1'b1;
						DMA_r_MRD_TLP_din <= {4'd0, 2'b11, 1'b1, 1'b0, dma_r_tlp_head_buf_dout[127:64]};
						DMA_r_MRD_TLP_wr_en <= 1'b1;
						tlp_is_64addr <= (dma_r_tlp_head_buf_dout[127:120] == `FT_MRD64)?1'b1:1'b0;
						tlp_h2_dly <= dma_r_tlp_head_buf_dout[63:0];
					end
					else begin
						dma_r_tlp_com_state <= `DMA_R_TLP_COM_INIT;
						dma_r_tlp_head_buf_rd_en <= 1'b0;
						dma_r_req_buf_wr_en <= 1'b0;
						DMA_r_MRD_TLP_wr_en <= 1'b0;
					end
				end
				`DMA_R_TLP_COM_H2:begin
					dma_r_tlp_com_state <= `DMA_R_TLP_COM_INIT;
					dma_r_tlp_head_buf_rd_en <= 1'b0;
					dma_r_req_buf_wr_en <= 1'b0;
					DMA_r_MRD_TLP_din <= {4'd0, 
												1'b1, 
												(tlp_is_64addr == 1'b1)?1'b1:1'b0,
												1'b0, // start of tlp
												1'b1, // end of tlp
												tlp_h2_dly};
					DMA_r_MRD_TLP_wr_en <= 1'b1;
				end
				default:begin
					dma_r_tlp_com_state <= `DMA_R_TLP_COM_INIT;
					dma_r_tlp_head_buf_rd_en <= 1'b0;
					dma_r_req_buf_wr_en <= 1'b0;
					DMA_r_MRD_TLP_wr_en <= 1'b0;
				end
			endcase
		end
	end

	FIFO_sync_width72_depth16_FWFT DMA_r_MRD_TLP(
		.clk(sys_clk),
		.rst(fifo_rst),
		.din(DMA_r_MRD_TLP_din),
		.wr_en(DMA_r_MRD_TLP_wr_en),
		.rd_en(DMA_r_MRD_TLP_rd_en),
		.dout(DMA_r_MRD_TLP_dout),
		.full(),
		.empty(DMA_r_MRD_TLP_empty),
		.prog_full(DMA_r_MRD_TLP_prog_full)
	);

endmodule
