`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_descriptor_FSM.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The main FSM (finite state machine) of DMA descriptor engine. It generates MRd TLPs
// for descriptors and receives the corresponding CplD TLP. It also analyses the CplD TLP
// and fetch DMA descriptors from it (CplD).
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

`define DMA_DCP_INIT      4'b0001
`define DMA_DCP_MRD_H2    4'b0010
`define DMA_DCP_CPLD_H1   4'b0100
`define DMA_DCP_CPLD_DATA 4'b1000

// timeout threshold is about 1 seconds when using 250MHz clk
`define DS_CTRL_TIMEOUT_THRESHOLD 64'd250000000

module DMA_descriptor_FSM(
	input sys_clk,
	input sys_rst_n,
	input dma_channel_rst,
	output reg dma_channel_rst_done,
	// to DMA_MRD_channel_collector
	output [71:0] DMA_descriptor_MRD_TLP_dout,
	output DMA_descriptor_MRD_TLP_empty,
	input  DMA_descriptor_MRD_TLP_rd_en,
	// from DMA_CPLD_channel_dispatcher
	input  [71:0] DMA_descriptor_CPLD_TLP_din,
	output DMA_descriptor_CPLD_TLP_prog_full,
	input  DMA_descriptor_CPLD_TLP_wr_en,
	// to DMA_descriptor_dispatcher
	output reg [71:0] descriptor_cpld_buf_din, // [63:0] is data, din[64] means [31:0] is valid, din[65] means [63:32] is valid
	input  descriptor_cpld_buf_prog_full,
	output reg descriptor_cpld_buf_wr_en,
	// from TLP_head_gen
	input  [127:0] descriptor_mrd_buf_dout,
	input  descriptor_mrd_buf_empty,
	output reg descriptor_mrd_buf_rd_en
);
	
	(*KEEP="TRUE"*)reg [3:0] descriptor_fsm_state;
	reg [9:0] req_len_left; // how many DWs haven't been received of last MRD require
	reg [9:0] cpld_len_left; // how many DWs haven't been received in current cpld
	reg [63:0] descriptor_mrd_h2_dly; // delay 2nd QW of MRD TLP
	reg        descriptor_mrd_is_64addr; // the current MRD TLP is 64bit address or not, 0: 32bit addr, 1:64bit addr
	
	reg [71:0] DMA_descriptor_MRD_TLP_din;
	reg        DMA_descriptor_MRD_TLP_wr_en;
	wire       DMA_descriptor_MRD_TLP_prog_full;
	
	wire[71:0] DMA_descriptor_CPLD_TLP_dout;
	wire       DMA_descriptor_CPLD_TLP_empty;
	reg        DMA_descriptor_CPLD_TLP_rd_en;
	
	reg reset;
	
	// for timeout logic
	reg [3:0]  descriptor_fsm_state_dly;
	reg [63:0] timeout_counter;
	reg        timeout_flag;
	
	// timeout logic, for recovering from deadlock
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			descriptor_fsm_state_dly <= `DMA_DCP_INIT;
			timeout_counter <= 64'd0;
			timeout_flag <= 1'b0;
		end else begin
			descriptor_fsm_state_dly <= descriptor_fsm_state;
			if(descriptor_fsm_state_dly == descriptor_fsm_state && descriptor_fsm_state_dly != `DMA_DCP_INIT)begin
				if(timeout_counter > `DS_CTRL_TIMEOUT_THRESHOLD)begin
					timeout_flag <= 1'b1;
				end else begin
					timeout_flag <= 1'b0;
					timeout_counter <= timeout_counter + 1;
				end
			end else begin
				timeout_flag <= 1'b0;
				timeout_counter <= 0;
			end
		end
	end
	
	// reset logic
	always @(posedge sys_clk)begin
		if(~sys_rst_n || timeout_flag == 1'b1)begin
			reset <= 1'b1;
			dma_channel_rst_done <= 1'b0;
		end
		else begin
			if(dma_channel_rst == 1'b1 && descriptor_fsm_state == `DMA_DCP_INIT)begin
				reset <= 1'b1;
				dma_channel_rst_done <= 1'b1;
			end
			else begin
				reset <= 1'b0;
				dma_channel_rst_done <= 1'b0;
			end
		end
	end	
	
	// main logic
	always @(posedge sys_clk)begin
		if(reset)begin
			descriptor_cpld_buf_din <= 0;
			descriptor_cpld_buf_wr_en <= 1'b0;
			descriptor_mrd_buf_rd_en <= 1'b0;
			req_len_left <= 10'd0;
			cpld_len_left <= 10'd0;
			descriptor_mrd_h2_dly <= 0;
			descriptor_fsm_state <= `DMA_DCP_INIT;
			DMA_descriptor_MRD_TLP_din <= 0;
			DMA_descriptor_MRD_TLP_wr_en <= 0;
			DMA_descriptor_CPLD_TLP_rd_en <= 0;
			descriptor_mrd_is_64addr <= 1'b0;
		end
		else begin
			case(descriptor_fsm_state)
				`DMA_DCP_INIT:begin
					descriptor_cpld_buf_wr_en <= 1'b0;
					if(dma_channel_rst == 1'b0 && descriptor_mrd_buf_empty == 1'b0 && 
						DMA_descriptor_MRD_TLP_prog_full == 1'b0 && descriptor_cpld_buf_prog_full == 1'b0)begin 
						// there is some descriptor requests && enough space is left in descriptor_cpld_buf for a new request
						descriptor_mrd_buf_rd_en <= 1'b1;
						descriptor_fsm_state <= `DMA_DCP_MRD_H2;
						descriptor_mrd_h2_dly <= descriptor_mrd_buf_dout[63:0];
						DMA_descriptor_MRD_TLP_din <= {4'd0, 2'b11, 1'd1, 1'd0, descriptor_mrd_buf_dout[127:64]};
						DMA_descriptor_MRD_TLP_wr_en <= 1'b1;
						req_len_left <= descriptor_mrd_buf_dout[105:96];
						descriptor_mrd_is_64addr <= ((descriptor_mrd_buf_dout[127:120] == `FT_MRD64)?1'b1:1'b0);
					end
					else begin
						descriptor_mrd_buf_rd_en <= 1'b0;
						descriptor_fsm_state <= `DMA_DCP_INIT;
					end
				end
				`DMA_DCP_MRD_H2:begin
					descriptor_mrd_buf_rd_en <= 1'b0;
					DMA_descriptor_MRD_TLP_din <= {4'd0, 1'b1, descriptor_mrd_is_64addr, 1'b0, 1'b1, descriptor_mrd_h2_dly};
					DMA_descriptor_MRD_TLP_wr_en <= 1'b1;
					descriptor_fsm_state <= `DMA_DCP_CPLD_H1;
				end
				`DMA_DCP_CPLD_H1:begin // deal with FIFO DMA_descriptor_CPLD_TLP and descriptor_cpld_buf
					DMA_descriptor_MRD_TLP_wr_en <= 1'b0;
					// for read enable signal of DMA_descriptor_CPLD_TLP
					if(DMA_descriptor_CPLD_TLP_empty == 1'b0)begin
						DMA_descriptor_CPLD_TLP_rd_en <= 1'b1;
					end
					else begin
						DMA_descriptor_CPLD_TLP_rd_en <= 1'b0;
					end
					// deal with the CPLD TLP, the CPLD TLP here has a shorter head accordding to DMA_CPLD_channel_dispatcher
					if(DMA_descriptor_CPLD_TLP_rd_en == 1'b1 && DMA_descriptor_CPLD_TLP_empty == 1'b0)begin
						descriptor_cpld_buf_wr_en <= 1'b1;
						descriptor_cpld_buf_din <= {6'd0, 1'b0, 1'b1, DMA_descriptor_CPLD_TLP_dout[63:0]};
						if(DMA_descriptor_CPLD_TLP_dout[64] == 1'b1 && req_len_left == 1)begin
							descriptor_fsm_state <= `DMA_DCP_INIT;
						end
						else if(DMA_descriptor_CPLD_TLP_dout[64] == 1'b1 && req_len_left > 1) begin
							req_len_left <= req_len_left - 2'd1;
							descriptor_fsm_state <= `DMA_DCP_CPLD_H1;
						end
						else begin
							req_len_left <= req_len_left - 2'd1;
							cpld_len_left <= DMA_descriptor_CPLD_TLP_dout[57:48] - 2'd1;
							descriptor_fsm_state <= `DMA_DCP_CPLD_DATA;
						end
					end
					else begin
						descriptor_cpld_buf_wr_en <= 1'b0;
						descriptor_fsm_state <= `DMA_DCP_CPLD_H1;
					end
				end
				`DMA_DCP_CPLD_DATA:begin
					// for read enable signal of DMA_descriptor_CPLD_TLP
					if(DMA_descriptor_CPLD_TLP_empty == 1'b0)begin
						// descriptor_cpld_buf_prog_full has been checked in status DMA_DCP_INIT, enough space has been left in buffer descriptor_cpld_buf
						DMA_descriptor_CPLD_TLP_rd_en <= 1'b1;
					end
					else begin
						DMA_descriptor_CPLD_TLP_rd_en <= 1'b0;
					end
					// deal with the CPLD TLP
					if(DMA_descriptor_CPLD_TLP_rd_en == 1'b1 && DMA_descriptor_CPLD_TLP_empty == 1'b0)begin
						if(DMA_descriptor_CPLD_TLP_dout[64] == 1'b1)begin
							descriptor_cpld_buf_wr_en <= 1'b1;
							descriptor_cpld_buf_din <={6'd0,
																1'b1,
																(cpld_len_left == 2)?1'b1 : 1'b0,
																DMA_descriptor_CPLD_TLP_dout[63:0]
																};
							if(req_len_left <= 2)begin
								descriptor_fsm_state <= `DMA_DCP_INIT;
								req_len_left <= req_len_left - cpld_len_left;
							end
							else begin
								descriptor_fsm_state <= `DMA_DCP_CPLD_H1;
								req_len_left <= req_len_left - cpld_len_left;
							end
						end
						else begin
							descriptor_cpld_buf_wr_en <= 1'b1;
							descriptor_cpld_buf_din <={6'd0,
																1'b1,
																1'b1,
																DMA_descriptor_CPLD_TLP_dout[63:0]
																};
							req_len_left <= req_len_left - 3'd2;
							cpld_len_left <= cpld_len_left - 3'd2;
							descriptor_fsm_state <= `DMA_DCP_CPLD_DATA;
						end
					end
					else begin
						descriptor_cpld_buf_wr_en <= 1'b0;
						descriptor_fsm_state <= `DMA_DCP_CPLD_DATA;
					end
				end
				default:begin
					descriptor_fsm_state <= `DMA_DCP_INIT;
					DMA_descriptor_MRD_TLP_wr_en <= 0;
					DMA_descriptor_CPLD_TLP_rd_en <= 0;
				end
			endcase
		end
	end
	
	FIFO_sync_width72_depth16_FWFT DMA_descriptor_MRD_TLP(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_descriptor_MRD_TLP_din),
		.wr_en(DMA_descriptor_MRD_TLP_wr_en),
		.rd_en(DMA_descriptor_MRD_TLP_rd_en),
		.dout(DMA_descriptor_MRD_TLP_dout),
		.full(),
		.empty(DMA_descriptor_MRD_TLP_empty),
		.prog_full(DMA_descriptor_MRD_TLP_prog_full)
	);
	
	FIFO_sync_width72_depth16_FWFT DMA_descriptor_CPLD_TLP(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_descriptor_CPLD_TLP_din),
		.wr_en(DMA_descriptor_CPLD_TLP_wr_en),
		.rd_en(DMA_descriptor_CPLD_TLP_rd_en),
		.dout(DMA_descriptor_CPLD_TLP_dout),
		.full(),
		.empty(DMA_descriptor_CPLD_TLP_empty),
		.prog_full(DMA_descriptor_CPLD_TLP_prog_full)
	);

endmodule
