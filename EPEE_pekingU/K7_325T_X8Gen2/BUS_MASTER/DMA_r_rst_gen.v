`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_r_rst_gen.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Control the reset process inside the DMA host2board engine. The reset can only done
// when all the MRd TLP has been replied with CplD TLP.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_r_rst_gen(
	input sys_clk,
	input sys_rst_n,
	input dma_host2board_rst,
	output reg dma_host2board_rst_done,
	// for request tag
	input [7:0] latest_req_tag,
	input [7:0] latest_free_tag,
	// reset signal
	output reg dma_r_rst
);

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			dma_host2board_rst_done <= 1'b0;
			dma_r_rst <= 1'b0;
		end
		else if(dma_host2board_rst == 1'b1)begin
			if(dma_r_rst == 1'b0)begin
				if(latest_req_tag == latest_free_tag || latest_req_tag == 0)begin
					dma_host2board_rst_done <= 1'b0;
					dma_r_rst <= 1'b1;
				end
				else begin
					dma_host2board_rst_done <= 1'b0;
					dma_r_rst <= 1'b0;
				end
			end
			else begin
				dma_host2board_rst_done <= 1'b1;
				dma_r_rst <= 1'b1;
			end
		end
		else begin
			dma_host2board_rst_done <= 1'b0;
			dma_r_rst <= 1'b0;
		end
	end

endmodule
