`timescale 1ns / 1ps
/////////////////////////////////////
//
// US_TLP_collector.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Upstream TLP collector. It collects the upstream TLPs from PIO and DMA engine.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define US_COL_INIT    4'b0001
`define US_COL_CPLD    4'b0010
`define US_COL_MWR     4'b0100
`define US_COL_MRD     4'b1000

module US_TLP_collector(
	input sys_clk,
	input sys_rst_n,
	// to US_CTRL
	output [133:0]US_TLP_BUF_DOUT,
	output US_TLP_BUF_EMPTY,
	input  US_TLP_BUF_RD_EN,
	// from PIO_ENGINE
	input      [71:0] PIO_CPLD_channel_buf_dout,
	input      PIO_CPLD_channel_buf_empty,
	output reg PIO_CPLD_channel_buf_rd_en,
	// from DMA_ENGINE
	input      [133:0] DMA_MWR_channel_buf_dout,
	input      DMA_MWR_channel_buf_empty,
	output reg DMA_MWR_channel_buf_rd_en,
	input      [71:0] DMA_MRD_channel_buf_dout,
	input      DMA_MRD_channel_buf_empty,
	output reg DMA_MRD_channel_buf_rd_en
);

	// local regs and wires
	reg [133:0] US_TLP_BUF_DIN;
	reg        US_TLP_BUF_WR_EN;
	wire       US_TLP_BUF_PROG_FULL;
	(*KEEP="TRUE"*)reg [3:0]  US_COL_state;
	reg [63:0] tlp_head_first64; // record the first 64bit of TLP header
	reg tlp_head_first64_valid;
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			PIO_CPLD_channel_buf_rd_en <= 1'b0;
			DMA_MWR_channel_buf_rd_en <= 1'b0;
			DMA_MRD_channel_buf_rd_en <= 1'b0;
			US_TLP_BUF_DIN <= 72'd0;
			US_TLP_BUF_WR_EN <= 1'b0;
			US_COL_state <= `US_COL_INIT;
			tlp_head_first64 <= 64'd0;
			tlp_head_first64_valid <= 1'b0;
		end
		else begin
			case(US_COL_state)
				`US_COL_INIT:begin
					US_TLP_BUF_DIN <= 72'd0;
					US_TLP_BUF_WR_EN <= 1'b0;
					tlp_head_first64 <= 64'd0;
					tlp_head_first64_valid <= 1'b0;
					if(US_TLP_BUF_PROG_FULL == 1'b0)begin
						if(PIO_CPLD_channel_buf_empty == 1'b0)begin
							// CPLD TLP
							PIO_CPLD_channel_buf_rd_en <= 1'b1;
							DMA_MWR_channel_buf_rd_en <= 1'b0;
							DMA_MRD_channel_buf_rd_en <= 1'b0;
							US_COL_state <= `US_COL_CPLD;
						end
						else if(DMA_MRD_channel_buf_empty == 1'b0)begin
							// Mem Read TLP
							PIO_CPLD_channel_buf_rd_en <= 1'b0;
							DMA_MWR_channel_buf_rd_en <= 1'b0;
							DMA_MRD_channel_buf_rd_en <= 1'b1;
							US_COL_state <= `US_COL_MRD;
						end
						else if(DMA_MWR_channel_buf_empty == 1'b0)begin
							// Mem write TLP
							PIO_CPLD_channel_buf_rd_en <= 1'b0;
							DMA_MWR_channel_buf_rd_en <= 1'b1;
							DMA_MRD_channel_buf_rd_en <= 1'b0;
							US_COL_state <= `US_COL_MWR;
						end
						else begin
							// no TLP to transfer
							PIO_CPLD_channel_buf_rd_en <= 1'b0;
							DMA_MWR_channel_buf_rd_en <= 1'b0;
							DMA_MRD_channel_buf_rd_en <= 1'b0;
							US_COL_state <= `US_COL_INIT;
						end
					end
					else begin
						PIO_CPLD_channel_buf_rd_en <= 1'b0;
						DMA_MWR_channel_buf_rd_en <= 1'b0;
						DMA_MRD_channel_buf_rd_en <= 1'b0;
						US_COL_state <= `US_COL_INIT;
					end
				end
				`US_COL_CPLD:begin
					if(tlp_head_first64_valid == 1'b1 && PIO_CPLD_channel_buf_empty == 1'b0)begin
						US_TLP_BUF_DIN <= {2'b11, PIO_CPLD_channel_buf_dout[67:66], 2'b11, tlp_head_first64, PIO_CPLD_channel_buf_dout[63:0]};
						US_TLP_BUF_WR_EN <= 1'b1;
						PIO_CPLD_channel_buf_rd_en <= 1'b0;
						DMA_MWR_channel_buf_rd_en <= 1'b0;
						DMA_MRD_channel_buf_rd_en <= 1'b0;
						US_COL_state <= `US_COL_INIT;
					end
					else if(tlp_head_first64_valid == 1'b0 && PIO_CPLD_channel_buf_empty == 1'b0)begin
						US_TLP_BUF_DIN <= 134'd0;
						US_TLP_BUF_WR_EN <= 1'b0;
						PIO_CPLD_channel_buf_rd_en <= 1'b1;
						DMA_MWR_channel_buf_rd_en <= 1'b0;
						DMA_MRD_channel_buf_rd_en <= 1'b0;
						US_COL_state <= `US_COL_CPLD;
						tlp_head_first64 <= PIO_CPLD_channel_buf_dout[63:0];
						tlp_head_first64_valid <= 1'b1;
					end
					else begin
						US_TLP_BUF_DIN <= 134'd0;
						US_TLP_BUF_WR_EN <= 1'b0;
						PIO_CPLD_channel_buf_rd_en <= 1'b1;
						DMA_MWR_channel_buf_rd_en <= 1'b0;
						DMA_MRD_channel_buf_rd_en <= 1'b0;
						US_COL_state <= `US_COL_CPLD;
					end
				end
				`US_COL_MWR:begin
					// generate read enable signal
					if(DMA_MWR_channel_buf_empty == 1'b0 && US_TLP_BUF_PROG_FULL == 1'b0)begin
						if(DMA_MWR_channel_buf_dout[128] == 1'b0 || (DMA_MWR_channel_buf_dout[128] == 1'b1 && DMA_MWR_channel_buf_rd_en == 1'b0))begin
							DMA_MWR_channel_buf_rd_en <= 1'b1;
						end
						else begin
							DMA_MWR_channel_buf_rd_en <= 1'b0;
						end
					end
					else begin
						DMA_MWR_channel_buf_rd_en <= 1'b0;
					end
					// inputs to the FIFO US_TLP_BUF
					if(DMA_MWR_channel_buf_rd_en == 1'b1 && DMA_MWR_channel_buf_empty == 1'b0)begin
						US_TLP_BUF_DIN <= DMA_MWR_channel_buf_dout;
						US_TLP_BUF_WR_EN <= 1'b1;
					end
					else begin
						US_TLP_BUF_WR_EN <= 1'b0;
					end
					// for state machine
					if(DMA_MWR_channel_buf_dout[128] == 1'b1 && DMA_MWR_channel_buf_rd_en == 1'b1 && DMA_MWR_channel_buf_empty == 1'b0)begin // end of the TLP
						US_COL_state <= `US_COL_INIT;
					end 
					else begin // current TLP is not end
						US_COL_state <= `US_COL_MWR;
					end
				end
				`US_COL_MRD:begin
					if(tlp_head_first64_valid == 1'b1 && DMA_MRD_channel_buf_empty == 1'b0)begin
						US_TLP_BUF_DIN <= {2'b11, DMA_MRD_channel_buf_dout[67:66], 2'b11, tlp_head_first64, DMA_MRD_channel_buf_dout[63:0]};
						US_TLP_BUF_WR_EN <= 1'b1;
						PIO_CPLD_channel_buf_rd_en <= 1'b0;
						DMA_MWR_channel_buf_rd_en <= 1'b0;
						DMA_MRD_channel_buf_rd_en <= 1'b0;
						US_COL_state <= `US_COL_INIT;
					end
					else if(tlp_head_first64_valid == 1'b0 && DMA_MRD_channel_buf_empty == 1'b0)begin
						US_TLP_BUF_DIN <= 134'd0;
						US_TLP_BUF_WR_EN <= 1'b0;
						PIO_CPLD_channel_buf_rd_en <= 1'b0;
						DMA_MWR_channel_buf_rd_en <= 1'b0;
						DMA_MRD_channel_buf_rd_en <= 1'b1;
						US_COL_state <= `US_COL_MRD;
						tlp_head_first64 <= DMA_MRD_channel_buf_dout[63:0];
						tlp_head_first64_valid <= 1'b1;
					end
					else begin
						US_TLP_BUF_DIN <= 134'd0;
						US_TLP_BUF_WR_EN <= 1'b0;
						PIO_CPLD_channel_buf_rd_en <= 1'b0;
						DMA_MWR_channel_buf_rd_en <= 1'b0;
						DMA_MRD_channel_buf_rd_en <= 1'b1;
						US_COL_state <= `US_COL_MRD;
					end
				end
				default:begin
					PIO_CPLD_channel_buf_rd_en <= 1'b0;
					DMA_MWR_channel_buf_rd_en <= 1'b0;
					DMA_MRD_channel_buf_rd_en <= 1'b0;
					US_TLP_BUF_DIN <= 134'd0;
					US_TLP_BUF_WR_EN <= 1'b0;
					US_COL_state <= `US_COL_INIT;
					tlp_head_first64 <= 64'd0;
					tlp_head_first64_valid <= 1'b0;
				end
			endcase
		end
	end

	FIFO_sync_width134_depth16_FWFT US_TLP_BUF(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(US_TLP_BUF_DIN),
		.wr_en(US_TLP_BUF_WR_EN),
		.rd_en(US_TLP_BUF_RD_EN),
		.dout(US_TLP_BUF_DOUT),
		.full(),
		.empty(US_TLP_BUF_EMPTY),
		.prog_full(US_TLP_BUF_PROG_FULL)
	);

endmodule
