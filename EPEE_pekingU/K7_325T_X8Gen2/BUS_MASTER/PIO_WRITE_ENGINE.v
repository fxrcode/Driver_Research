`timescale 1ns / 1ps
/////////////////////////////////////
//
// PIO_WRITE_ENGINE.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// PIO write engine. It deals with the PIO write tasks.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

`define PIO_WR_INIT       6'b000001
`define PIO_WR32_H2       6'b000010
`define PIO_WR64_H2       6'b000100
`define PIO_WR64_DATA_SYS 6'b001000
`define PIO_WR64_DATA_USR 6'b010000
`define PIO_WR_USR_WAIT   6'b100000

module PIO_WRITE_ENGINE(
	input sys_clk,
	input sys_rst_n,
	// from PIO_MWR_channel_buf
	input      [71:0] PIO_MWR_channel_buf_dout,
	input      PIO_MWR_channel_buf_empty,
	output reg PIO_MWR_channel_buf_rd_en,
	// to SYS_CSR
	output reg [16:0] SYS_PIO_WR_ADDR,
	output reg [31:0] SYS_PIO_WR_DATA,
	output reg SYS_PIO_WR_WR_EN,
	// to user PIO bus
	output reg [16:0] usr_pio_wr_addr,
	output reg [31:0] usr_pio_wr_data,
	output reg usr_pio_wr_req,
	input      usr_pio_wr_ack
);

	// local regs
	(*KEEP="TRUE"*)reg [5:0] pio_wr_state;
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			pio_wr_state <= `PIO_WR_INIT;
			PIO_MWR_channel_buf_rd_en <= 1'b0;
			SYS_PIO_WR_ADDR <= 17'd0;
			SYS_PIO_WR_DATA <= 32'd0;
			SYS_PIO_WR_WR_EN <= 1'b0;
			usr_pio_wr_addr <= 17'd0;
			usr_pio_wr_data <= 32'd0;
			usr_pio_wr_req <= 1'b0;
		end
		else begin
			case(pio_wr_state)
				`PIO_WR_INIT:begin
					SYS_PIO_WR_WR_EN <= 1'b0;
					usr_pio_wr_req <= 1'b0;
					PIO_MWR_channel_buf_rd_en <= 1'b1;
					if(PIO_MWR_channel_buf_empty == 1'b0 && PIO_MWR_channel_buf_rd_en == 1'b1 && PIO_MWR_channel_buf_dout[65] == 1'b1)begin
						if(PIO_MWR_channel_buf_dout[63:56] == `FT_MWR32)begin
							pio_wr_state <= `PIO_WR32_H2;
						end
						else if(PIO_MWR_channel_buf_dout[63:56] == `FT_MWR64)begin
							pio_wr_state <= `PIO_WR64_H2;
						end
						else begin
							pio_wr_state <= `PIO_WR_INIT;
						end
					end 
					else begin
						pio_wr_state <= `PIO_WR_INIT;
					end
				end
				`PIO_WR32_H2:begin
					if(PIO_MWR_channel_buf_empty == 1'b0 && PIO_MWR_channel_buf_rd_en == 1'b1 && PIO_MWR_channel_buf_dout[64] == 1'b1)begin // TLP ends here
						PIO_MWR_channel_buf_rd_en <= 1'b0;
						if(PIO_MWR_channel_buf_dout[51] == 1'b0)begin // this write is for SYS_CSR
							SYS_PIO_WR_ADDR <= PIO_MWR_channel_buf_dout[50:34];
							SYS_PIO_WR_DATA <= PIO_MWR_channel_buf_dout[31:0];
							SYS_PIO_WR_WR_EN <= 1'b1;
							usr_pio_wr_req <= 1'b0;
							pio_wr_state <= `PIO_WR_INIT;
						end
						else begin // this write is for user PIO bus
							usr_pio_wr_addr <= PIO_MWR_channel_buf_dout[50:34];
							usr_pio_wr_data <= PIO_MWR_channel_buf_dout[31:0];
							usr_pio_wr_req <= 1'b1;
							SYS_PIO_WR_WR_EN <= 1'b0;
							pio_wr_state <= `PIO_WR_USR_WAIT;
						end
					end
					else begin
						PIO_MWR_channel_buf_rd_en <= 1'b1;
						pio_wr_state <= `PIO_WR32_H2;
						SYS_PIO_WR_WR_EN <= 1'b0;
						usr_pio_wr_req <= 1'b0;
					end
				end
				`PIO_WR64_H2:begin
					PIO_MWR_channel_buf_rd_en <= 1'b1;
					if(PIO_MWR_channel_buf_empty == 1'b0 && PIO_MWR_channel_buf_rd_en == 1'b1)begin
						if(PIO_MWR_channel_buf_dout[19] == 1'b0)begin // this write is for SYS_CSR
							SYS_PIO_WR_ADDR <= PIO_MWR_channel_buf_dout[18:2];
							pio_wr_state <= `PIO_WR64_DATA_SYS;
						end
						else begin
							usr_pio_wr_addr <= PIO_MWR_channel_buf_dout[18:2];
							pio_wr_state <= `PIO_WR64_DATA_USR;
						end
					end
					else begin
						pio_wr_state <= `PIO_WR64_H2;
					end
				end
				`PIO_WR64_DATA_SYS:begin
					if(PIO_MWR_channel_buf_empty == 1'b0 && PIO_MWR_channel_buf_rd_en == 1'b1 && PIO_MWR_channel_buf_dout[64] == 1'b1)begin // TLP ends here
						pio_wr_state <= `PIO_WR_INIT;
						SYS_PIO_WR_DATA <= PIO_MWR_channel_buf_dout[63:32];
						SYS_PIO_WR_WR_EN <= 1'b1;
						usr_pio_wr_req <= 1'b0;
						PIO_MWR_channel_buf_rd_en <= 1'b0;
					end
					else begin
						pio_wr_state <= `PIO_WR64_DATA_SYS;
						SYS_PIO_WR_WR_EN <= 1'b0;
						usr_pio_wr_req <= 1'b0;
						PIO_MWR_channel_buf_rd_en <= 1'b1;
					end
				end
				`PIO_WR64_DATA_USR:begin
					if(PIO_MWR_channel_buf_empty == 1'b0 && PIO_MWR_channel_buf_rd_en == 1'b1 && PIO_MWR_channel_buf_dout[64] == 1'b1)begin // TLP ends here
						pio_wr_state <= `PIO_WR_USR_WAIT;
						usr_pio_wr_data <= PIO_MWR_channel_buf_dout[63:32];
						usr_pio_wr_req <= 1'b1;
						SYS_PIO_WR_WR_EN <= 1'b0;
						PIO_MWR_channel_buf_rd_en <= 1'b0;
					end
					else begin
						pio_wr_state <= `PIO_WR64_DATA_USR;
						SYS_PIO_WR_WR_EN <= 1'b0;
						usr_pio_wr_req <= 1'b0;
						PIO_MWR_channel_buf_rd_en <= 1'b1;
					end
				end
				`PIO_WR_USR_WAIT:begin
					PIO_MWR_channel_buf_rd_en <= 1'b0;
					if(usr_pio_wr_ack == 1'b1)begin
						usr_pio_wr_req <= 1'b0;
						pio_wr_state <= `PIO_WR_INIT;
					end
					else begin
						usr_pio_wr_req <= 1'b1;
						pio_wr_state <= `PIO_WR_USR_WAIT;
					end
				end
				default:begin
					pio_wr_state <= `PIO_WR_INIT;
				end
			endcase
		end
	end

endmodule
