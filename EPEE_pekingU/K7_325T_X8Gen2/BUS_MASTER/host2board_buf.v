`timescale 1ns / 1ps
/////////////////////////////////////
//
// host2board_buf.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The host to board buffer. It buffers the DMA data from host to board.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module host2board_buf(
	input sys_clk,
	input sys_rst_n,
	input RST_HOST2BOARD,  // from sys_csr
	// from DMA_host2board_engine
	input      [131:0] host2board_buf_din, // {DW_valid[3:0], data[127:0]}
	input      host2board_buf_wr_en,
	output     host2board_buf_prog_full,
	// to FIFO_host2board
	output reg [129:0] FIFO_host2board_din, // {QW_valid[1:0], data[127:0]}
	output reg FIFO_host2board_wr_en,
	input      FIFO_host2board_prog_full,
	// to SYS_CSR
	output reg [31:0] host2board_data_count // count in DW
);

	reg  host2board_buf_rd_en;
	wire [131:0] host2board_buf_dout;
	wire host2board_buf_empty;
	
	// for better timing of reset
	reg reset;
	always @(posedge sys_clk)begin
		reset = RST_HOST2BOARD || (~sys_rst_n);
	end
	
	always @(posedge sys_clk)begin
		if(reset)begin
			FIFO_host2board_din <= 130'd0;
			FIFO_host2board_wr_en <= 1'b0;
			host2board_buf_rd_en <= 1'b0;
			host2board_data_count <= 32'd0;
		end
		else begin
			if(FIFO_host2board_prog_full == 1'b0 && host2board_buf_empty == 1'b0)begin
				host2board_buf_rd_en <= 1'b1;
			end
			else begin
				host2board_buf_rd_en <= 1'b0;
			end
			if(host2board_buf_empty == 1'b0 && host2board_buf_rd_en == 1'b1)begin
				host2board_data_count <= host2board_data_count + host2board_buf_dout[128] + host2board_buf_dout[129] +
													host2board_buf_dout[130] + host2board_buf_dout[131];
				FIFO_host2board_din <= {host2board_buf_dout[131] | host2board_buf_dout[130], 
												host2board_buf_dout[129] | host2board_buf_dout[128], 
												host2board_buf_dout[127:0]};
				FIFO_host2board_wr_en <= 1'b1;
			end
			else begin
				FIFO_host2board_wr_en <= 1'b0;
			end
		end
	end

	FIFO_sync_width132_depth16_FWFT host2board_buf(
		.clk(sys_clk),
		.rst(reset),
		.din(host2board_buf_din),
		.wr_en(host2board_buf_wr_en),
		.rd_en(host2board_buf_rd_en),
		.dout(host2board_buf_dout),
		.full(),
		.empty(host2board_buf_empty),
		.prog_full(host2board_buf_prog_full)
	);

endmodule
