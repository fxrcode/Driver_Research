`timescale 1ns / 1ps
/////////////////////////////////////
//
// PIO_READ_ENGINE.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// PIO read engine. It deals with the PIO read tasks.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

`define PIO_RD_INIT  5'b00001
`define PIO_RD_H2    5'b00010
`define PIO_RD_WAIT  5'b00100
`define PIO_CPLD_H1  5'b01000
`define PIO_CPLD_H2  5'b10000

module PIO_READ_ENGINE(
	input sys_clk,
	input sys_rst_n,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	// from PIO_MRD_channel_buf
	input      [71:0] PIO_MRD_channel_buf_dout,
	output reg PIO_MRD_channel_buf_rd_en,
	input      PIO_MRD_channel_buf_empty,
	// to PIO_CPLD_channel_buf
	output reg [71:0] PIO_CPLD_channel_buf_din,
	output reg PIO_CPLD_channel_buf_wr_en,
	input      PIO_CPLD_channel_buf_prog_full,
	// to SYS_CSR
	output reg [16:0] SYS_PIO_RD_ADDR,
	input      [31:0] SYS_PIO_RD_DATA,
	// to user PIO bus
	output reg [16:0] usr_pio_rd_addr,
	input      [31:0] usr_pio_rd_data,
	output reg usr_pio_rd_req,
	input      usr_pio_rd_ack
);

	(*KEEP="TRUE"*)reg [4:0] pio_rd_state;
	reg is_data_from_usr; // record whether the read data is from user data bus or SYS_CSR
	reg is_64bit_addr; // record whether the read TLP is 64bit address
	reg [31:0] usr_data; // record usr_pio_rd_data
	// record MEM read TLP
	reg  [2:0] req_tc;
	reg  req_td;
	reg  req_ep;
	reg  [1:0] req_attr;
	reg  [9:0] req_len;
	//reg  [7:0] req_be;
	reg  [15:0] req_id;
	reg  [7:0] req_tag;
	reg  [6:0] req_addr;
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			PIO_MRD_channel_buf_rd_en <= 1'b0;
			PIO_CPLD_channel_buf_din <= 72'd0;
			PIO_CPLD_channel_buf_wr_en <= 1'b0;
			SYS_PIO_RD_ADDR <= 17'd0;
			usr_pio_rd_addr <= 17'd0;
			usr_pio_rd_req <= 1'b0;
			pio_rd_state <= `PIO_RD_INIT;
			is_data_from_usr <= 1'b0;
			is_64bit_addr <= 1'b0;
			usr_data <= 32'd0;
			req_tc <= 3'd0;
			req_td <= 1'd0;
			req_ep <= 1'd0;
			req_attr <= 0;
			req_len <= 10'd0;
			//req_be <= 0;
			req_id <= 16'd0;
			req_tag <= 8'd0;
			req_addr <= 7'd0;
		end
		else begin
			case(pio_rd_state)
				`PIO_RD_INIT:begin
					PIO_MRD_channel_buf_rd_en <= 1'b1;
					PIO_CPLD_channel_buf_wr_en <= 1'b0;
					usr_pio_rd_req <= 1'b0;
					if(PIO_MRD_channel_buf_empty == 1'b0 && PIO_MRD_channel_buf_rd_en == 1'b1 &&
						PIO_MRD_channel_buf_dout[65] == 1'b1)begin
						if(PIO_MRD_channel_buf_dout[63:56] == `FT_MRD64)begin
							is_64bit_addr <= 1'b1;
						end
						else begin
							is_64bit_addr <= 1'b0;
						end
						pio_rd_state <= `PIO_RD_H2;
						req_tc <= PIO_MRD_channel_buf_dout[54:52];
						req_td <= PIO_MRD_channel_buf_dout[47];
						req_ep <= PIO_MRD_channel_buf_dout[46];
						req_attr <= PIO_MRD_channel_buf_dout[45:44];
						req_len <= PIO_MRD_channel_buf_dout[41:32];
						//req_be <= PIO_MRD_channel_buf_dout[7:0];
						req_id <= PIO_MRD_channel_buf_dout[31:16];
						req_tag <= PIO_MRD_channel_buf_dout[15:8];
					end
					else begin
						pio_rd_state <= `PIO_RD_INIT;
					end
				end
				`PIO_RD_H2:begin
					PIO_CPLD_channel_buf_wr_en <= 1'b0;
					if(PIO_MRD_channel_buf_empty == 1'b0 && PIO_MRD_channel_buf_rd_en == 1'b1)begin
						PIO_MRD_channel_buf_rd_en <= 1'b0;
						if(is_64bit_addr == 1'b1)begin
							SYS_PIO_RD_ADDR <= PIO_MRD_channel_buf_dout[18:2];
							usr_pio_rd_addr <= PIO_MRD_channel_buf_dout[18:2];
							is_data_from_usr <= PIO_MRD_channel_buf_dout[19];
							req_addr <= PIO_MRD_channel_buf_dout[6:0];
							if(PIO_MRD_channel_buf_dout[19] == 1'b1)begin
								// the data is for user
								usr_pio_rd_req <= 1'b1;
								pio_rd_state <= `PIO_RD_WAIT;
							end
							else begin
								usr_pio_rd_req <= 1'b0;
								pio_rd_state <= `PIO_CPLD_H1;
							end
						end
						else begin
							SYS_PIO_RD_ADDR <= PIO_MRD_channel_buf_dout[50:34];
							usr_pio_rd_addr <= PIO_MRD_channel_buf_dout[50:34];
							is_data_from_usr <= PIO_MRD_channel_buf_dout[51];
							req_addr <= PIO_MRD_channel_buf_dout[38:32];
							if(PIO_MRD_channel_buf_dout[51] == 1'b1)begin
								// the data is for user
								usr_pio_rd_req <= 1'b1;
								pio_rd_state <= `PIO_RD_WAIT;
							end
							else begin
								usr_pio_rd_req <= 1'b0;
								pio_rd_state <= `PIO_CPLD_H1;
							end
						end
					end
					else begin
						PIO_MRD_channel_buf_rd_en <= 1'b1;
						usr_pio_rd_req <= 1'b0;
						pio_rd_state <= `PIO_RD_H2;
					end
				end
				`PIO_RD_WAIT:begin
					if(usr_pio_rd_ack == 1'b1)begin
						pio_rd_state <= `PIO_CPLD_H1;
						usr_pio_rd_req <= 1'b0;
						usr_data <= usr_pio_rd_data;
					end
					else begin
						pio_rd_state <= `PIO_RD_WAIT;
						usr_pio_rd_req <= 1'b1;
					end
				end
				`PIO_CPLD_H1:begin
					usr_pio_rd_req <= 1'b0;
					if(PIO_CPLD_channel_buf_prog_full == 1'b0)begin
						pio_rd_state <= `PIO_CPLD_H2;
						PIO_CPLD_channel_buf_din <={8'b0000_11_1_0,
															`FT_CPLD,
															1'b0, // reserved
															req_tc,
															3'd0, // reserved, attr2, reserved
															1'b0, // TH
															req_td,
															req_ep,
															req_attr,
															2'b00, // AT
															req_len,
															CFG_COMPLETER_ID,
															3'b000, // status
															1'b0, // BCM
															12'd4 // byte count
															};
						PIO_CPLD_channel_buf_wr_en <= 1'b1;
					end
					else begin
						pio_rd_state <= `PIO_CPLD_H1;
						PIO_CPLD_channel_buf_wr_en <= 1'b0;
					end
				end
				`PIO_CPLD_H2:begin
					usr_pio_rd_req <= 1'b0;
					PIO_CPLD_channel_buf_din <={8'b0000_11_0_1,
														req_id,
														req_tag,
														1'b0, // reserved
														req_addr,
														((is_data_from_usr == 1'b1)?usr_data:SYS_PIO_RD_DATA)
														};
					PIO_CPLD_channel_buf_wr_en <= 1'b1;
					PIO_MRD_channel_buf_rd_en <= 1'b1;
					pio_rd_state <= `PIO_RD_INIT;
				end
				default:begin
					PIO_CPLD_channel_buf_wr_en <= 1'b0;
					pio_rd_state <= `PIO_RD_INIT;
				end
			endcase
		end
	end

endmodule
