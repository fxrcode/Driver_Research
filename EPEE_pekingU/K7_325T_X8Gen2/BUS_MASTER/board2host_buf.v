`timescale 1ns / 1ps
/////////////////////////////////////
//
// board2host_buf.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Buffer data from board (FPGA) to host in DMA interface. 
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module board2host_buf(
	input sys_clk,
	input sys_rst_n,
	input RST_BOARD2HOST, // this reset is from CSR
	// from FIFO board2host
	input      [129:0] FIFO_board2host_dout, // {QW_Valid[1:0], data[127:0]}
	output reg FIFO_board2host_rd_en,
	input      FIFO_board2host_empty,
	// to DMA_board2host_engine
	output     [63:0] board2host_buf_odd_dout,
	input      board2host_buf_odd_rd_en,
	output     board2host_buf_odd_empty,
	output     [63:0] board2host_buf_even_dout,
	input      board2host_buf_even_rd_en,
	output     board2host_buf_even_empty,
	// to SYS_CSR
	output reg [31:0] board2host_data_count // how many datas are ready in DW
);

	reg [63:0] board2host_buf_odd_din;
	reg board2host_buf_odd_wr_en;
	wire board2host_buf_odd_prog_full;
	reg [63:0] board2host_buf_even_din;
	reg board2host_buf_even_wr_en;
	wire board2host_buf_even_prog_full;
	
	reg write_to_odd_first; // if the next DW is write to buf_odd, assert this

	reg reset;
	always @(posedge sys_clk)begin
		reset <= ~sys_rst_n || RST_BOARD2HOST;
	end
	
	always @(posedge sys_clk)begin
		if(reset == 1'b1)begin
			board2host_buf_odd_din <= 0;
			board2host_buf_odd_wr_en <= 0;
			board2host_buf_even_din <= 0;
			board2host_buf_even_wr_en <= 0;
			FIFO_board2host_rd_en <= 0;
			board2host_data_count <= 0;
			write_to_odd_first <= 1'b1;
		end
		else begin
			if(FIFO_board2host_empty == 1'b0 && board2host_buf_odd_prog_full == 1'b0 && board2host_buf_even_prog_full == 1'b0)begin
				FIFO_board2host_rd_en <= 1'b1;
			end
			else begin
				FIFO_board2host_rd_en <= 1'b0;
			end
			if(FIFO_board2host_empty == 1'b0 && FIFO_board2host_rd_en == 1'b1)begin
				if(write_to_odd_first == 1'b1)begin // write buf_odd first
					case(FIFO_board2host_dout[129:128])
						2'b00:begin
							board2host_buf_odd_wr_en <= 0;
							board2host_buf_even_wr_en <= 0;
							board2host_data_count <= board2host_data_count;
						end
						2'b01:begin
							board2host_buf_odd_din <= FIFO_board2host_dout[63:0];
							board2host_buf_odd_wr_en <= 1;
							board2host_buf_even_wr_en <= 0;
							write_to_odd_first <= 1'b0;
							board2host_data_count <= board2host_data_count + 2'd2;
						end
						2'b10:begin
							board2host_buf_odd_din <= FIFO_board2host_dout[127:64];
							board2host_buf_odd_wr_en <= 1;
							board2host_buf_even_wr_en <= 0;
							write_to_odd_first <= 1'b0;
							board2host_data_count <= board2host_data_count + 2'd2;
						end
						2'b11:begin
							board2host_buf_odd_din <= FIFO_board2host_dout[127:64];
							board2host_buf_odd_wr_en <= 1;
							board2host_buf_even_din <= FIFO_board2host_dout[63:0];
							board2host_buf_even_wr_en <= 1;
							board2host_data_count <= board2host_data_count + 3'd4;
						end
					endcase
				end
				else begin // write buf_even first
					case(FIFO_board2host_dout[129:128])
						2'b00:begin
							board2host_buf_odd_wr_en <= 0;
							board2host_buf_even_wr_en <= 0;
							board2host_data_count <= board2host_data_count;
						end
						2'b01:begin
							board2host_buf_odd_wr_en <= 0;
							board2host_buf_even_din <= FIFO_board2host_dout[63:0];
							board2host_buf_even_wr_en <= 1;
							write_to_odd_first <= 1'b1;
							board2host_data_count <= board2host_data_count + 2'd2;
						end
						2'b10:begin
							board2host_buf_odd_wr_en <= 0;
							board2host_buf_even_din <= FIFO_board2host_dout[127:64];
							board2host_buf_even_wr_en <= 1;
							write_to_odd_first <= 1'b1;
							board2host_data_count <= board2host_data_count + 2'd2;
						end
						2'b11:begin
							board2host_buf_odd_din <= FIFO_board2host_dout[63:0];
							board2host_buf_odd_wr_en <= 1;
							board2host_buf_even_din <= FIFO_board2host_dout[127:64];
							board2host_buf_even_wr_en <= 1;
							board2host_data_count <= board2host_data_count + 3'd4;
						end
					endcase
				end
			end
			else begin
				board2host_buf_odd_wr_en <= 0;
				board2host_buf_even_wr_en <= 0;
			end
		end
	end

	FIFO_sync_width64_depth512_FWFT board2host_buf_odd(
		.clk(sys_clk),
		.rst(reset),
		.din(board2host_buf_odd_din),
		.wr_en(board2host_buf_odd_wr_en),
		.rd_en(board2host_buf_odd_rd_en),
		.prog_full_thresh(9'd500),
		.dout(board2host_buf_odd_dout),
		.full(),
		.empty(board2host_buf_odd_empty),
		.prog_full(board2host_buf_odd_prog_full)
	);

	FIFO_sync_width64_depth512_FWFT board2host_buf_even(
		.clk(sys_clk),
		.rst(reset),
		.din(board2host_buf_even_din),
		.wr_en(board2host_buf_even_wr_en),
		.rd_en(board2host_buf_even_rd_en),
		.prog_full_thresh(9'd500),
		.dout(board2host_buf_even_dout),
		.full(),
		.empty(board2host_buf_even_empty),
		.prog_full(board2host_buf_even_prog_full)
	);

endmodule
