`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_CPLD_channel_dispatcher.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Dispatch the CplD TLP to DMA_board2host_engine, DMA_host2board_descriptor_engine and 
// DMA_host2board_engine.
//            *  For 64bit interface, CPLD head's 3 DWs will be simplfied. 
//            *  The simplefied head is (1DW): 
//            *      31-29  28-26   25-16    15-8   7     6-0
//            *      Status   R     Length   TAG    R     Lower Address
//            *  For 128bit interface, CPLD head is not simplfied.
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define DMA_CPLD_DIS_INIT   9'b000000001
`define DMA_CPLD_DIS_H2     9'b000000010
`define DMA_CPLD_DIS_W_DCP  9'b000000100
`define DMA_CPLD_DIS_W_DCP2 9'b000001000
`define DMA_CPLD_DIS_W_ZR   9'b000010000
`define DMA_CPLD_DIS_W_ZR2  9'b000100000
`define DMA_CPLD_DIS_R_DCP  9'b001000000
`define DMA_CPLD_DIS_R_DCP2 9'b010000000
`define DMA_CPLD_DIS_R      9'b100000000

module DMA_CPLD_channel_dispatcher(
	input sys_clk,
	input sys_rst_n,
	// from DS_CTRL
	input [133:0] DMA_CPLD_channel_buf_din,
	output DMA_CPLD_channel_buf_prog_full,
	input  DMA_CPLD_channel_buf_wr_en,
	// to DMA_board2host_descriptor_engine
	output reg [71:0] DMA_w_descriptor_CPLD_TLP_din,
	input DMA_w_descriptor_CPLD_TLP_prog_full,
	output reg DMA_w_descriptor_CPLD_TLP_wr_en,
	// to DMA_board2host_engine
	output reg [71:0] DMA_w_zero_read_TLP_din,
	input DMA_w_zero_read_TLP_prog_full,
	output reg DMA_w_zero_read_TLP_wr_en,
	// to DMA_host2board_descriptor_engine
	output reg [71:0] DMA_r_descriptor_CPLD_TLP_din,
	input DMA_r_descriptor_CPLD_TLP_prog_full,
	output reg DMA_r_descriptor_CPLD_TLP_wr_en,
	// DMA_host2board_engine
	output reg [133:0] DMA_r_CPLD_TLP_din,
	input DMA_r_CPLD_TLP_prog_full,
	output reg DMA_r_CPLD_TLP_wr_en
);

	wire [133:0] DMA_CPLD_channel_buf_dout;
	wire DMA_CPLD_channel_buf_empty;
	reg DMA_CPLD_channel_buf_rd_en;
	(*KEEP="TRUE"*)reg [8:0] DMA_CPLD_DIS_state;

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			DMA_w_descriptor_CPLD_TLP_din <= 72'd0;
			DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
			DMA_w_zero_read_TLP_din <= 72'd0;
			DMA_w_zero_read_TLP_wr_en <= 1'b0;
			DMA_r_descriptor_CPLD_TLP_din <= 72'd0;
			DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
			DMA_r_CPLD_TLP_din <= 134'd0;
			DMA_r_CPLD_TLP_wr_en <= 1'b0;
			DMA_CPLD_channel_buf_rd_en <= 1'b0;
			DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_INIT;
		end
		else begin
			case(DMA_CPLD_DIS_state)
				`DMA_CPLD_DIS_INIT   : begin
					if(DMA_CPLD_channel_buf_empty == 1'b0 && DMA_w_descriptor_CPLD_TLP_prog_full == 1'b0 &&
						DMA_w_zero_read_TLP_prog_full == 1'b0 && DMA_r_descriptor_CPLD_TLP_prog_full == 1'b0 &&
						DMA_r_CPLD_TLP_prog_full == 1'b0)begin
						DMA_CPLD_channel_buf_rd_en <= 1'b1;
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_H2;
					end
					else begin
						DMA_CPLD_channel_buf_rd_en <= 1'b0;
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_INIT;
					end
					DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_w_zero_read_TLP_wr_en <= 1'b0;
					DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
				end
				`DMA_CPLD_DIS_H2     : begin
					case(DMA_CPLD_channel_buf_dout[47 : 40])
						8'd0:begin // this CPLD tag = 0, for DMA_board2host_descriptor_engine
							DMA_CPLD_DIS_state <= ((DMA_CPLD_channel_buf_dout[128] == 1'b1) ? `DMA_CPLD_DIS_INIT : `DMA_CPLD_DIS_W_DCP);
							DMA_w_descriptor_CPLD_TLP_din <= {4'd0, 2'b11, DMA_CPLD_channel_buf_dout[129:128],
																		DMA_CPLD_channel_buf_dout[79:77], 3'd0, // status
																		DMA_CPLD_channel_buf_dout[105:96], // length
																		DMA_CPLD_channel_buf_dout[47:0]
																		};
							DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b1;
							DMA_w_zero_read_TLP_wr_en <= 1'b0;
							DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
							DMA_r_CPLD_TLP_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_rd_en <= 1'b0;
						end
						8'd1:begin // this CPLD tag = 1, for DMA_board2host_engine
							DMA_CPLD_DIS_state <= ((DMA_CPLD_channel_buf_dout[128] == 1'b1) ? `DMA_CPLD_DIS_INIT : `DMA_CPLD_DIS_W_ZR);
							DMA_w_zero_read_TLP_din <= {4'd0, 2'b11, DMA_CPLD_channel_buf_dout[129:128],
																		DMA_CPLD_channel_buf_dout[79:77], 3'd0, // status
																		DMA_CPLD_channel_buf_dout[105:96], // length
																		DMA_CPLD_channel_buf_dout[47:0]
																		};
							DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
							DMA_w_zero_read_TLP_wr_en <= 1'b1;
							DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
							DMA_r_CPLD_TLP_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_rd_en <= 1'b0;
						end
						8'd2:begin // this CPLD tag = 2, for DMA_host2board_descriptor_engine
							DMA_CPLD_DIS_state <= ((DMA_CPLD_channel_buf_dout[128] == 1'b1) ? `DMA_CPLD_DIS_INIT : `DMA_CPLD_DIS_R_DCP);
							DMA_r_descriptor_CPLD_TLP_din <= {4'd0, 2'b11, DMA_CPLD_channel_buf_dout[129:128],
																		DMA_CPLD_channel_buf_dout[79:77], 3'd0, // status
																		DMA_CPLD_channel_buf_dout[105:96], // length
																		DMA_CPLD_channel_buf_dout[47:0]
																		};
							DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
							DMA_w_zero_read_TLP_wr_en <= 1'b0;
							DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b1;
							DMA_r_CPLD_TLP_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_rd_en <= 1'b0;
						end
						default:begin // other tags is for DMA_host2board_engine
							DMA_CPLD_DIS_state <= ((DMA_CPLD_channel_buf_dout[128] == 1'b1) ? `DMA_CPLD_DIS_INIT : `DMA_CPLD_DIS_R);
							DMA_r_CPLD_TLP_din <= DMA_CPLD_channel_buf_dout;
							DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
							DMA_w_zero_read_TLP_wr_en <= 1'b0;
							DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
							DMA_r_CPLD_TLP_wr_en <= 1'b1;
					DMA_CPLD_channel_buf_rd_en <= ((DMA_CPLD_channel_buf_dout[128] == 1'b1) ? 1'b0 : 1'b1);
						end
					endcase
				end
				`DMA_CPLD_DIS_W_DCP  : begin
					if(DMA_CPLD_channel_buf_empty == 1'b0 && DMA_w_descriptor_CPLD_TLP_prog_full == 1'b0)begin
						DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b1;
						DMA_w_descriptor_CPLD_TLP_din <= {4'd0,
																	DMA_CPLD_channel_buf_dout[133:132], // DW enable
																	1'b0, // TLP start
																	((DMA_CPLD_channel_buf_dout[131] == 1'b0) ? 1'b1 : 1'b0),
																	DMA_CPLD_channel_buf_dout[127:64]};
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_W_DCP2;
						DMA_CPLD_channel_buf_rd_en <= 1'b1;
					end
					else begin
						DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_W_DCP;
						DMA_CPLD_channel_buf_rd_en <= 1'b0;
					end
					DMA_w_zero_read_TLP_wr_en <= 1'b0;
					DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
				end
				`DMA_CPLD_DIS_W_DCP2 : begin
					DMA_w_descriptor_CPLD_TLP_wr_en <= ((DMA_CPLD_channel_buf_dout[131:130] == 2'b0) ? 1'b0 : 1'b1);
					DMA_w_descriptor_CPLD_TLP_din <= {4'd0,
																DMA_CPLD_channel_buf_dout[131:130], // DW enable
																1'b0, // TLP start
																DMA_CPLD_channel_buf_dout[128], // TLP end
																DMA_CPLD_channel_buf_dout[63:0]};
					DMA_CPLD_DIS_state <= ((DMA_CPLD_channel_buf_dout[128] == 1'b0) ? `DMA_CPLD_DIS_W_DCP : `DMA_CPLD_DIS_INIT);
					DMA_CPLD_channel_buf_rd_en <= 1'b0;
					DMA_w_zero_read_TLP_wr_en <= 1'b0;
					DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
				end
				`DMA_CPLD_DIS_W_ZR   : begin
					if(DMA_CPLD_channel_buf_empty == 1'b0 && DMA_w_zero_read_TLP_prog_full == 1'b0)begin
						DMA_w_zero_read_TLP_wr_en <= 1'b1;
						DMA_w_zero_read_TLP_din <= {4'd0,
																	DMA_CPLD_channel_buf_dout[133:132], // DW enable
																	1'b0, // TLP start
																	((DMA_CPLD_channel_buf_dout[131] == 1'b0) ? 1'b1 : 1'b0),
																	DMA_CPLD_channel_buf_dout[127:64]};
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_W_ZR2;
						DMA_CPLD_channel_buf_rd_en <= 1'b1;
					end
					else begin
						DMA_w_zero_read_TLP_wr_en <= 1'b0;
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_W_ZR;
						DMA_CPLD_channel_buf_rd_en <= 1'b0;
					end
					DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
				end
				`DMA_CPLD_DIS_W_ZR2  : begin
					DMA_w_zero_read_TLP_wr_en <= ((DMA_CPLD_channel_buf_dout[131:130] == 2'b0) ? 1'b0 : 1'b1);
					DMA_w_zero_read_TLP_din <= {4'd0,
																DMA_CPLD_channel_buf_dout[131:130], // DW enable
																1'b0, // TLP start
																DMA_CPLD_channel_buf_dout[128], // TLP end
																DMA_CPLD_channel_buf_dout[63:0]};
					DMA_CPLD_DIS_state <= ((DMA_CPLD_channel_buf_dout[128] == 1'b0) ? `DMA_CPLD_DIS_W_ZR : `DMA_CPLD_DIS_INIT);
					DMA_CPLD_channel_buf_rd_en <= 1'b0;
					DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
				end
				`DMA_CPLD_DIS_R_DCP  : begin
					if(DMA_CPLD_channel_buf_empty == 1'b0 && DMA_r_descriptor_CPLD_TLP_prog_full == 1'b0)begin
						DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b1;
						DMA_r_descriptor_CPLD_TLP_din <= {4'd0,
																	DMA_CPLD_channel_buf_dout[133:132], // DW enable
																	1'b0, // TLP start
																	((DMA_CPLD_channel_buf_dout[131] == 1'b0) ? 1'b1 : 1'b0),
																	DMA_CPLD_channel_buf_dout[127:64]};
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_R_DCP2;
						DMA_CPLD_channel_buf_rd_en <= 1'b1;
					end
					else begin
						DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_R_DCP;
						DMA_CPLD_channel_buf_rd_en <= 1'b0;
					end
					DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_w_zero_read_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
				end
				`DMA_CPLD_DIS_R_DCP2 : begin
					DMA_r_descriptor_CPLD_TLP_wr_en <= ((DMA_CPLD_channel_buf_dout[131:130] == 2'b0) ? 1'b0 : 1'b1);
					DMA_r_descriptor_CPLD_TLP_din <= {4'd0,
																DMA_CPLD_channel_buf_dout[131:130], // DW enable
																1'b0, // TLP start
																DMA_CPLD_channel_buf_dout[128], // TLP end
																DMA_CPLD_channel_buf_dout[63:0]};
					DMA_CPLD_DIS_state <= ((DMA_CPLD_channel_buf_dout[128] == 1'b0) ? `DMA_CPLD_DIS_R_DCP : `DMA_CPLD_DIS_INIT);
					DMA_CPLD_channel_buf_rd_en <= 1'b0;
					DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_w_zero_read_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
				end
				`DMA_CPLD_DIS_R      : begin
					if(DMA_CPLD_channel_buf_empty == 1'b0 && DMA_r_CPLD_TLP_prog_full == 1'b0 && 
						(DMA_CPLD_channel_buf_dout[128] == 1'b0 || (DMA_CPLD_channel_buf_dout[128] == 1'b1 && DMA_CPLD_channel_buf_rd_en == 1'b0)))begin
						DMA_CPLD_channel_buf_rd_en <= 1'b1;
					end
					else begin
						DMA_CPLD_channel_buf_rd_en <= 1'b0;
					end
					if(DMA_CPLD_channel_buf_empty == 1'b0 && DMA_CPLD_channel_buf_rd_en == 1'b1)begin
						DMA_r_CPLD_TLP_din <= DMA_CPLD_channel_buf_dout;
						DMA_r_CPLD_TLP_wr_en <= 1'b1;
					end
					else begin
						DMA_r_CPLD_TLP_din <= 134'd0;
						DMA_r_CPLD_TLP_wr_en <= 1'b0;
					end
					if(DMA_CPLD_channel_buf_empty == 1'b0 && DMA_CPLD_channel_buf_rd_en == 1'b1 &&
						DMA_CPLD_channel_buf_dout[128] == 1'b1)begin
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_INIT;
					end
					else begin
						DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_R;
					end
					DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_w_zero_read_TLP_wr_en <= 1'b0;
					DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
				end
				default             : begin
					DMA_w_descriptor_CPLD_TLP_din <= 72'd0;
					DMA_w_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_w_zero_read_TLP_din <= 72'd0;
					DMA_w_zero_read_TLP_wr_en <= 1'b0;
					DMA_r_descriptor_CPLD_TLP_din <= 72'd0;
					DMA_r_descriptor_CPLD_TLP_wr_en <= 1'b0;
					DMA_r_CPLD_TLP_din <= 134'd0;
					DMA_r_CPLD_TLP_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_rd_en <= 1'b0;
					DMA_CPLD_DIS_state <= `DMA_CPLD_DIS_INIT;
				end
			endcase
		end
	end

	FIFO_sync_width134_depth16_FWFT DMA_CPLD_channel_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_CPLD_channel_buf_din),
		.wr_en(DMA_CPLD_channel_buf_wr_en),
		.rd_en(DMA_CPLD_channel_buf_rd_en),
		.dout(DMA_CPLD_channel_buf_dout),
		.full(),
		.empty(DMA_CPLD_channel_buf_empty),
		.prog_full(DMA_CPLD_channel_buf_prog_full)
	);

endmodule
