`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_MWR_channel_collector.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Collect the MWr TLPs from DMA board to host module
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_MWR_channel_collector(
	input sys_clk,
	input sys_rst_n,
	// from DMA_board2host_engine
	input  [133:0] DMA_W_MWR_TLP_dout,
	input  DMA_W_MWR_TLP_empty,
	output reg DMA_W_MWR_TLP_rd_en,
	// to US_TLP_collector
	output [133:0] DMA_MWR_channel_buf_dout,
	input  DMA_MWR_channel_buf_rd_en,
	output DMA_MWR_channel_buf_empty
);

	reg  [133:0] DMA_MWR_channel_buf_din;
	reg  DMA_MWR_channel_buf_wr_en;
	wire DMA_MWR_channel_buf_prog_full;

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			DMA_W_MWR_TLP_rd_en <= 1'b0;
			DMA_MWR_channel_buf_din <= 134'd0;
			DMA_MWR_channel_buf_wr_en <= 1'b0;
		end
		else begin
			if(DMA_W_MWR_TLP_empty == 1'b0 && DMA_MWR_channel_buf_prog_full == 1'b0)begin
				DMA_W_MWR_TLP_rd_en <= 1'b1;
			end
			else begin
				DMA_W_MWR_TLP_rd_en <= 1'b0;
			end
			if(DMA_W_MWR_TLP_rd_en == 1'b1 && DMA_W_MWR_TLP_empty == 1'b0)begin
				DMA_MWR_channel_buf_din <= DMA_W_MWR_TLP_dout;
				DMA_MWR_channel_buf_wr_en <= 1'b1;
			end
			else begin
				DMA_MWR_channel_buf_wr_en <= 1'b0;
			end
		end
	end

	FIFO_sync_width134_depth16_FWFT DMA_MWR_channel_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_MWR_channel_buf_din),
		.wr_en(DMA_MWR_channel_buf_wr_en),
		.rd_en(DMA_MWR_channel_buf_rd_en),
		.dout(DMA_MWR_channel_buf_dout),
		.full(),
		.empty(DMA_MWR_channel_buf_empty),
		.prog_full(DMA_MWR_channel_buf_prog_full)
	);

endmodule
