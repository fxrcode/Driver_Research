`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_host2board_INT_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Control the interrupt of DMA host to board side. It sends interrupt when DMA (host to
// board) process done / error.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define DMA_R_INT_IDLE   4'b0001
`define DMA_R_INT_REQ    4'b0010
`define DMA_R_INT_WAIT   4'b0100
`define DMA_R_INT_SERVED 4'b1000

// timeout threshold is about 10 seconds when using 250MHz clk
`define INT_TIMEOUT_THRESHOLD  64'd2500000000

module DMA_host2board_INT_CTRL(
	input sys_clk,
	input sys_rst_n,
	// from DMA_host2board_RST_CTRL
	input dma_host2board_rst,
	output reg dma_host2board_rst_done,
	// from DMA_host2board_engine
	input dma_r_start_state,
	input dma_r_done_state,
	input dma_r_error_state,
	// to SYS_CSR
	input int_enable,
	output reg dma_r_int_req_state,
	input dma_r_int_served,
	output reg dma_r_int_served_state,
	// to INT_GEN
	output reg IG_DMA_r_int_req,
	input      IG_DMA_r_int_req_ack,
	output reg IG_DMA_r_int_served,
	input      IG_DMA_r_int_served_ack
);

	(*KEEP="TRUE"*)reg [3:0] dma_r_int_state;
	reg reset;

	// for timeout logic
	reg [3:0] dma_r_int_state_dly;
	reg [63:0]timeout_counter;
	reg timeout_flag;
	
	// timeout logic , for recovering from deadlock
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			timeout_counter <= 0;
			timeout_flag <= 0;
			dma_r_int_state_dly <= `DMA_R_INT_IDLE;
		end else begin
			dma_r_int_state_dly <= dma_r_int_state;
			if(dma_r_int_state == dma_r_int_state_dly && dma_r_int_state_dly != `DMA_R_INT_IDLE)begin
				if(timeout_counter >= `INT_TIMEOUT_THRESHOLD)begin
					timeout_flag <= 1'b1;
				end else begin
					timeout_counter <= timeout_counter + 1'b1;
					timeout_flag <= 1'b0;
				end
			end else begin
				timeout_flag <= 0;
				timeout_counter <= 0;
			end
		end
	end
	
	// reset logic, for reset ack and better timing
	always @(posedge sys_clk)begin
		if(~sys_rst_n || timeout_flag == 1'b1)begin
		// when sys_rst_n or timeout, reset will be asserted
			reset <= 1'b1;
			dma_host2board_rst_done <= 1'b0;
		end
		else if(dma_host2board_rst == 1'b1) begin
			if(dma_r_int_state == `DMA_R_INT_IDLE)begin
				reset <= 1'b1;
				dma_host2board_rst_done <= 1'b1;
			end
			else begin
				reset <= 1'b0;
				dma_host2board_rst_done <= 1'b0;
			end
		end
		else begin
			reset <= 1'b0;
			dma_host2board_rst_done <= 1'b0;
		end
	end
	
	// main logic of this module
	always @(posedge sys_clk)begin
		if(reset == 1'b1)begin
			dma_r_int_state <= `DMA_R_INT_IDLE;
			dma_r_int_req_state <= 1'b0;
			dma_r_int_served_state <= 1'b0;
			IG_DMA_r_int_req <= 1'b0;
			IG_DMA_r_int_served <= 1'b0;
		end
		else begin
			case(dma_r_int_state)
				`DMA_R_INT_IDLE:begin
					dma_r_int_req_state <= 1'b0;
					if(dma_host2board_rst == 1'b0 && dma_r_int_served_state == 1'b0 && dma_r_start_state == 1'b1 &&
						(dma_r_done_state == 1'b1 || dma_r_error_state == 1'b1) && int_enable == 1'b1)begin
						// when dma done or error, and the interrupt havn't been sent, start to send the interrupt
						IG_DMA_r_int_req <= 1'b1;
						dma_r_int_state <= `DMA_R_INT_REQ;
					end
					else begin
						IG_DMA_r_int_req <= 1'b0;
						dma_r_int_state <= `DMA_R_INT_IDLE;
					end
				end
				`DMA_R_INT_REQ:begin
					if(IG_DMA_r_int_req_ack == 1'b1)begin
						dma_r_int_req_state <= 1'b1;
						IG_DMA_r_int_req <= 1'b0;
						dma_r_int_state <= `DMA_R_INT_WAIT;
					end
					else begin
						dma_r_int_req_state <= 1'b0;
						IG_DMA_r_int_req <= 1'b1;
						dma_r_int_state <= `DMA_R_INT_REQ;
					end
				end
				`DMA_R_INT_WAIT:begin
					if(dma_r_int_served == 1'b1)begin
						dma_r_int_req_state <= 1'b0;
						IG_DMA_r_int_served <= 1'b1;
						IG_DMA_r_int_req <= 1'b0;
						dma_r_int_served_state <= 1'b1;
						dma_r_int_state <= `DMA_R_INT_SERVED;
					end
					else begin
						dma_r_int_req_state <= 1'b1;
						IG_DMA_r_int_served <= 1'b0;
						IG_DMA_r_int_req <= 1'b0;
						dma_r_int_served_state <= 1'b0;
						dma_r_int_state <= `DMA_R_INT_WAIT;
					end
				end
				`DMA_R_INT_SERVED:begin
					if(IG_DMA_r_int_served_ack == 1'b1)begin
						IG_DMA_r_int_served <= 1'b0;
						IG_DMA_r_int_req <= 1'b0;
						dma_r_int_served_state <= 1'b1;
						dma_r_int_state <= `DMA_R_INT_IDLE;
					end
					else begin
						IG_DMA_r_int_served <= 1'b1;
						IG_DMA_r_int_req <= 1'b0;
						dma_r_int_state <= `DMA_R_INT_SERVED;
					end
				end
				default:begin
					dma_r_int_state <= `DMA_R_INT_IDLE;
				end
			endcase
		end
	end
	
endmodule
