`timescale 1ns / 1ps
/////////////////////////////////////
//
// PIO_ENGINE.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The wrapper of the PIO engine. It deals with all PIO tasks. 
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module PIO_ENGINE(
	input sys_clk,
	input sys_rst_n,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	// from DS_TLP_dispactor
	input  [71:0] PIO_MWR_channel_buf_din,
	output PIO_MWR_channel_buf_prog_full,
	input  PIO_MWR_channel_buf_wr_en,
	input  [71:0] PIO_MRD_channel_buf_din,
	output PIO_MRD_channel_buf_prog_full,
	input  PIO_MRD_channel_buf_wr_en,
	// to US_TLP_collector
	output [71:0] PIO_CPLD_channel_buf_dout,
	output PIO_CPLD_channel_buf_empty,
	input  PIO_CPLD_channel_buf_rd_en,
	// to SYS_CSR
	output [16:0] SYS_PIO_WR_ADDR,
	output [31:0] SYS_PIO_WR_DATA,
	output SYS_PIO_WR_WR_EN,
	output [16:0] SYS_PIO_RD_ADDR,
	input  [31:0] SYS_PIO_RD_DATA,
	// to user, in sys_clk clock domain
	output [16:0] usr_pio_wr_addr,
	output [31:0] usr_pio_wr_data,
	output usr_pio_wr_req,
	input  usr_pio_wr_ack,
	output [16:0] usr_pio_rd_addr,
	input  [31:0] usr_pio_rd_data,
	output usr_pio_rd_req,
	input  usr_pio_rd_ack
);

	// connect pio_write_engine and PIO_MWR_channel_buf
	wire [71:0] PIO_MWR_channel_buf_dout;
	wire PIO_MWR_channel_buf_rd_en;
	wire PIO_MWR_channel_buf_empty;
	
	// connect pio_read_engine and PIO_MRD_channel_buf
	wire [71:0] PIO_MRD_channel_buf_dout;
	wire PIO_MRD_channel_buf_rd_en;
	wire PIO_MRD_channel_buf_empty;

	// connect pio_read_engine and PIO_CPLD_channel_buf
	wire [71:0] PIO_CPLD_channel_buf_din;
	wire PIO_CPLD_channel_buf_wr_en;
	wire PIO_CPLD_channel_buf_prog_full;


	PIO_WRITE_ENGINE pio_write_engine(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		// from PIO_MWR_channel_buf
		.PIO_MWR_channel_buf_dout(PIO_MWR_channel_buf_dout),
		.PIO_MWR_channel_buf_empty(PIO_MWR_channel_buf_empty),
		.PIO_MWR_channel_buf_rd_en(PIO_MWR_channel_buf_rd_en),
		// to SYS_CSR
		.SYS_PIO_WR_ADDR(SYS_PIO_WR_ADDR),
		.SYS_PIO_WR_DATA(SYS_PIO_WR_DATA),
		.SYS_PIO_WR_WR_EN(SYS_PIO_WR_WR_EN),
		// to user PIO bus
		.usr_pio_wr_addr(usr_pio_wr_addr),
		.usr_pio_wr_data(usr_pio_wr_data),
		.usr_pio_wr_req(usr_pio_wr_req),
		.usr_pio_wr_ack(usr_pio_wr_ack)
	);

	PIO_READ_ENGINE pio_read_engine(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID),
		// from PIO_MRD_channel_buf
		.PIO_MRD_channel_buf_dout(PIO_MRD_channel_buf_dout),
		.PIO_MRD_channel_buf_rd_en(PIO_MRD_channel_buf_rd_en),
		.PIO_MRD_channel_buf_empty(PIO_MRD_channel_buf_empty),
		// to PIO_CPLD_channel_buf
		.PIO_CPLD_channel_buf_din(PIO_CPLD_channel_buf_din),
		.PIO_CPLD_channel_buf_wr_en(PIO_CPLD_channel_buf_wr_en),
		.PIO_CPLD_channel_buf_prog_full(PIO_CPLD_channel_buf_prog_full),
		// to SYS_CSR
		.SYS_PIO_RD_ADDR(SYS_PIO_RD_ADDR),
		.SYS_PIO_RD_DATA(SYS_PIO_RD_DATA),
		// to user PIO bus
		.usr_pio_rd_addr(usr_pio_rd_addr),
		.usr_pio_rd_data(usr_pio_rd_data),
		.usr_pio_rd_req(usr_pio_rd_req),
		.usr_pio_rd_ack(usr_pio_rd_ack)
	);

	FIFO_sync_width72_depth16_FWFT PIO_MWR_channel_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(PIO_MWR_channel_buf_din),
		.wr_en(PIO_MWR_channel_buf_wr_en),
		.rd_en(PIO_MWR_channel_buf_rd_en),
		.dout(PIO_MWR_channel_buf_dout),
		.full(),
		.empty(PIO_MWR_channel_buf_empty),
		.prog_full(PIO_MWR_channel_buf_prog_full)
	);

	FIFO_sync_width72_depth16_FWFT PIO_MRD_channel_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(PIO_MRD_channel_buf_din),
		.wr_en(PIO_MRD_channel_buf_wr_en),
		.rd_en(PIO_MRD_channel_buf_rd_en),
		.dout(PIO_MRD_channel_buf_dout),
		.full(),
		.empty(PIO_MRD_channel_buf_empty),
		.prog_full(PIO_MRD_channel_buf_prog_full)
	);

	FIFO_sync_width72_depth16_FWFT PIO_CPLD_channel_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(PIO_CPLD_channel_buf_din),
		.wr_en(PIO_CPLD_channel_buf_wr_en),
		.rd_en(PIO_CPLD_channel_buf_rd_en),
		.dout(PIO_CPLD_channel_buf_dout),
		.full(),
		.empty(PIO_CPLD_channel_buf_empty),
		.prog_full(PIO_CPLD_channel_buf_prog_full)
	);

endmodule
