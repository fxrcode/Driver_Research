`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_w_MWR_TLP_committer.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Commit the MWr TLP in DMA write (board to host) side.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

`define DMA_W_COM_INIT 3'b001
`define DMA_W_COM_SEND 3'b010
`define DMA_W_COM_WAIT 3'b100

module DMA_w_MWR_TLP_committer(
	input sys_clk,
	input sys_rst_n,
	input dma_board2host_rst,
	output reg dma_board2host_rst_done,
	// from mwr_tlp_buf
	input [133:0] mwr_tlp_buf_din,
	input mwr_tlp_buf_wr_en,
	output mwr_tlp_buf_prog_full,
	// to DMA_W_MWR_TLP FIFO
	output [133:0] DMA_W_MWR_TLP_dout,
	input DMA_W_MWR_TLP_rd_en,
	output DMA_W_MWR_TLP_empty,
	// to DMA_w_zero_read_TLP_waiter
	output reg DMA_w_zero_read_TLP_sent,
	input DMA_w_zero_read_TLP_acked
);
	
	wire [133:0] mwr_tlp_buf_dout;
	reg  mwr_tlp_buf_rd_en;
	wire mwr_tlp_buf_empty;
	
	reg [133:0] DMA_W_MWR_TLP_din;
	reg DMA_W_MWR_TLP_wr_en;
	wire DMA_W_MWR_TLP_prog_full;
	
	(*KEEP="TRUE"*)reg [2:0] dma_w_com_state;
	reg current_tlp_type; // 0: MWR, 1: MRD
	reg fifo_reset;

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			dma_w_com_state <= `DMA_W_COM_INIT;
			dma_board2host_rst_done <= 1'b0;
			DMA_w_zero_read_TLP_sent <= 0;
			fifo_reset <= 1'b1;
			mwr_tlp_buf_rd_en <= 1'b0;
			DMA_W_MWR_TLP_din <= 0;
			DMA_W_MWR_TLP_wr_en <= 1'b0;
			current_tlp_type <= 0;
		end
		else if(dma_board2host_rst == 1'b1 && dma_w_com_state == `DMA_W_COM_INIT)begin
			dma_board2host_rst_done <= 1'b1;
			DMA_w_zero_read_TLP_sent <= 0;
			fifo_reset <= 1'b1;
			mwr_tlp_buf_rd_en <= 1'b0;
			DMA_W_MWR_TLP_din <= 0;
			DMA_W_MWR_TLP_wr_en <= 1'b0;
			current_tlp_type <= 0;
		end
		else begin
			dma_board2host_rst_done <= 1'b0;
			fifo_reset <= 1'b0;
			case(dma_w_com_state)
				`DMA_W_COM_INIT:begin
					DMA_W_MWR_TLP_wr_en <= 1'b0;
					if(mwr_tlp_buf_empty == 1'b0 && DMA_W_MWR_TLP_prog_full == 1'b0)begin
						mwr_tlp_buf_rd_en <= 1'b1;
						dma_w_com_state <= `DMA_W_COM_SEND;
						if(mwr_tlp_buf_dout[127:120]==`FT_MWR32 || mwr_tlp_buf_dout[127:120]==`FT_MWR64)begin
							current_tlp_type <= 0;
						end
						else begin
							current_tlp_type <= 1;
						end
					end
					else begin
						mwr_tlp_buf_rd_en <= 1'b0;
						dma_w_com_state <= `DMA_W_COM_INIT;
					end
				end
				`DMA_W_COM_SEND:begin
					// for read_enable singal
					if(mwr_tlp_buf_empty == 1'b0 && DMA_W_MWR_TLP_prog_full == 1'b0 && 
						(!mwr_tlp_buf_rd_en == 1'b1 || !mwr_tlp_buf_dout[128] == 1'b1))begin
					// current TLP not end
						mwr_tlp_buf_rd_en <= 1'b1;
					end
					else begin
						mwr_tlp_buf_rd_en <= 1'b0;
					end
					// write to DMA_W_MWR_TLP FIFO
					if(mwr_tlp_buf_empty == 1'b0 && mwr_tlp_buf_rd_en == 1'b1)begin
						DMA_W_MWR_TLP_din <= mwr_tlp_buf_dout;
						DMA_W_MWR_TLP_wr_en <= 1'b1;
						// for state machine
						if(mwr_tlp_buf_dout[128] == 1'b1)begin
							if(current_tlp_type == 0)begin
								dma_w_com_state <= `DMA_W_COM_INIT;
							end
							else begin
								dma_w_com_state <= `DMA_W_COM_WAIT;
								DMA_w_zero_read_TLP_sent <= 0;
							end
						end
						else begin
							dma_w_com_state <= `DMA_W_COM_SEND;
							DMA_w_zero_read_TLP_sent <= 0;
						end
					end
					else begin
						DMA_W_MWR_TLP_wr_en <= 1'b0;
						dma_w_com_state <= `DMA_W_COM_SEND;
						DMA_w_zero_read_TLP_sent <= 0;
					end
				end
				`DMA_W_COM_WAIT:begin
					DMA_W_MWR_TLP_wr_en <= 1'b0;
					if(DMA_w_zero_read_TLP_acked == 1'b1)begin
						DMA_w_zero_read_TLP_sent <= 0;
						dma_w_com_state <= `DMA_W_COM_INIT;
					end
					else begin
						DMA_w_zero_read_TLP_sent <= 1;
						dma_w_com_state <= `DMA_W_COM_WAIT;
					end
				end
				default:begin
					dma_w_com_state <= `DMA_W_COM_INIT;
				end
			endcase
		end
	end

	FIFO_sync_width134_depth16_FWFT mwr_tlp_buf(
		.clk(sys_clk),
		.rst(fifo_reset),
		.din(mwr_tlp_buf_din),
		.wr_en(mwr_tlp_buf_wr_en),
		.rd_en(mwr_tlp_buf_rd_en),
		.dout(mwr_tlp_buf_dout),
		.full(),
		.empty(mwr_tlp_buf_empty),
		.prog_full(mwr_tlp_buf_prog_full)
	);

	FIFO_sync_width134_depth16_FWFT DMA_W_MWR_TLP(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_W_MWR_TLP_din),
		.wr_en(DMA_W_MWR_TLP_wr_en),
		.rd_en(DMA_W_MWR_TLP_rd_en),
		.dout(DMA_W_MWR_TLP_dout),
		.full(),
		.empty(DMA_W_MWR_TLP_empty),
		.prog_full(DMA_W_MWR_TLP_prog_full)
	);
	
endmodule
