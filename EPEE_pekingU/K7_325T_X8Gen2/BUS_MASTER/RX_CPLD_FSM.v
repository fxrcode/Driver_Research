`timescale 1ns / 1ps
/////////////////////////////////////
//
// RX_CPLD_FSM.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// This FSM (state machine) deals with CplD TLPs in DMA read (from host to board).
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define RX_CPLD_FSM_INIT 2'b01
`define RX_CPLD_FSM_DATA 2'b10

module RX_CPLD_FSM(
	input sys_clk,
	input sys_rst_n,
	input dma_r_rst,
	// from DMA_CPLD_channel_dispatcher
	input [133:0] DMA_r_CPLD_TLP_din,
	output DMA_r_CPLD_TLP_prog_full,
	input DMA_r_CPLD_TLP_wr_en,
	// to RX_sequencing
	output [139:0] RX_buf_dout, // {DW_valid[1:0], tag[7:0], data[63:0]}
	output RX_buf_empty,
	input RX_buf_rd_en
);

	reg [1:0] rx_cpld_state;
	reg [7:0] cur_cpld_tag;
	
	wire [133:0] DMA_r_CPLD_TLP_dout;
	reg DMA_r_CPLD_TLP_rd_en;
	wire DMA_r_CPLD_TLP_empty;
	
	reg [139:0] RX_buf_din;
	reg RX_buf_wr_en;
	wire RX_buf_prog_full;
	
	// for better timing of reset
	reg reset;
	always @(posedge sys_clk)begin
		reset <= ~sys_rst_n || dma_r_rst;
	end

	always @(posedge sys_clk)begin
		if(reset)begin
			rx_cpld_state <= `RX_CPLD_FSM_INIT;
			cur_cpld_tag <= 0;
			DMA_r_CPLD_TLP_rd_en <= 1'b0;
			RX_buf_din <= 0;
			RX_buf_wr_en <= 1'b0;
		end
		else begin
			if(RX_buf_prog_full == 1'b0 && DMA_r_CPLD_TLP_empty == 1'b0)begin
				DMA_r_CPLD_TLP_rd_en <= 1'b1;
			end
			else begin
				DMA_r_CPLD_TLP_rd_en <= 1'b0;
			end
			if(DMA_r_CPLD_TLP_rd_en == 1'b1 && DMA_r_CPLD_TLP_empty == 1'b0)begin
				case(rx_cpld_state)
					`RX_CPLD_FSM_INIT:begin
						if(DMA_r_CPLD_TLP_dout[129] == 1'b1)begin
							rx_cpld_state <= (DMA_r_CPLD_TLP_dout[128] == 1'b1)?`RX_CPLD_FSM_INIT:`RX_CPLD_FSM_DATA;
							cur_cpld_tag <= DMA_r_CPLD_TLP_dout[32+15:32+8];
							RX_buf_din <= {4'b0001, // data DW valid
												DMA_r_CPLD_TLP_dout[32+15:32+8], // tag
												DMA_r_CPLD_TLP_dout[127:0]
												};
							RX_buf_wr_en <= 1'b1;
						end
						else begin
							RX_buf_wr_en <= 1'b0;
						end
					end
					`RX_CPLD_FSM_DATA:begin
						RX_buf_din <= {DMA_r_CPLD_TLP_dout[133:130], // data DW valid
											cur_cpld_tag, // tag
											DMA_r_CPLD_TLP_dout[127:0]
											};
						RX_buf_wr_en <= 1'b1;
						rx_cpld_state <= ((DMA_r_CPLD_TLP_dout[128] == 1'b1) ? `RX_CPLD_FSM_INIT : `RX_CPLD_FSM_DATA);
					end
					default:begin
						rx_cpld_state <= `RX_CPLD_FSM_INIT;
						RX_buf_wr_en <= 1'b0;
					end
				endcase
			end
			else begin
				RX_buf_wr_en <= 1'b0;
			end
		end
	end

	FIFO_sync_width134_depth16_FWFT DMA_r_CPLD_TLP(
		.clk(sys_clk),
		.rst(reset),
		.din(DMA_r_CPLD_TLP_din),
		.wr_en(DMA_r_CPLD_TLP_wr_en),
		.rd_en(DMA_r_CPLD_TLP_rd_en),
		.dout(DMA_r_CPLD_TLP_dout),
		.full(),
		.empty(DMA_r_CPLD_TLP_empty),
		.prog_full(DMA_r_CPLD_TLP_prog_full)
	);
	
	FIFO_sync_width140_depth16_FWFT RX_buf(
		.clk(sys_clk),
		.rst(reset),
		.din(RX_buf_din),
		.wr_en(RX_buf_wr_en),
		.rd_en(RX_buf_rd_en),
		.dout(RX_buf_dout),
		.full(),
		.empty(RX_buf_empty),
		.prog_full(RX_buf_prog_full)
	);

endmodule
