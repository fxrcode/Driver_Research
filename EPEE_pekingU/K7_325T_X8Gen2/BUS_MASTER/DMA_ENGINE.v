`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_ENGINE.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// This is the wrapper of DMA_ENGINE. It controls all of the DMA operations, including
// DMA host2board / board2host engine and the DMA of DMA-descriptors.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_ENGINE(
	input sys_clk,
	input sys_rst_n,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	input CFG_EXT_TAG_EN,
	input [2:0]  CFG_MAX_PAYLOAD_SIZE,
	input [2:0]  CFG_MAX_RD_REQ_SIZE,
	input CFG_BUS_MSTR_EN,
	// from DS_TLP_dispatcher
	input  [133:0] DMA_CPLD_channel_buf_din,
	output DMA_CPLD_channel_buf_prog_full,
	input  DMA_CPLD_channel_buf_wr_en,
	// to US_TLP_COLLECTOR
	output [71:0] DMA_MRD_channel_buf_dout,
	input  DMA_MRD_channel_buf_rd_en,
	output DMA_MRD_channel_buf_empty,
	output [133:0] DMA_MWR_channel_buf_dout,
	input  DMA_MWR_channel_buf_rd_en,
	output DMA_MWR_channel_buf_empty,
	// to INT_GEN
	output IG_DMA_w_int_req,
	input  IG_DMA_w_int_req_ack,
	output IG_DMA_w_int_served,
	input  IG_DMA_w_int_served_ack,
	output IG_DMA_r_int_req,
	input  IG_DMA_r_int_req_ack,
	output IG_DMA_r_int_served,
	input  IG_DMA_r_int_served_ack,
	// connect with SYS_CSR
	/*DMA reset*/
	input  RST_HOST2BOARD,
	input  RST_BOARD2HOST,
	output RST_DMA_R_DONE,
	output RST_DMA_W_DONE,
	/* for DMA read/write descriptor buf address*/
	input  [31:0] dma_r_descriptor_addr_l,
	input  [31:0] dma_r_descriptor_addr_h,
	input  [31:0] dma_w_descriptor_addr_l,
	input  [31:0] dma_w_descriptor_addr_h,
	/* for DMA read/write control*/
	input  dma_r_start,
	input  dma_r_init_descriptor_64addr_en, // use 64bit address TLP or not
	input  [13:0] dma_r_descriptor_num,
	input  dma_w_start,
	input  dma_w_init_descriptor_64addr_en, // use 64bit address TLP or not
	input  [13:0] dma_w_descriptor_num,
	/* for hardware state*/
	output dma_r_start_state_o,
	output dma_r_done_state_o,
	output dma_r_error_state_o,
	output dma_r_int_req_state,
	output dma_r_int_served_state,
	output dma_w_start_state_o,
	output dma_w_done_state_o,
	output dma_w_error_state_o,
	output dma_w_int_req_state,
	output dma_w_int_served_state,
	/* for interrupt control*/
	input  int_enable,
	input  dma_r_int_served,
	input  dma_w_int_served,
	input  dma_r_int_disable, // DMA read interrupt will enable when int_enable = 1 and dma_r_int_disable = 0
	input  dma_w_int_disable, // DMA write interrupt will enable when int_enable = 1 and dma_w_int_disable = 0
	/* data count*/
	output [31:0] host2board_data_count,
	output [31:0] board2host_data_count_o,
	/* timeout threshold*/
	input  [63:0] dma_r_timeout_threshold,
	input  [63:0] dma_w_timeout_threshold,
	// from FIFO board2host
	input  [129:0] FIFO_board2host_dout, // {QW_Valid[1:0], data[127:0]}
	output FIFO_board2host_rd_en,
	input  FIFO_board2host_empty,
	// to FIFO_host2board
	output [129:0] FIFO_host2board_din, // {QW_Valid[1:0], data[127:0]}
	output FIFO_host2board_wr_en,
	input  FIFO_host2board_prog_full
);

	// connect dma_mwr_channel_collector and DMA_board2host_engine
	wire [133:0] DMA_W_MWR_TLP_dout;
	wire DMA_W_MWR_TLP_empty;
	wire DMA_W_MWR_TLP_rd_en;
	
	// connect dma_mrd_channel_collector and DMA_board2host_descriptor_engine
	wire [71:0] DMA_w_descriptor_MRD_TLP_dout;
	wire DMA_w_descriptor_MRD_TLP_empty;
	wire DMA_w_descriptor_MRD_TLP_rd_en;
	
	// connect dma_mrd_channel_collector and DMA_host2board_descriptor_engine
	wire [71:0] DMA_r_descriptor_MRD_TLP_dout;
	wire DMA_r_descriptor_MRD_TLP_empty;
	wire DMA_r_descriptor_MRD_TLP_rd_en;
	
	// connect dma_mrd_channel_collector and DMA_host2board_engine
	wire [71:0] DMA_r_MRD_TLP_dout;
	wire DMA_r_MRD_TLP_empty;
	wire DMA_r_MRD_TLP_rd_en;
	
	// connect dma_cpld_channel_dispatcher and DMA_board2host_descriptor_engine
	wire [71:0] DMA_w_descriptor_CPLD_TLP_din;
	wire DMA_w_descriptor_CPLD_TLP_prog_full;
	wire DMA_w_descriptor_CPLD_TLP_wr_en;
	
	// connect dma_cpld_channel_dispatcher and DMA_board2host_engine
	wire [71:0] DMA_w_zero_read_TLP_din;
	wire DMA_w_zero_read_TLP_prog_full;
	wire DMA_w_zero_read_TLP_wr_en;
	
	// connect dma_cpld_channel_dispatcher and DMA_host2board_descriptor_engine
	wire [71:0] DMA_r_descriptor_CPLD_TLP_din;
	wire DMA_r_descriptor_CPLD_TLP_prog_full;
	wire DMA_r_descriptor_CPLD_TLP_wr_en;
	
	// connect dma_cpld_channel_dispatcher and DMA_host2board_engine
	wire [133:0] DMA_r_CPLD_TLP_din;
	wire DMA_r_CPLD_TLP_prog_full;
	wire DMA_r_CPLD_TLP_wr_en;
	
	// connect DMA_board2host_descriptor_engine and DMA_board2host_engine
	wire [95:0] DMA_w_descriptor_buf_din;
	wire DMA_w_descriptor_buf_prog_full;
	wire DMA_w_descriptor_buf_wr_en;
	
	// connect DMA_host2board_descriptor_engine and DMA_host2board_engine
	wire [95:0] DMA_r_descriptor_buf_din;
	wire DMA_r_descriptor_buf_wr_en;
	wire DMA_r_descriptor_buf_prog_full;
	
	// connect DMA_board2host_engine and board2host_buf
	wire [63:0] board2host_buf_odd_dout;
	wire board2host_buf_odd_empty;
	wire board2host_buf_odd_rd_en;
	wire [63:0] board2host_buf_even_dout;
	wire board2host_buf_even_empty;
	wire board2host_buf_even_rd_en;
	wire [31:0] board2host_data_count;
	
	// connect DMA_host2board_engine and host2board_buf
	wire [131:0] host2board_buf_din;
	wire host2board_buf_wr_en;
	wire host2board_buf_prog_full;
	
	// connect DMA_board2host_INIT_CTRL and DMA_board2host_descriptor_engine
	wire dma_w_init_descriptor_mux_ctrl;
	wire [95:0] dma_w_init_descriptor_din;
	wire dma_w_init_descriptor_wr_en;
	wire dma_w_init_descriptor_prog_full;
	
	// connect DMA_board2host_INIT_CTRL and DMA_board2host_RST_CTRL
	wire dma_w_init_req;
	wire dma_w_init_done;
	
	// DMA_board2host_RST_CTRL's reset signals
	wire dma_board2host_rst;
	wire dma_board2host_discriptor_engine_rst_done;
	wire dma_board2host_engine_rst_done;
	wire dma_board2host_int_ctrl_rst_done;

	// connect DMA_host2board_INIT_CTRL and DMA_host2board_descriptor_engine
	wire dma_r_descriptor_mux_ctrl;
	wire [95:0] dma_r_init_descriptor_din;
	wire dma_r_init_descriptor_wr_en;
	wire dma_r_init_descriptor_prog_full;
	
	// connect DMA_host2board_INIT_CTRL and DMA_host2board_RST_CTRL
	wire dma_r_init_req;
	wire dma_r_init_done;
	
	// DMA_host2board_RST_CTRL's reset signals
	wire dma_host2board_rst;
	wire dma_host2board_discriptor_engine_rst_done;
	wire dma_host2board_engine_rst_done;
	wire dma_host2board_int_ctrl_rst_done;
	
	// connect DMA_board2host_engine, DMA_board2host_INT_CTRL, DMA_board2host_INIT_CTRL etc.
	wire dma_r_start_state;
	wire dma_r_done_state;
	wire dma_r_error_state;
	
	// connect DMA_host2board_engine, DMA_host2board_INT_CTRL, DMA_host2board_INIT_CTRL etc.
	wire dma_w_start_state;
	wire dma_w_done_state;
	wire dma_w_error_state;

	// assigns for output
	assign dma_r_start_state_o = dma_r_start_state;
	assign dma_r_done_state_o = dma_r_done_state;
	assign dma_r_error_state_o = dma_r_error_state;
	assign dma_w_start_state_o = dma_w_start_state;
	assign dma_w_done_state_o = dma_w_done_state;
	assign dma_w_error_state_o = dma_w_error_state;
	
	assign board2host_data_count_o = board2host_data_count;

	DMA_MWR_channel_collector dma_mwr_channel_collector(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		// from DMA_board2host_engine
		.DMA_W_MWR_TLP_dout(DMA_W_MWR_TLP_dout),
		.DMA_W_MWR_TLP_empty(DMA_W_MWR_TLP_empty),
		.DMA_W_MWR_TLP_rd_en(DMA_W_MWR_TLP_rd_en),
		// to US_TLP_collector
		.DMA_MWR_channel_buf_dout(DMA_MWR_channel_buf_dout),
		.DMA_MWR_channel_buf_rd_en(DMA_MWR_channel_buf_rd_en),
		.DMA_MWR_channel_buf_empty(DMA_MWR_channel_buf_empty)
	);
	
	DMA_MRD_channel_collector dma_mrd_channel_collector(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		// to US_TLP_COLLECTOR
		.DMA_MRD_channel_buf_dout(DMA_MRD_channel_buf_dout),
		.DMA_MRD_channel_buf_rd_en(DMA_MRD_channel_buf_rd_en),
		.DMA_MRD_channel_buf_empty(DMA_MRD_channel_buf_empty),
		// from DMA_board2host_descriptor_engine
		.DMA_w_descriptor_MRD_TLP_dout(DMA_w_descriptor_MRD_TLP_dout),
		.DMA_w_descriptor_MRD_TLP_empty(DMA_w_descriptor_MRD_TLP_empty),
		.DMA_w_descriptor_MRD_TLP_rd_en(DMA_w_descriptor_MRD_TLP_rd_en),
		// from DMA_host2board_descriptor engine
		.DMA_r_descriptor_MRD_TLP_dout(DMA_r_descriptor_MRD_TLP_dout),
		.DMA_r_descriptor_MRD_TLP_empty(DMA_r_descriptor_MRD_TLP_empty),
		.DMA_r_descriptor_MRD_TLP_rd_en(DMA_r_descriptor_MRD_TLP_rd_en),
		// from DMA_host2board_engine
		.DMA_r_MRD_TLP_dout(DMA_r_MRD_TLP_dout),
		.DMA_r_MRD_TLP_empty(DMA_r_MRD_TLP_empty),
		.DMA_r_MRD_TLP_rd_en(DMA_r_MRD_TLP_rd_en)
	);
	
	DMA_CPLD_channel_dispatcher DMA_CPLD_channel_dispatcher(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		// from DS_CTRL
		.DMA_CPLD_channel_buf_din(DMA_CPLD_channel_buf_din),
		.DMA_CPLD_channel_buf_prog_full(DMA_CPLD_channel_buf_prog_full),
		.DMA_CPLD_channel_buf_wr_en(DMA_CPLD_channel_buf_wr_en),
		// to DMA_board2host_descriptor_engine
		.DMA_w_descriptor_CPLD_TLP_din(DMA_w_descriptor_CPLD_TLP_din),
		.DMA_w_descriptor_CPLD_TLP_prog_full(DMA_w_descriptor_CPLD_TLP_prog_full),
		.DMA_w_descriptor_CPLD_TLP_wr_en(DMA_w_descriptor_CPLD_TLP_wr_en),
		// to DMA_board2host_engine
		.DMA_w_zero_read_TLP_din(DMA_w_zero_read_TLP_din),
		.DMA_w_zero_read_TLP_prog_full(DMA_w_zero_read_TLP_prog_full),
		.DMA_w_zero_read_TLP_wr_en(DMA_w_zero_read_TLP_wr_en),
		// to DMA_host2board_descriptor_engine
		.DMA_r_descriptor_CPLD_TLP_din(DMA_r_descriptor_CPLD_TLP_din),
		.DMA_r_descriptor_CPLD_TLP_prog_full(DMA_r_descriptor_CPLD_TLP_prog_full),
		.DMA_r_descriptor_CPLD_TLP_wr_en(DMA_r_descriptor_CPLD_TLP_wr_en),
		// DMA_host2board_engine
		.DMA_r_CPLD_TLP_din(DMA_r_CPLD_TLP_din),
		.DMA_r_CPLD_TLP_prog_full(DMA_r_CPLD_TLP_prog_full),
		.DMA_r_CPLD_TLP_wr_en(DMA_r_CPLD_TLP_wr_en)
	);
	
	DMA_board2host_INIT_CTRL DMA_board2host_INIT_CTRL(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.RST_BOARD2HOST(RST_BOARD2HOST), 
		// from SYS_CSR
		.dma_w_start(dma_w_start), 
		.dma_w_init_descriptor_64addr_en(dma_w_init_descriptor_64addr_en), 
		.dma_w_descriptor_addr_l(dma_w_descriptor_addr_l), 
		.dma_w_descriptor_addr_h(dma_w_descriptor_addr_h), 
		.dma_w_descriptor_num(dma_w_descriptor_num), 
		.dma_w_start_state(dma_w_start_state), // start state is 0 only when reset occurred
		// for DMA_board2host_descriptor_engine
		.dma_w_init_descriptor_mux_ctrl(dma_w_init_descriptor_mux_ctrl), 
		.dma_w_init_descriptor_din(dma_w_init_descriptor_din), 
		.dma_w_init_descriptor_wr_en(dma_w_init_descriptor_wr_en), 
		.dma_w_init_descriptor_prog_full(dma_w_init_descriptor_prog_full), 
		// to DMA_board2host_RST_CTRL
		.dma_w_init_req(dma_w_init_req), 
		.dma_w_init_done(dma_w_init_done)
	);
	
	DMA_board2host_INT_CTRL DMA_board2host_INT_CTRL(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n),
		// from DMA_board2host_RST_CTRL		
		.dma_board2host_rst(dma_board2host_rst), 
		.dma_board2host_rst_done(dma_board2host_int_ctrl_rst_done), 
		// from DMA_board2host_engine
		.dma_w_start_state(dma_w_start_state), 
		.dma_w_done_state(dma_w_done_state), 
		.dma_w_error_state(dma_w_error_state), 
		// to SYS_CSR
		.int_enable(int_enable & (!dma_w_int_disable)), 
		.dma_w_int_req_state(dma_w_int_req_state), 
		.dma_w_int_served(dma_w_int_served), 
		.dma_w_int_served_state(dma_w_int_served_state), 
		// to INT_GEN
		.IG_DMA_w_int_req(IG_DMA_w_int_req), 
		.IG_DMA_w_int_req_ack(IG_DMA_w_int_req_ack), 
		.IG_DMA_w_int_served(IG_DMA_w_int_served), 
		.IG_DMA_w_int_served_ack(IG_DMA_w_int_served_ack)
	);
	
	DMA_board2host_RST_CTRL DMA_board2host_RST_CTRL(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// from SYS_CSR
		.RST_BOARD2HOST(RST_BOARD2HOST), 
		.RST_DMA_W_DONE(RST_DMA_W_DONE), 
		// from DMA_board2host_INIT_CTRL
		.dma_w_init_req(dma_w_init_req), 
		.dma_w_init_done(dma_w_init_done), 
		// reset signals for other DMA write modules
		.dma_board2host_rst(dma_board2host_rst), 
		.dma_board2host_discriptor_engine_rst_done(dma_board2host_discriptor_engine_rst_done), 
		.dma_board2host_engine_rst_done(dma_board2host_engine_rst_done), 
		.dma_board2host_int_ctrl_rst_done(dma_board2host_int_ctrl_rst_done)
	);
	
	DMA_descriptor_engine #(
		.TAG_DEFAULT(8'd0)
	)DMA_board2host_descriptor_engine(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_channel_rst(dma_board2host_rst),
		.dma_channel_rst_done(dma_board2host_discriptor_engine_rst_done),
		// to DMA_MRD_channel_collector
		.DMA_descriptor_MRD_TLP_dout(DMA_w_descriptor_MRD_TLP_dout),
		.DMA_descriptor_MRD_TLP_empty(DMA_w_descriptor_MRD_TLP_empty),
		.DMA_descriptor_MRD_TLP_rd_en(DMA_w_descriptor_MRD_TLP_rd_en),
		// from DMA_CPLD_channel_dispatcher
		.DMA_descriptor_CPLD_TLP_din(DMA_w_descriptor_CPLD_TLP_din),
		.DMA_descriptor_CPLD_TLP_prog_full(DMA_w_descriptor_CPLD_TLP_prog_full),
		.DMA_descriptor_CPLD_TLP_wr_en(DMA_w_descriptor_CPLD_TLP_wr_en),
		// to DMA_engine's DMA_descriptor_buf
		.dma_descriptor_buf_din(DMA_w_descriptor_buf_din),
		.dma_descriptor_buf_prog_full(DMA_w_descriptor_buf_prog_full),
		.dma_descriptor_buf_wr_en(DMA_w_descriptor_buf_wr_en),
		// from DMA_INIT_CTRL
		.dma_init_descriptor_mux_ctrl(dma_w_init_descriptor_mux_ctrl),
		.dma_init_descriptor_din(dma_w_init_descriptor_din),
		.dma_init_descriptor_wr_en(dma_w_init_descriptor_wr_en),
		.dma_init_descriptor_prog_full(dma_w_init_descriptor_prog_full),
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID),
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN),
		.CFG_MAX_RD_REQ_SIZE(CFG_MAX_RD_REQ_SIZE),
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN)
	);
	
	DMA_board2host_engine #(
		.TAG_DEFAULT(8'd1)
	)dma_board2host_engine(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_board2host_rst(dma_board2host_rst), 
		.dma_board2host_rst_done(dma_board2host_engine_rst_done), 
		.RST_BOARD2HOST(RST_BOARD2HOST),
		// to DMA_board2host_INT_CTRL & SYS_CSR
		.DMA_w_timeout_threshold(dma_w_timeout_threshold), 
		.DMA_w_start_state(dma_w_start_state), 
		.DMA_w_done_state(dma_w_done_state), 
		.DMA_w_error_state(dma_w_error_state), 
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID), 
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN), 
		.CFG_MAX_PAYLOAD_SIZE(CFG_MAX_PAYLOAD_SIZE), 
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN), 
		// from board2host_buf
		.board2host_buf_even_dout(board2host_buf_even_dout), 
		.board2host_buf_even_empty(board2host_buf_even_empty), 
		.board2host_buf_even_rd_en(board2host_buf_even_rd_en),
		.board2host_buf_odd_dout(board2host_buf_odd_dout), 
		.board2host_buf_odd_empty(board2host_buf_odd_empty), 
		.board2host_buf_odd_rd_en(board2host_buf_odd_rd_en),
		.board2host_data_count(board2host_data_count),
		// from DMA_board2host_descriptor_engine
		.DMA_w_descriptor_buf_din(DMA_w_descriptor_buf_din), 
		.DMA_w_descriptor_buf_prog_full(DMA_w_descriptor_buf_prog_full), 
		.DMA_w_descriptor_buf_wr_en(DMA_w_descriptor_buf_wr_en), 
		// to DMA_W_MWR_TLP FIFO
		.DMA_W_MWR_TLP_dout(DMA_W_MWR_TLP_dout), 
		.DMA_W_MWR_TLP_rd_en(DMA_W_MWR_TLP_rd_en), 
		.DMA_W_MWR_TLP_empty(DMA_W_MWR_TLP_empty), 
		// from DMA_w_zero_read_TLP_buf
		.DMA_w_zero_read_TLP_din(DMA_w_zero_read_TLP_din), 
		.DMA_w_zero_read_TLP_wr_en(DMA_w_zero_read_TLP_wr_en), 
		.DMA_w_zero_read_TLP_prog_full(DMA_w_zero_read_TLP_prog_full)
	);
	
	board2host_buf board2host_buf(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.RST_BOARD2HOST(RST_BOARD2HOST),
		// from FIFO board2host
		.FIFO_board2host_dout(FIFO_board2host_dout),
		.FIFO_board2host_rd_en(FIFO_board2host_rd_en),
		.FIFO_board2host_empty(FIFO_board2host_empty),
		// to DMA_board2host_engine
		.board2host_buf_even_dout(board2host_buf_even_dout),
		.board2host_buf_even_rd_en(board2host_buf_even_rd_en),
		.board2host_buf_even_empty(board2host_buf_even_empty),
		.board2host_buf_odd_dout(board2host_buf_odd_dout),
		.board2host_buf_odd_rd_en(board2host_buf_odd_rd_en),
		.board2host_buf_odd_empty(board2host_buf_odd_empty),
		// to SYS_CSR
		.board2host_data_count(board2host_data_count)
	);
	
	DMA_host2board_INIT_CTRL DMA_host2board_INIT_CTRL(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.RST_HOST2BOARD(RST_HOST2BOARD), 
		// from SYS_CSR
		.dma_r_start(dma_r_start), 
		.dma_r_init_descriptor_64addr_en(dma_r_init_descriptor_64addr_en), 
		.dma_r_descriptor_addr_l(dma_r_descriptor_addr_l), 
		.dma_r_descriptor_addr_h(dma_r_descriptor_addr_h), 
		.dma_r_descriptor_num(dma_r_descriptor_num), 
		.dma_r_start_state(dma_r_start_state), // this is 0 only when reset occur
		// from DMA_board2host_descriptor_engine
		.dma_r_descriptor_mux_ctrl(dma_r_descriptor_mux_ctrl), 
		.dma_r_init_descriptor_din(dma_r_init_descriptor_din), 
		.dma_r_init_descriptor_wr_en(dma_r_init_descriptor_wr_en), 
		.dma_r_init_descriptor_prog_full(dma_r_init_descriptor_prog_full), 
		// to DMA_host2board_RST_CTRL
		.dma_r_init_req(dma_r_init_req), 
		.dma_r_init_done(dma_r_init_done)
	);
	
	DMA_host2board_INT_CTRL DMA_host2board_INT_CTRL(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// from DMA_host2board_RST_CTRL
		.dma_host2board_rst(dma_host2board_rst), 
		.dma_host2board_rst_done(dma_host2board_int_ctrl_rst_done), 
		// from DMA_host2board_engine
		.dma_r_start_state(dma_r_start_state), 
		.dma_r_done_state(dma_r_done_state), 
		.dma_r_error_state(dma_r_error_state), 
		// to SYS_CSR
		.int_enable(int_enable & (~dma_r_int_disable)), 
		.dma_r_int_req_state(dma_r_int_req_state), 
		.dma_r_int_served(dma_r_int_served), 
		.dma_r_int_served_state(dma_r_int_served_state), 
		// to INT_GEN
		.IG_DMA_r_int_req(IG_DMA_r_int_req), 
		.IG_DMA_r_int_req_ack(IG_DMA_r_int_req_ack), 
		.IG_DMA_r_int_served(IG_DMA_r_int_served), 
		.IG_DMA_r_int_served_ack(IG_DMA_r_int_served_ack)
	);
	
	DMA_host2board_RST_CTRL DMA_host2board_RST_CTRL(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// from SYS_CSR
		.RST_HOST2BOARD(RST_HOST2BOARD), 
		.RST_DMA_R_DONE(RST_DMA_R_DONE), 
		// from DMA_host2board_INIT_CTRL
		.dma_r_init_req(dma_r_init_req), 
		.dma_r_init_done(dma_r_init_done), 
		// reset signals for other DMA read modules
		.dma_host2board_rst(dma_host2board_rst), 
		.dma_host2board_discriptor_engine_rst_done(dma_host2board_discriptor_engine_rst_done), 
		.dma_host2board_engine_rst_done(dma_host2board_engine_rst_done), 
		.dma_host2board_int_ctrl_rst_done(dma_host2board_int_ctrl_rst_done)
	);
	
	DMA_descriptor_engine #(
		.TAG_DEFAULT(8'd2)
	)DMA_host2board_descriptor_engine(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_channel_rst(dma_host2board_rst),
		.dma_channel_rst_done(dma_host2board_discriptor_engine_rst_done),
		// to DMA_MRD_channel_collector
		.DMA_descriptor_MRD_TLP_dout(DMA_r_descriptor_MRD_TLP_dout),
		.DMA_descriptor_MRD_TLP_empty(DMA_r_descriptor_MRD_TLP_empty),
		.DMA_descriptor_MRD_TLP_rd_en(DMA_r_descriptor_MRD_TLP_rd_en),
		// from DMA_CPLD_channel_dispatcher
		.DMA_descriptor_CPLD_TLP_din(DMA_r_descriptor_CPLD_TLP_din),
		.DMA_descriptor_CPLD_TLP_prog_full(DMA_r_descriptor_CPLD_TLP_prog_full),
		.DMA_descriptor_CPLD_TLP_wr_en(DMA_r_descriptor_CPLD_TLP_wr_en),
		// to DMA_engine's DMA_descriptor_buf
		.dma_descriptor_buf_din(DMA_r_descriptor_buf_din),
		.dma_descriptor_buf_prog_full(DMA_r_descriptor_buf_prog_full),
		.dma_descriptor_buf_wr_en(DMA_r_descriptor_buf_wr_en),
		// from DMA_INIT_CTRL
		.dma_init_descriptor_mux_ctrl(dma_r_descriptor_mux_ctrl),
		.dma_init_descriptor_din(dma_r_init_descriptor_din),
		.dma_init_descriptor_wr_en(dma_r_init_descriptor_wr_en),
		.dma_init_descriptor_prog_full(dma_r_init_descriptor_prog_full),
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID),
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN),
		.CFG_MAX_RD_REQ_SIZE(CFG_MAX_RD_REQ_SIZE),
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN)
	);
	
	DMA_host2board_engine DMA_host2board_engine(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_host2board_rst(dma_host2board_rst), 
		.dma_host2board_rst_done(dma_host2board_engine_rst_done), 
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID), 
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN), 
		.CFG_MAX_RD_REQ_SIZE(CFG_MAX_RD_REQ_SIZE), 
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN), 
		// from DMA_host2board_descriptor_engine
		.DMA_r_descriptor_buf_din(DMA_r_descriptor_buf_din), 
		.DMA_r_descriptor_buf_wr_en(DMA_r_descriptor_buf_wr_en), 
		.DMA_r_descriptor_buf_prog_full(DMA_r_descriptor_buf_prog_full), 
		// to DMA_MRD_channel_collector
		.DMA_r_MRD_TLP_dout(DMA_r_MRD_TLP_dout), 
		.DMA_r_MRD_TLP_empty(DMA_r_MRD_TLP_empty), 
		.DMA_r_MRD_TLP_rd_en(DMA_r_MRD_TLP_rd_en), 
		// from DMA_CPLD_channel_dispatcher
		.DMA_r_CPLD_TLP_din(DMA_r_CPLD_TLP_din), 
		.DMA_r_CPLD_TLP_wr_en(DMA_r_CPLD_TLP_wr_en), 
		.DMA_r_CPLD_TLP_prog_full(DMA_r_CPLD_TLP_prog_full), 
		// to host2board_buf
		.host2board_buf_din(host2board_buf_din), 
		.host2board_buf_wr_en(host2board_buf_wr_en), 
		.host2board_buf_prog_full(host2board_buf_prog_full), 
		// connect with SYS_CSR
		.DMA_r_timeout_threshold(dma_r_timeout_threshold), 
		.DMA_r_start_state(dma_r_start_state), 
		.DMA_r_done_state(dma_r_done_state), 
		.DMA_r_error_state(dma_r_error_state)
	);
		
	host2board_buf host2board_buf(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.RST_HOST2BOARD(RST_HOST2BOARD), 
		// from DMA_host2board_engine
		.host2board_buf_din(host2board_buf_din), 
		.host2board_buf_wr_en(host2board_buf_wr_en), 
		.host2board_buf_prog_full(host2board_buf_prog_full), 
		// to FIFO_host2board
		.FIFO_host2board_din(FIFO_host2board_din), 
		.FIFO_host2board_wr_en(FIFO_host2board_wr_en), 
		.FIFO_host2board_prog_full(FIFO_host2board_prog_full), 
		// to SYS_CSR
		.host2board_data_count(host2board_data_count)
	);
	
endmodule
