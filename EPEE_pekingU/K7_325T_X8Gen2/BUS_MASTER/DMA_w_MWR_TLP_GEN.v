`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_w_MWR_TLP_GEN.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description: Generate the memory write TLP using MWR tlp head and data from 
//              board2host FIFO
//              MWR request's payload should be in multiples of QW
//              Only 64bit address MWR is supported, 32bit address MWR head will be
//              converted into 64bit address automatically
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

// timeout threshold is about 1 seconds when using 250MHz clk
`define DMA_W_TLP_GEN_TIMEOUT_THRESHOLD 64'd250000000

`define DMA_W_TLP_GEN_INIT       3'b001
`define DMA_W_TLP_GEN_MRD_WAIT   3'b010
`define DMA_W_TLP_GEN_MWR_DATA   3'b100

module DMA_w_MWR_TLP_GEN(
	input sys_clk,
	input sys_rst_n,
	input RST_BOARD2HOST,
	output reg rst_board2host_done,
	// from TLP_head_gen
	input [127:0] mwr_head_buf_dout, // MWR and MRD(zero-read) TLP head
	input mwr_head_buf_empty,
	output reg mwr_head_buf_rd_en,
	// from board2host_buf
	input [63:0] board2host_buf_odd_dout,
	input board2host_buf_odd_empty,
	output reg board2host_buf_odd_rd_en,
	input [63:0] board2host_buf_even_dout,
	input board2host_buf_even_empty,
	output reg board2host_buf_even_rd_en,
	input [31:0] board2host_data_count, // data count is in DW
	// to DMA_w_MWR_TLP_committer, FIFOs are in DMA_w_MWR_TLP_committer for easier control when reset
	output reg [133:0] mwr_tlp_buf_din,
	output reg mwr_tlp_buf_wr_en,
	input mwr_tlp_buf_prog_full
);

	(*KEEP="TRUE"*) reg [2:0] dma_w_tlp_gen_state;
	reg [9:0] tlp_len_left; // how many payload DWs haven't been sent (for MWR tlp)
	reg       read_from_odd_first; // if the next DW should be read from board2host_odd, assert this
	reg [31:0] board2host_data_sent_count; // record how many DWs has been packed into TLP

	// for timeout logic
	reg [2:0]  dma_w_tlp_gen_state_dly;
	reg [63:0] timeout_counter;
	reg        timeout_flag;
	
	// timeout logic, for recovering from deadlock
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			dma_w_tlp_gen_state_dly <= `DMA_W_TLP_GEN_INIT;
			timeout_counter <= 64'd0;
			timeout_flag <= 1'b0;
		end else begin
			dma_w_tlp_gen_state_dly <= dma_w_tlp_gen_state;
			if(dma_w_tlp_gen_state_dly == dma_w_tlp_gen_state && dma_w_tlp_gen_state_dly != `DMA_W_TLP_GEN_INIT)begin
				if(timeout_counter > `DMA_W_TLP_GEN_TIMEOUT_THRESHOLD)begin
					timeout_flag <= 1'b1;
				end else begin
					timeout_flag <= 1'b0;
					timeout_counter <= timeout_counter + 1;
				end
			end else begin
				timeout_flag <= 1'b0;
				timeout_counter <= 0;
			end
		end
	end
	
	// for better timing of reset
	reg reset;
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			rst_board2host_done <= 1'b0;
			reset <= 1'b1;
		end
		else if((RST_BOARD2HOST == 1'b1 && dma_w_tlp_gen_state == `DMA_W_TLP_GEN_INIT) || 
			(RST_BOARD2HOST == 1'b1 && timeout_flag == 1'b1))begin
			rst_board2host_done <= 1'b1;
			reset <= 1'b1;
		end
		else if(timeout_flag == 1'b1)begin
			rst_board2host_done <= 1'b0;
			reset <= 1'b1;
		end
		else begin
			rst_board2host_done <= 1'b0;
			reset <= 1'b0;
		end
	end
	
	// for easier control of the board2host_buf FIFO
	reg [3:0] board2host_odd_getter_valid;
	reg [3:0] board2host_odd_getter_clear;
	reg [4*64-1:0] board2host_odd_getter_data;
	reg [1:0] board2host_odd_getter_wr_pointer;
	reg [1:0] board2host_odd_getter_rd_pointer;
	wire [3:0] board2host_odd_getter_flag;
	wire [63:0] board2host_odd_getter_rd_data;
	assign board2host_odd_getter_flag = board2host_odd_getter_valid ^ board2host_odd_getter_clear;
	assign board2host_odd_getter_rd_data = (board2host_odd_getter_rd_pointer == 2'b00)?board2host_odd_getter_data[4*64-1:3*64]:
														(board2host_odd_getter_rd_pointer == 2'b01)?board2host_odd_getter_data[3*64-1:2*64]:
														(board2host_odd_getter_rd_pointer == 2'b10)?board2host_odd_getter_data[2*64-1:1*64]:
																											board2host_odd_getter_data[1*64-1:0];
	
	reg [3:0] board2host_even_getter_valid;
	reg [3:0] board2host_even_getter_clear;
	reg [4*64-1:0] board2host_even_getter_data;
	reg [1:0] board2host_even_getter_wr_pointer;
	reg [1:0] board2host_even_getter_rd_pointer;
	wire [3:0] board2host_even_getter_flag;
	wire [63:0] board2host_even_getter_rd_data;
	assign board2host_even_getter_flag = board2host_even_getter_valid ^ board2host_even_getter_clear;
	assign board2host_even_getter_rd_data = (board2host_even_getter_rd_pointer == 2'b00)?board2host_even_getter_data[4*64-1:3*64]:
														 (board2host_even_getter_rd_pointer == 2'b01)?board2host_even_getter_data[3*64-1:2*64]:
														 (board2host_even_getter_rd_pointer == 2'b10)?board2host_even_getter_data[2*64-1:1*64]:
																											board2host_even_getter_data[1*64-1:0];
	
	// for board2host_odd_getter
	always @(posedge sys_clk)begin
		if(reset)begin
			board2host_odd_getter_valid <= 0;
			board2host_odd_getter_data <= 0;
			board2host_odd_getter_wr_pointer <= 0;
			board2host_buf_odd_rd_en <= 1'b0;
		end
		else begin
			if(board2host_buf_odd_empty == 1'b0 && board2host_odd_getter_flag[board2host_odd_getter_wr_pointer] == 1'b0 && 
			board2host_odd_getter_flag[(board2host_odd_getter_wr_pointer == 2'b11)?2'd0:(board2host_odd_getter_wr_pointer+1)] == 1'b0)begin
				board2host_buf_odd_rd_en <= 1'b1;
			end
			else begin
				board2host_buf_odd_rd_en <= 1'b0;
			end
			if(board2host_buf_odd_empty == 1'b0 && board2host_buf_odd_rd_en == 1'b1)begin
				board2host_odd_getter_wr_pointer <= board2host_odd_getter_wr_pointer + 1'b1;
				board2host_odd_getter_valid[board2host_odd_getter_wr_pointer] <= ~board2host_odd_getter_clear[board2host_odd_getter_wr_pointer];
				case(board2host_odd_getter_wr_pointer)
					2'b00:board2host_odd_getter_data[4*64-1:3*64] <= board2host_buf_odd_dout;
					2'b01:board2host_odd_getter_data[3*64-1:2*64] <= board2host_buf_odd_dout;
					2'b10:board2host_odd_getter_data[2*64-1:1*64] <= board2host_buf_odd_dout;
					2'b11:board2host_odd_getter_data[1*64-1:0] <= board2host_buf_odd_dout;
				endcase
			end
			else begin
				// do nothing
			end
		end
	end
	
	// for board2host_even_getter
	always @(posedge sys_clk)begin
		if(reset)begin
			board2host_even_getter_valid <= 0;
			board2host_even_getter_data <= 0;
			board2host_even_getter_wr_pointer <= 0;
			board2host_buf_even_rd_en <= 1'b0;
		end
		else begin
			if(board2host_buf_even_empty == 1'b0 && board2host_even_getter_flag[board2host_even_getter_wr_pointer] == 1'b0 && 
			board2host_even_getter_flag[(board2host_even_getter_wr_pointer == 2'b11)?2'd0:(board2host_even_getter_wr_pointer+1)] == 1'b0)begin
				board2host_buf_even_rd_en <= 1'b1;
			end
			else begin
				board2host_buf_even_rd_en <= 1'b0;
			end
			if(board2host_buf_even_empty == 1'b0 && board2host_buf_even_rd_en == 1'b1)begin
				board2host_even_getter_wr_pointer <= board2host_even_getter_wr_pointer + 1'b1;
				board2host_even_getter_valid[board2host_even_getter_wr_pointer] <= ~board2host_even_getter_clear[board2host_even_getter_wr_pointer];
				case(board2host_even_getter_wr_pointer)
					2'b00:board2host_even_getter_data[4*64-1:3*64] <= board2host_buf_even_dout;
					2'b01:board2host_even_getter_data[3*64-1:2*64] <= board2host_buf_even_dout;
					2'b10:board2host_even_getter_data[2*64-1:1*64] <= board2host_buf_even_dout;
					2'b11:board2host_even_getter_data[1*64-1:0] <= board2host_buf_even_dout;
				endcase
			end
			else begin
				// do nothing
			end
		end
	end
	
	// main logic
	always @(posedge sys_clk)begin
		if(reset)begin
			mwr_head_buf_rd_en <= 0;
			mwr_tlp_buf_din <= 0;
			mwr_tlp_buf_wr_en <= 0;
			dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
			tlp_len_left <= 0;
			read_from_odd_first <= 1'b1; // when reset, we will read odd first
			board2host_odd_getter_clear <= 0;
			board2host_odd_getter_rd_pointer <= 0;
			board2host_even_getter_clear <= 0;
			board2host_even_getter_rd_pointer <= 0;
			board2host_data_sent_count <= 0;
		end
		else begin
			case(dma_w_tlp_gen_state)
				`DMA_W_TLP_GEN_INIT:begin
					if(mwr_head_buf_empty == 1'b0 && mwr_tlp_buf_prog_full == 1'b0)begin
						tlp_len_left <= mwr_head_buf_dout[105:96]; // when the TLP is MRD, this is of no use
						case(mwr_head_buf_dout[127:120])
							`FT_MWR64:begin
								if((board2host_data_sent_count <=  board2host_data_count && 
									{1'b0, board2host_data_sent_count} + mwr_head_buf_dout[105:96] <= board2host_data_count) ||
									(board2host_data_sent_count >  board2host_data_count && 
									{1'b0, board2host_data_sent_count} + mwr_head_buf_dout[105:96] <= {1'b1, board2host_data_count}))begin
									// start only when there is enough data
									dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_MWR_DATA;
									mwr_head_buf_rd_en <= 1'b1;
									mwr_tlp_buf_din <= {4'b1111, 2'b10, mwr_head_buf_dout};
									mwr_tlp_buf_wr_en <= 1'b1;
								end
								else begin
									dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
									mwr_head_buf_rd_en <= 1'b0;
									mwr_tlp_buf_wr_en <= 1'b0;
								end
							end
							`FT_MWR32:begin
								if((board2host_data_sent_count <=  board2host_data_count && 
									{1'b0, board2host_data_sent_count} + mwr_head_buf_dout[105:96] <= board2host_data_count) ||
									(board2host_data_sent_count >  board2host_data_count && 
									{1'b0, board2host_data_sent_count} + mwr_head_buf_dout[105:96] <= {1'b1, board2host_data_count}))begin
									// start only when there is enough data
									dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_MWR_DATA;
									mwr_head_buf_rd_en <= 1'b1;
									// transform 32bit address MWR TLP into 64bit address TLP
									mwr_tlp_buf_din <= {4'b1111, 2'b10, 
																`FT_MWR64, mwr_head_buf_dout[119:64], 32'd0, mwr_head_buf_dout[63:32]};
									mwr_tlp_buf_wr_en <= 1'b1;
								end
								else begin
									dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
									mwr_head_buf_rd_en <= 1'b0;
									mwr_tlp_buf_wr_en <= 1'b0;
								end
							end
							`FT_MRD64:begin
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_MRD_WAIT;
								mwr_head_buf_rd_en <= 1'b1;
								mwr_tlp_buf_din <= {4'b1111, 2'b11, mwr_head_buf_dout};
								mwr_tlp_buf_wr_en <= 1'b1;
							end
							`FT_MRD32:begin
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_MRD_WAIT;
								mwr_head_buf_rd_en <= 1'b1;
								mwr_tlp_buf_din <= {4'b1110, 2'b11, mwr_head_buf_dout};
								mwr_tlp_buf_wr_en <= 1'b1;
							end
							default:begin
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
								mwr_head_buf_rd_en <= 1'b1;
								mwr_tlp_buf_wr_en <= 1'b0;
							end
						endcase
					end
					else begin
						mwr_head_buf_rd_en <= 1'b0;
						mwr_tlp_buf_wr_en <= 1'b0;
						dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
					end
				end
				`DMA_W_TLP_GEN_MRD_WAIT:begin
					dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
					mwr_head_buf_rd_en <= 1'b0;
					mwr_tlp_buf_wr_en <= 1'b0;
				end
				`DMA_W_TLP_GEN_MWR_DATA:begin
					mwr_head_buf_rd_en <= 1'b0;
					if(read_from_odd_first == 1'b1)begin // get data from board2host_odd first
						if(tlp_len_left == 1 || tlp_len_left == 2)begin // last QW left, current tlp will end
							if(board2host_odd_getter_flag[board2host_odd_getter_rd_pointer] == 1'b1 && mwr_tlp_buf_prog_full == 1'b0)begin
								read_from_odd_first <= 1'b0;
								board2host_data_sent_count <= board2host_data_sent_count + 3'd2;
								tlp_len_left <= tlp_len_left - 3'd2;
								mwr_tlp_buf_wr_en <= 1'b1;
								board2host_odd_getter_clear[board2host_odd_getter_rd_pointer] <= board2host_odd_getter_valid[board2host_odd_getter_rd_pointer];
								board2host_odd_getter_rd_pointer <= board2host_odd_getter_rd_pointer + 2'd1;
								mwr_tlp_buf_din <= {4'b1100, 2'b01, board2host_odd_getter_rd_data, 64'd0};
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
							end
							else begin
								mwr_tlp_buf_wr_en <= 1'b0;
							end
						end
						else if(tlp_len_left == 3 || tlp_len_left == 4)begin // last 2 QW left, current tlp will end
							if(board2host_odd_getter_flag[board2host_odd_getter_rd_pointer] == 1'b1 && 
								board2host_even_getter_flag[board2host_even_getter_rd_pointer] == 1'b1 && mwr_tlp_buf_prog_full == 1'b0)begin
								board2host_data_sent_count <= board2host_data_sent_count + 3'd4;
								tlp_len_left <= tlp_len_left - 3'd4;
								mwr_tlp_buf_wr_en <= 1'b1;
								board2host_odd_getter_clear[board2host_odd_getter_rd_pointer] <= board2host_odd_getter_valid[board2host_odd_getter_rd_pointer];
								board2host_odd_getter_rd_pointer <= board2host_odd_getter_rd_pointer + 2'd1;
								board2host_even_getter_clear[board2host_even_getter_rd_pointer] <= board2host_even_getter_valid[board2host_even_getter_rd_pointer];
								board2host_even_getter_rd_pointer <= board2host_even_getter_rd_pointer + 2'd1;
								mwr_tlp_buf_din <= {4'b1111, 2'b01, board2host_odd_getter_rd_data, board2host_even_getter_rd_data};
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
							end
							else begin
								mwr_tlp_buf_wr_en <= 1'b0;
							end
						end
						else begin // the current tlp is not end
							if(board2host_odd_getter_flag[board2host_odd_getter_rd_pointer] == 1'b1 && 
								board2host_even_getter_flag[board2host_even_getter_rd_pointer] == 1'b1 && mwr_tlp_buf_prog_full == 1'b0)begin
								board2host_data_sent_count <= board2host_data_sent_count + 3'd4;
								tlp_len_left <= tlp_len_left - 3'd4;
								mwr_tlp_buf_wr_en <= 1'b1;
								board2host_odd_getter_clear[board2host_odd_getter_rd_pointer] <= board2host_odd_getter_valid[board2host_odd_getter_rd_pointer];
								board2host_odd_getter_rd_pointer <= board2host_odd_getter_rd_pointer + 2'd1;
								board2host_even_getter_clear[board2host_even_getter_rd_pointer] <= board2host_even_getter_valid[board2host_even_getter_rd_pointer];
								board2host_even_getter_rd_pointer <= board2host_even_getter_rd_pointer + 2'd1;
								mwr_tlp_buf_din <= {4'b1111, 2'b00, 
															board2host_odd_getter_rd_data,
															board2host_even_getter_rd_data};
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_MWR_DATA;
							end
							else begin
								mwr_tlp_buf_wr_en <= 1'b0;
							end
						end
					end
					else begin // get data from board2host_even first
						if(tlp_len_left == 1 || tlp_len_left == 2)begin // last qW left, current tlp will end
							if(board2host_even_getter_flag[board2host_even_getter_rd_pointer] == 1'b1 && mwr_tlp_buf_prog_full == 1'b0)begin
								read_from_odd_first <= 1'b1;
								board2host_data_sent_count <= board2host_data_sent_count + 3'd2;
								tlp_len_left <= tlp_len_left - 3'd2;
								mwr_tlp_buf_wr_en <= 1'b1;
								board2host_even_getter_clear[board2host_even_getter_rd_pointer] <= board2host_even_getter_valid[board2host_even_getter_rd_pointer];
								board2host_even_getter_rd_pointer <= board2host_even_getter_rd_pointer + 2'd1;
								mwr_tlp_buf_din <= {4'b1100, 2'b01, 
															board2host_even_getter_rd_data, 64'd0};
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
							end
							else begin
								mwr_tlp_buf_wr_en <= 1'b0;
							end
						end
						else if(tlp_len_left == 3 || tlp_len_left == 4)begin // last 2 QW left, current tlp will end
							if(board2host_even_getter_flag[board2host_even_getter_rd_pointer] == 1'b1 && 
								board2host_odd_getter_flag[board2host_odd_getter_rd_pointer] == 1'b1 && mwr_tlp_buf_prog_full == 1'b0)begin
								board2host_data_sent_count <= board2host_data_sent_count + 3'd4;
								tlp_len_left <= tlp_len_left - 3'd4;
								mwr_tlp_buf_wr_en <= 1'b1;
								board2host_even_getter_clear[board2host_even_getter_rd_pointer] <= board2host_even_getter_valid[board2host_even_getter_rd_pointer];
								board2host_even_getter_rd_pointer <= board2host_even_getter_rd_pointer + 2'd1;
								board2host_odd_getter_clear[board2host_odd_getter_rd_pointer] <= board2host_odd_getter_valid[board2host_odd_getter_rd_pointer];
								board2host_odd_getter_rd_pointer <= board2host_odd_getter_rd_pointer + 2'd1;
								mwr_tlp_buf_din <= {4'b1111, 2'b01, 
															board2host_even_getter_rd_data,
															board2host_odd_getter_rd_data};
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
							end
							else begin
								mwr_tlp_buf_wr_en <= 1'b0;
							end
						end
						else begin // the current tlp is not end
							if(board2host_even_getter_flag[board2host_even_getter_rd_pointer] == 1'b1 && 
								board2host_odd_getter_flag[board2host_odd_getter_rd_pointer] == 1'b1 && mwr_tlp_buf_prog_full == 1'b0)begin
								board2host_data_sent_count <= board2host_data_sent_count + 3'd4;
								tlp_len_left <= tlp_len_left - 3'd4;
								mwr_tlp_buf_wr_en <= 1'b1;
								board2host_even_getter_clear[board2host_even_getter_rd_pointer] <= board2host_even_getter_valid[board2host_even_getter_rd_pointer];
								board2host_even_getter_rd_pointer <= board2host_even_getter_rd_pointer + 2'd1;
								board2host_odd_getter_clear[board2host_odd_getter_rd_pointer] <= board2host_odd_getter_valid[board2host_odd_getter_rd_pointer];
								board2host_odd_getter_rd_pointer <= board2host_odd_getter_rd_pointer + 2'd1;
								mwr_tlp_buf_din <= {4'b1111, 2'b00, 
															board2host_even_getter_rd_data,
															board2host_odd_getter_rd_data};
								dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_MWR_DATA;
							end
							else begin
								mwr_tlp_buf_wr_en <= 1'b0;
							end
						end
					end
				end
				default:begin
					dma_w_tlp_gen_state <= `DMA_W_TLP_GEN_INIT;
				end
			endcase
		end
	end

endmodule
