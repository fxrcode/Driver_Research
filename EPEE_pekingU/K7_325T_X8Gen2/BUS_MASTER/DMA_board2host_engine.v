`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_board2host_engine.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// This is the DMA engine of the board to host direction. It generates MWr TLP with data.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_board2host_engine #
(
	parameter TAG_DEFAULT = 8'd1
)
(
	input sys_clk,
	input sys_rst_n,
	input dma_board2host_rst,
	output reg dma_board2host_rst_done,
	input RST_BOARD2HOST,
	// to DMA_board2host_INT_CTRL & SYS_CSR
	input [63:0] DMA_w_timeout_threshold,
	input DMA_w_start_state,
	output DMA_w_done_state,
	output DMA_w_error_state,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	input CFG_EXT_TAG_EN,
	input [2:0] CFG_MAX_PAYLOAD_SIZE,
	input CFG_BUS_MSTR_EN,
	// from board2host_buf
	input [63:0] board2host_buf_odd_dout,
	input board2host_buf_odd_empty,
	output board2host_buf_odd_rd_en,
	input [63:0] board2host_buf_even_dout,
	input board2host_buf_even_empty,
	output board2host_buf_even_rd_en,
	input [31:0] board2host_data_count,
	// from DMA_board2host_descriptor_engine
	input [95:0] DMA_w_descriptor_buf_din,
	output DMA_w_descriptor_buf_prog_full,
	input DMA_w_descriptor_buf_wr_en,
	// to DMA_W_MWR_TLP FIFO
	output [133:0] DMA_W_MWR_TLP_dout,
	input DMA_W_MWR_TLP_rd_en,
	output DMA_W_MWR_TLP_empty,
	// from DMA_w_zero_read_TLP_buf
	input [71:0] DMA_w_zero_read_TLP_din,
	input DMA_w_zero_read_TLP_wr_en,
	output DMA_w_zero_read_TLP_prog_full
);

	// connect DMA_w_descriptor_buf and TLP_head_gen
	wire [95:0] DMA_w_descriptor_buf_dout;
	wire DMA_w_descriptor_buf_empty;
	wire DMA_w_descriptor_buf_rd_en;
	
	// connect TLP_head_gen and DMA_w_MWR_TLP_GEN
	wire [127:0] mwr_head_buf_dout;
	wire mwr_head_buf_empty;
	wire mwr_head_buf_rd_en;
	
	// connect DMA_w_MWR_TLP_GEN and DMA_w_MWR_TLP_committer
	wire [133:0] mwr_tlp_buf_din;
	wire mwr_tlp_buf_wr_en;
	wire mwr_tlp_buf_prog_full;
	
	// connect DMA_w_MWR_TLP_committer and DMA_w_zero_read_TLP_waiter
	wire DMA_w_zero_read_TLP_sent;
	wire DMA_w_zero_read_TLP_acked;
	
	// reset done
	wire dma_w_mwr_tlp_gen_rst_done;
	wire DMA_w_MWR_TLP_committer_rst_done;
	always@(posedge sys_clk)begin
		if(~sys_rst_n)begin
			dma_board2host_rst_done <= 1'b0;
		end
		else if(RST_BOARD2HOST == 1'b1)begin
			if(dma_w_mwr_tlp_gen_rst_done == 1'b1 && DMA_w_MWR_TLP_committer_rst_done == 1'b1)begin
				dma_board2host_rst_done <= 1'b1;
			end
			else begin
				dma_board2host_rst_done <= 1'b0;
			end
		end
		else if(dma_board2host_rst) begin
			if(DMA_w_MWR_TLP_committer_rst_done == 1'b1)begin
				dma_board2host_rst_done <= 1'b1;
			end
			else begin
				dma_board2host_rst_done <= 1'b0;
			end
		end
		else begin
			dma_board2host_rst_done <= 1'b0;
		end
	end

	DMA_w_MWR_TLP_GEN dma_w_mwr_tlp_gen(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.RST_BOARD2HOST(RST_BOARD2HOST), 
		.rst_board2host_done(dma_w_mwr_tlp_gen_rst_done),
		// from TLP_head_gen
		.mwr_head_buf_dout(mwr_head_buf_dout), 
		.mwr_head_buf_empty(mwr_head_buf_empty), 
		.mwr_head_buf_rd_en(mwr_head_buf_rd_en), 
		// from board2host_buf
		.board2host_buf_even_dout(board2host_buf_even_dout), 
		.board2host_buf_even_empty(board2host_buf_even_empty), 
		.board2host_buf_even_rd_en(board2host_buf_even_rd_en),
		.board2host_buf_odd_dout(board2host_buf_odd_dout), 
		.board2host_buf_odd_empty(board2host_buf_odd_empty), 
		.board2host_buf_odd_rd_en(board2host_buf_odd_rd_en),
		.board2host_data_count(board2host_data_count),
		// to DMA_w_MWR_TLP_committer, FIFOs are in DMA_w_MWR_TLP_committer for easier control when reset
		.mwr_tlp_buf_din(mwr_tlp_buf_din), 
		.mwr_tlp_buf_wr_en(mwr_tlp_buf_wr_en), 
		.mwr_tlp_buf_prog_full(mwr_tlp_buf_prog_full)
	);

	DMA_w_MWR_TLP_committer DMA_w_MWR_TLP_committer(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_board2host_rst(dma_board2host_rst), 
		.dma_board2host_rst_done(DMA_w_MWR_TLP_committer_rst_done), 
		// from mwr_tlp_buf
		.mwr_tlp_buf_din(mwr_tlp_buf_din), 
		.mwr_tlp_buf_wr_en(mwr_tlp_buf_wr_en), 
		.mwr_tlp_buf_prog_full(mwr_tlp_buf_prog_full), 
		// from DMA_W_MWR_TLP FIFO
		.DMA_W_MWR_TLP_dout(DMA_W_MWR_TLP_dout), 
		.DMA_W_MWR_TLP_rd_en(DMA_W_MWR_TLP_rd_en), 
		.DMA_W_MWR_TLP_empty(DMA_W_MWR_TLP_empty), 
		// to DMA_w_zero_read_TLP_waiter
		.DMA_w_zero_read_TLP_sent(DMA_w_zero_read_TLP_sent), 
		.DMA_w_zero_read_TLP_acked(DMA_w_zero_read_TLP_acked)
	);
	
	TLP_head_gen tlp_head_gen(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		.dma_channel_rst(dma_board2host_rst),
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID),
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN),
		.CFG_MAX_TLP_SIZE(CFG_MAX_PAYLOAD_SIZE),
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN),
		// from descriptor FIFO
		.descriptor_buf_dout(DMA_w_descriptor_buf_dout),
		.descriptor_buf_empty(DMA_w_descriptor_buf_empty),
		.descriptor_buf_rd_en(DMA_w_descriptor_buf_rd_en),
		// from tag FIFO
		.tag_buf_dout(TAG_DEFAULT),
		.tag_buf_empty(1'b0),
		.tag_buf_rd_en(),
		// to TLP head FIFO
		.tlp_head_buf_dout(mwr_head_buf_dout),
		.tlp_head_buf_empty(mwr_head_buf_empty),
		.tlp_head_buf_rd_en(mwr_head_buf_rd_en)
	);
	
	DMA_w_zero_read_LTP_waiter dma_w_zero_read_tlp_waiter(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		.dma_board2host_rst(dma_board2host_rst), 
		// from DMA_w_zero_read_TLP_buf
		.DMA_w_zero_read_TLP_din(DMA_w_zero_read_TLP_din), 
		.DMA_w_zero_read_TLP_wr_en(DMA_w_zero_read_TLP_wr_en), 
		.DMA_w_zero_read_TLP_prog_full(DMA_w_zero_read_TLP_prog_full), 
		// from DMA_w_MWR_TLP_committer
		.DMA_w_zero_read_TLP_sent(DMA_w_zero_read_TLP_sent), 
		.DMA_w_zero_read_TLP_acked(DMA_w_zero_read_TLP_acked),
		// connect with SYS_CSR 
		.DMA_w_timeout_threshold(DMA_w_timeout_threshold), 
		.DMA_w_start_state(DMA_w_start_state), 
		.DMA_w_done_state(DMA_w_done_state), 
		.DMA_w_error_state(DMA_w_error_state)
	);
	
	FIFO_sync_width96_depth16_FWFT DMA_w_descriptor_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_w_descriptor_buf_din),
		.wr_en(DMA_w_descriptor_buf_wr_en),
		.rd_en(DMA_w_descriptor_buf_rd_en),
		.dout(DMA_w_descriptor_buf_dout),
		.full(),
		.empty(DMA_w_descriptor_buf_empty),
		.prog_full(DMA_w_descriptor_buf_prog_full)
	);

endmodule
