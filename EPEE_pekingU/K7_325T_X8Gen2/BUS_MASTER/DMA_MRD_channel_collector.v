`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_MRD_channel_collector.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Collector MRd TLPs from DMA_board2host_descriptor_engine, DMA_host2board_engine and
// DMA_host2board_engine's descriptor engine
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define DMA_MRD_COL_INIT  4'b0001
`define DMA_MRD_COL_W_DIS 4'b0010
`define DMA_MRD_COL_R_DIS 4'b0100
`define DMA_MRD_COL_R     4'b1000

module DMA_MRD_channel_collector(
	input sys_clk,
	input sys_rst_n,
	// to US_TLP_COLLECTOR
	output     [71:0] DMA_MRD_channel_buf_dout,
	input      DMA_MRD_channel_buf_rd_en,
	output     DMA_MRD_channel_buf_empty,
	// from DMA_board2host_descriptor_engine
	input      [71:0] DMA_w_descriptor_MRD_TLP_dout,
	input      DMA_w_descriptor_MRD_TLP_empty,
	output reg DMA_w_descriptor_MRD_TLP_rd_en,
	// from DMA_host2board_engine
	input      [71:0] DMA_r_descriptor_MRD_TLP_dout,
	input      DMA_r_descriptor_MRD_TLP_empty,
	output reg DMA_r_descriptor_MRD_TLP_rd_en,
	// from DMA_host2board_engine
	input      [71:0] DMA_r_MRD_TLP_dout,
	input      DMA_r_MRD_TLP_empty,
	output reg DMA_r_MRD_TLP_rd_en
);

	reg  [71:0] DMA_MRD_channel_buf_din;
	reg  DMA_MRD_channel_buf_wr_en;
	wire DMA_MRD_channel_buf_prog_full;
	
	(*KEEP="TRUE"*)reg [3:0] DMA_MRD_COL_state;

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			DMA_MRD_COL_state <= `DMA_MRD_COL_INIT;
			DMA_w_descriptor_MRD_TLP_rd_en <= 1'b0;
			DMA_r_descriptor_MRD_TLP_rd_en <= 1'b0;
			DMA_r_MRD_TLP_rd_en <= 1'b0;
			DMA_MRD_channel_buf_din <= 72'd0;
			DMA_MRD_channel_buf_wr_en <= 1'b0;
		end
		else begin
			case(DMA_MRD_COL_state)
				`DMA_MRD_COL_INIT:begin
					DMA_MRD_channel_buf_din <= 72'd0;
					DMA_MRD_channel_buf_wr_en <= 1'b0;
					if(DMA_w_descriptor_MRD_TLP_empty == 1'b0)begin
						DMA_w_descriptor_MRD_TLP_rd_en <= ((DMA_MRD_channel_buf_prog_full == 1'b0) ? 1'b1 : 1'b0);
						DMA_r_descriptor_MRD_TLP_rd_en <= 1'b0;
						DMA_r_MRD_TLP_rd_en <= 1'b0;
						DMA_MRD_COL_state <= `DMA_MRD_COL_W_DIS;
					end
					else if(DMA_r_descriptor_MRD_TLP_empty == 1'b0)begin
						DMA_r_descriptor_MRD_TLP_rd_en <= ((DMA_MRD_channel_buf_prog_full == 1'b0) ? 1'b1 : 1'b0);
						DMA_w_descriptor_MRD_TLP_rd_en <= 1'b0;
						DMA_r_MRD_TLP_rd_en <= 1'b0;
						DMA_MRD_COL_state <= `DMA_MRD_COL_R_DIS;
					end
					else if(DMA_r_MRD_TLP_empty == 1'b0)begin
						DMA_r_MRD_TLP_rd_en <= ((DMA_MRD_channel_buf_prog_full == 1'b0) ? 1'b1 : 1'b0);
						DMA_w_descriptor_MRD_TLP_rd_en <= 1'b0;
						DMA_r_descriptor_MRD_TLP_rd_en <= 1'b0;
						DMA_MRD_COL_state <= `DMA_MRD_COL_R;
					end
					else begin
						DMA_w_descriptor_MRD_TLP_rd_en <= 1'b0;
						DMA_r_descriptor_MRD_TLP_rd_en <= 1'b0;
						DMA_r_MRD_TLP_rd_en <= 1'b0;
						DMA_MRD_COL_state <= `DMA_MRD_COL_INIT;
					end
				end
				`DMA_MRD_COL_W_DIS:begin
					// for read enable
					if(DMA_w_descriptor_MRD_TLP_empty == 1'b0 && DMA_MRD_channel_buf_prog_full == 1'b0)begin
						if(DMA_w_descriptor_MRD_TLP_dout[64] == 1'b0 || (DMA_w_descriptor_MRD_TLP_dout[64] == 1'b1 && DMA_w_descriptor_MRD_TLP_rd_en == 1'b0))begin
							DMA_w_descriptor_MRD_TLP_rd_en <= 1'b1;
						end
						else begin
							DMA_w_descriptor_MRD_TLP_rd_en <= 1'b0;
						end
					end
					else begin
						DMA_w_descriptor_MRD_TLP_rd_en <= 1'b0;
					end
					// for din and write enable, when empty = 0 and rd_en = 1, the next data will be poped out,
					// so that data has to be inserted to output FIFO
					if(DMA_w_descriptor_MRD_TLP_empty == 1'b0 && DMA_w_descriptor_MRD_TLP_rd_en == 1'b1)begin
						DMA_MRD_channel_buf_din <= DMA_w_descriptor_MRD_TLP_dout;
						DMA_MRD_channel_buf_wr_en <= 1'b1;
					end
					else begin
						DMA_MRD_channel_buf_wr_en <= 1'b0;
					end
					// if it is the end of the TLP, go back to initial state
					if(DMA_w_descriptor_MRD_TLP_dout[64] == 1'b1 && DMA_w_descriptor_MRD_TLP_empty == 1'b0 && DMA_w_descriptor_MRD_TLP_rd_en == 1'b1)begin
						DMA_MRD_COL_state <= `DMA_MRD_COL_INIT;
					end
					else begin
						DMA_MRD_COL_state <= `DMA_MRD_COL_W_DIS;
					end
				end
				`DMA_MRD_COL_R_DIS:begin
					// for read enable
					if(DMA_r_descriptor_MRD_TLP_empty == 1'b0 && DMA_MRD_channel_buf_prog_full == 1'b0)begin
						if(DMA_r_descriptor_MRD_TLP_dout[64] == 1'b0 || (DMA_r_descriptor_MRD_TLP_dout[64] == 1'b1 && DMA_r_descriptor_MRD_TLP_rd_en == 1'b0))begin
							DMA_r_descriptor_MRD_TLP_rd_en <= 1'b1;
						end
						else begin
							DMA_r_descriptor_MRD_TLP_rd_en <= 1'b0;
						end
					end
					else begin
						DMA_r_descriptor_MRD_TLP_rd_en <= 1'b0;
					end
					// for din and write enable, when empty = 0 and rd_en = 1, the next data will be poped out,
					// so that data has to be inserted to output FIFO
					if(DMA_r_descriptor_MRD_TLP_empty == 1'b0 && DMA_r_descriptor_MRD_TLP_rd_en == 1'b1)begin
						DMA_MRD_channel_buf_din <= DMA_r_descriptor_MRD_TLP_dout;
						DMA_MRD_channel_buf_wr_en <= 1'b1;
					end
					else begin
						DMA_MRD_channel_buf_wr_en <= 1'b0;
					end
					// if it is the end of the TLP, go back to initial state
					if(DMA_r_descriptor_MRD_TLP_dout[64] == 1'b1 && DMA_r_descriptor_MRD_TLP_empty == 1'b0 && DMA_r_descriptor_MRD_TLP_rd_en == 1'b1)begin
						DMA_MRD_COL_state <= `DMA_MRD_COL_INIT;
					end
					else begin
						DMA_MRD_COL_state <= `DMA_MRD_COL_R_DIS;
					end
				end
				`DMA_MRD_COL_R:begin
					// for read enable singal
					if(DMA_r_MRD_TLP_empty == 1'b0 && 
						DMA_MRD_channel_buf_prog_full == 1'b0)begin
						if(DMA_r_MRD_TLP_dout[64] == 1'b0 || (DMA_r_MRD_TLP_dout[64] == 1'b1 &&DMA_r_MRD_TLP_rd_en == 1'b0))begin
							DMA_r_MRD_TLP_rd_en <= 1'b1;
						end
						else begin
							DMA_r_MRD_TLP_rd_en <= 1'b0;
						end
					end
					else begin
						DMA_r_MRD_TLP_rd_en <= 1'b0;
					end
					// for din and write enable, when empty = 0 and rd_en = 1, the next data will be poped out,
					// so that data has to be inserted to output FIFO
					if(DMA_r_MRD_TLP_empty == 1'b0 && DMA_r_MRD_TLP_rd_en == 1'b1)begin
						DMA_MRD_channel_buf_din <= DMA_r_MRD_TLP_dout;
						DMA_MRD_channel_buf_wr_en <= 1'b1;
					end
					else begin
						DMA_MRD_channel_buf_wr_en <= 1'b0;
					end
					// if it is the end of the TLP, go back to initial state
					if(DMA_r_MRD_TLP_dout[64] == 1'b1 && DMA_r_MRD_TLP_empty == 1'b0 && DMA_r_MRD_TLP_rd_en == 1'b1)begin
						DMA_MRD_COL_state <= `DMA_MRD_COL_INIT;
					end
					else begin
						DMA_MRD_COL_state <= `DMA_MRD_COL_R;
					end
				end
				default:begin
					DMA_MRD_COL_state <= `DMA_MRD_COL_INIT;
				end
			endcase
		end
	end

	FIFO_sync_width72_depth16_FWFT DMA_MRD_channel_buf(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_MRD_channel_buf_din),
		.wr_en(DMA_MRD_channel_buf_wr_en),
		.rd_en(DMA_MRD_channel_buf_rd_en),
		.dout(DMA_MRD_channel_buf_dout),
		.full(),
		.empty(DMA_MRD_channel_buf_empty),
		.prog_full(DMA_MRD_channel_buf_prog_full)
	);

endmodule
