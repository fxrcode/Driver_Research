`timescale 1ns / 1ps
/////////////////////////////////////
//
// BUS_MASTER.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// This file is the top of BUS_MASTER module. The BUS_MASTER module contains the PIO and
// DMA engine, along with the collector and dispatcher of the TLPs.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module BUS_MASTER(
	input sys_clk,
	input sys_rst_n,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	input CFG_EXT_TAG_EN,
	input [2:0]  CFG_MAX_PAYLOAD_SIZE,
	input [2:0]  CFG_MAX_RD_REQ_SIZE,
	input CFG_BUS_MSTR_EN,
	// from DS_CTRL
	input  [133:0] DS_TLP_BUF_DIN,
	output DS_TLP_BUF_PROG_FULL,
	input  DS_TLP_BUF_WR_EN,
	input  DS_CTRL_ERROR,
	// to US_CTRL
	output [133:0]US_TLP_BUF_DOUT,
	output US_TLP_BUF_EMPTY,
	input  US_TLP_BUF_RD_EN,
	input  US_CTRL_ERROR,
	// to INT_CTRL
	output INT_REQ,
	input  INT_REQ_ACK,
	output INT_SERVED,
	input  INT_SERVED_ACK,
	// for user
	/*user reset, in PCIe clock domain*/
	output RST_USR,
	input  RST_USR_done,
	output RST_HOST2BOARD,
	output RST_BOARD2HOST,
	/*user PIO interface, in PCIe clock domain*/
	output [16:0] usr_pio_wr_addr,
	output [31:0] usr_pio_wr_data,
	output usr_pio_wr_req,
	input  usr_pio_wr_ack,
	output [16:0] usr_pio_rd_addr,
	input  [31:0] usr_pio_rd_data,
	output usr_pio_rd_req,
	input  usr_pio_rd_ack,
	/*user interrupt interface, in user "user_int_clk" clock domain*/
	input  usr_int_req,
	input  [2:0] usr_int_vector,
	output [7:0] usr_int_sw_waiting,
	output usr_int_clr,
	output usr_int_enable,
	/*user DMA interface, in system clock domain*/
	output [129:0] FIFO_host2board_dout,
	output FIFO_host2board_empty,
	input  FIFO_host2board_rd_en,
	input  [129:0] FIFO_board2host_din,
	output FIFO_board2host_prog_full,
	input  FIFO_board2host_wr_en
);
	
	// connect DS_TLP_dispatcher and PIO_ENGINE
	wire [71:0] PIO_MWR_channel_buf_din;
	wire PIO_MWR_channel_buf_prog_full;
	wire PIO_MWR_channel_buf_wr_en;
	wire [71:0] PIO_MRD_channel_buf_din;
	wire PIO_MRD_channel_buf_prog_full;
	wire PIO_MRD_channel_buf_wr_en;
	
	// connect US_TLP_collector and DMA_ENGINE
	wire [71:0] PIO_CPLD_channel_buf_dout;
	wire PIO_CPLD_channel_buf_empty;
	wire PIO_CPLD_channel_buf_rd_en;
	
	// connect DS_TLP_dispatcher and DMA_ENGINE
	wire [133:0] DMA_CPLD_channel_buf_din;
	wire DMA_CPLD_channel_buf_prog_full;
	wire DMA_CPLD_channel_buf_wr_en;
	
	// connect US_TLP_collector and DMA_ENGINE
	wire [133:0] DMA_MWR_channel_buf_dout;
	wire DMA_MWR_channel_buf_empty;
	wire DMA_MWR_channel_buf_rd_en;
	wire [71:0] DMA_MRD_channel_buf_dout;
	wire DMA_MRD_channel_buf_empty;
	wire DMA_MRD_channel_buf_rd_en;
	
	// connect INT_GEN and DMA_ENGINE
	wire IG_DMA_r_int_req;
	wire IG_DMA_r_int_req_ack;
	wire IG_DMA_r_int_served;
	wire IG_DMA_r_int_served_ack;
	wire IG_DMA_w_int_req;
	wire IG_DMA_w_int_req_ack;
	wire IG_DMA_w_int_served;
	wire IG_DMA_w_int_served_ack;

	// connect INT_GEN and USR_INT_CTRL
	wire IG_usr_int_req;
	wire IG_usr_int_req_ack;
	wire IG_usr_int_served;
	wire IG_usr_int_served_ack;
	
	// connect PIO_ENGINE and SYS_CSR
	wire [16:0] SYS_PIO_WR_ADDR;
	wire [31:0] SYS_PIO_WR_DATA;
	wire SYS_PIO_WR_WR_EN;
	wire [16:0] SYS_PIO_RD_ADDR;
	wire [31:0] SYS_PIO_RD_DATA;
	
	// connect SYS_CSR and USR_INT_CTRL
	wire  int_enable;
	wire  usr_int_served;
	wire  dma_w_int_disable;
	wire  dma_r_int_disable;
	wire  usr_int_req_state;
	wire  [2:0] usr_int_vector_state;
	
	// connect SYS_CSR and DMA_ENGINE
	/*DMA reset*/
	wire RST_DMA_R_DONE;
	wire RST_DMA_W_DONE;
	/* for DMA read/write descriptor buf address*/
	wire  [31:0] dma_r_descriptor_addr_l;
	wire  [31:0] dma_r_descriptor_addr_h;
	wire  [31:0] dma_w_descriptor_addr_l;
	wire  [31:0] dma_w_descriptor_addr_h;
	/* for DMA read/write control*/
	wire  dma_r_start;
	wire  dma_r_init_descriptor_64addr_en; // use 64bit address TLP or not
	wire  [13:0] dma_r_descriptor_num;
	wire  dma_w_start;
	wire  dma_w_init_descriptor_64addr_en; // use 64bit address TLP or not
	wire  [13:0] dma_w_descriptor_num;
	/* for hardware state*/
	wire dma_r_start_state;
	wire dma_r_done_state;
	wire dma_r_error_state;
	wire dma_r_int_req_state;
	wire dma_r_int_served_state;
	wire dma_w_start_state;
	wire dma_w_done_state;
	wire dma_w_error_state;
	wire dma_w_int_req_state;
	wire dma_w_int_served_state;
	/* for interrupt control*/
		//wire  int_enable; defined before this line
	wire  dma_r_int_served;
	wire  dma_w_int_served;
	/* data count*/
	wire [31:0] host2board_data_count;
	wire [31:0] board2host_data_count;
	/* timeout threshold*/
	wire  [63:0] dma_r_timeout_threshold;
	wire  [63:0] dma_w_timeout_threshold;
	
	// connect DMA_ENGINE and host2board/board2host FIFO
	wire [129:0] FIFO_board2host_dout;
	wire FIFO_board2host_rd_en;
	wire FIFO_board2host_empty;
	wire [129:0] FIFO_host2board_din;
	wire FIFO_host2board_wr_en;
	wire FIFO_host2board_prog_full;
		
	// for FIFO host2board & FIFO board2host's reset
	reg FIFO_host2board_rst;
	reg FIFO_board2host_rst;
	always @(posedge sys_clk)begin
		if(~sys_rst_n || RST_HOST2BOARD)begin
			FIFO_host2board_rst <= 1'b1;
		end
		else begin
			FIFO_host2board_rst <= 1'b0;
		end
		if(~sys_rst_n || RST_BOARD2HOST)begin
			FIFO_board2host_rst <= 1'b1;
		end
		else begin
			FIFO_board2host_rst <= 1'b0;
		end
	end
	
	DS_TLP_dispatcher DS_TLP_dispatcher(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		// from DS_CTRL
		.DS_TLP_BUF_DIN(DS_TLP_BUF_DIN),
		.DS_TLP_BUF_PROG_FULL(DS_TLP_BUF_PROG_FULL),
		.DS_TLP_BUF_WR_EN(DS_TLP_BUF_WR_EN),
		// to PIO_ENGINE
		.PIO_MWR_channel_buf_din(PIO_MWR_channel_buf_din),
		.PIO_MWR_channel_buf_prog_full(PIO_MWR_channel_buf_prog_full),
		.PIO_MWR_channel_buf_wr_en(PIO_MWR_channel_buf_wr_en),
		.PIO_MRD_channel_buf_din(PIO_MRD_channel_buf_din),
		.PIO_MRD_channel_buf_prog_full(PIO_MRD_channel_buf_prog_full),
		.PIO_MRD_channel_buf_wr_en(PIO_MRD_channel_buf_wr_en),
		// to DMA_ENGINE
		.DMA_CPLD_channel_buf_din(DMA_CPLD_channel_buf_din),
		.DMA_CPLD_channel_buf_prog_full(DMA_CPLD_channel_buf_prog_full),
		.DMA_CPLD_channel_buf_wr_en(DMA_CPLD_channel_buf_wr_en)
	);

	US_TLP_collector US_TLP_collector(
		.sys_clk(sys_clk),
		.sys_rst_n(sys_rst_n),
		// to US_CTRL
		.US_TLP_BUF_DOUT(US_TLP_BUF_DOUT),
		.US_TLP_BUF_EMPTY(US_TLP_BUF_EMPTY),
		.US_TLP_BUF_RD_EN(US_TLP_BUF_RD_EN),
		// from PIO_ENGINE
		.PIO_CPLD_channel_buf_dout(PIO_CPLD_channel_buf_dout),
		.PIO_CPLD_channel_buf_empty(PIO_CPLD_channel_buf_empty),
		.PIO_CPLD_channel_buf_rd_en(PIO_CPLD_channel_buf_rd_en),
		// from DMA_ENGINE
		.DMA_MWR_channel_buf_dout(DMA_MWR_channel_buf_dout),
		.DMA_MWR_channel_buf_empty(DMA_MWR_channel_buf_empty),
		.DMA_MWR_channel_buf_rd_en(DMA_MWR_channel_buf_rd_en),
		.DMA_MRD_channel_buf_dout(DMA_MRD_channel_buf_dout),
		.DMA_MRD_channel_buf_empty(DMA_MRD_channel_buf_empty),
		.DMA_MRD_channel_buf_rd_en(DMA_MRD_channel_buf_rd_en)
	);
	
	PIO_ENGINE PIO_ENGINE(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID), 
		// from DS_TLP_dispactor
		.PIO_MWR_channel_buf_din(PIO_MWR_channel_buf_din), 
		.PIO_MWR_channel_buf_prog_full(PIO_MWR_channel_buf_prog_full), 
		.PIO_MWR_channel_buf_wr_en(PIO_MWR_channel_buf_wr_en), 
		.PIO_MRD_channel_buf_din(PIO_MRD_channel_buf_din), 
		.PIO_MRD_channel_buf_prog_full(PIO_MRD_channel_buf_prog_full), 
		.PIO_MRD_channel_buf_wr_en(PIO_MRD_channel_buf_wr_en), 
		// to US_TLP_collector
		.PIO_CPLD_channel_buf_dout(PIO_CPLD_channel_buf_dout), 
		.PIO_CPLD_channel_buf_empty(PIO_CPLD_channel_buf_empty), 
		.PIO_CPLD_channel_buf_rd_en(PIO_CPLD_channel_buf_rd_en), 
		// to SYS_CSR
		.SYS_PIO_WR_ADDR(SYS_PIO_WR_ADDR), 
		.SYS_PIO_WR_DATA(SYS_PIO_WR_DATA), 
		.SYS_PIO_WR_WR_EN(SYS_PIO_WR_WR_EN), 
		.SYS_PIO_RD_ADDR(SYS_PIO_RD_ADDR), 
		.SYS_PIO_RD_DATA(SYS_PIO_RD_DATA), 
		// to user, in sys_clk clock domain
		.usr_pio_wr_addr(usr_pio_wr_addr), 
		.usr_pio_wr_data(usr_pio_wr_data), 
		.usr_pio_wr_req(usr_pio_wr_req),
		.usr_pio_wr_ack(usr_pio_wr_ack),
		.usr_pio_rd_addr(usr_pio_rd_addr), 
		.usr_pio_rd_data(usr_pio_rd_data),
		.usr_pio_rd_req(usr_pio_rd_req),
		.usr_pio_rd_ack(usr_pio_rd_ack)
	);

	SYS_CSR SYS_CSR(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// from PIO_ENGINE
		.SYS_PIO_WR_ADDR(SYS_PIO_WR_ADDR), 
		.SYS_PIO_WR_DATA(SYS_PIO_WR_DATA), 
		.SYS_PIO_WR_WR_EN(SYS_PIO_WR_WR_EN), 
		.SYS_PIO_RD_ADDR(SYS_PIO_RD_ADDR), 
		.SYS_PIO_RD_DATA(SYS_PIO_RD_DATA), 
		// for SYSTEM_RESET
		.RST_USR_o(RST_USR), 
		.RST_USR_done(RST_USR_done),
		.RST_HOST2BOARD_o(RST_HOST2BOARD), 
		.RST_BOARD2HOST_o(RST_BOARD2HOST), 
		.RST_DMA_R_DONE_i(RST_DMA_R_DONE), 
		.RST_DMA_W_DONE_i(RST_DMA_W_DONE), 
		// for DMA read/write descriptor buf address
		.dma_r_descriptor_addr_l(dma_r_descriptor_addr_l), 
		.dma_r_descriptor_addr_h(dma_r_descriptor_addr_h), 
		.dma_w_descriptor_addr_l(dma_w_descriptor_addr_l), 
		.dma_w_descriptor_addr_h(dma_w_descriptor_addr_h), 
		// for DMA read/write control
		.dma_r_start(dma_r_start), 
		.dma_r_init_descriptor_64addr_en(dma_r_init_descriptor_64addr_en), 
		.dma_r_descriptor_num(dma_r_descriptor_num), 
		.dma_w_start(dma_w_start), 
		.dma_w_init_descriptor_64addr_en(dma_w_init_descriptor_64addr_en), 
		.dma_w_descriptor_num(dma_w_descriptor_num), 
		// for hardware state
		.dma_r_start_state_i(dma_r_start_state), 
		.dma_r_done_state_i(dma_r_done_state), 
		.dma_r_error_state_i(dma_r_error_state), 
		.dma_r_int_req_state_i(dma_r_int_req_state), 
		.dma_r_int_served_state_i(dma_r_int_served_state), 
		.dma_w_start_state_i(dma_w_start_state), 
		.dma_w_done_state_i(dma_w_done_state), 
		.dma_w_error_state_i(dma_w_error_state), 
		.dma_w_int_req_state_i(dma_w_int_req_state), 
		.dma_w_int_served_state_i(dma_w_int_served_state),
		.ds_ctrl_error_i(DS_CTRL_ERROR), 
		.us_ctrl_error_i(US_CTRL_ERROR),  
		.usr_int_req_state_i(usr_int_req_state), 
		.usr_int_vector_state(usr_int_vector_state),
		// for interrupt control
		.int_enable(int_enable), 
		.dma_r_int_served(dma_r_int_served), 
		.dma_w_int_served(dma_w_int_served), 
		.usr_int_sw_waiting(usr_int_sw_waiting), 
		.usr_int_served(usr_int_served), 
		.dma_r_int_disable(dma_r_int_disable),
		.dma_w_int_disable(dma_w_int_disable),
		// data count
		.host2board_data_count_i(host2board_data_count), 
		.board2host_data_count_i(board2host_data_count), 
		// timeout threshold
		.dma_r_timeout_threshold(dma_r_timeout_threshold), 
		.dma_w_timeout_threshold(dma_w_timeout_threshold)
	);

	DMA_ENGINE DMA_ENGINE(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID), 
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN), 
		.CFG_MAX_PAYLOAD_SIZE(CFG_MAX_PAYLOAD_SIZE), 
		.CFG_MAX_RD_REQ_SIZE(CFG_MAX_RD_REQ_SIZE), 
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN), 
		// from DS_TLP_dispatcher
		.DMA_CPLD_channel_buf_din(DMA_CPLD_channel_buf_din), 
		.DMA_CPLD_channel_buf_prog_full(DMA_CPLD_channel_buf_prog_full), 
		.DMA_CPLD_channel_buf_wr_en(DMA_CPLD_channel_buf_wr_en), 
		// to US_TLP_COLLECTOR
		.DMA_MRD_channel_buf_dout(DMA_MRD_channel_buf_dout), 
		.DMA_MRD_channel_buf_rd_en(DMA_MRD_channel_buf_rd_en), 
		.DMA_MRD_channel_buf_empty(DMA_MRD_channel_buf_empty), 
		.DMA_MWR_channel_buf_dout(DMA_MWR_channel_buf_dout), 
		.DMA_MWR_channel_buf_rd_en(DMA_MWR_channel_buf_rd_en), 
		.DMA_MWR_channel_buf_empty(DMA_MWR_channel_buf_empty), 
		// to INT_GEN
		.IG_DMA_w_int_req(IG_DMA_w_int_req), 
		.IG_DMA_w_int_req_ack(IG_DMA_w_int_req_ack), 
		.IG_DMA_w_int_served(IG_DMA_w_int_served), 
		.IG_DMA_w_int_served_ack(IG_DMA_w_int_served_ack), 
		.IG_DMA_r_int_req(IG_DMA_r_int_req), 
		.IG_DMA_r_int_req_ack(IG_DMA_r_int_req_ack), 
		.IG_DMA_r_int_served(IG_DMA_r_int_served), 
		.IG_DMA_r_int_served_ack(IG_DMA_r_int_served_ack), 
		// connect with SYS_CSR
		/*DMA reset*/
		.RST_HOST2BOARD(RST_HOST2BOARD), 
		.RST_BOARD2HOST(RST_BOARD2HOST), 
		.RST_DMA_R_DONE(RST_DMA_R_DONE), 
		.RST_DMA_W_DONE(RST_DMA_W_DONE), 
		/* for DMA read/write descriptor buf address*/
		.dma_r_descriptor_addr_l(dma_r_descriptor_addr_l), 
		.dma_r_descriptor_addr_h(dma_r_descriptor_addr_h), 
		.dma_w_descriptor_addr_l(dma_w_descriptor_addr_l), 
		.dma_w_descriptor_addr_h(dma_w_descriptor_addr_h),
		/* for DMA read/write control*/		
		.dma_r_start(dma_r_start), 
		.dma_r_init_descriptor_64addr_en(dma_r_init_descriptor_64addr_en), // use 64bit address TLP or not
		.dma_r_descriptor_num(dma_r_descriptor_num), 
		.dma_w_start(dma_w_start), 
		.dma_w_init_descriptor_64addr_en(dma_w_init_descriptor_64addr_en), // use 64bit address TLP or not
		.dma_w_descriptor_num(dma_w_descriptor_num), 
		/* for hardware state*/
		.dma_r_start_state_o(dma_r_start_state), 
		.dma_r_done_state_o(dma_r_done_state), 
		.dma_r_error_state_o(dma_r_error_state), 
		.dma_r_int_req_state(dma_r_int_req_state), 
		.dma_r_int_served_state(dma_r_int_served_state), 
		.dma_w_start_state_o(dma_w_start_state), 
		.dma_w_done_state_o(dma_w_done_state), 
		.dma_w_error_state_o(dma_w_error_state), 
		.dma_w_int_req_state(dma_w_int_req_state), 
		.dma_w_int_served_state(dma_w_int_served_state), 
		/* for interrupt control*/
		.int_enable(int_enable), 
		.dma_r_int_served(dma_r_int_served), 
		.dma_w_int_served(dma_w_int_served), 
		.dma_r_int_disable(dma_r_int_disable),
		.dma_w_int_disable(dma_w_int_disable),
		/* data count*/
		.host2board_data_count(host2board_data_count), 
		.board2host_data_count_o(board2host_data_count), 
		/* timeout threshold*/
		.dma_r_timeout_threshold(dma_r_timeout_threshold), 
		.dma_w_timeout_threshold(dma_w_timeout_threshold),
		// from FIFO board2host
		.FIFO_board2host_dout(FIFO_board2host_dout), // {QW_Valid[1:0], data[127:0]}
		.FIFO_board2host_rd_en(FIFO_board2host_rd_en),
		.FIFO_board2host_empty(FIFO_board2host_empty),
		// to FIFO_host2board
		.FIFO_host2board_din(FIFO_host2board_din), // {QW_Valid[1:0], data[127:0]}
		.FIFO_host2board_wr_en(FIFO_host2board_wr_en),
		.FIFO_host2board_prog_full(FIFO_host2board_prog_full)
	);
	
	INT_GEN INT_GEN(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// from DMA_ENGINE
		.IG_DMA_r_int_req(IG_DMA_r_int_req), 
		.IG_DMA_r_int_req_ack(IG_DMA_r_int_req_ack), 
		.IG_DMA_r_int_served(IG_DMA_r_int_served), 
		.IG_DMA_r_int_served_ack(IG_DMA_r_int_served_ack), 
		.IG_DMA_w_int_req(IG_DMA_w_int_req), 
		.IG_DMA_w_int_req_ack(IG_DMA_w_int_req_ack), 
		.IG_DMA_w_int_served(IG_DMA_w_int_served), 
		.IG_DMA_w_int_served_ack(IG_DMA_w_int_served_ack), 
		// from USR_INT_CTRL
		.IG_usr_int_req(IG_usr_int_req), 
		.IG_usr_int_req_ack(IG_usr_int_req_ack), 
		.IG_usr_int_served(IG_usr_int_served), 
		.IG_usr_int_served_ack(IG_usr_int_served_ack), 
		// to INT_CTRL
		.INT_REQ(INT_REQ), 
		.INT_REQ_ACK(INT_REQ_ACK), 
		.INT_SERVED(INT_SERVED), 
		.INT_SERVED_ACK(INT_SERVED_ACK)
	);
	
	USR_INT_CTRL USR_INT_CTRL(
		.sys_clk(sys_clk), 
		.sys_rst_n(sys_rst_n), 
		// for INT_GEN, sys_clk domain
		.IG_usr_int_req(IG_usr_int_req), 
		.IG_usr_int_req_ack(IG_usr_int_req_ack), 
		.IG_usr_int_served(IG_usr_int_served), 
		.IG_usr_int_served_ack(IG_usr_int_served_ack), 
		// for SYS_CSR, sys_clk domain
		.int_enable(int_enable), 
		.usr_int_served(usr_int_served), 
		.usr_int_req_state(usr_int_req_state), 
		.usr_int_vector_state(usr_int_vector_state),
		.usr_int_sw_waiting(usr_int_sw_waiting), 
		// for user, user clk domain
		.usr_int_req(usr_int_req), 
		.usr_int_vector(usr_int_vector),
		.usr_int_clr(usr_int_clr), 
		.usr_int_enable(usr_int_enable)
	);

	FIFO_sync_width130_depth512_FWFT FIFO_host2board(
		.clk(sys_clk),
		.rst(FIFO_host2board_rst),
		.din(FIFO_host2board_din),
		.wr_en(FIFO_host2board_wr_en),
		.rd_en(FIFO_host2board_rd_en),
		.prog_full_thresh(9'd500),
		.dout(FIFO_host2board_dout),
		.full(),
		.empty(FIFO_host2board_empty),
		.prog_full(FIFO_host2board_prog_full)
	);
		
	FIFO_sync_width130_depth512_FWFT FIFO_board2host(
		.clk(sys_clk),
		.rst(FIFO_board2host_rst),
		.din(FIFO_board2host_din),
		.wr_en(FIFO_board2host_wr_en),
		.rd_en(FIFO_board2host_rd_en),
		.prog_full_thresh(9'd500),
		.dout(FIFO_board2host_dout),
		.full(),
		.empty(FIFO_board2host_empty),
		.prog_full(FIFO_board2host_prog_full)
	);
	
endmodule
