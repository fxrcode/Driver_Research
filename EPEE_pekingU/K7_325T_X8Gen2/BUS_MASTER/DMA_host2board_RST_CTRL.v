`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_host2board_RST_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Control the reset process of DMA host to board side.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_host2board_RST_CTRL(
	input sys_clk,
	input sys_rst_n,
	// from SYS_CSR
	input RST_HOST2BOARD,
	output reg RST_DMA_R_DONE,
	// from DMA_host2board_INIT_CTRL
	input dma_r_init_req,
	output reg dma_r_init_done,
	// reset signals for other DMA read modules
	output reg dma_host2board_rst,
	input dma_host2board_discriptor_engine_rst_done,
	input dma_host2board_engine_rst_done,
	input dma_host2board_int_ctrl_rst_done
);

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			// when sys_rst_n assert, all the components will reset
			RST_DMA_R_DONE <= 1'b0;
			dma_r_init_done <= 1'b0;
			dma_host2board_rst <= 1'b0;
		end
		else if(RST_HOST2BOARD == 1'b1)begin
			dma_r_init_done <= 1'b0;
			dma_host2board_rst <= 1'b1;
			if(dma_host2board_discriptor_engine_rst_done == 1'b1 && dma_host2board_engine_rst_done == 1'b1 && 
				dma_host2board_int_ctrl_rst_done == 1'b1)begin
				RST_DMA_R_DONE <= 1'b1;
			end
			else begin
				RST_DMA_R_DONE <= 1'b0;
			end
		end
		else if(dma_r_init_req == 1'b1)begin
			RST_DMA_R_DONE <= 1'b0;
			dma_host2board_rst <= 1'b1;
			if(dma_host2board_discriptor_engine_rst_done == 1'b1 && dma_host2board_engine_rst_done == 1'b1 && 
				dma_host2board_int_ctrl_rst_done == 1'b1)begin
				dma_r_init_done <= 1'b1;
			end
			else begin
				dma_r_init_done <= 1'b0;
			end
		end
		else begin
			RST_DMA_R_DONE <= 1'b0;
			dma_r_init_done <= 1'b0;
			dma_host2board_rst <= 1'b0;
		end
	end

endmodule
