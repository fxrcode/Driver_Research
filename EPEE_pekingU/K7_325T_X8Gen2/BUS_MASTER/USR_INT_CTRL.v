`timescale 1ns / 1ps
/////////////////////////////////////
//
// USR_INT_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Generate user interrupt requests.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define USR_INT_INIT   5'b00001
`define USR_INT_REQ    5'b00010
`define USR_INT_WAIT   5'b00100
`define USR_INT_SERVED 5'b01000
`define USR_INT_CLR    5'b10000

module USR_INT_CTRL(
	input sys_clk,
	input sys_rst_n,	
	// for INT_GEN, sys_clk domain
	output reg IG_usr_int_req,
	input      IG_usr_int_req_ack,
	output reg IG_usr_int_served,
	input      IG_usr_int_served_ack,
	
	// for SYS_CSR, sys_clk domain
	input      int_enable,
	input      usr_int_served,
	output reg usr_int_req_state,
	output reg [2:0] usr_int_vector_state, // when user interrupt, this is used to point to which interrupt is act
	input      [7:0] usr_int_sw_waiting, // indicates which kind of interrupt is software waiting
	
	// for user, sys_clk domain
	input      usr_int_req,
	input      [2:0] usr_int_vector, // 0~7 means different kinds of interrupt
	output reg usr_int_clr,
	output reg usr_int_enable
);

	(*KEEP="TRUE"*)reg [4:0] usr_int_state;
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n || ~int_enable)begin
			IG_usr_int_req <= 1'b0;
			IG_usr_int_served <= 1'b0;
			usr_int_req_state <= 1'b0;
			usr_int_vector_state <= 3'd0;
			
			usr_int_clr <= 1'b0;
			usr_int_enable <= 1'b0;
		
			usr_int_state <= `USR_INT_INIT;
		end
		else begin
			usr_int_enable <= 1'b1;
			case(usr_int_state)
				`USR_INT_INIT:begin
					usr_int_req_state <= 1'b0;
					IG_usr_int_served <= 1'b0;
					if(usr_int_req == 1'b1 && usr_int_sw_waiting[usr_int_vector] == 1'b1)begin
						usr_int_state <= `USR_INT_REQ;
						IG_usr_int_req <= 1'b1;
					end
					else begin
						usr_int_state <= `USR_INT_INIT;
						IG_usr_int_req <= 1'b0;
					end
				end
				`USR_INT_REQ:begin
					if(IG_usr_int_req_ack == 1'b1)begin
						IG_usr_int_req <= 1'b0;
						usr_int_state <= `USR_INT_WAIT;
						usr_int_req_state <= 1'b1;
						usr_int_vector_state <= usr_int_vector;
					end
					else begin
						IG_usr_int_req <= 1'b1;
						usr_int_state <= `USR_INT_REQ;
						usr_int_req_state <= 1'b0;
						usr_int_vector_state <= 3'd0;
					end
				end
				`USR_INT_WAIT:begin
					if(usr_int_served == 1'b1)begin
					// software has handled the interrupt
						usr_int_state <= `USR_INT_SERVED;
						IG_usr_int_served <= 1'b1;
					end
					else begin
					// software hasn't handled the interrupt
						usr_int_state <= `USR_INT_WAIT;
						IG_usr_int_served <= 1'b0;
					end
				end
				`USR_INT_SERVED:begin
					if(IG_usr_int_served_ack == 1'b1)begin
					// INT_GEN knows the interrupt has been served by software
						IG_usr_int_served <= 1'b0;
						usr_int_req_state <= 1'b0;
						usr_int_state <= `USR_INT_CLR;
						usr_int_clr <= 1'b1;
					end
					else begin
					// INT_GEN havn't know the interrupt has been served by software
						IG_usr_int_served <= 1'b1;
						usr_int_state <= `USR_INT_SERVED;
					end
				end
				`USR_INT_CLR:begin
					if(usr_int_req == 1'b0)begin
						usr_int_state <= `USR_INT_INIT;
						usr_int_clr <= 1'b0;
						usr_int_vector_state <= 3'd0;
					end
					else begin
						usr_int_state <= `USR_INT_CLR;
						usr_int_clr <= 1'b1;
					end
				end
				default:begin
					usr_int_state <= `USR_INT_INIT;
				end
			endcase
		end
	end

endmodule
