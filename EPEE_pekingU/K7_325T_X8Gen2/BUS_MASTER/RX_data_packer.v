`timescale 1ns / 1ps
/////////////////////////////////////
//
// RX_data_packer.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// It packs the RX data. Its input data can be uncontinuous. This module will pack those
// data and output the continuous data for a sigle DMA read (host to baord) process.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module RX_data_packer(
	input sys_clk,
	input sys_rst_n,
	input dma_r_rst,
	// connect with SYS_CSR
	input [63:0] DMA_r_timeout_threshold,
	input DMA_r_start_state,
	output reg DMA_r_done_state,
	output reg DMA_r_error_state,
	// from DMA_r_descriptor_buf
	input [17:0] DMA_r_descirptor_req_len_buf_dout, // {RESERVED, End_of_DMA, Req_Length[15:0]}
	output reg DMA_r_descirptor_req_len_buf_rd_en,
	input DMA_r_descirptor_req_len_buf_empty,
	// from RX_sequencing
	input [129:0] RX_buf_seq_dout, // {DW_count[1:0], data[127:0]}, valid data should begin at MSB
	output reg RX_buf_seq_rd_en,
	input RX_buf_seq_empty,
	// to host2board_buf
	output reg [131:0] host2board_buf_din, // {DW_valid[3:0], data[127:0]}
	output reg host2board_buf_wr_en,
	input host2board_buf_prog_full
);

	
	// descriptor req DW counter, count how many DWs is required
	reg [63:0] descriptor_DW_counter;
	reg descriptor_all_counted; // assert this when all the descriptors has been counted
	always @(posedge sys_clk)begin
		if(~sys_rst_n || dma_r_rst)begin
			descriptor_DW_counter <= 0;
			descriptor_all_counted <= 0;
			DMA_r_descirptor_req_len_buf_rd_en <= 1'b0;
		end
		else begin
			if(DMA_r_descirptor_req_len_buf_empty == 1'b0)begin
				DMA_r_descirptor_req_len_buf_rd_en <= 1'b1;
			end
			else begin
				DMA_r_descirptor_req_len_buf_rd_en <= 1'b0;
			end
			if(DMA_r_descirptor_req_len_buf_rd_en == 1'b1 && DMA_r_descirptor_req_len_buf_empty == 1'b0 &&
				descriptor_all_counted == 1'b0)begin
				descriptor_DW_counter <= descriptor_DW_counter + DMA_r_descirptor_req_len_buf_dout[15:0];
				if(DMA_r_descirptor_req_len_buf_dout[16] == 1'b1)begin
					descriptor_all_counted <= 1'b1;
				end
				else begin
					// do nothing
				end
			end
			else begin
				// do nothing
			end
		end
	end
	
	// data packer, pack the data from RX_buf_seq to host2board_buf
	reg [95:0] RX_buf_seq_data_dly; // valid DWs begins at MSB
	reg [1:0] RX_buf_seq_data_dly_valid_count; // How many DWs are valid in RX_buf_seq_data_dly
	reg [63:0] received_DW_counter;
	reg flush; // when only the last DW is left, and the DW is in RX_buf_seq_data_dly, assert this to flush it to host2board_buf
	always @(posedge sys_clk)begin
		if(~sys_rst_n || dma_r_rst)begin
			RX_buf_seq_data_dly <= 96'd0;
			RX_buf_seq_data_dly_valid_count <= 2'd0;
			received_DW_counter <= 64'd0;
			RX_buf_seq_rd_en <= 1'b0;
			host2board_buf_din <= 132'd0;
			host2board_buf_wr_en <= 1'b0;
			DMA_r_done_state <= 1'b0;
			flush <= 1'b0;
		end
		else begin
			// for flush and dma_r_done logic
			if(RX_buf_seq_data_dly_valid_count != 0 && received_DW_counter + RX_buf_seq_data_dly_valid_count >= descriptor_DW_counter && 
				descriptor_all_counted == 1'b1 && flush == 1'b0)begin
				flush <= 1'b1;
			end
			else begin
				flush <= 1'b0;
			end
			if(received_DW_counter >= descriptor_DW_counter && descriptor_all_counted == 1'b1)begin
				DMA_r_done_state <= 1'b1;
			end
			else begin
				DMA_r_done_state <= 1'b0;
			end
			// for read enable signal
			if(RX_buf_seq_empty == 1'b0 && host2board_buf_prog_full == 1'b0 && flush == 1'b0)begin
				RX_buf_seq_rd_en <= 1'b1;
			end
			else begin
				RX_buf_seq_rd_en <= 1'b0;
			end
			// write to host2board_buf
			if(flush == 1'b1)begin
				received_DW_counter <= received_DW_counter + RX_buf_seq_data_dly_valid_count;
				host2board_buf_wr_en <= 1'b1;
				host2board_buf_din <= {((RX_buf_seq_data_dly_valid_count == 2'd1) ? 4'b1000 : 
												(RX_buf_seq_data_dly_valid_count == 2'd2) ? 4'b1100 : 
												(RX_buf_seq_data_dly_valid_count == 2'd3) ? 4'b1110 : 4'b0000), 
												RX_buf_seq_data_dly, 32'd0};
				RX_buf_seq_data_dly_valid_count <= 2'd0;
			end
			else if(RX_buf_seq_empty == 1'b0 && RX_buf_seq_rd_en == 1'b1)begin
				case({RX_buf_seq_data_dly_valid_count, RX_buf_seq_dout[129:128]})
					4'b0000:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_dout[127:0]};
					end
					4'b0001:begin
						host2board_buf_wr_en <= 1'b0;
						RX_buf_seq_data_dly <= {RX_buf_seq_dout[127:96], 64'd0};
						RX_buf_seq_data_dly_valid_count <= 2'd1;
					end
					4'b0010:begin
						host2board_buf_wr_en <= 1'b0;
						RX_buf_seq_data_dly <= {RX_buf_seq_dout[127:64], 32'd0};
						RX_buf_seq_data_dly_valid_count <= 2'd2;
					end
					4'b0011:begin
						host2board_buf_wr_en <= 1'b0;
						RX_buf_seq_data_dly <= {RX_buf_seq_dout[127:32]};
						RX_buf_seq_data_dly_valid_count <= 2'd3;
					end
					4'b0100:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly[95:64], RX_buf_seq_dout[127:32]};
						RX_buf_seq_data_dly <= {RX_buf_seq_dout[31:0], 64'd0};
						RX_buf_seq_data_dly_valid_count <= 2'd1;
					end
					4'b0101:begin
						host2board_buf_wr_en <= 1'b0;
						RX_buf_seq_data_dly <= {RX_buf_seq_data_dly[95:64], RX_buf_seq_dout[127:96], 32'd0};
						RX_buf_seq_data_dly_valid_count <= 2'd2;
					end
					4'b0110:begin
						host2board_buf_wr_en <= 1'b0;
						RX_buf_seq_data_dly <= {RX_buf_seq_data_dly[95:64], RX_buf_seq_dout[127:64]};
						RX_buf_seq_data_dly_valid_count <= 2'd3;
					end
					4'b0111:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly[95:64], RX_buf_seq_dout[127:32]};
						RX_buf_seq_data_dly_valid_count <= 2'd0;
					end
					4'b1000:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly[95:32], RX_buf_seq_dout[127:64]};
						RX_buf_seq_data_dly <= {RX_buf_seq_dout[63:0], 32'd0};
						RX_buf_seq_data_dly_valid_count <= 2'd2;
					end
					4'b1001:begin
						host2board_buf_wr_en <= 1'b0;
						RX_buf_seq_data_dly <= {RX_buf_seq_data_dly[95:32], RX_buf_seq_dout[127:96]};
						RX_buf_seq_data_dly_valid_count <= 2'd3;
					end
					4'b1010:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly[95:32], RX_buf_seq_dout[127:64]};
						RX_buf_seq_data_dly_valid_count <= 2'd0;
					end
					4'b1011:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly[95:32], RX_buf_seq_dout[127:64]};
						RX_buf_seq_data_dly <= {RX_buf_seq_dout[63:32], 64'd0};
						RX_buf_seq_data_dly_valid_count <= 2'd1;
					end
					4'b1100:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly, RX_buf_seq_dout[127:96]};
						RX_buf_seq_data_dly <= RX_buf_seq_dout[95:0];
						RX_buf_seq_data_dly_valid_count <= 2'd3;
					end
					4'b1101:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly, RX_buf_seq_dout[127:96]};
						RX_buf_seq_data_dly_valid_count <= 2'd0;
					end
					4'b1110:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly, RX_buf_seq_dout[127:96]};
						RX_buf_seq_data_dly <= RX_buf_seq_dout[95:0];
						RX_buf_seq_data_dly_valid_count <= 2'd1;
					end
					4'b1111:begin
						received_DW_counter <= received_DW_counter + 4;
						host2board_buf_wr_en <= 1'b1;
						host2board_buf_din <= {4'b1111, RX_buf_seq_data_dly, RX_buf_seq_dout[127:96]};
						RX_buf_seq_data_dly <= RX_buf_seq_dout[95:0];
						RX_buf_seq_data_dly_valid_count <= 2'd2;
					end
				endcase
			end
			else begin
				host2board_buf_wr_en <= 1'b0;
			end
		end
	end
	
	// timeout logic
	reg [63:0] timeout_counter;
	always @(posedge sys_clk)begin
		if(~sys_rst_n || dma_r_rst)begin
			DMA_r_error_state <= 1'b0;
			timeout_counter <= 0;
		end
		else begin
			if(DMA_r_start_state == 1'b1 && DMA_r_done_state == 1'b0 && DMA_r_error_state == 1'b0)begin
				timeout_counter <= timeout_counter + 1'b1;
				if(timeout_counter > DMA_r_timeout_threshold)begin
					DMA_r_error_state <= 1'b1;
				end
			end
			else begin
				timeout_counter <= 0;
			end
		end
	end


endmodule
