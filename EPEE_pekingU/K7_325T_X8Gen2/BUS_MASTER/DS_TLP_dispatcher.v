`timescale 1ns / 1ps
/////////////////////////////////////
//
// DS_TLP_dispatcher.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Downstream TLP dispatcher. Dispatch the downstream TLPs to PIO MWr / MRd channel and 
// DMA CplD channel.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

`define DS_DIS_INIT     8'b00000001
`define DS_DIS_MWR32_H2 8'b00000010
`define DS_DIS_MWR64_H2 8'b00000100
`define DS_DIS_MWR64_D0 8'b00001000
`define DS_DIS_MWR64_WT 8'b00010000
`define DS_DIS_MRD      8'b00100000
`define DS_DIS_CPLD     8'b01000000
`define DS_DIS_UNKNOWN  8'b10000000

module DS_TLP_dispatcher(
	input sys_clk,
	input sys_rst_n,
	// from DS_CTRL
	input  [133:0] DS_TLP_BUF_DIN,
	output DS_TLP_BUF_PROG_FULL,
	input  DS_TLP_BUF_WR_EN,
	// to PIO_ENGINE
	output reg [71:0] PIO_MWR_channel_buf_din,
	input      PIO_MWR_channel_buf_prog_full,
	output reg PIO_MWR_channel_buf_wr_en,
	output reg [71:0] PIO_MRD_channel_buf_din,
	input      PIO_MRD_channel_buf_prog_full,
	output reg PIO_MRD_channel_buf_wr_en,
	// to DMS_ENGINE
	output reg [133:0] DMA_CPLD_channel_buf_din,
	input      DMA_CPLD_channel_buf_prog_full,
	output reg DMA_CPLD_channel_buf_wr_en
);

	// local reg & wires for DS_TLP_BUF
	reg  DS_TLP_BUF_rd_en;
	wire [133:0] DS_TLP_BUF_dout;
	wire DS_TLP_BUF_empty;
	// for state machine
	(*KEEP="TRUE"*)reg [7:0] DS_DIS_state;

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			PIO_MWR_channel_buf_din <= 72'd0;
			PIO_MWR_channel_buf_wr_en <= 1'b0;
			PIO_MRD_channel_buf_din <= 72'd0;
			PIO_MRD_channel_buf_wr_en <= 1'b0;
			DMA_CPLD_channel_buf_din <= 134'd0;
			DMA_CPLD_channel_buf_wr_en <= 1'b0;
			DS_TLP_BUF_rd_en <= 1'b0;
			DS_DIS_state <= `DS_DIS_INIT;
		end
		else begin
			case(DS_DIS_state)
				`DS_DIS_INIT:begin
					if(DS_TLP_BUF_empty == 1'b0)begin
						case(DS_TLP_BUF_dout[127:120])
							`FT_MWR32:begin
								if(PIO_MWR_channel_buf_prog_full == 1'b0)begin
									PIO_MWR_channel_buf_din <= {4'd0, 2'b11, 2'b10, DS_TLP_BUF_dout[127:64]};
									PIO_MWR_channel_buf_wr_en <= 1'b1;
									DS_TLP_BUF_rd_en <= 1'b1;
									DS_DIS_state <= `DS_DIS_MWR32_H2;
								end
								else begin
									PIO_MWR_channel_buf_din <= 72'd0;
									PIO_MWR_channel_buf_wr_en <= 1'b0;
									DS_TLP_BUF_rd_en <= 1'b0;
									DS_DIS_state <= `DS_DIS_INIT;
								end
								PIO_MRD_channel_buf_din <= 72'd0;
								PIO_MRD_channel_buf_wr_en <= 1'b0;
								DMA_CPLD_channel_buf_din <= 134'd0;
								DMA_CPLD_channel_buf_wr_en <= 1'b0;
							end
							`FT_MWR64:begin
								if(PIO_MWR_channel_buf_prog_full == 1'b0)begin
									PIO_MWR_channel_buf_din <= {4'd0, 2'b11, 2'b10, DS_TLP_BUF_dout[127:64]};
									PIO_MWR_channel_buf_wr_en <= 1'b1;
									DS_TLP_BUF_rd_en <= 1'b1;
									DS_DIS_state <= `DS_DIS_MWR64_H2;
								end
								else begin
									PIO_MWR_channel_buf_din <= 72'd0;
									PIO_MWR_channel_buf_wr_en <= 1'b0;
									DS_TLP_BUF_rd_en <= 1'b0;
									DS_DIS_state <= `DS_DIS_INIT;
								end
								PIO_MRD_channel_buf_din <= 72'd0;
								PIO_MRD_channel_buf_wr_en <= 1'b0;
								DMA_CPLD_channel_buf_din <= 134'd0;
								DMA_CPLD_channel_buf_wr_en <= 1'b0;
							end
							`FT_MRD32:begin
								if(PIO_MRD_channel_buf_prog_full == 1'b0)begin
									PIO_MRD_channel_buf_din <= {4'd0, 2'b11, 2'b10, DS_TLP_BUF_dout[127:64]};
									PIO_MRD_channel_buf_wr_en <= 1'b1;
									DS_TLP_BUF_rd_en <= 1'b1;
									DS_DIS_state <= `DS_DIS_MRD;
								end
								else begin
									PIO_MRD_channel_buf_din <= 72'd0;
									PIO_MRD_channel_buf_wr_en <= 1'b0;
									DS_TLP_BUF_rd_en <= 1'b0;
									DS_DIS_state <= `DS_DIS_INIT;
								end
								PIO_MWR_channel_buf_din <= 72'd0;
								PIO_MWR_channel_buf_wr_en <= 1'b0;
								DMA_CPLD_channel_buf_din <= 134'd0;
								DMA_CPLD_channel_buf_wr_en <= 1'b0;
							end
							`FT_MRD64:begin
								if(PIO_MRD_channel_buf_prog_full == 1'b0)begin
									PIO_MRD_channel_buf_din <= {4'd0, 2'b11, 2'b10, DS_TLP_BUF_dout[127:64]};
									PIO_MRD_channel_buf_wr_en <= 1'b1;
									DS_TLP_BUF_rd_en <= 1'b1;
									DS_DIS_state <= `DS_DIS_MRD;
								end
								else begin
									PIO_MRD_channel_buf_din <= 72'd0;
									PIO_MRD_channel_buf_wr_en <= 1'b0;
									DS_TLP_BUF_rd_en <= 1'b0;
									DS_DIS_state <= `DS_DIS_INIT;
								end
								PIO_MWR_channel_buf_din <= 72'd0;
								PIO_MWR_channel_buf_wr_en <= 1'b0;
								DMA_CPLD_channel_buf_din <= 134'd0;
								DMA_CPLD_channel_buf_wr_en <= 1'b0;
							end
							`FT_CPLD:begin
								PIO_MWR_channel_buf_din <= 72'd0;
								PIO_MWR_channel_buf_wr_en <= 1'b0;
								PIO_MRD_channel_buf_din <= 72'd0;
								PIO_MRD_channel_buf_wr_en <= 1'b0;
								if(DMA_CPLD_channel_buf_prog_full == 1'b0)begin
									DS_TLP_BUF_rd_en <= 1'b1;
									DS_DIS_state <= `DS_DIS_CPLD;
								end
								else begin
									DS_TLP_BUF_rd_en <= 1'b0;
									DS_DIS_state <= `DS_DIS_INIT;
								end
								DMA_CPLD_channel_buf_din <= 134'd0;
								DMA_CPLD_channel_buf_wr_en <= 1'b0;
							end
							default:begin
								PIO_MWR_channel_buf_din <= 72'd0;
								PIO_MWR_channel_buf_wr_en <= 1'b0;
								PIO_MRD_channel_buf_din <= 72'd0;
								PIO_MRD_channel_buf_wr_en <= 1'b0;
								DMA_CPLD_channel_buf_din <= 134'd0;
								DMA_CPLD_channel_buf_wr_en <= 1'b0;
								DS_TLP_BUF_rd_en <= 1'b1;
								DS_DIS_state <= `DS_DIS_UNKNOWN;
							end
						endcase
					end
					else begin
						PIO_MWR_channel_buf_din <= 72'd0;
						PIO_MWR_channel_buf_wr_en <= 1'b0;
						PIO_MRD_channel_buf_din <= 72'd0;
						PIO_MRD_channel_buf_wr_en <= 1'b0;
						DMA_CPLD_channel_buf_din <= 134'd0;
						DMA_CPLD_channel_buf_wr_en <= 1'b0;
						DS_TLP_BUF_rd_en <= 1'b0;
						DS_DIS_state <= `DS_DIS_INIT;
					end
				end
				`DS_DIS_MWR32_H2:begin
					PIO_MWR_channel_buf_din <= {4'd0, 2'b11, 2'b01, DS_TLP_BUF_dout[63:0]};
					PIO_MWR_channel_buf_wr_en <= 1'b1;
					DS_TLP_BUF_rd_en <= 1'b0;
					DS_DIS_state <= `DS_DIS_INIT;
					PIO_MRD_channel_buf_din <= 72'd0;
					PIO_MRD_channel_buf_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_din <= 134'd0;
					DMA_CPLD_channel_buf_wr_en <= 1'b0;
				end
				`DS_DIS_MWR64_H2:begin
					PIO_MWR_channel_buf_din <= {4'd0, 2'b11, 2'b00, DS_TLP_BUF_dout[63:0]};
					PIO_MWR_channel_buf_wr_en <= 1'b1;
					DS_TLP_BUF_rd_en <= 1'b0;
					DS_DIS_state <= `DS_DIS_MWR64_D0;
					PIO_MRD_channel_buf_din <= 72'd0;
					PIO_MRD_channel_buf_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_din <= 134'd0;
					DMA_CPLD_channel_buf_wr_en <= 1'b0;
				end
				`DS_DIS_MWR64_D0:begin
					if(PIO_MWR_channel_buf_prog_full == 1'b0 && DS_TLP_BUF_empty == 1'b0)begin
						PIO_MWR_channel_buf_din <= {4'd0, 2'b10, 2'b01, DS_TLP_BUF_dout[127:64]};
						PIO_MWR_channel_buf_wr_en <= 1'b1;
						DS_TLP_BUF_rd_en <= 1'b1;
						DS_DIS_state <= `DS_DIS_MWR64_WT;
					end
					else begin
						PIO_MWR_channel_buf_din <= 72'd0;
						PIO_MWR_channel_buf_wr_en <= 1'b0;
						DS_TLP_BUF_rd_en <= 1'b0;
						DS_DIS_state <= `DS_DIS_MWR64_D0;
					end
					PIO_MRD_channel_buf_din <= 72'd0;
					PIO_MRD_channel_buf_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_din <= 134'd0;
					DMA_CPLD_channel_buf_wr_en <= 1'b0;
				end
				`DS_DIS_MWR64_WT:begin
					PIO_MWR_channel_buf_din <= 72'd0;
					PIO_MWR_channel_buf_wr_en <= 1'b0;
					PIO_MRD_channel_buf_din <= 72'd0;
					PIO_MRD_channel_buf_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_din <= 134'd0;
					DMA_CPLD_channel_buf_wr_en <= 1'b0;
					DS_TLP_BUF_rd_en <= 1'b0;
					DS_DIS_state <= `DS_DIS_INIT;
				end
				`DS_DIS_MRD:begin
					PIO_MWR_channel_buf_din <= 72'd0;
					PIO_MWR_channel_buf_wr_en <= 1'b0;
					PIO_MRD_channel_buf_din <= {4'd0, DS_TLP_BUF_dout[131:130], 2'b01, DS_TLP_BUF_dout[63:0]};
					PIO_MRD_channel_buf_wr_en <= 1'b1;
					DMA_CPLD_channel_buf_din <= 134'd0;
					DMA_CPLD_channel_buf_wr_en <= 1'b0;
					DS_TLP_BUF_rd_en <= 1'b0;
					DS_DIS_state <= `DS_DIS_INIT;
				end
				`DS_DIS_CPLD:begin
					if(DMA_CPLD_channel_buf_prog_full == 1'b0 && DS_TLP_BUF_empty == 1'b0 && 
						(DS_TLP_BUF_dout[128] == 1'b0 || (DS_TLP_BUF_dout[128] == 1'b1 && DS_TLP_BUF_rd_en == 1'b0)))begin
						DS_TLP_BUF_rd_en <= 1'b1;
					end
					else begin
						DS_TLP_BUF_rd_en <= 1'b0;
					end
					if(DS_TLP_BUF_rd_en == 1'b1 && DS_TLP_BUF_empty == 1'b0)begin
						DMA_CPLD_channel_buf_din <= DS_TLP_BUF_dout;
						DMA_CPLD_channel_buf_wr_en <= 1'b1;
						if(DS_TLP_BUF_dout[128] == 1'b1)
							DS_DIS_state <= `DS_DIS_INIT;
						else
							DS_DIS_state <= `DS_DIS_CPLD;
					end
					else begin
						DMA_CPLD_channel_buf_din <= 134'd0;
						DMA_CPLD_channel_buf_wr_en <= 1'b0;
						DS_DIS_state <= `DS_DIS_CPLD;
					end
					PIO_MWR_channel_buf_din <= 72'd0;
					PIO_MWR_channel_buf_wr_en <= 1'b0;
					PIO_MRD_channel_buf_din <= 72'd0;
					PIO_MRD_channel_buf_wr_en <= 1'b0;
				end
				`DS_DIS_UNKNOWN:begin
					PIO_MWR_channel_buf_din <= 72'd0;
					PIO_MWR_channel_buf_wr_en <= 1'b0;
					PIO_MRD_channel_buf_din <= 72'd0;
					PIO_MRD_channel_buf_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_din <= 134'd0;
					DMA_CPLD_channel_buf_wr_en <= 1'b0;
					if(DS_TLP_BUF_dout[128] == 1'b1)begin
						DS_TLP_BUF_rd_en <= 1'b0;
						DS_DIS_state <= `DS_DIS_INIT;
					end
					else begin
						DS_TLP_BUF_rd_en <= 1'b1;
						DS_DIS_state <= `DS_DIS_UNKNOWN;
					end
				end
				default:begin
					PIO_MWR_channel_buf_din <= 72'd0;
					PIO_MWR_channel_buf_wr_en <= 1'b0;
					PIO_MRD_channel_buf_din <= 72'd0;
					PIO_MRD_channel_buf_wr_en <= 1'b0;
					DMA_CPLD_channel_buf_din <= 134'd0;
					DMA_CPLD_channel_buf_wr_en <= 1'b0;
					DS_TLP_BUF_rd_en <= 1'b0;
					DS_DIS_state <= `DS_DIS_INIT;
				end
			endcase
		end
	end

	FIFO_sync_width134_depth2048_FWFT DS_TLP_BUF(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DS_TLP_BUF_DIN),
		.wr_en(DS_TLP_BUF_WR_EN),
		.rd_en(DS_TLP_BUF_rd_en),
		.dout(DS_TLP_BUF_dout),
		.full(),
		.empty(DS_TLP_BUF_empty),
		.prog_full(DS_TLP_BUF_PROG_FULL)
	);

endmodule
