`timescale 1ns / 1ps
/////////////////////////////////////
//
// valid_tag_buf.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// This buffer contains valid tag. Each Mrd TLP is with a tag. When a Mrd TLP is send and
// the corresponding CplD TLP(s) haven't been received, the Mrd's tag can't be reused.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module valid_tag_buf(
	input sys_clk,
	input sys_rst_n,
	input dma_r_rst, // this reset is from DMA_r_rst_gen
	// from RX_sequencing
	input [7:0] valid_tag_buf_din,
	input valid_tag_buf_wr_en,
	output valid_tag_buf_prog_full,
	// to DMA_r_TLP_head_gen
	output [7:0] valid_tag_buf_dout,
	input valid_tag_buf_rd_en,
	output valid_tag_buf_empty,
	// to DMA_r_rst_gen
	output reg [7:0] latest_free_tag
);

	always @(posedge sys_clk)begin
		if(~sys_rst_n || dma_r_rst)begin
			latest_free_tag <= 0;
		end
		else begin
			if(valid_tag_buf_wr_en == 1'b1)begin
				if(latest_free_tag != valid_tag_buf_din)begin
					latest_free_tag <= valid_tag_buf_din;
				end
				else begin
					latest_free_tag <= latest_free_tag;
				end
			end
		end
	end

	FIFO_sync_width8_depth32_FWFT valid_tag_buf_fifo(
		.clk(sys_clk),
		.rst(~sys_rst_n || dma_r_rst),
		.din(valid_tag_buf_din),
		.wr_en(valid_tag_buf_wr_en),
		.rd_en(valid_tag_buf_rd_en),
		.dout(valid_tag_buf_dout),
		.full(),
		.empty(valid_tag_buf_empty),
		.prog_full(valid_tag_buf_prog_full)
	);
	
endmodule
