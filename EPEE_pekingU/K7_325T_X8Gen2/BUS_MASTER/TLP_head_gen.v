`timescale 1ns / 1ps
/////////////////////////////////////
//
// TLP_head_gen.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Generate the TLP head using DMA descriptors.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`include "TLP_FM_TYPE.v"

`define TLP_HEAD_INIT 3'b001
`define TLP_HEAD_LEN  3'b010
`define TLP_HEAD_GEN  3'b100

module TLP_head_gen(
	input sys_clk,
	input sys_rst_n,
	input dma_channel_rst,
	// from CFG_CTRL
	input [15:0] CFG_COMPLETER_ID,
	input CFG_EXT_TAG_EN,
	input [2:0] CFG_MAX_TLP_SIZE,
	input CFG_BUS_MSTR_EN,
	// from descriptor FIFO
	input      [95:0] descriptor_buf_dout,
	input      descriptor_buf_empty,
	output reg descriptor_buf_rd_en,
	// from tag FIFO
	input      [7:0] tag_buf_dout,
	input      tag_buf_empty,
	output reg tag_buf_rd_en,
	// to TLP head FIFO
	output     [127:0] tlp_head_buf_dout,
	output     tlp_head_buf_empty,
	input      tlp_head_buf_rd_en
);
	reg [127:0] tlp_head_buf_din;
	reg         tlp_head_buf_wr_en;
	wire        tlp_head_buf_prog_full;
	
	(*KEEP="TRUE"*)reg [2:0] tlp_head_state;
	reg [15:0] req_len_left; // require length in DW
	reg [63:0] req_addr_current; // when req_64bit_addr_en = 1'b0, only req_addr[31:0] will be used
	reg req_64bit_addr_en; // use 64bit address or not
	reg req_read_or_write_tlp; // 0: memory read TLP, 1: memory write TLP
	
	reg [9:0] tlp_max_len_this_time; // the next tlp's length
	reg [7:0] tlp_fm_type; // prepare the next tlp's formate and type
	reg [7:0] tlp_tag_this_time; // the next tlp's tag
	
	reg reset;
	always @(posedge sys_clk)begin
		reset <= (~sys_rst_n) || dma_channel_rst;
	end
	
	always @(posedge sys_clk)begin
		if(reset)begin
			descriptor_buf_rd_en <= 1'b0;
			tag_buf_rd_en <= 1'b0;
			tlp_head_buf_din <= 128'd0;
			tlp_head_buf_wr_en <= 1'b0;
			tlp_head_state <= `TLP_HEAD_INIT;
			
			req_len_left <= 16'd0;
			req_addr_current <= 64'd0;
			req_64bit_addr_en <= 1'b0;
			req_read_or_write_tlp <= 1'b0;
			
			tlp_max_len_this_time <= 10'd0;
			tlp_fm_type <= 8'd0;
			tlp_tag_this_time <= 8'd0;
		end
		else begin
			case(tlp_head_state)
				`TLP_HEAD_INIT:begin
					tlp_head_buf_wr_en <= 1'b0;
					if(descriptor_buf_empty == 1'b0 && CFG_BUS_MSTR_EN == 1'b1)begin
						req_len_left <= descriptor_buf_dout[79:64];
						req_addr_current <= descriptor_buf_dout[63:0];
						req_64bit_addr_en <= descriptor_buf_dout[90];
						req_read_or_write_tlp <= descriptor_buf_dout[91];
						descriptor_buf_rd_en <= 1'b1;
						tlp_head_state <= `TLP_HEAD_LEN;
					end
					else begin
						descriptor_buf_rd_en <= 1'b0;
						tlp_head_state <= `TLP_HEAD_INIT;
					end
				end
				`TLP_HEAD_LEN:begin
					tlp_head_buf_wr_en <= 1'b0;
					descriptor_buf_rd_en <= 1'b0;
					if(tag_buf_empty == 1'b0)begin
						case({req_64bit_addr_en, req_read_or_write_tlp})
							2'b00: tlp_fm_type <= `FT_MRD32;
							2'b01: tlp_fm_type <= `FT_MWR32;
							2'b10: tlp_fm_type <= `FT_MRD64;
							2'b11: tlp_fm_type <= `FT_MWR64;
						endcase
						case(CFG_MAX_TLP_SIZE)
							3'b101:begin // max tlp payload/ require length is 1024DW, support only 128DW currently
								tlp_max_len_this_time <= {req_addr_current[63:9] + 1, 7'd0} - req_addr_current[63:2];
							end
							3'b100:begin // 512 DW, support only 128DW currently
								tlp_max_len_this_time <= {req_addr_current[63:9] + 1, 7'd0} - req_addr_current[63:2];
							end
							3'b011:begin // 256 DW, support only 128DW currently
								tlp_max_len_this_time <= {req_addr_current[63:9] + 1, 7'd0} - req_addr_current[63:2];
							end
							3'b010:begin // 128DW, support only 128DW currently
								tlp_max_len_this_time <= {req_addr_current[63:9] + 1, 7'd0} - req_addr_current[63:2];
							end
							3'b001:begin // max require length is 64DW
								tlp_max_len_this_time <= {req_addr_current[63:8] + 1, 6'd0} - req_addr_current[63:2];
							end
							default:begin // max require length is 128B = 32DW
								tlp_max_len_this_time <= {req_addr_current[63:7] + 1, 5'd0} - req_addr_current[63:2];
							end
						endcase
						tag_buf_rd_en <= 1'b1;
						tlp_tag_this_time <= tag_buf_dout;
						tlp_head_state <= `TLP_HEAD_GEN;
					end
					else begin
						tag_buf_rd_en <= 1'b0;
						tlp_head_state <= `TLP_HEAD_LEN;
					end
				end
				`TLP_HEAD_GEN:begin
					descriptor_buf_rd_en <= 1'b0;
					tag_buf_rd_en <= 1'b0;
					if(tlp_head_buf_prog_full == 1'b0)begin
						if(req_len_left == 0)begin // zero read tlp
							tlp_head_buf_din <= {tlp_fm_type,
														14'd0, // R-TC[2:0]-R-ATTR2-R-TH-TD-EP-ATTR[1:0]-AT
														10'd1,
														CFG_COMPLETER_ID,
														tlp_tag_this_time, // tag
														4'b0000,// last be
														4'b0000,// first be
														((req_64bit_addr_en == 1'b1)?req_addr_current:{req_addr_current[31:0], 32'd0})// address
														};
						end
						else begin
							tlp_head_buf_din <= {tlp_fm_type,
														14'd0, // R-TC[2:0]-R-ATTR2-R-TH-TD-EP-ATTR[1:0]-AT
														((req_len_left < tlp_max_len_this_time)?req_len_left[9:0]:tlp_max_len_this_time),
														CFG_COMPLETER_ID,
														tlp_tag_this_time, // tag
														((tlp_max_len_this_time == 1 || req_len_left == 1'b1)?4'b0000:4'b1111),// last be
														4'b1111,// first be
														((req_64bit_addr_en == 1'b1)?req_addr_current:{req_addr_current[31:0], 32'd0})// address
														};
						end
						tlp_head_buf_wr_en <= 1'b1;
						req_addr_current <= req_addr_current + {tlp_max_len_this_time, 2'b00}; // req_addr_current is only valid when this descriptor's TLP not end
						req_len_left <= req_len_left - tlp_max_len_this_time; // it is only valid when this descriptor's TLP not end, so needn't to consider the case that req_len_left < tlp_max_len_this_time
						if(req_len_left <= tlp_max_len_this_time)begin // end of this descriptor
							tlp_head_state <= `TLP_HEAD_INIT;
						end
						else begin // this descriptor's TLP is not end
							tlp_head_state <= `TLP_HEAD_LEN;
						end
					end
					else begin
						tlp_head_buf_wr_en <= 1'b0;
					end
				end
				default:begin
					descriptor_buf_rd_en <= 1'b0;
					tlp_head_buf_wr_en <= 1'b0;
					tlp_head_state <= `TLP_HEAD_INIT;
				end
			endcase
		end
	end

	FIFO_sync_width128_depth16_FWFT tlp_head_buf(
		.clk(sys_clk),
		.rst(reset),
		.din(tlp_head_buf_din),
		.wr_en(tlp_head_buf_wr_en),
		.rd_en(tlp_head_buf_rd_en),
		.dout(tlp_head_buf_dout),
		.full(),
		.empty(tlp_head_buf_empty),
		.prog_full(tlp_head_buf_prog_full)
	);

endmodule
