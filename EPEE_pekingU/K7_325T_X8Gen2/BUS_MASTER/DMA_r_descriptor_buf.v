`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_r_descriptor_buf.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Descriptor buffer of DMA host to board (DMA read) side.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_r_descriptor_buf(
	input sys_clk,
	input sys_rst_n,
	input dma_r_rst,
	// from DMA_host2board_descriptor_engine
	input [95:0] DMA_r_descriptor_buf_din,
	input DMA_r_descriptor_buf_wr_en,
	output DMA_r_descriptor_buf_prog_full,
	// to DMA_r_TLP_head_gen
	output [95:0] DMA_r_descriptor_buf_dout,
	input DMA_r_descriptor_buf_rd_en,
	output DMA_r_descriptor_buf_empty,
	// to RX_data_packer
	output [17:0] DMA_r_descirptor_req_len_buf_dout, // {RESERVED, End_of_DMA, Req_Length}
	input DMA_r_descirptor_req_len_buf_rd_en,
	output DMA_r_descirptor_req_len_buf_empty
);

	wire DMA_r_descriptor_buf_prog_full_pre;
	wire DMA_r_descirptor_req_len_buf_prog_full;
	assign DMA_r_descriptor_buf_prog_full = DMA_r_descriptor_buf_prog_full_pre || DMA_r_descirptor_req_len_buf_prog_full;

	reg reset; // for better timing of reset
	always @(posedge sys_clk)begin
		reset <= ~sys_rst_n || dma_r_rst;
	end

	FIFO_sync_width96_depth16_FWFT DMA_r_descriptor_buf(
		.clk(sys_clk),
		.rst(reset),
		.din(DMA_r_descriptor_buf_din),
		.wr_en(DMA_r_descriptor_buf_wr_en),
		.rd_en(DMA_r_descriptor_buf_rd_en),
		.dout(DMA_r_descriptor_buf_dout),
		.full(),
		.empty(DMA_r_descriptor_buf_empty),
		.prog_full(DMA_r_descriptor_buf_prog_full_pre)
	);
	
	FIFO_sync_width18_depth32_FWFT DMA_r_descirptor_req_len_buf(
		.clk(sys_clk),
		.rst(reset),
		.din({1'b0, DMA_r_descriptor_buf_din[92], DMA_r_descriptor_buf_din[79:64]}),
		.wr_en(DMA_r_descriptor_buf_wr_en),
		.rd_en(DMA_r_descirptor_req_len_buf_rd_en),
		.dout(DMA_r_descirptor_req_len_buf_dout),
		.full(),
		.empty(DMA_r_descirptor_req_len_buf_empty),
		.prog_full(DMA_r_descirptor_req_len_buf_prog_full)
	);

endmodule
