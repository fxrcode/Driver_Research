`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_r_req_buf.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// DMA host to board requrest buffer. It buffers the DMA read's MRd information. (MRd 
// TLP's require length and the tags of those TLPs)
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_r_req_buf(
	input sys_clk,
	input sys_rst_n,
	input dma_r_rst, // this reset is from DMA_r_rst_gen
	// from DMA_r_TLP_committer
	input [17:0] dma_r_req_buf_din, // {tag[7:0], length[9:0]}
	input dma_r_req_buf_wr_en,
	output dma_r_req_buf_prog_full,
	// to RX_sequencing
	output [17:0] dma_r_req_buf_dout,
	input dma_r_req_buf_rd_en,
	output dma_r_req_buf_empty,
	// to DMA_r_rst_gen
	output reg [7:0] latest_req_tag
);

	always @(posedge sys_clk)begin
		if(~sys_rst_n || dma_r_rst)begin
			latest_req_tag <= 0;
		end
		else begin
			if(dma_r_req_buf_wr_en == 1'b1)begin
				if(latest_req_tag != dma_r_req_buf_din[17:10])begin
					latest_req_tag <= dma_r_req_buf_din[17:10];
				end
				else begin
					latest_req_tag <= latest_req_tag;
				end
			end
		end
	end
	
	FIFO_sync_width18_depth32_FWFT dma_r_req_buf_fifo(
		.clk(sys_clk),
		.rst(~sys_rst_n || dma_r_rst),
		.din(dma_r_req_buf_din),
		.wr_en(dma_r_req_buf_wr_en),
		.rd_en(dma_r_req_buf_rd_en),
		.dout(dma_r_req_buf_dout),
		.full(),
		.empty(dma_r_req_buf_empty),
		.prog_full(dma_r_req_buf_prog_full)
	);

endmodule
