`timescale 1ns / 1ps
/////////////////////////////////////
//
// SYS_CSR.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The control and status register (CSR) of the EPEE system. It includes the CSRs for 
// reset logic, DMA logic and so on.
// Other discriptoin for 128 bit version:
//                    The PCIe IP core has 1MByte Bar space. The MSB of the addr
//                    is used to choose the addr is for SYS_CSR or user CSR bus.
//                    The 2 LSB is always zero as all the registers are 32bit.
//                    So the address space of SYS_CSR is 17bit.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module SYS_CSR(
	input sys_clk,
	input sys_rst_n,
	// from PIO_ENGINE
	input      [16:0] SYS_PIO_WR_ADDR,
	input      [31:0] SYS_PIO_WR_DATA,
	input      SYS_PIO_WR_WR_EN,
	input      [16:0] SYS_PIO_RD_ADDR,
	output reg [31:0] SYS_PIO_RD_DATA,
	// for SYSTEM_RESET
	output reg RST_USR_o,
	input      RST_USR_done,
	output reg RST_HOST2BOARD_o,
	output reg RST_BOARD2HOST_o,
	input      RST_DMA_R_DONE_i,
	input      RST_DMA_W_DONE_i,
	// for DMA read/write descriptor buf address
	output reg [31:0] dma_r_descriptor_addr_l,
	output reg [31:0] dma_r_descriptor_addr_h,
	output reg [31:0] dma_w_descriptor_addr_l,
	output reg [31:0] dma_w_descriptor_addr_h,
	// for DMA read/write control
	output reg dma_r_start,
	output reg dma_r_init_descriptor_64addr_en, // use 64bit address TLP or not
	output reg [13:0] dma_r_descriptor_num,
	output reg dma_w_start,
	output reg dma_w_init_descriptor_64addr_en, // use 64bit address TLP or not
	output reg [13:0] dma_w_descriptor_num,
	// for hardware state
	input dma_r_start_state_i,
	input dma_r_done_state_i,
	input dma_r_error_state_i,
	input dma_r_int_req_state_i,
	input dma_r_int_served_state_i,
	input dma_w_start_state_i,
	input dma_w_done_state_i,
	input dma_w_error_state_i,
	input dma_w_int_req_state_i,
	input dma_w_int_served_state_i,
	input ds_ctrl_error_i,
	input us_ctrl_error_i,
	input usr_int_req_state_i,
	input [2:0] usr_int_vector_state, // indicate which user interrupt is act
	// for interrupt control
	output reg int_enable,
	output reg dma_r_int_served,
	output reg dma_w_int_served,
	output reg [7:0]usr_int_sw_waiting,
	output reg usr_int_served,
	output reg dma_r_int_disable, // DMA read interrupt will enable when int_enable = 1 and dma_r_int_disable = 0
	output reg dma_w_int_disable, // DMA write interrupt will enable when int_enable = 1 and dma_w_int_disable = 0
	// data count
	input [31:0] host2board_data_count_i,
	input [31:0] board2host_data_count_i,
	// timeout threshold
	output reg [63:0] dma_r_timeout_threshold,
	output reg [63:0] dma_w_timeout_threshold
);
	// for system_info
	wire [31:0] SYSTEM_INFO;
	assign SYSTEM_INFO = {8'd2, 8'd0, 16'd0};
	// for reset
	reg RST_DMA_R_DONE;
	reg RST_DMA_W_DONE;
	// regs for DMA read/write control
	reg dma_r_is_blocking;
	reg dma_w_is_blocking;
	// regs for hardware state
	reg dma_r_start_state;
	reg dma_r_is_blocking_state;
	reg dma_r_done_state;
	reg dma_r_error_state;
	reg dma_r_int_req_state;
	reg dma_r_int_served_state;
	reg dma_w_start_state;
	reg dma_w_is_blocking_state;
	reg dma_w_done_state;
	reg dma_w_error_state;
	reg dma_w_int_req_state;
	reg dma_w_int_served_state;
	reg usr_int_req_state;
	reg ds_ctrl_error;
	reg us_ctrl_error;
	// data count
	reg [31:0] host2board_data_count;
	reg [31:0] board2host_data_count;
	
	// for reset logic
	reg RST_SYSTEM;
	reg RST_USR;
	reg RST_HOST2BOARD;
	reg RST_BOARD2HOST;
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			RST_USR_o <= 1'b0;
			RST_HOST2BOARD_o <= 1'b0;
			RST_BOARD2HOST_o <= 1'b0;
		end
		else begin
			if(RST_SYSTEM == 1'b1)begin
				RST_USR_o <= 1'b1;
				RST_HOST2BOARD_o <= 1'b1;
				RST_BOARD2HOST_o <= 1'b1;
			end
			else begin
				RST_USR_o <= RST_USR;
				RST_HOST2BOARD_o <= RST_HOST2BOARD;
				RST_BOARD2HOST_o <= RST_BOARD2HOST;
			end
		end
	end
	
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			SYS_PIO_RD_DATA <= 32'd0;
			// #0 reg SYSTEM_INFO
			// SYSTEM_INFO is a constant
			// #1 reg SYSTEM_RESET
			RST_SYSTEM <= 1'b0;
			RST_USR <= 1'b0;
			RST_HOST2BOARD <= 1'b0;
			RST_BOARD2HOST <= 1'b0;
			RST_DMA_R_DONE <= 1'b0;
			RST_DMA_W_DONE <= 1'b0;
			// #2-5 reg initial descriptors
			dma_r_descriptor_addr_l <= 0;
			dma_r_descriptor_addr_h <= 0;
			dma_w_descriptor_addr_l <= 0;
			dma_w_descriptor_addr_h <= 0;
			// #6-7 reg dma read/write control
			dma_r_start <= 1'b0;
			dma_r_is_blocking <= 1'b0;
			dma_r_init_descriptor_64addr_en <= 1'b0; // use 64bit address TLP or not
			dma_r_descriptor_num <= 14'd0;
			dma_w_start <= 1'b0;
			dma_w_is_blocking <= 1'b0;
			dma_w_init_descriptor_64addr_en <= 1'b0; // use 64bit address TLP or not
			dma_w_descriptor_num <= 14'd0;
			// #8 reg hardware state
			dma_r_start_state <= 1'b0;
			dma_r_is_blocking_state <= 1'b0;
			dma_r_done_state <= 1'b0;
			dma_r_error_state <= 1'b0;
			dma_r_int_req_state <= 1'b0;
			dma_r_int_served_state <= 1'b0;
			dma_w_start_state <= 1'b0;
			dma_w_is_blocking_state <= 1'b0;
			dma_w_done_state <= 1'b0;
			dma_w_error_state <= 1'b0;
			dma_w_int_req_state <= 1'b0;
			dma_w_int_served_state <= 1'b0;
			usr_int_req_state <= 1'b0;
			ds_ctrl_error <= 1'b0;
			us_ctrl_error <= 1'b0;
			// #9 reg interrupt control
			int_enable <= 1'b0;
			dma_r_int_served <= 1'b0;
			dma_w_int_served <= 1'b0;
			usr_int_sw_waiting <= 8'd0;
			usr_int_served <= 1'b0;
			dma_w_int_disable <= 1'b0;
			dma_r_int_disable <= 1'b0;
			// #10-11 reg data count
			host2board_data_count <= 32'd0;
			board2host_data_count <= 32'd0;
			// #12-14 reg timeout threshold
			dma_r_timeout_threshold <= 64'hFFFF_FFFF_FFFF_FFFF;
			dma_w_timeout_threshold <= 64'hFFFF_FFFF_FFFF_FFFF;
		end
		else begin
			// PIO read
			case(SYS_PIO_RD_ADDR)
				17'd0 : SYS_PIO_RD_DATA <= SYSTEM_INFO;
				17'd1 : SYS_PIO_RD_DATA <= {25'd0, RST_USR_done, RST_DMA_W_DONE, RST_DMA_R_DONE, RST_BOARD2HOST ,RST_HOST2BOARD , RST_USR, RST_SYSTEM};
				17'd2 : SYS_PIO_RD_DATA <= dma_r_descriptor_addr_l;
				17'd3 : SYS_PIO_RD_DATA <= dma_r_descriptor_addr_h;
				17'd4 : SYS_PIO_RD_DATA <= dma_w_descriptor_addr_l;
				17'd5 : SYS_PIO_RD_DATA <= dma_w_descriptor_addr_h;
				17'd6 : SYS_PIO_RD_DATA <= {2'd0, dma_r_descriptor_num, 13'd0, dma_r_init_descriptor_64addr_en, dma_r_is_blocking, dma_r_start};
				17'd7 : SYS_PIO_RD_DATA <= {2'd0, dma_w_descriptor_num, 13'd0, dma_w_init_descriptor_64addr_en, dma_w_is_blocking, dma_w_start};
				17'd8 : SYS_PIO_RD_DATA <= {6'd0, 
													usr_int_vector_state[2:0], 
													usr_int_sw_waiting[7:0],
													usr_int_req_state,
													us_ctrl_error, 
													ds_ctrl_error, 
													dma_w_int_served_state,
													dma_w_int_req_state,
													dma_w_error_state,
													dma_w_done_state,
													dma_w_is_blocking_state,
													dma_w_start_state,
													dma_r_int_served_state,
													dma_r_int_req_state,
													dma_r_error_state,
													dma_r_done_state,
													dma_r_is_blocking_state,
													dma_r_start_state};
				17'd9 : SYS_PIO_RD_DATA <= {26'd0,
													dma_w_int_disable,
													dma_r_int_disable,
													usr_int_served,
													dma_w_int_served,
													dma_r_int_served,
													int_enable};
				17'd10: SYS_PIO_RD_DATA <= {24'd0,
													usr_int_sw_waiting[7:0]};
				17'd11: SYS_PIO_RD_DATA <= host2board_data_count;
				17'd12: SYS_PIO_RD_DATA <= board2host_data_count;
				17'd13: SYS_PIO_RD_DATA <= dma_r_timeout_threshold[31:0];
				17'd14: SYS_PIO_RD_DATA <= dma_r_timeout_threshold[63:32];
				17'd15: SYS_PIO_RD_DATA <= dma_w_timeout_threshold[31:0];
				17'd16: SYS_PIO_RD_DATA <= dma_w_timeout_threshold[63:32];
				default: SYS_PIO_RD_DATA <= 32'd0;
			endcase
			// PIO write
			if(SYS_PIO_WR_WR_EN == 1'b1)begin
				case(SYS_PIO_WR_ADDR)
					17'd0:begin
						// SYSTEM_INFO is read only
					end
					17'd1:begin
						// reg #1, system reset
						RST_SYSTEM <= SYS_PIO_WR_DATA[0];
						RST_USR <= SYS_PIO_WR_DATA[1];
						RST_HOST2BOARD <= SYS_PIO_WR_DATA[2];
						RST_BOARD2HOST <= SYS_PIO_WR_DATA[3];
					end
					17'd2:begin
						// reg #2, DMA read descriptor address low
						dma_r_descriptor_addr_l <= SYS_PIO_WR_DATA;
					end
					17'd3:begin
						// reg #3, DMA read descriptor address high
						dma_r_descriptor_addr_h <= SYS_PIO_WR_DATA;
					end
					17'd4:begin
						// reg #4, DMA write descriptor address low
						dma_w_descriptor_addr_l <= SYS_PIO_WR_DATA;
					end
					17'd5:begin
						// reg #5, DMA write descriptor address high
						dma_w_descriptor_addr_h <= SYS_PIO_WR_DATA;
					end
					17'd6:begin
						// reg #6, DMA_R_CTRL
						dma_r_descriptor_num <= SYS_PIO_WR_DATA[29:16];
						dma_r_init_descriptor_64addr_en <= SYS_PIO_WR_DATA[2];
						dma_r_is_blocking <= SYS_PIO_WR_DATA[1];
						dma_r_start <= SYS_PIO_WR_DATA[0];
					end
					17'd7:begin
						// reg #7, DMA_W_CTRL
						dma_w_descriptor_num <= SYS_PIO_WR_DATA[29:16];
						dma_w_init_descriptor_64addr_en <= SYS_PIO_WR_DATA[2];
						dma_w_is_blocking <= SYS_PIO_WR_DATA[1];
						dma_w_start <= SYS_PIO_WR_DATA[0];
					end
					17'd8:begin
						// reg #8, hardware state, is read only for sofware
					end
					17'd9:begin
						// reg #9, interrupt control register
						dma_w_int_disable <= SYS_PIO_WR_DATA[5];
						dma_r_int_disable <= SYS_PIO_WR_DATA[4];
						usr_int_served <= SYS_PIO_WR_DATA[3];
						dma_w_int_served <= SYS_PIO_WR_DATA[2];
						dma_r_int_served <= SYS_PIO_WR_DATA[1];
						int_enable <= SYS_PIO_WR_DATA[0];
					end
					17'd10:begin
						// reg #10 User interrupt control
						usr_int_sw_waiting[SYS_PIO_WR_DATA[2:0]] <= SYS_PIO_WR_DATA[3];
					end
					17'd11:begin
						// reg #11 host2board_data_count is read only for software
					end
					17'd12:begin
						// reg #12 board2host_data_count is read only for software
					end
					17'd13:begin
						// reg #13, dma read timeout threshold lower 32bit
						dma_r_timeout_threshold[31:0] <= SYS_PIO_WR_DATA;
					end
					17'd14:begin
						// reg #14, dma read timeout threshold higher 32bit
						dma_r_timeout_threshold[63:32] <= SYS_PIO_WR_DATA;
					end
					17'd15:begin
						// reg #15, dma write timeout threshold lower 32bit
						dma_w_timeout_threshold[31:0] <= SYS_PIO_WR_DATA;
					end
					17'd16:begin
						// reg #16, dma write timeout threshold higher 32bit
						dma_w_timeout_threshold[63:32] <= SYS_PIO_WR_DATA;
					end
					default:begin
						// do nothing
					end
				endcase
			end
			else begin
				// do nothing
			end
			
			// logic of register own
			// reg #1, reset
			RST_DMA_W_DONE <= RST_DMA_W_DONE_i;
			RST_DMA_R_DONE <= RST_DMA_R_DONE_i;
			// reg #6, dma_r_start will be cleared in one cycle
			if(dma_r_start == 1'b1)begin
				dma_r_start <= 1'b0;
			end
			// reg #7, dma_w_start will be cleared in one cycle
			if(dma_w_start == 1'b1)begin
				dma_w_start <= 1'b0;
			end
			// reg #8, states of the hardware
			us_ctrl_error <= us_ctrl_error_i; 
			ds_ctrl_error <= ds_ctrl_error_i; 
			usr_int_req_state <= usr_int_req_state_i;
			dma_w_int_served_state <= dma_w_int_served_state_i;
			dma_w_int_req_state <= dma_w_int_req_state_i;
			dma_w_error_state <= dma_w_error_state_i;
			dma_w_done_state <= dma_w_done_state_i;
			dma_w_is_blocking_state <= dma_w_is_blocking;
			dma_w_start_state <= dma_w_start_state_i;
			dma_r_int_served_state <= dma_r_int_served_state_i;
			dma_r_int_req_state <= dma_r_int_req_state_i;
			dma_r_error_state <= dma_r_error_state_i;
			dma_r_done_state <= dma_r_done_state_i;
			dma_r_is_blocking_state <= dma_r_is_blocking;
			dma_r_start_state <= dma_r_start_state_i;
			// reg #9, interrupt control
			if(usr_int_served == 1'b1)begin
				usr_int_served <= 1'b0;
				usr_int_sw_waiting[usr_int_vector_state] <= 1'b0;
			end
			if(dma_w_int_served == 1'b1)begin
				dma_w_int_served <= 1'b0;
			end
			if(dma_r_int_served == 1'b1)begin
				dma_r_int_served <= 1'b0;
			end
			// reg #10
			if(usr_int_served == 1'b1)begin
				usr_int_sw_waiting[usr_int_vector_state] <= 1'b0;
			end
			// reg #11
			host2board_data_count <= host2board_data_count_i;
			// reg #12
			board2host_data_count <= board2host_data_count_i;
		end
	end

endmodule
