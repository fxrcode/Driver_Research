`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_w_zero_read_TLP_waiter.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// This is a FSM waiting for zero read TLP in DMA board to host process. The last TLP in 
// DMA board to host is a zero read TLP, which is used for verifing all the MWr TLP has
// been received and processed by PCIe RP.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////
`define ZERO_READ_INIT 3'b001
`define ZERO_READ_WAIT 3'b010
`define ZERO_READ_ACK  3'b100

// timeout threshold is about 1 seconds when using 250MHz clk
`define ZERO_READ_TIMEOUT_THRESHOLD 64'd250000000

module DMA_w_zero_read_LTP_waiter(
	input sys_clk,
	input sys_rst_n,
	input dma_board2host_rst,
	// from DMA_w_zero_read_TLP_buf
	input [71:0] DMA_w_zero_read_TLP_din,
	input DMA_w_zero_read_TLP_wr_en,
	output DMA_w_zero_read_TLP_prog_full,
	// from DMA_w_MWR_TLP_committer
	input DMA_w_zero_read_TLP_sent,
	output reg DMA_w_zero_read_TLP_acked,
	// connect with SYS_CSR
	input [63:0] DMA_w_timeout_threshold,
	input DMA_w_start_state,
	output reg DMA_w_done_state, // it will be cleared by reset
	output reg DMA_w_error_state // it will be cleared by reset
);

	reg DMA_w_zero_read_TLP_rd_en;
	wire DMA_w_zero_read_TLP_empty;
	
	(*KEEP="TRUE"*)reg [2:0] zero_read_state;
	reg [63:0] dma_timeout_counter;
	reg [63:0] cpld_timeout_counter;
	reg dma_timeout;
	reg cpld_timeout;
	// error state
	always @(posedge sys_clk)begin
		if(~sys_rst_n || dma_board2host_rst)begin
			DMA_w_error_state <= 1'b0;
		end
		else begin
			if(dma_timeout == 1'b1 || cpld_timeout == 1'b1)begin
				DMA_w_error_state <= 1'b1;
			end
			else begin
				DMA_w_error_state <= DMA_w_error_state;
			end
		end
	end
	// DMA timeout
	always @(posedge sys_clk)begin
		if(~sys_rst_n || dma_board2host_rst)begin
			dma_timeout_counter <= 0;
			dma_timeout <= 0;
		end
		else begin
			if(DMA_w_start_state == 1'b1 && DMA_w_done_state == 1'b0 && DMA_w_error_state == 1'b0)begin
				dma_timeout_counter <= dma_timeout_counter + 1;
				if(dma_timeout_counter > DMA_w_timeout_threshold)begin
					dma_timeout <= 1;
				end
				else begin
					dma_timeout <= 0;
				end
			end
			else begin
				dma_timeout_counter <= dma_timeout_counter;
				dma_timeout <= dma_timeout;
			end
		end
	end

	// main logic
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			zero_read_state <= `ZERO_READ_INIT;
			DMA_w_done_state <= 1'b0;
			DMA_w_zero_read_TLP_acked <= 1'b0;
			DMA_w_zero_read_TLP_rd_en <= 0;
			cpld_timeout_counter <= 0;
			cpld_timeout <= 0;
		end
		else if(dma_board2host_rst == 1'b1 && zero_read_state == `ZERO_READ_INIT)begin
			DMA_w_done_state <= 1'b0;
			DMA_w_zero_read_TLP_acked <= 1'b0;
			DMA_w_zero_read_TLP_rd_en <= 0;
			cpld_timeout_counter <= 0;
			cpld_timeout <= 0;
		end
		else begin
			case(zero_read_state)
				`ZERO_READ_INIT:begin
					DMA_w_zero_read_TLP_acked <= 1'b0;
					DMA_w_zero_read_TLP_rd_en <= 0;
					cpld_timeout_counter <= 0;
					if(DMA_w_zero_read_TLP_sent == 1'b1)begin
						zero_read_state <= `ZERO_READ_WAIT;
					end
					else begin
						zero_read_state <= `ZERO_READ_INIT;
					end
				end
				`ZERO_READ_WAIT:begin
					if(DMA_w_zero_read_TLP_empty == 1'b0)begin // CPLD received
						DMA_w_zero_read_TLP_acked <= 1'b1;
						DMA_w_zero_read_TLP_rd_en <= 1;
						zero_read_state <= `ZERO_READ_ACK;
					end
					else if(cpld_timeout_counter > `ZERO_READ_TIMEOUT_THRESHOLD)begin // timeout
						DMA_w_zero_read_TLP_acked <= 1'b1;
						DMA_w_zero_read_TLP_rd_en <= 0;
						zero_read_state <= `ZERO_READ_ACK;
						cpld_timeout <= 1'b1; // assert cpld timeout
					end
					else begin // haven't received CPLD and haven't timeout
						DMA_w_zero_read_TLP_acked <= 1'b0;
						DMA_w_zero_read_TLP_rd_en <= 0;
						cpld_timeout_counter <= cpld_timeout_counter + 1;
						zero_read_state <= `ZERO_READ_WAIT;
					end
				end
				`ZERO_READ_ACK:begin
					if(DMA_w_zero_read_TLP_sent == 1'b0)begin
						DMA_w_done_state <= (cpld_timeout == 1'b0)?1'b1:1'b0;
						DMA_w_zero_read_TLP_acked <= 1'b0;
						DMA_w_zero_read_TLP_rd_en <= 0;
						cpld_timeout_counter <= 0;
						zero_read_state <= `ZERO_READ_INIT;
					end
					else begin
						DMA_w_zero_read_TLP_acked <= 1'b1;
						DMA_w_zero_read_TLP_rd_en <= 0;
						cpld_timeout_counter <= 0;
						zero_read_state <= `ZERO_READ_ACK;
					end
				end
				default:begin
					zero_read_state <= `ZERO_READ_INIT;
				end
			endcase
		end
	end

	FIFO_sync_width72_depth16_FWFT DMA_w_zero_read_TLP(
		.clk(sys_clk),
		.rst(~sys_rst_n),
		.din(DMA_w_zero_read_TLP_din),
		.wr_en(DMA_w_zero_read_TLP_wr_en),
		.rd_en(DMA_w_zero_read_TLP_rd_en),
		.dout(),
		.full(),
		.empty(DMA_w_zero_read_TLP_empty),
		.prog_full(DMA_w_zero_read_TLP_prog_full)
	);

endmodule
