`timescale 1ns / 1ps
/////////////////////////////////////
//
// PCIE_TOP.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The top file of EPEE library
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////
module PCIE_TOP #(
	parameter PCIE_LINK_WIDTH = 1,
	parameter PCIE_LINK_GEN = 1
)
(
	// System interface for PCIe IP core
	input sys_clk_n,
	input sys_clk_p,
	input sys_reset_n,
	// PCIe interface
	output [PCIE_LINK_WIDTH-1:0] pci_exp_txp,
	output [PCIE_LINK_WIDTH-1:0] pci_exp_txn,
	input  [PCIE_LINK_WIDTH-1:0] pci_exp_rxp,
	input  [PCIE_LINK_WIDTH-1:0] pci_exp_rxn,
	
	// for user
	/*PCIe system clock and reset*/
	output trn_clk,
	output trn_reset_n,
	/*user reset, in PCIe clock domain*/
	output RST_USR,
	input  RST_USR_done,
	output RST_HOST2BOARD,
	output RST_BOARD2HOST,
	/*user PIO interface, in PCIe clock domain*/
	output [16:0] usr_pio_wr_addr,
	output [31:0] usr_pio_wr_data,
	output usr_pio_wr_req,
	input  usr_pio_wr_ack,
	output [16:0] usr_pio_rd_addr,
	input  [31:0] usr_pio_rd_data,
	output usr_pio_rd_req,
	input  usr_pio_rd_ack,
	/*user interrupt interface, in user "user_int_clk" clock domain*/
	input  usr_int_req,
	input  [2:0] usr_int_vector,
	output [7:0] usr_int_sw_waiting,
	output usr_int_clr,
	output usr_int_enable,
	/*user DMA interface, in system clock domain*/
	output [129:0] FIFO_host2board_dout,
	output FIFO_host2board_empty,
	input  FIFO_host2board_rd_en,
	input  [129:0] FIFO_board2host_din,
	output FIFO_board2host_prog_full,
	input  FIFO_board2host_wr_en
);

	// configure interface
	wire [15:0] CFG_COMPLETER_ID;
	wire CFG_EXT_TAG_EN;
	wire [2:0]  CFG_MAX_PAYLOAD_SIZE;
	wire [2:0]  CFG_MAX_RD_REQ_SIZE;
	wire CFG_BUS_MSTR_EN;
	
	// downstream interface
	wire [133:0] DS_TLP_BUF_DIN;
	wire DS_TLP_BUF_PROG_FULL;
	wire DS_TLP_BUF_WR_EN;
	wire DS_CTRL_ERROR;
	
	// upstream interface
	wire [133:0]US_TLP_BUF_DOUT;
	wire US_TLP_BUF_EMPTY;
	wire US_TLP_BUF_RD_EN;
	wire US_CTRL_ERROR;
	
	// interrupt interface
	wire INT_REQ;
	wire INT_REQ_ACK;
	wire INT_SERVED;
	wire INT_SERVED_ACK;

	CORE_WRAPPER  # (
		.PCIE_LINK_WIDTH(PCIE_LINK_WIDTH), // PCIe lane
		.PCIE_LINK_GEN(PCIE_LINK_GEN) // PCIe gen
	)
	CORE_WRAPPER(
		// System interface for PCIe IP core
		.sys_clk_n(sys_clk_n), 
		.sys_clk_p(sys_clk_p), 
		.sys_reset_n(sys_reset_n),
		// PCIe interface
		.pci_exp_txp(pci_exp_txp),
		.pci_exp_txn(pci_exp_txn),
		.pci_exp_rxp(pci_exp_rxp),
		.pci_exp_rxn(pci_exp_rxn),
		// TRN system signals
		.trn_clk(trn_clk),
		.trn_reset_n(trn_reset_n),
		// CFG_CTRL output
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID),
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN),
		.CFG_MAX_PAYLOAD_SIZE(CFG_MAX_PAYLOAD_SIZE),
		.CFG_MAX_RD_REQ_SIZE(CFG_MAX_RD_REQ_SIZE),
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN),
		// for DS_TLP_dispatcher
		.DS_TLP_BUF_DIN(DS_TLP_BUF_DIN),
		.DS_TLP_BUF_PROG_FULL(DS_TLP_BUF_PROG_FULL),
		.DS_TLP_BUF_WR_EN(DS_TLP_BUF_WR_EN),
		// from US_TLP_collector
		.US_TLP_BUF_DOUT(US_TLP_BUF_DOUT),
		.US_TLP_BUF_EMPTY(US_TLP_BUF_EMPTY),
		.US_TLP_BUF_RD_EN(US_TLP_BUF_RD_EN),
		// for INT_GEN
		.INT_REQ(INT_REQ),
		.INT_REQ_ACK(INT_REQ_ACK),
		.INT_SERVED(INT_SERVED),
		.INT_SERVED_ACK(INT_SERVED_ACK),
		// for SYS_CSR
		.DS_ERROR(DS_CTRL_ERROR),
		.US_ERROR(US_CTRL_ERROR)
	);

	BUS_MASTER BUS_MASTER(
		// clock and reset from PCIe IP core
		.sys_clk(trn_clk), 
		.sys_rst_n(trn_reset_n), 
		// from CFG_CTRL
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID), 
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN), 
		.CFG_MAX_PAYLOAD_SIZE(CFG_MAX_PAYLOAD_SIZE), 
		.CFG_MAX_RD_REQ_SIZE(CFG_MAX_RD_REQ_SIZE), 
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN), 
		// from DS_CTRL
		.DS_TLP_BUF_DIN(DS_TLP_BUF_DIN), 
		.DS_TLP_BUF_PROG_FULL(DS_TLP_BUF_PROG_FULL), 
		.DS_TLP_BUF_WR_EN(DS_TLP_BUF_WR_EN), 
		.DS_CTRL_ERROR(DS_CTRL_ERROR),
		// to US_CTRL
		.US_TLP_BUF_DOUT(US_TLP_BUF_DOUT), 
		.US_TLP_BUF_EMPTY(US_TLP_BUF_EMPTY), 
		.US_TLP_BUF_RD_EN(US_TLP_BUF_RD_EN), 
		.US_CTRL_ERROR(US_CTRL_ERROR),
		// to INT_CTRL
		.INT_REQ(INT_REQ), 
		.INT_REQ_ACK(INT_REQ_ACK), 
		.INT_SERVED(INT_SERVED), 
		.INT_SERVED_ACK(INT_SERVED_ACK), 
		// for user
		/*user reset, in PCIe clock domain*/
		.RST_USR(RST_USR), 
		.RST_USR_done(RST_USR_done),
		.RST_HOST2BOARD(RST_HOST2BOARD),
		.RST_BOARD2HOST(RST_BOARD2HOST),
		/*user PIO interface, in PCIe clock domain*/
		.usr_pio_wr_addr(usr_pio_wr_addr), 
		.usr_pio_wr_data(usr_pio_wr_data), 
		.usr_pio_wr_req(usr_pio_wr_req),
		.usr_pio_wr_ack(usr_pio_wr_ack),
		.usr_pio_rd_addr(usr_pio_rd_addr), 
		.usr_pio_rd_data(usr_pio_rd_data),
		.usr_pio_rd_req(usr_pio_rd_req),
		.usr_pio_rd_ack(usr_pio_rd_ack),
		/*user interrupt interface, in user "user_int_clk" clock domain*/	
		.usr_int_req(usr_int_req), 
		.usr_int_vector(usr_int_vector), 
		.usr_int_sw_waiting(usr_int_sw_waiting),
		.usr_int_clr(usr_int_clr), 
		.usr_int_enable(usr_int_enable), 
		/*user DMA interface, in system clock domain*/
		.FIFO_host2board_dout(FIFO_host2board_dout), 
		.FIFO_host2board_empty(FIFO_host2board_empty), 
		.FIFO_host2board_rd_en(FIFO_host2board_rd_en), 
		.FIFO_board2host_din(FIFO_board2host_din), 
		.FIFO_board2host_prog_full(FIFO_board2host_prog_full), 
		.FIFO_board2host_wr_en(FIFO_board2host_wr_en)
	);

endmodule
