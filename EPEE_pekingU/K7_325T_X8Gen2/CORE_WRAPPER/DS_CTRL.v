`timescale 1ns / 1ps
/////////////////////////////////////
//
// DS_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Downstream control: deal with the downstream TLPs
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

// timeout threshold is about 1 seconds when using 250MHz clk
`define DS_CTRL_TIMEOUT_THRESHOLD 64'd250000000

module DS_CTRL(
	input sys_clk,
	input sys_rst_n,
	// from IP core
	input  [128-1:0] m_axis_rx_tdata,
	input  [ 16-1:0] m_axis_rx_tkeep,
	input           m_axis_rx_tlast,
	input           m_axis_rx_tvalid,
	output reg      m_axis_rx_tready,
	input  [21:0]   m_axis_rx_tuser,
	// for DS_TLP_dispatcher
	output reg [133:0] DS_TLP_BUF_DIN,
	input             DS_TLP_BUF_PROG_FULL,
	output reg        DS_TLP_BUF_WR_EN,
	// for SYS_CSR
	output         DS_ERROR
);
	wire [4:0] rx_is_eof = m_axis_rx_tuser[21:17]; // rx_is_eof[4] : end of frame,   rx_is_eof[3:0] : end of frame location
	wire [4:0] rx_is_sof = m_axis_rx_tuser[14:10]; // rx_is_sof[4] : start of frame, rx_is_sof[3:0] : start of frame location
	assign DS_ERROR = m_axis_rx_tuser[1] | m_axis_rx_tuser[0];
	
	reg [65:0] last_data; // 65 : sof, 64 : eof, 63-0 : data(e.g. 63-32 : H0, 31-0 : H1)
	reg [1:0] last_data_valid; // 2'b11 : last_data[63:0] valid, 2'b10 : last_data[63:32] valid, 2'b00 : last_data is not valid
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			m_axis_rx_tready <= 1'b0;
			DS_TLP_BUF_DIN <= 134'd0;
			DS_TLP_BUF_WR_EN <= 1'd0;
			last_data <= 66'd0;
			last_data_valid <= 2'b0;
		end
		else begin
			if(last_data[64] == 1'b1)begin // flush TLP's eof into FIFO
				if(DS_TLP_BUF_PROG_FULL == 1'b1)begin
					m_axis_rx_tready <= 1'b0;
					DS_TLP_BUF_DIN <= 134'd0;
					DS_TLP_BUF_WR_EN <= 1'd0;
					last_data <= last_data;
					last_data_valid <= last_data_valid;
				end
				else begin
					m_axis_rx_tready <= 1'b1;
					DS_TLP_BUF_DIN <= {last_data_valid, 2'b00, 1'b0, 1'b1, last_data[63:0], 64'd0};
					DS_TLP_BUF_WR_EN <= 1'd1;
					last_data <= 66'd0;
					last_data_valid <= 2'b0;
				end
			end
			else begin // no eof is in reg last_data
				if(m_axis_rx_tready == 1'b1 && m_axis_rx_tvalid == 1'b1)begin
					if(last_data_valid == 2'b0)begin
						// no data is stored in reg last_data
						if(DS_TLP_BUF_PROG_FULL == 1'b1)
							m_axis_rx_tready <= 1'b0;
						else
							m_axis_rx_tready <= 1'b1;
						if(rx_is_eof[4])begin
							DS_TLP_BUF_WR_EN <= 1'd1;
							// data
							DS_TLP_BUF_DIN[127:0] <= {m_axis_rx_tdata[31:0], m_axis_rx_tdata[63:32], m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
							// DW valid
							case(rx_is_eof[3:0])
								4'd3:  DS_TLP_BUF_DIN[133:130] <= 4'b1000;
								4'd7:  DS_TLP_BUF_DIN[133:130] <= 4'b1100;
								4'd11: DS_TLP_BUF_DIN[133:130] <= 4'b1110;
								4'd15: DS_TLP_BUF_DIN[133:130] <= 4'b1111;
								default: DS_TLP_BUF_DIN[133:130] <= 4'd0; // fatal error occurs in IP core
							endcase
							// sof & last_data
							if(rx_is_sof[4])begin
								if(rx_is_sof[3:0] == 4'd0) begin // sof is for current eof TLP
									DS_TLP_BUF_DIN[129] <= 1'b1;
									last_data <= 66'd0;
									last_data_valid <= 2'b0;
								end
								else begin // sof is for next TLP
									DS_TLP_BUF_DIN[129] <= 1'b0;
									last_data <= {1'd1, 1'b0, m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
									last_data_valid <= 2'b11;
								end
							end
							else begin
								DS_TLP_BUF_DIN[129] <= 1'b0;
							end
							// eof
							DS_TLP_BUF_DIN[128] <= 1'b1;
						end // endif -- eof
						else begin
							if(rx_is_sof[4])begin
								if(rx_is_sof[3:0] == 4'd0) begin // sof is in DW0
									DS_TLP_BUF_DIN <= {4'b1111, 1'b1, 1'b0, m_axis_rx_tdata[31:0], m_axis_rx_tdata[63:32], 
														m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
									DS_TLP_BUF_WR_EN <= 1'd1;
									last_data <= 66'd0;
									last_data_valid <= 2'b0;
								end
								else begin // sof is in middle(DW2)
									DS_TLP_BUF_DIN <= 134'd0;
									DS_TLP_BUF_WR_EN <= 1'd0;
									last_data <= {1'd1, 1'b0, m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
									last_data_valid <= 2'b11;
								end
							end
							else begin
								DS_TLP_BUF_DIN <= {4'b1111, 1'b0, 1'b0, m_axis_rx_tdata[31:0], m_axis_rx_tdata[63:32], 
														m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
								DS_TLP_BUF_WR_EN <= 1'd1;
								last_data <= 66'd0;
								last_data_valid <= 2'b0;
							end
						end // endif -- not eof
					end
					else begin
						// some data is stored in reg last_data
						if(rx_is_eof[4])begin
							if(rx_is_eof[3:0] == 4'd3 || rx_is_eof[3:0] == 4'd7)begin // eof in DW0/1 of m_axis_rx_tdata
								if(DS_TLP_BUF_PROG_FULL == 1'b1)
									m_axis_rx_tready <= 1'b0;
								else
									m_axis_rx_tready <= 1'b1;
								if(rx_is_sof[4])begin // sof exists in m_axis_rx_tdata
									DS_TLP_BUF_DIN <= {2'b11, ((rx_is_eof[3:0] == 4'd3) ? 2'b10 : 2'b11), last_data[65], 1'b1, 
															last_data[63:0], m_axis_rx_tdata[31:0], m_axis_rx_tdata[63:32]};
									DS_TLP_BUF_WR_EN <= 1'd1;
									last_data <= {1'b1, 1'b0, m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
									last_data_valid <= 2'b11;
								end
								else begin // sof not exist in m_axis_rx_tdata
									DS_TLP_BUF_DIN <= {2'b11, ((rx_is_eof[3:0] == 4'd3) ? 2'b10 : 2'b11), last_data[65], 1'b1, 
															last_data[63:0], m_axis_rx_tdata[31:0], m_axis_rx_tdata[63:32]};
									DS_TLP_BUF_WR_EN <= 1'd1;
									last_data <= 66'd0;
									last_data_valid <= 2'b0;
								end
							end
							else begin // eof in DW 2/3 in m_axis_rx_tdata
								m_axis_rx_tready <= 1'b0;
								DS_TLP_BUF_DIN <= {4'b1111, last_data[65], 1'b0, 
														last_data[63:0], m_axis_rx_tdata[31:0], m_axis_rx_tdata[63:32]};
								DS_TLP_BUF_WR_EN <= 1'd1;
								last_data <= {1'b0, 1'b1, m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
								last_data_valid <= ((rx_is_eof[3:0] == 4'd11) ? 2'b10 : 2'b11);
							end
						end // endif -- eof
						else begin
							if(DS_TLP_BUF_PROG_FULL == 1'b1)
								m_axis_rx_tready <= 1'b0;
							else
								m_axis_rx_tready <= 1'b1;
							DS_TLP_BUF_DIN <= {4'b1111, last_data[65], 1'b0, 
													last_data[63:0], m_axis_rx_tdata[31:0], m_axis_rx_tdata[63:32]};
							DS_TLP_BUF_WR_EN <= 1'd1;
							last_data <= {1'b0, 1'b0, m_axis_rx_tdata[95:64], m_axis_rx_tdata[127:96]};
							last_data_valid <= 2'b11;
						end // endif -- not eof
					end
				end
				else begin
					DS_TLP_BUF_DIN <= 134'd0;
					DS_TLP_BUF_WR_EN <= 1'd0;
					last_data <= last_data;
					last_data_valid <= last_data_valid;
					if(DS_TLP_BUF_PROG_FULL == 1'b1)
						m_axis_rx_tready <= 1'b0;
					else
						m_axis_rx_tready <= 1'b1;
				end
			end
		end
	end

endmodule
