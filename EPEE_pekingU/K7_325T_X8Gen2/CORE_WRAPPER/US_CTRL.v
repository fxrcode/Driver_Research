`timescale 1ns / 1ps
/////////////////////////////////////
//
// US_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Upstream TLP control.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////


module US_CTRL(
	input sys_clk,
	input sys_rst_n,
	// to PCIe logicore
	input                 s_axis_tx_tready,
	output reg [128-1:0]  s_axis_tx_tdata,
	output reg [16 -1:0]  s_axis_tx_tkeep,
	output reg            s_axis_tx_tlast,
	output reg            s_axis_tx_tvalid,
	input                 tx_err_drop,
	// from US_TLP_collector
	input      [133:0] US_TLP_BUF_DOUT, // 133-130 : DW Enable, 129 : Sof, 128 : Eof, 127-0 : data
	input             US_TLP_BUF_EMPTY,
	output reg        US_TLP_BUF_RD_EN,
	// to SYS_CSR
	output reg        US_ERROR
);

	// a FIFO_getter is used here as trn_tdst_rdy_n is hard to meet
	reg [134*4-1:0] us_tlp_buf_getter_data; // used to buffer data from FIFO
	reg [1:0] us_tlp_buf_getter_wr_pointer;
	reg [3:0] us_tlp_buf_getter_valid; // indicate which data in us_tlp_buf_getter_data is set
	reg [1:0] us_tlp_buf_getter_rd_pointer;
	reg [3:0] us_tlp_buf_getter_clear; // indicate which data in us_tlp_buf_getter_data has been used
	wire[3:0] us_tlp_buf_getter_flag; // indicate which data in us_tlp_buf_getter_data is valid
	assign us_tlp_buf_getter_flag = us_tlp_buf_getter_clear ^ us_tlp_buf_getter_valid;
	wire[133:0]us_tlp_buf_getter_rd_data;
	assign us_tlp_buf_getter_rd_data =  (us_tlp_buf_getter_rd_pointer == 2'b00)?us_tlp_buf_getter_data[134*4-1:134*3] : 
													(us_tlp_buf_getter_rd_pointer == 2'b01)?us_tlp_buf_getter_data[134*3-1:134*2] : 
													(us_tlp_buf_getter_rd_pointer == 2'b10)?us_tlp_buf_getter_data[134*2-1:134*1] : 
																										 us_tlp_buf_getter_data[134*1-1:0];

	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			us_tlp_buf_getter_data <= 536'h0;
			us_tlp_buf_getter_wr_pointer <= 2'b00;
			us_tlp_buf_getter_valid <= 4'b0000;
			US_TLP_BUF_RD_EN <= 1'b0;
		end else begin
			if(US_TLP_BUF_EMPTY == 1'b0 && us_tlp_buf_getter_flag[us_tlp_buf_getter_wr_pointer] == 1'b0 &&
				us_tlp_buf_getter_flag[(us_tlp_buf_getter_wr_pointer == 2'b11)?2'b00 : us_tlp_buf_getter_wr_pointer+1] == 1'b0)begin
				US_TLP_BUF_RD_EN <= 1'b1;
			end else begin
				US_TLP_BUF_RD_EN <= 1'b0;
			end
			if(US_TLP_BUF_RD_EN == 1'b1 && US_TLP_BUF_EMPTY == 1'b0)begin
				us_tlp_buf_getter_valid[us_tlp_buf_getter_wr_pointer] <= ~us_tlp_buf_getter_clear[us_tlp_buf_getter_wr_pointer];
				us_tlp_buf_getter_wr_pointer <= us_tlp_buf_getter_wr_pointer + 2'd1;
				case(us_tlp_buf_getter_wr_pointer)
					2'b00 : us_tlp_buf_getter_data[134*4-1:134*3] <= US_TLP_BUF_DOUT;
					2'b01 : us_tlp_buf_getter_data[134*3-1:134*2] <= US_TLP_BUF_DOUT;
					2'b10 : us_tlp_buf_getter_data[134*2-1:134*1] <= US_TLP_BUF_DOUT;
					2'b11 : us_tlp_buf_getter_data[134*1-1:0] <= US_TLP_BUF_DOUT;
				endcase
			end
		end
	end
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			s_axis_tx_tdata <= 64'd0;
			s_axis_tx_tkeep <= 8'd0;
			s_axis_tx_tlast <= 1'b0;
			s_axis_tx_tvalid <= 1'b0;
			
			US_ERROR <= 1'b0;
			
			us_tlp_buf_getter_rd_pointer <= 2'b00;
			us_tlp_buf_getter_clear <= 4'b0000;
		end else begin
			if(s_axis_tx_tready == 1'b1)begin
				if(us_tlp_buf_getter_flag[us_tlp_buf_getter_rd_pointer] == 1'b1 && tx_err_drop == 1'b0)begin
					s_axis_tx_tdata <= {us_tlp_buf_getter_rd_data[31:0], us_tlp_buf_getter_rd_data[63:32],
												us_tlp_buf_getter_rd_data[95:64], us_tlp_buf_getter_rd_data[127:96]};
					case(us_tlp_buf_getter_rd_data[133:130])
						4'b1111 : s_axis_tx_tkeep <= 16'hFFFF;
						4'b1110 : s_axis_tx_tkeep <= 16'h0FFF;
						4'b1100 : s_axis_tx_tkeep <= 16'h00FF;
						4'b1000 : s_axis_tx_tkeep <= 16'h000F;
						default : s_axis_tx_tkeep <= 16'h0000;
					endcase
					s_axis_tx_tlast <= us_tlp_buf_getter_rd_data[128];
					s_axis_tx_tvalid <= 1'b1;

					us_tlp_buf_getter_rd_pointer <= us_tlp_buf_getter_rd_pointer + 2'd1;
					us_tlp_buf_getter_clear[us_tlp_buf_getter_rd_pointer] <= us_tlp_buf_getter_valid[us_tlp_buf_getter_rd_pointer];
					
					US_ERROR <= 1'b0;
				end
				else if(us_tlp_buf_getter_flag[us_tlp_buf_getter_rd_pointer] == 1'b0 && tx_err_drop == 1'b0) begin
					s_axis_tx_tdata  <= 128'd0;
					s_axis_tx_tkeep  <= 16'd0;
					s_axis_tx_tlast  <= 1'b0;
					s_axis_tx_tvalid <= 1'b0;
					
					US_ERROR <= 1'b0;
				end else begin
					US_ERROR <= 1'b1;
				end
			end
			else begin
				s_axis_tx_tdata  <= s_axis_tx_tdata;
				s_axis_tx_tkeep  <= s_axis_tx_tkeep;
				s_axis_tx_tlast  <= s_axis_tx_tlast;
				s_axis_tx_tvalid <= s_axis_tx_tvalid;
			end
		end
	end

endmodule
