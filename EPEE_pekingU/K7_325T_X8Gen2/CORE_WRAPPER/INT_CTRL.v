`timescale 1ns / 1ps
/////////////////////////////////////
//
// INT_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Deal with the interrupt interface of the PCIe IP
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define INT_INIT         6'b00_0001
`define MSI_INT_WAIT     6'b00_0010
`define MSI_INT_SENT     6'b00_0100
`define LEA_INT_WAIT     6'b00_1000
`define LEA_INT_SENT     6'b01_0000
`define LEA_INT_CLR_WAIT 6'b10_0000

// timeout threshold is about 10 seconds when using 250MHz clk
`define INT_TIMEOUT_THRESHOLD 64'd2500000000

module INT_CTRL(
	input sys_clk,
	input sys_rst_n,
	
	// for IP core
	output reg cfg_interrupt,
	input  cfg_interrupt_rdy,
	output reg cfg_interrupt_assert,
	output reg [7:0]cfg_interrupt_di,
	input  [7:0] cfg_interrupt_do,
	input  [2:0] cfg_interrupt_mmenable,
	input  cfg_interrupt_msienable,
	
	// for my logic
	input INT_REQ,
	output reg INT_REQ_ACK,
	input INT_SERVED,
	output reg INT_SERVED_ACK
);

	// local regs
	(*KEEP="TRUE"*)reg [5:0] int_state;
	// for timeout logic
	reg [5:0] int_state_dly;
	reg [63:0]timeout_counter;
	reg timeout_flag;
	
	// timeout logic , for recovering from deadlock
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			timeout_counter <= 0;
			timeout_flag <= 0;
			int_state_dly <= `INT_INIT;
		end else begin
			int_state_dly <= int_state;
			if(int_state == int_state_dly && int_state_dly != `INT_INIT)begin
				if(timeout_counter >= `INT_TIMEOUT_THRESHOLD)begin
					timeout_flag <= 1'b1;
				end else begin
					timeout_counter <= timeout_counter + 1'b1;
					timeout_flag <= 1'b0;
				end
			end else begin
				timeout_flag <= 0;
				timeout_counter <= 0;
			end
		end
	end
	
	// interrupt logic
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			int_state <= `INT_INIT;
			cfg_interrupt <= 1'b0;
			cfg_interrupt_assert <= 1'b0;
			cfg_interrupt_di <= 0;
			INT_REQ_ACK <= 1'b0;
			INT_SERVED_ACK <= 1'b0;
		end
		else if(timeout_flag)begin
			int_state <= `INT_INIT;
			cfg_interrupt <= 1'b0;
			cfg_interrupt_assert <= 1'b0;
			cfg_interrupt_di <= 0;
			INT_REQ_ACK <= 1'b0;
			INT_SERVED_ACK <= 1'b0;
		end
		else begin
			case(int_state)
				`INT_INIT:begin
					// initial state of interrupt FSM
					INT_REQ_ACK <= 1'b0;
					INT_SERVED_ACK <= 1'b0;
					if(INT_REQ)begin
						cfg_interrupt <= 1'b1;
						if(cfg_interrupt_msienable)begin
							// msi interrput, interrupt assert is not used
							cfg_interrupt_assert <= 1'b0;
							int_state <= `MSI_INT_WAIT;
						end else begin
							// leagacy interrupt, assert "interrupt_assert"
							cfg_interrupt_assert <= 1'b1;
							int_state <= `LEA_INT_WAIT;
						end
					end
					else begin
						cfg_interrupt <= 1'b0;
						cfg_interrupt_assert <= 1'b0;
					end
				end
				`MSI_INT_WAIT:begin
					// wait for IP core's interrupt_rdy signal
					INT_SERVED_ACK <= 1'b0;
					cfg_interrupt_assert <= 1'b0;
					if(cfg_interrupt_rdy == 1'b1)begin
						INT_REQ_ACK <= 1'b1;
						cfg_interrupt <= 1'b0;
						int_state <= `MSI_INT_SENT;
					end else begin
						INT_REQ_ACK <= 1'b0;
						cfg_interrupt <= 1'b1;
						int_state <= `MSI_INT_WAIT;
					end
				end
				`MSI_INT_SENT:begin
					// IP core has sent the interrupt
					// wait for software handle the interrupt and assert INT_SERVED
					INT_REQ_ACK <= 1'b0;
					cfg_interrupt <= 1'b0;
					cfg_interrupt_assert <= 1'b0;
					if(INT_SERVED == 1'b0)begin
						INT_SERVED_ACK <= 1'b0;
						int_state <= `MSI_INT_SENT;
					end else begin
						INT_SERVED_ACK <= 1'b1;
						int_state <= `INT_INIT;
					end
				end
				`LEA_INT_WAIT:begin
					// wait for IP cores's interrupt_rdy signal
					INT_SERVED_ACK <= 1'b0;
					cfg_interrupt_assert <= 1'b1;
					if(cfg_interrupt_rdy == 1'b1)begin
						INT_REQ_ACK <= 1'b1;
						cfg_interrupt <= 1'b0;
						int_state <= `LEA_INT_SENT;
					end else begin
						INT_REQ_ACK <= 1'b0;
						cfg_interrupt <= 1'b1;
						int_state <= `LEA_INT_WAIT;
					end
				end
				`LEA_INT_SENT:begin
					// IP core has sent the interrupt
					// wait for software handle the interrupt and assert INT_SERVED
					INT_REQ_ACK <= 1'b0;
					INT_SERVED_ACK <= 1'b0;
					if(INT_SERVED == 1'b1)begin
						cfg_interrupt_assert <= 1'b0;
						cfg_interrupt <= 1'b1;
						int_state <= `LEA_INT_CLR_WAIT;
					end else begin
						cfg_interrupt_assert <= 1'b1;
						cfg_interrupt <= 1'b0;
						int_state <= `LEA_INT_SENT;
					end
				end
				`LEA_INT_CLR_WAIT:begin
					// wait for IP core's interrput_rdy signal
					// clear the IP core's interrupt assert
					INT_REQ_ACK <= 1'b0;
					if(cfg_interrupt_rdy == 1'b1)begin
						INT_SERVED_ACK <= 1'b1;
						cfg_interrupt_assert <= 1'b0;
						cfg_interrupt <= 1'b0;
						int_state <= `INT_INIT;
					end else begin
						INT_SERVED_ACK <= 1'b0;
						cfg_interrupt_assert <= 1'b0;
						cfg_interrupt <= 1'b1;
						int_state <= `LEA_INT_CLR_WAIT;
					end
				end
			endcase
		end
	end

endmodule
