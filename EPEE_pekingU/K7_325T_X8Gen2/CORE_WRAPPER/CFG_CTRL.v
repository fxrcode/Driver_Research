`timescale 1ns / 1ps
/////////////////////////////////////
//
// CFG_CTRL.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Get the configure signals from PCIe IP Core and output them to other modules in 
// EPEE system.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module CFG_CTRL(
	input        sys_clk,
	input        sys_rst_n,
	input  [7:0] cfg_bus_number,
	input  [4:0] cfg_device_number,
	input  [2:0] cfg_function_number,
	input  [15:0]cfg_command,
	input  [15:0]cfg_dcommand,
	output [15:0]CFG_COMPLETER_ID,
	output       CFG_EXT_TAG_EN,
	output [2:0] CFG_MAX_PAYLOAD_SIZE,
	output [2:0] CFG_MAX_RD_REQ_SIZE,
	output       CFG_BUS_MSTR_EN
);

	(*KEEP="TRUE"*)reg [15:0] cfg_completer_id_reg;
	(*KEEP="TRUE"*)reg        cfg_ext_tag_en_reg;
	(*KEEP="TRUE"*)reg [2:0]  cfg_max_payload_size_reg;
	(*KEEP="TRUE"*)reg [2:0]  cfg_max_rd_req_size_reg;
	(*KEEP="TRUE"*)reg        cfg_bus_mstr_en_reg;
	
	assign CFG_COMPLETER_ID = cfg_completer_id_reg;
	assign CFG_EXT_TAG_EN = cfg_ext_tag_en_reg;
	assign CFG_MAX_PAYLOAD_SIZE = cfg_max_payload_size_reg;
	assign CFG_MAX_RD_REQ_SIZE = cfg_max_rd_req_size_reg;
	assign CFG_BUS_MSTR_EN = cfg_bus_mstr_en_reg;
	
	always @(posedge sys_clk)begin
		if(~sys_rst_n)begin
			cfg_completer_id_reg <= 16'd0;
			cfg_ext_tag_en_reg <= 1'b0;
			cfg_max_payload_size_reg <= 3'b000;
			cfg_max_rd_req_size_reg <= 3'b000;
			cfg_bus_mstr_en_reg <= 1'b0;
		end else begin
			cfg_completer_id_reg <= {cfg_bus_number, cfg_device_number, cfg_function_number};
			cfg_ext_tag_en_reg <= cfg_dcommand[8];
			cfg_max_payload_size_reg <= cfg_dcommand[7:5];
			cfg_max_rd_req_size_reg <= cfg_dcommand[14:12];
			cfg_bus_mstr_en_reg <= cfg_command[2];
		end
	end

endmodule
