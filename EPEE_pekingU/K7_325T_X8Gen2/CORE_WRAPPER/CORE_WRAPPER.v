`timescale 1ns / 1ps
/////////////////////////////////////
//
// CORE_WRAPPER.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Wrapper the PCIe IP Core and provide interface to FPGA platform independent part of 
// EPEE system.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define PCI_EXP_EP_OUI                           24'h000A35
`define PCI_EXP_EP_DSN_1                         {{8'h1},`PCI_EXP_EP_OUI}
`define PCI_EXP_EP_DSN_2                         32'h00000001

module CORE_WRAPPER #(
	parameter PCIE_LINK_WIDTH = 1, // PCIe lane number
	parameter PCIE_LINK_GEN = 1 // PCIe generation
)
(
	// System interface for PCIe IP core
	input sys_clk_n,
	input sys_clk_p,
	input sys_reset_n,
	// PCIe interface
	output [PCIE_LINK_WIDTH-1:0] pci_exp_txp,
	output [PCIE_LINK_WIDTH-1:0] pci_exp_txn,
	input  [PCIE_LINK_WIDTH-1:0] pci_exp_rxp,
	input  [PCIE_LINK_WIDTH-1:0] pci_exp_rxn,
	// TRN system signals
	output      trn_clk,
	output      trn_reset_n,
	// CFG_CTRL output
	output [15:0]CFG_COMPLETER_ID,
	output       CFG_EXT_TAG_EN,
	output [2:0] CFG_MAX_PAYLOAD_SIZE,
	output [2:0] CFG_MAX_RD_REQ_SIZE,
	output       CFG_BUS_MSTR_EN,
	// for DS_TLP_dispatcher
	output [133:0] DS_TLP_BUF_DIN,
	input         DS_TLP_BUF_PROG_FULL,
	output        DS_TLP_BUF_WR_EN,
	// from US_TLP_collector
	input  [133:0] US_TLP_BUF_DOUT,
	input         US_TLP_BUF_EMPTY,
	output        US_TLP_BUF_RD_EN,
	// for INT_GEN
	input  INT_REQ,
	output INT_REQ_ACK,
	input  INT_SERVED,
	output INT_SERVED_ACK,
	// for SYS_CSR
	output DS_ERROR,
	output US_ERROR
);
	// clock and reset
	wire  user_clk;
	reg   user_reset_n;
	assign trn_clk = user_clk;
	assign trn_reset_n = user_reset_n;

	// connect IP core with CFG_CTRL
	wire  [7:0] cfg_bus_number;
	wire  [4:0] cfg_device_number;
	wire  [2:0] cfg_function_number;
	wire  [15:0]cfg_command;
	wire  [15:0]cfg_dcommand;
	
	// connect IP core with DS_CTRL
	wire  [128-1:0] m_axis_rx_tdata;
	wire  [ 16-1:0] m_axis_rx_tkeep;
	wire           m_axis_rx_tlast;
	wire           m_axis_rx_tvalid;
	wire           m_axis_rx_tready;
	wire  [21:0]   m_axis_rx_tuser;
	
	// connect IP core with US_CTRL
	wire           s_axis_tx_tready;
	wire [128-1:0]  s_axis_tx_tdata;
	wire [16 -1:0]  s_axis_tx_tkeep;
	wire           s_axis_tx_tlast;
	wire           s_axis_tx_tvalid;
	wire           tx_err_drop;
	
	// connect IP core with INT_CTRL
	wire  cfg_interrupt;
	wire  cfg_interrupt_rdy;
	wire  cfg_interrupt_assert;
	wire  [7:0]cfg_interrupt_di;
	wire  [7:0] cfg_interrupt_do;
	wire  [2:0] cfg_interrupt_mmenable;
	wire  cfg_interrupt_msienable;

	// Wires used for external clocking connectivity
	wire                                        PIPE_PCLK_IN;
	wire                                        PIPE_RXUSRCLK_IN;
	wire   [7:0]   PIPE_RXOUTCLK_IN;
	wire                                        PIPE_DCLK_IN;
	wire                                        PIPE_USERCLK1_IN;
	wire                                        PIPE_USERCLK2_IN;
	wire                                        PIPE_MMCM_LOCK_IN;

	wire                                        PIPE_TXOUTCLK_OUT;
	wire [7:0]     PIPE_RXOUTCLK_OUT;

	wire [7:0]     PIPE_PCLK_SEL_OUT;
	wire                                        PIPE_GEN3_OUT;

	wire                                        PIPE_OOBCLK_IN;
	
	localparam                                  TCQ = 1;
	localparam USER_CLK_FREQ = (PCIE_LINK_GEN == 1) ? ((PCIE_LINK_WIDTH == 8) ? 3 :
																(PCIE_LINK_WIDTH == 4) ? 2 :1) : // PCIe gen 1
														((PCIE_LINK_WIDTH == 8) ? 4 : 
																(PCIE_LINK_WIDTH == 4) ? 3 : 
																(PCIE_LINK_WIDTH == 2) ? 2 : 1); // PCIe gen 2
	localparam USER_CLK2_DIV2 = "TRUE";
	localparam USERCLK2_FREQ = (USER_CLK2_DIV2 == "TRUE") ?
                             (USER_CLK_FREQ == 4) ? 3 :
                             (USER_CLK_FREQ == 3) ? 2 : USER_CLK_FREQ :
                             USER_CLK_FREQ;
	localparam PCIE_EXT_CLK      = "TRUE";  // Use External Clocking Module
	//-------------------------------------------------------
	IBUF   sys_reset_n_ibuf (.O(sys_rst_n_c), .I(sys_reset_n));

	IBUFDS_GTE2 refclk_ibuf (.O(sys_clk), .ODIV2(), .I(sys_clk_p), .CEB(1'b0), .IB(sys_clk_n));

	reg user_reset_q;
	reg user_lnk_up_q;

	always @(posedge user_clk) begin
	 user_reset_q  <= user_reset;
	 user_lnk_up_q <= user_lnk_up;
	end

	// Generate External Clock Module if External Clocking is selected
	generate
		if (PCIE_EXT_CLK == "TRUE") begin : ext_clk

		//---------- PIPE Clock Module -------------------------------------------------
		PCIE_CORE_1MBar_pipe_clock #
		(
			 .PCIE_ASYNC_EN                  ( "FALSE" ),     // PCIe async enable
			 .PCIE_TXBUF_EN                  ( "FALSE" ),     // PCIe TX buffer enable for Gen1/Gen2 only
			 .PCIE_LANE                      ( 6'h08 ),     // PCIe number of lanes
			 // synthesis translate_off
			 .PCIE_LINK_SPEED                ( 2 ),
			 // synthesis translate_on
			 .PCIE_REFCLK_FREQ               ( 0 ),     // PCIe reference clock frequency
			 .PCIE_USERCLK1_FREQ             ( USER_CLK_FREQ +1 ),     // PCIe user clock 1 frequency
			 .PCIE_USERCLK2_FREQ             ( USERCLK2_FREQ +1 ),     // PCIe user clock 2 frequency
			 .PCIE_DEBUG_MODE                ( 0 )
		)
		pipe_clock_i
		(

			 //---------- Input -------------------------------------
			 .CLK_CLK                        ( sys_clk ),
			 .CLK_TXOUTCLK                   ( PIPE_TXOUTCLK_OUT ),     // Reference clock from lane 0
			 .CLK_RXOUTCLK_IN                ( PIPE_RXOUTCLK_OUT ),
			 .CLK_RST_N                      ( 1'b1 ),
			 .CLK_PCLK_SEL                   ( PIPE_PCLK_SEL_OUT ),
			 .CLK_GEN3                       ( PIPE_GEN3_OUT ),

			 //---------- Output ------------------------------------
			 .CLK_PCLK                       ( PIPE_PCLK_IN ),
			 .CLK_RXUSRCLK                   ( PIPE_RXUSRCLK_IN ),
			 .CLK_RXOUTCLK_OUT               ( PIPE_RXOUTCLK_IN ),
			 .CLK_DCLK                       ( PIPE_DCLK_IN ),
			 .CLK_OOBCLK                     ( PIPE_OOBCLK_IN ),
			 .CLK_USERCLK1                   ( PIPE_USERCLK1_IN ),
			 .CLK_USERCLK2                   ( PIPE_USERCLK2_IN ),
			 .CLK_MMCM_LOCK                  ( PIPE_MMCM_LOCK_IN )

		);
		end
	endgenerate
 
	PCIE_CORE_1MBar #(
		.PL_FAST_TRAIN      ( "FALSE" ),
		.PCIE_EXT_CLK       ( PCIE_EXT_CLK )
	) PCIE_CORE_1MBar_i
	(

		//----------------------------------------------------------------------------------------------------------------//
		// 1. PCI Express (pci_exp) Interface                                                                             //
		//----------------------------------------------------------------------------------------------------------------//

		// Tx
		.pci_exp_txn                                ( pci_exp_txn ),
		.pci_exp_txp                                ( pci_exp_txp ),

		// Rx
		.pci_exp_rxn                                ( pci_exp_rxn ),
		.pci_exp_rxp                                ( pci_exp_rxp ),

		//----------------------------------------------------------------------------------------------------------------//
		// 2. Clocking Interface                                                                                          //
		//----------------------------------------------------------------------------------------------------------------//
		.PIPE_PCLK_IN                              ( PIPE_PCLK_IN ),
		.PIPE_RXUSRCLK_IN                          ( PIPE_RXUSRCLK_IN ),
		.PIPE_RXOUTCLK_IN                          ( PIPE_RXOUTCLK_IN ),
		.PIPE_DCLK_IN                              ( PIPE_DCLK_IN ),
		.PIPE_USERCLK1_IN                          ( PIPE_USERCLK1_IN ),
		.PIPE_OOBCLK_IN                            ( PIPE_OOBCLK_IN ),
		.PIPE_USERCLK2_IN                          ( PIPE_USERCLK2_IN ),
		.PIPE_MMCM_LOCK_IN                         ( PIPE_MMCM_LOCK_IN ),

		.PIPE_TXOUTCLK_OUT                         ( PIPE_TXOUTCLK_OUT ),
		.PIPE_RXOUTCLK_OUT                         ( PIPE_RXOUTCLK_OUT ),
		.PIPE_PCLK_SEL_OUT                         ( PIPE_PCLK_SEL_OUT ),
		.PIPE_GEN3_OUT                             ( PIPE_GEN3_OUT ),


		//----------------------------------------------------------------------------------------------------------------//
		// 3. AXI-S Interface                                                                                             //
		//----------------------------------------------------------------------------------------------------------------//

		// Common
		.user_clk_out                               ( user_clk ),
		.user_reset_out                             ( user_reset ),
		.user_lnk_up                                ( user_lnk_up ),

		// TX

		.tx_buf_av                                  (  ), // O 
		.tx_err_drop                                ( tx_err_drop ),
		.tx_cfg_req                                 (  ), // O
		.s_axis_tx_tready                           ( s_axis_tx_tready ),
		.s_axis_tx_tdata                            ( s_axis_tx_tdata ),
		.s_axis_tx_tkeep                            ( s_axis_tx_tkeep ),
		.s_axis_tx_tuser                            ( 4'd0 ), // I, including src_dsc, tx_str
		.s_axis_tx_tlast                            ( s_axis_tx_tlast ),
		.s_axis_tx_tvalid                           ( s_axis_tx_tvalid ),

		.tx_cfg_gnt                                 ( 1'b1 ),  // I

		// Rx
		.m_axis_rx_tdata                            ( m_axis_rx_tdata ),
		.m_axis_rx_tkeep                            ( m_axis_rx_tkeep ),
		.m_axis_rx_tlast                            ( m_axis_rx_tlast ),
		.m_axis_rx_tvalid                           ( m_axis_rx_tvalid ),
		.m_axis_rx_tready                           ( m_axis_rx_tready ),
		.m_axis_rx_tuser                            ( m_axis_rx_tuser ),
		.rx_np_ok                                   ( 1'b1 ), // I
		.rx_np_req                                  ( 1'b1 ), // I

		// Flow Control
		.fc_cpld                                    (  ), // O
		.fc_cplh                                    (  ), // O
		.fc_npd                                     (  ), // O
		.fc_nph                                     (  ), // O
		.fc_pd                                      (  ), // O
		.fc_ph                                      (  ), // O
		.fc_sel                                     ( 3'd0 ), // I


		//----------------------------------------------------------------------------------------------------------------//
		// 4. Configuration (CFG) Interface                                                                               //
		//----------------------------------------------------------------------------------------------------------------//

		//------------------------------------------------//
		// EP and RP                                      //
		//------------------------------------------------//
		.cfg_mgmt_do                                ( ), // O
		.cfg_mgmt_rd_wr_done                        ( ), // O

		.cfg_status                                 (  ), // O
		.cfg_command                                ( cfg_command ),
		.cfg_dstatus                                (  ), // O
		.cfg_dcommand                               ( cfg_dcommand ),
		.cfg_lstatus                                (  ), // O
		.cfg_lcommand                               (  ), // O
		.cfg_dcommand2                              (  ), // O
		.cfg_pcie_link_state                        (  ), // O

		.cfg_pmcsr_pme_en                           ( ), // O
		.cfg_pmcsr_powerstate                       ( ), // O
		.cfg_pmcsr_pme_status                       ( ), // O
		.cfg_received_func_lvl_rst                  ( ), // O

		// Management Interface
		.cfg_mgmt_di                                ( 32'd0 ), // I
		.cfg_mgmt_byte_en                           ( 4'd0 ), // I
		.cfg_mgmt_dwaddr                            ( 10'h0 ), // I
		.cfg_mgmt_wr_en                             ( 1'h0 ), // I
		.cfg_mgmt_rd_en                             ( 1'h0 ), // I
		.cfg_mgmt_wr_readonly                       ( 1'h0 ), // I

		// Error Reporting Interface
		.cfg_err_ecrc                               ( 1'b0 ),
		.cfg_err_ur                                 ( 1'b0 ),
		.cfg_err_cpl_timeout                        ( 1'b0 ),
		.cfg_err_cpl_unexpect                       ( 1'b0 ),
		.cfg_err_cpl_abort                          ( 1'b0 ),
		.cfg_err_posted                             ( 1'b0 ),
		.cfg_err_cor                                ( 1'b0 ),
		.cfg_err_atomic_egress_blocked              ( 1'b0 ),
		.cfg_err_internal_cor                       ( 1'b0 ),
		.cfg_err_malformed                          ( 1'b0 ),
		.cfg_err_mc_blocked                         ( 1'b0 ),
		.cfg_err_poisoned                           ( 1'b0 ),
		.cfg_err_norecovery                         ( 1'b0 ),
		.cfg_err_tlp_cpl_header                     ( 48'h0 ),
		.cfg_err_cpl_rdy                            (  ), // O
		.cfg_err_locked                             ( 1'b0 ),
		.cfg_err_acs                                ( 1'b0 ),
		.cfg_err_internal_uncor                     ( 1'b0 ),

		.cfg_trn_pending                            ( 1'b0 ),
		.cfg_pm_halt_aspm_l0s                       ( 1'b0 ),
		.cfg_pm_halt_aspm_l1                        ( 1'b0 ),
		.cfg_pm_force_state_en                      ( 1'b0 ),
		.cfg_pm_force_state                         ( 2'b00 ),

		.cfg_dsn                                    ( {`PCI_EXP_EP_DSN_2, `PCI_EXP_EP_DSN_1} ),

		//------------------------------------------------//
		// EP Only                                        //
		//------------------------------------------------//
		.cfg_interrupt                              ( cfg_interrupt ),
		.cfg_interrupt_rdy                          ( cfg_interrupt_rdy ),
		.cfg_interrupt_assert                       ( cfg_interrupt_assert ),
		.cfg_interrupt_di                           ( cfg_interrupt_di ),
		.cfg_interrupt_do                           ( cfg_interrupt_do ),
		.cfg_interrupt_mmenable                     ( cfg_interrupt_mmenable ),
		.cfg_interrupt_msienable                    ( cfg_interrupt_msienable ),
		.cfg_interrupt_msixenable                   (  ), // O
		.cfg_interrupt_msixfm                       (  ), // O
		.cfg_interrupt_stat                         ( 1'b0 ),
		.cfg_pciecap_interrupt_msgnum               ( 5'b00000 ),
		.cfg_to_turnoff                             (  ), // O
		.cfg_turnoff_ok                             ( 1'b1 ),
		.cfg_bus_number                             ( cfg_bus_number ),
		.cfg_device_number                          ( cfg_device_number ),
		.cfg_function_number                        ( cfg_function_number ),
		.cfg_pm_wake                                ( 1'b0 ),

		//------------------------------------------------//
		// RP Only                                        //
		//------------------------------------------------//
		.cfg_pm_send_pme_to                         ( 1'b0 ),
		.cfg_ds_bus_number                          ( 8'b0 ),
		.cfg_ds_device_number                       ( 5'b0 ),
		.cfg_ds_function_number                     ( 3'b0 ),
		.cfg_mgmt_wr_rw1c_as_rw                     ( 1'b0 ),
		.cfg_msg_received                           ( ),
		.cfg_msg_data                               ( ),

		.cfg_bridge_serr_en                         ( ),
		.cfg_slot_control_electromech_il_ctl_pulse  ( ),
		.cfg_root_control_syserr_corr_err_en        ( ),
		.cfg_root_control_syserr_non_fatal_err_en   ( ),
		.cfg_root_control_syserr_fatal_err_en       ( ),
		.cfg_root_control_pme_int_en                ( ),
		.cfg_aer_rooterr_corr_err_reporting_en      ( ),
		.cfg_aer_rooterr_non_fatal_err_reporting_en ( ),
		.cfg_aer_rooterr_fatal_err_reporting_en     ( ),
		.cfg_aer_rooterr_corr_err_received          ( ),
		.cfg_aer_rooterr_non_fatal_err_received     ( ),
		.cfg_aer_rooterr_fatal_err_received         ( ),

		.cfg_msg_received_err_cor                   ( ),
		.cfg_msg_received_err_non_fatal             ( ),
		.cfg_msg_received_err_fatal                 ( ),
		.cfg_msg_received_pm_as_nak                 ( ),
		.cfg_msg_received_pme_to_ack                ( ),
		.cfg_msg_received_assert_int_a              ( ),
		.cfg_msg_received_assert_int_b              ( ),
		.cfg_msg_received_assert_int_c              ( ),
		.cfg_msg_received_assert_int_d              ( ),
		.cfg_msg_received_deassert_int_a            ( ),
		.cfg_msg_received_deassert_int_b            ( ),
		.cfg_msg_received_deassert_int_c            ( ),
		.cfg_msg_received_deassert_int_d            ( ),

		//----------------------------------------------------------------------------------------------------------------//
		// 5. Physical Layer Control and Status (PL) Interface                                                            //
		//----------------------------------------------------------------------------------------------------------------//
		.pl_directed_link_change                    ( 2'b00 ),
		.pl_directed_link_width                     ( 2'b00 ),
		.pl_directed_link_speed                     ( 1'b0 ),
		.pl_directed_link_auton                     ( 1'b0 ),
		.pl_upstream_prefer_deemph                  ( 1'b1 ),



		.pl_sel_lnk_rate                            (  ), // O
		.pl_sel_lnk_width                           (  ), // O
		.pl_ltssm_state                             (  ), // O
		.pl_lane_reversal_mode                      (  ), // O

		.pl_phy_lnk_up                              ( ), // O
		.pl_tx_pm_state                             ( ),
		.pl_rx_pm_state                             ( ),

		.pl_link_upcfg_cap                          (  ), // O
		.pl_link_gen2_cap                           (  ),
		.pl_link_partner_gen2_supported             (  ),
		.pl_initial_link_width                      (  ),

		.pl_directed_change_done                    ( ),

		//------------------------------------------------//
		// EP Only                                        //
		//------------------------------------------------//
		.pl_received_hot_rst                        (  ), // O

		//------------------------------------------------//
		// RP Only                                        //
		//------------------------------------------------//
		.pl_transmit_hot_rst                        ( 1'b0 ),
		.pl_downstream_deemph_source                ( 1'b0 ),

		//----------------------------------------------------------------------------------------------------------------//
		// 6. AER Interface                                                                                               //
		//----------------------------------------------------------------------------------------------------------------//

		.cfg_err_aer_headerlog                      ( 128'h0 ),
		.cfg_aer_interrupt_msgnum                   ( 5'd0 ),
		.cfg_err_aer_headerlog_set                  (  ),// O
		.cfg_aer_ecrc_check_en                      (  ),// O
		.cfg_aer_ecrc_gen_en                        (  ),// O

		//----------------------------------------------------------------------------------------------------------------//
		// 7. VC interface                                                                                                //
		//----------------------------------------------------------------------------------------------------------------//

		.cfg_vc_tcvc_map                            ( ),// O

		//----------------------------------------------------------------------------------------------------------------//
		// 8. System  (SYS) Interface                                                                                     //
		//----------------------------------------------------------------------------------------------------------------//


		.sys_clk                                    ( sys_clk ),
		.sys_rst_n                                  ( sys_rst_n_c )
	);

	// Clock buffer for the 100MHz configuration clock
	BUFG sysclk_buf_i (
		.I (sys_clk),
		.O (sysclk_buf)
	);

	// for better timing of reset
	
	always @(posedge user_clk)begin
		user_reset_n <= ~user_reset_q;
	end
	
	CFG_CTRL CFG_CTRL(
		.sys_clk(user_clk), 
		.sys_rst_n(user_reset_n), 
		.cfg_bus_number(cfg_bus_number), 
		.cfg_device_number(cfg_device_number), 
		.cfg_function_number(cfg_function_number), 
		.cfg_command(cfg_command),
		.cfg_dcommand(cfg_dcommand), 
		.CFG_COMPLETER_ID(CFG_COMPLETER_ID), 
		.CFG_EXT_TAG_EN(CFG_EXT_TAG_EN), 
		.CFG_MAX_PAYLOAD_SIZE(CFG_MAX_PAYLOAD_SIZE), 
		.CFG_MAX_RD_REQ_SIZE(CFG_MAX_RD_REQ_SIZE), 
		.CFG_BUS_MSTR_EN(CFG_BUS_MSTR_EN)
	);
	
	DS_CTRL DS_CTRL(
		.sys_clk(user_clk), 
		.sys_rst_n(user_reset_n),
		// from IP core 
		.m_axis_rx_tdata(m_axis_rx_tdata), 
		.m_axis_rx_tkeep(m_axis_rx_tkeep), 
		.m_axis_rx_tlast(m_axis_rx_tlast), 
		.m_axis_rx_tvalid(m_axis_rx_tvalid), 
		.m_axis_rx_tready(m_axis_rx_tready), 
		.m_axis_rx_tuser(m_axis_rx_tuser), 
		// for DS_TLP_dispatcher
		.DS_TLP_BUF_DIN(DS_TLP_BUF_DIN), 
		.DS_TLP_BUF_PROG_FULL(DS_TLP_BUF_PROG_FULL), 
		.DS_TLP_BUF_WR_EN(DS_TLP_BUF_WR_EN), 
		// for SYS_CSR
		.DS_ERROR(DS_ERROR)
	);
	
	US_CTRL US_CTRL(
		.sys_clk(user_clk),
		.sys_rst_n(user_reset_n),
		// to PCIe logicore
		.s_axis_tx_tready(s_axis_tx_tready),
		.s_axis_tx_tdata(s_axis_tx_tdata),
		.s_axis_tx_tkeep(s_axis_tx_tkeep),
		.s_axis_tx_tlast(s_axis_tx_tlast),
		.s_axis_tx_tvalid(s_axis_tx_tvalid),
		.tx_err_drop(tx_err_drop),
		// from US_TLP_collector
		.US_TLP_BUF_DOUT(US_TLP_BUF_DOUT),
		.US_TLP_BUF_EMPTY(US_TLP_BUF_EMPTY),
		.US_TLP_BUF_RD_EN(US_TLP_BUF_RD_EN),
		// to SYS_CSR
		.US_ERROR(US_ERROR)
	);
	
	INT_CTRL INT_CTRL(
		.sys_clk(user_clk), 
		.sys_rst_n(user_reset_n), 
		// for IP core
		.cfg_interrupt(cfg_interrupt), 
		.cfg_interrupt_rdy(cfg_interrupt_rdy), 
		.cfg_interrupt_assert(cfg_interrupt_assert), 
		.cfg_interrupt_di(cfg_interrupt_di), 
		.cfg_interrupt_do(cfg_interrupt_do), 
		.cfg_interrupt_mmenable(cfg_interrupt_mmenable), 
		.cfg_interrupt_msienable(cfg_interrupt_msienable), 
		// for my logic
		.INT_REQ(INT_REQ), 
		.INT_REQ_ACK(INT_REQ_ACK), 
		.INT_SERVED(INT_SERVED), 
		.INT_SERVED_ACK(INT_SERVED_ACK)
	);
	
endmodule
