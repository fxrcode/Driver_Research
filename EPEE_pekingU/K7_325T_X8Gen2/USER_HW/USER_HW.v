`timescale 1ns / 1ps
/////////////////////////////////////
//
// USER_HW.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// User hardware of the demo. This is the top module. The DMA part is a loop back but it 
// does bitwise operation to each data; the PIO part is a for register demo; the UDI 
// part contains two interrupt channels.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module USER_HW(
	input usr_clk,
	/*user reset, in usr_clk clock domain*/
	input usr_rst,
	/*user PIO interface, in user clock domain*/
	input  [14:0] usr_pio_ch0_wr_addr,
	input  [31:0] usr_pio_ch0_wr_data,
	input  usr_pio_ch0_wr_req,
	output usr_pio_ch0_wr_ack,
	input  [14:0] usr_pio_ch0_rd_addr,
	output [31:0] usr_pio_ch0_rd_data,
	input  usr_pio_ch0_rd_req,
	output usr_pio_ch0_rd_ack,	
	/*user interrupt interface, in user "user_int_clk" clock domain*/
	// user interrupt 0 (vector = 0)
	output usr0_int_req,
	input  usr0_int_clr,
	input  usr0_int_enable,
	// user interrupt 1 (vector = 1)
	output usr1_int_req,
	input  usr1_int_clr,
	input  usr1_int_enable,
	/*user DMA interface, in user clock domain, clock is from user's usr_host2board_clk and usr_board2host_clk*/
	//output  usr_host2board_clk,
	input   [129:0] usr_host2board_dout,
	input   usr_host2board_empty,
	input   usr_host2board_almost_empty,
	output  usr_host2board_rd_en,
	//output  usr_board2host_clk,
	output  [129:0] usr_board2host_din,
	input   usr_board2host_prog_full,
	output  usr_board2host_wr_en
);

	usr_dma usr_dma(
		.clk(usr_clk),
		.reset(usr_rst),
		/*user DMA interface*/
		.FIFO_host2board_dout(usr_host2board_dout),
		.FIFO_host2board_empty(usr_host2board_empty),
		.FIFO_host2board_almost_empty(usr_host2board_almost_empty),
		.FIFO_host2board_rd_en(usr_host2board_rd_en),
		.FIFO_board2host_din(usr_board2host_din),
		.FIFO_board2host_prog_full(usr_board2host_prog_full),
		.FIFO_board2host_wr_en(usr_board2host_wr_en)
	);

	usr_pio usr_pio(
		.clk(usr_clk),
		.reset(usr_rst),
		.usr_pio_wr_addr(usr_pio_ch0_wr_addr),
		.usr_pio_wr_data(usr_pio_ch0_wr_data),
		.usr_pio_wr_req(usr_pio_ch0_wr_req),
		.usr_pio_wr_ack(usr_pio_ch0_wr_ack),
		.usr_pio_rd_addr(usr_pio_ch0_rd_addr),
		.usr_pio_rd_data(usr_pio_ch0_rd_data),
		.usr_pio_rd_req(usr_pio_ch0_rd_req),
		.usr_pio_rd_ack(usr_pio_ch0_rd_ack)
	);
	
	usr_int usr_int(
		.clk(usr_clk),
		.reset(usr_rst),
		// user interrupt 0 (vector = 0)
		.usr0_int_req(usr0_int_req),
		.usr0_int_clr(usr0_int_clr),
		.usr0_int_enable(usr0_int_enable),
		// user interrupt 1 (vector = 1)
		.usr1_int_req(usr1_int_req),
		.usr1_int_clr(usr1_int_clr),
		.usr1_int_enable(usr1_int_enable)
	);

endmodule
