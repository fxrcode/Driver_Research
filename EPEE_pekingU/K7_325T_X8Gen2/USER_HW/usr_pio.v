`timescale 1ns / 1ps
/////////////////////////////////////
//
// usr_pio.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// xxxxxxxx
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module usr_pio(
	input clk,
	input reset,
	// PIO FIFO
	input  [14:0] usr_pio_wr_addr,
	input  [31:0] usr_pio_wr_data,
	input  usr_pio_wr_req,
	output reg usr_pio_wr_ack,
	input  [14:0] usr_pio_rd_addr,
	output reg [31:0] usr_pio_rd_data,
	input  usr_pio_rd_req,
	output reg usr_pio_rd_ack
);
	
	reg [31:0] reg_0;
	reg [31:0] reg_1;
	reg [31:0] reg_2;
	reg [31:0] reg_3;

	// PCIe PIO interface
	always @(posedge clk)begin
		if(reset == 1'b1)begin
			// for PCIe PIO
			usr_pio_wr_ack <= 1'b0;
			usr_pio_rd_data <= 34'd0;
			usr_pio_rd_ack <= 1'b0;
			// for registers
			reg_0 <= 32'd0;
			reg_1 <= 32'd0;
			reg_2 <= 32'd0;
			reg_3 <= 32'd0;
		end
		else begin
			reg_2 <= reg_0 - reg_1;
			reg_3 <= reg_0 + reg_1;
			if(usr_pio_wr_req == 1'b1 && usr_pio_wr_ack == 1'b0)begin
				usr_pio_wr_ack <= 1'b1;
				case(usr_pio_wr_addr)
					15'd0: reg_0 <= usr_pio_wr_data;// reg #0
					15'd1: reg_1 <= usr_pio_wr_data; // reg #1
					15'd2: ;// reg #2, read only by software
					15'd3: ;// reg #3, read only by software
					default : ;// do nothing
				endcase
			end
			else begin 
				usr_pio_wr_ack <= 1'b0;
			end
			
			if(usr_pio_rd_req == 1'b1 && usr_pio_rd_ack == 1'b0)begin
				usr_pio_rd_ack <= 1'b1;
				case(usr_pio_rd_addr)
					15'd0: usr_pio_rd_data <= reg_0;// reg #0
					15'd1: usr_pio_rd_data <= reg_1;// reg #1
					15'd2: usr_pio_rd_data <= reg_2;// reg #2
					15'd3: usr_pio_rd_data <= reg_3;// reg #3
					default : ;// do nothing
				endcase
			end
			else begin
				usr_pio_rd_ack <= 1'b0;
			end
		end
	end

endmodule
