`timescale 1ns / 1ps
/////////////////////////////////////
//
// usr_int.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// xxxxxxxx
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define USR_HW_INT_INIT  2'b01
`define USR_HW_INT_REQ   2'b10
module usr_int(
	input  clk,
	input  reset,
	// user interrupt 0 (vector = 0)
	output usr0_int_req,
	input  usr0_int_clr,
	input  usr0_int_enable,
	// user interrupt 1 (vector = 1)
	output usr1_int_req,
	input  usr1_int_clr,
	input  usr1_int_enable
);

	(*KEEP = "TRUE"*)reg int_req0;
	assign usr0_int_req = int_req0;
	(*KEEP = "TRUE"*)reg [31:0] counter0;
	(*KEEP = "TRUE"*)reg [1:0]  usr0_int_state;
	
	always @(posedge clk)begin
		if(reset == 1'b1)begin
			int_req0 <= 1'b00;
			counter0 <= 32'd0;
			usr0_int_state <= `USR_HW_INT_INIT;
		end
		else begin
			case(usr0_int_state)
				`USR_HW_INT_INIT:begin
					if(usr0_int_enable == 1'b1)begin
						if(counter0 >= 32'd10)begin
							int_req0 <= 1'b1;
							counter0 <= 32'd0;
							usr0_int_state <= `USR_HW_INT_REQ;
						end
						else begin
							int_req0 <= 1'b0;
							counter0 <= counter0 + 1'b1;
							usr0_int_state <= `USR_HW_INT_INIT;
						end
					end
					else begin
						int_req0 <= 1'b0;
						counter0 <= 32'd0;
						usr0_int_state <= `USR_HW_INT_INIT;
					end
				end
				`USR_HW_INT_REQ:begin
					if(usr0_int_clr == 1'b1)begin
						int_req0 <= 1'b0;
						counter0 <= 32'd0;
						usr0_int_state <= `USR_HW_INT_INIT;
					end
					else begin
						int_req0 <= 1'b1;
						counter0 <= 32'd0;
						usr0_int_state <= `USR_HW_INT_REQ;
					end
				end
				default:begin
					int_req0 <= 1'b0;
					counter0 <= 32'd0;
					usr0_int_state <= `USR_HW_INT_INIT;
				end
			endcase
		end
	end

	(*KEEP = "TRUE"*)reg int_req1;
	assign usr1_int_req = int_req1;
	(*KEEP = "TRUE"*)reg [31:0] counter1;
	(*KEEP = "TRUE"*)reg [1:0]  usr1_int_state;
	
	always @(posedge clk)begin
		if(reset == 1'b1)begin
			int_req1 <= 1'b00;
			counter1 <= 32'd0;
			usr1_int_state <= `USR_HW_INT_INIT;
		end
		else begin
			case(usr1_int_state)
				`USR_HW_INT_INIT:begin
					if(usr1_int_enable == 1'b1)begin
						if(counter1 >= 32'd1000)begin
							int_req1 <= 1'b1;
							counter1 <= 32'd0;
							usr1_int_state <= `USR_HW_INT_REQ;
						end
						else begin
							int_req1 <= 1'b0;
							counter1 <= counter1 + 1'b1;
							usr1_int_state <= `USR_HW_INT_INIT;
						end
					end
					else begin
						int_req1 <= 1'b0;
						counter1 <= 32'd0;
						usr1_int_state <= `USR_HW_INT_INIT;
					end
				end
				`USR_HW_INT_REQ:begin
					if(usr1_int_clr == 1'b1)begin
						int_req1 <= 1'b0;
						counter1 <= 32'd0;
						usr1_int_state <= `USR_HW_INT_INIT;
					end
					else begin
						int_req1 <= 1'b1;
						counter1 <= 32'd0;
						usr1_int_state <= `USR_HW_INT_REQ;
					end
				end
				default:begin
					int_req1 <= 1'b0;
					counter1 <= 32'd0;
					usr1_int_state <= `USR_HW_INT_INIT;
				end
			endcase
		end
	end

endmodule
