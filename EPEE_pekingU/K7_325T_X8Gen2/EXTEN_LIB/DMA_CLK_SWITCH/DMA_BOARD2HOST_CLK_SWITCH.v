`timescale 1ns / 1ps
/////////////////////////////////////
//
// DMA_BOARD2HOST_CLK_SWITCH.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Switch clock domain between EPEE and user hardware for DMA board to host interface
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module DMA_BOARD2HOST_CLK_SWITCH(
	input  trn_clk,
	input  usr_board2host_clk,
	input  reset,
	input  [129:0] usr_board2host_din,
	output usr_board2host_prog_full,
	input  usr_board2host_wr_en,
	output reg [129:0] FIFO_board2host_din,
	input  FIFO_board2host_prog_full,
	output reg FIFO_board2host_wr_en
);
	wire [129:0] usr_board2host_dout;
	wire usr_board2host_empty;
	reg  usr_board2host_rd_en;

	always @(posedge trn_clk)begin
		if(reset)begin
			FIFO_board2host_din <= 130'd0;
			FIFO_board2host_wr_en <= 1'b0;
			usr_board2host_rd_en <= 1'b0;
		end
		else begin
			if(usr_board2host_empty == 1'b0 && FIFO_board2host_prog_full == 1'b0)begin
				usr_board2host_rd_en <= 1'b1;
			end
			else begin
				usr_board2host_rd_en <= 1'b0;
			end
			if(usr_board2host_empty == 1'b0 && usr_board2host_rd_en == 1'b1)begin
				FIFO_board2host_din <= usr_board2host_dout;
				FIFO_board2host_wr_en <= 1'b1;
			end
			else begin
				FIFO_board2host_din <= 130'd0;
				FIFO_board2host_wr_en <= 1'b0;
			end
		end
	end

	FIFO_asyn_width130_depth512_FWFT_almpt usr_board2host(
		.wr_clk(usr_board2host_clk),
		.rd_clk(trn_clk),
		.rst(reset),
		.din(usr_board2host_din),
		.wr_en(usr_board2host_wr_en),
		.rd_en(usr_board2host_rd_en),
		.prog_full_thresh(9'd500),
		.dout(usr_board2host_dout),
		.full(),
		.empty(usr_board2host_empty),
		.almost_empty(),
		.prog_full(usr_board2host_prog_full)
	);

endmodule
