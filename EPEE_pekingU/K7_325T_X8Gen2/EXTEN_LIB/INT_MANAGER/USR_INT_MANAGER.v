`timescale 1ns / 1ps
/////////////////////////////////////
//
// USR_INT_MANAGER.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Provide simpler interface for user defined interrupt (UDI). It supports 2 interrupt
// channels. It also switches the clock domain between EPEE and user hardware.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define USR_INT_MAN_INIT 3'b001
`define USR_INT_MAN_REQ  3'b010
`define USR_INT_MAN_CLR  3'b100

module USR_INT_MANAGER(
	// TO PCIe communication lib, in PCIe clock domain
	input  sys_clk,
	input  reset,
	output usr_int_req,
	output [2:0] usr_int_vector,
	input  [7:0] usr_int_sw_waiting,
	input  usr_int_clr,
	input  usr_int_enable,
	// To user, in user clock domain
	input  usr_clk,
	// user interrupt 0 (vector = 0)
	input  usr0_int_req,
	output usr0_int_clr,
	output usr0_int_enable,
	// user interrupt 1 (vector = 1)
	input  usr1_int_req,
	output usr1_int_clr,
	output usr1_int_enable
);

	/*in PCIe Clock domain*/
	(*KEEP="TRUE"*)reg [2:0] state;
	
	(*KEEP="TRUE"*)reg int_req;
	(*KEEP="TRUE"*)reg [2:0] int_vector;
	assign usr_int_req = int_req;
	assign usr_int_vector = int_vector;
	
	wire sys_int_req_0;
	(*KEEP="TRUE"*)reg sys_int_clr_0;
	(*KEEP="TRUE"*)reg sys_int_enable_0;
	
	wire sys_int_req_1;
	(*KEEP="TRUE"*)reg sys_int_clr_1;
	(*KEEP="TRUE"*)reg sys_int_enable_1;
	
	/*for interrupt enable*/
	always @(posedge sys_clk)begin
		if(reset == 1'b1)begin
			sys_int_enable_0 <= 1'b0;
			sys_int_enable_1 <= 1'b0;
		end
		else begin
			sys_int_enable_0 <= (usr_int_enable & usr_int_sw_waiting[0]);
			sys_int_enable_1 <= (usr_int_enable & usr_int_sw_waiting[1]);
		end
	end
	
	/* for user interrupt */
	always @(posedge sys_clk)begin
		if(reset == 1'b1 || usr_int_enable == 1'b0)begin
			state <= `USR_INT_MAN_INIT;
			
			int_req <= 1'b0;
			int_vector <= 3'd0;
			
			sys_int_clr_0 <= 1'b0;
			sys_int_clr_1 <= 1'b0;
		end
		else begin
			case(state)
				`USR_INT_MAN_INIT:begin
					sys_int_clr_0 <= 1'b0;
					sys_int_clr_1 <= 1'b0;
					if(sys_int_req_0 == 1'b1)begin
						int_req <= 1'b1;
						int_vector <= 3'd0;
						state <= `USR_INT_MAN_REQ;
					end
					else if(sys_int_req_1 == 1'b1)begin
						int_req <= 1'b1;
						int_vector <= 3'd1;
						state <= `USR_INT_MAN_REQ;
					end
					else begin
						int_req <= 1'b0;
						int_vector <= 3'd0;
						state <= `USR_INT_MAN_INIT;
					end
				end
				`USR_INT_MAN_REQ:begin
					if(usr_int_clr == 1'b1)begin
						case(int_vector)
							3'd0:begin
								state <= `USR_INT_MAN_CLR;
								int_req <= 1'b1;
								int_vector <= int_vector;
								sys_int_clr_0 <= 1'b1;
								sys_int_clr_1 <= 1'b0;
							end
							3'd1:begin
								state <= `USR_INT_MAN_CLR;
								int_req <= 1'b1;
								int_vector <= int_vector;
								sys_int_clr_0 <= 1'b0;
								sys_int_clr_1 <= 1'b1;
							end
							default:begin
								state <= `USR_INT_MAN_INIT;
								int_req <= 1'b0;
								int_vector <= 3'd0;
								sys_int_clr_0 <= 1'b0;
								sys_int_clr_1 <= 1'b0;
							end
						endcase
					end
					else begin
						state <= `USR_INT_MAN_REQ;
						int_req <= 1'b1;
						int_vector <= int_vector;
						sys_int_clr_0 <= 1'b0;
						sys_int_clr_1 <= 1'b0;
					end
				end
				`USR_INT_MAN_CLR:begin
					case(int_vector)
						3'd0:begin
							if(sys_int_req_0 == 1'b0)begin
								state <= `USR_INT_MAN_INIT;
								int_req <= 1'b0;
								int_vector <= 3'd0;
								sys_int_clr_0 <= 1'b0;
								sys_int_clr_1 <= 1'b0;
							end
							else begin
								state <= state;
							end
						end
						3'd1:begin
							if(sys_int_req_1 == 1'b0)begin
								state <= `USR_INT_MAN_INIT;
								int_req <= 1'b0;
								int_vector <= 3'd0;
								sys_int_clr_0 <= 1'b0;
								sys_int_clr_1 <= 1'b0;
							end
							else begin
								state <= state;
							end
						end
						default:begin
							state <= `USR_INT_MAN_INIT;
							int_req <= 1'b0;
							int_vector <= 3'd0;
							sys_int_clr_0 <= 1'b0;
							sys_int_clr_1 <= 1'b0;
						end
					endcase
				end
				default:begin
					state <= `USR_INT_MAN_INIT;
					int_req <= 1'b0;
					int_vector <= 3'd0;
					sys_int_clr_0 <= 1'b0;
					sys_int_clr_1 <= 1'b0;
				end
			endcase
		end
	end

	clock_domain_switch_width1 int_req_0_switch(
		.in_clk(usr_clk),
		.out_clk(sys_clk),
		.din(usr0_int_req),
		.dout(sys_int_req_0)
	);
	
	clock_domain_switch_width1 int_clr_0_switch(
		.in_clk(sys_clk),
		.out_clk(usr_clk),
		.din(sys_int_clr_0),
		.dout(usr0_int_clr)
	);
	
	clock_domain_switch_width1 int_enable_0_switch(
		.in_clk(sys_clk),
		.out_clk(usr_clk),
		.din(sys_int_enable_0),
		.dout(usr0_int_enable)
	);
	
	clock_domain_switch_width1 int_req_1_switch(
		.in_clk(usr_clk),
		.out_clk(sys_clk),
		.din(usr1_int_req),
		.dout(sys_int_req_1)
	);
	
	clock_domain_switch_width1 int_clr_1_switch(
		.in_clk(sys_clk),
		.out_clk(usr_clk),
		.din(sys_int_clr_1),
		.dout(usr1_int_clr)
	);
	
	clock_domain_switch_width1 int_enable_1_switch(
		.in_clk(sys_clk),
		.out_clk(usr_clk),
		.din(sys_int_enable_1),
		.dout(usr1_int_enable)
	);

endmodule
