`timescale 1ns / 1ps
/////////////////////////////////////
//
// RESET_GEN.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// Generate reset signals.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define USR_RST_INIT  3'b001
`define USR_RST_COUNT 3'b010
`define USR_RST_CLR   3'b100

module RESET_GEN #(
	parameter USR_RST_LEAST_CYCLE = 10 // range from 1-1023, in user clock domain
)
(
	input trn_clk,
	input usr_rst_clk,
	input trn_reset_n,
	input RST_USR,
	output reg RST_USR_done,
	output reg usr_rst
);
	// from PCIe clock domain to user clock domain
	reg RST_USR_pre;
	reg RST_USR_post;
	always @(posedge usr_rst_clk)begin
		RST_USR_pre <= RST_USR;
		RST_USR_post <= RST_USR_pre;
	end
	reg trn_reset_n_pre;
	reg trn_reset_n_post;
	always @(posedge usr_rst_clk)begin
		trn_reset_n_pre <= trn_reset_n;
		trn_reset_n_post <= trn_reset_n_pre;
	end
	
	// from user clock domain to PCIe clock domain
	reg RST_USR_done_pre;
	reg RST_USR_done_post;
	always @(posedge trn_clk)begin
		RST_USR_done_post <= RST_USR_done_pre;
		RST_USR_done <= RST_USR_done_post;
	end
	
	// in user clock domain
	reg [2 : 0] reset_status;
	reg [9 : 0] reset_counter;
	always @(posedge usr_rst_clk)begin
		if(~trn_reset_n_post)begin
			RST_USR_done_pre <= 1'b0;
			usr_rst <= 1'b1;
			reset_counter <= 10'd0;
			reset_status <= `USR_RST_INIT;
		end
		else begin
			case(reset_status)
				`USR_RST_INIT:begin
					RST_USR_done_pre <= 1'b0;
					usr_rst <= 1'b0;
					reset_counter <= 10'd0;
					reset_status <= ((RST_USR_post == 1'b1) ? `USR_RST_COUNT : `USR_RST_INIT);
				end
				`USR_RST_COUNT:begin
					RST_USR_done_pre <= 1'b0;
					usr_rst <= 1'b1;
					reset_counter <= reset_counter + 1'd1;
					reset_status <= ((reset_counter >= USR_RST_LEAST_CYCLE) ? `USR_RST_CLR : `USR_RST_COUNT);
				end
				`USR_RST_CLR:begin
					RST_USR_done_pre <= 1'b1;
					usr_rst <= 1'b1;
					reset_counter <= 10'd0;
					reset_status <= ((RST_USR_post == 1'b0) ? `USR_RST_INIT : `USR_RST_CLR);
				end
				default:begin
					RST_USR_done_pre <= 1'b0;
					usr_rst <= 1'b1;
					reset_counter <= 10'd0;
					reset_status <= `USR_RST_INIT;
				end
			endcase
		end
	end

endmodule
