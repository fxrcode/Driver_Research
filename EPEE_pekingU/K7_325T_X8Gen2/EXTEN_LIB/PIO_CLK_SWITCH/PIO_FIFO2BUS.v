`timescale 1ns / 1ps
/////////////////////////////////////
//
// PIO_FIFO2BUS.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// PIO FIFO to bus interface. It analyzes the PIO transaction in PIO bus and translate it
// to PIO bus.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define PIO_FIFO2BUS_INIT     3'b001
`define PIO_FIFO2BUS_RD_WAIT  3'b010
`define PIO_FIFO2BUS_WR_WAIT  3'b100

module PIO_FIFO2BUS(
	input usr_clk,
	input reset,
	// for PIO FIFO, in user clock domain
	input  [47:0] pio_2usr_fifo_dout, // {write_enable, addr[14:0], write_data[31:0]}
	input  pio_2usr_fifo_empty,
	output reg  pio_2usr_fifo_rd_en,
	output reg  [33:0] pio_2pcie_fifo_din, // {write_ack, read_ack, read_data[31:0]}
	output reg  pio_2pcie_fifo_wr_en,
	input  pio_2pcie_fifo_full,
	// PIO bus in user side
	output reg  [14:0] usr_pio_wr_addr,
	output reg  [31:0] usr_pio_wr_data,
	output reg  usr_pio_wr_req,
	input  usr_pio_wr_ack,
	output reg  [14:0] usr_pio_rd_addr,
	input  [31:0] usr_pio_rd_data,
	output reg  usr_pio_rd_req,
	input  usr_pio_rd_ack
	);

	(*KEEP="TRUE"*)reg [2:0] pio_fifo2bus_state;

	always @(posedge usr_clk)begin
		if(reset == 1'b1)begin
			pio_2usr_fifo_rd_en <= 1'b0;
			pio_2pcie_fifo_din <= 33'd0;
			pio_2pcie_fifo_wr_en <= 1'b0;
			usr_pio_wr_addr <= 15'd0;
			usr_pio_wr_data <= 32'd0;
			usr_pio_wr_req <= 1'b0;
			usr_pio_rd_addr <= 15'd0;
			usr_pio_rd_req <= 1'b0;
			pio_fifo2bus_state <= `PIO_FIFO2BUS_INIT;
		end
		else begin
			case(pio_fifo2bus_state)
				`PIO_FIFO2BUS_INIT:begin
					if(pio_2usr_fifo_empty == 1'b0)begin
						if(pio_2usr_fifo_dout[47] == 1'b1)begin
							// pio write operation
							pio_fifo2bus_state <= `PIO_FIFO2BUS_WR_WAIT;
							usr_pio_wr_addr <= pio_2usr_fifo_dout[14+32:32];
							usr_pio_wr_data <= pio_2usr_fifo_dout[31:0];
							usr_pio_wr_req <= 1'b1;
							usr_pio_rd_req <= 1'b0;
						end
						else begin
							// pio read operation
							pio_fifo2bus_state <= `PIO_FIFO2BUS_RD_WAIT;
							usr_pio_rd_addr <= pio_2usr_fifo_dout[14+32:32];
							usr_pio_rd_req <= 1'b1;
							usr_pio_wr_req <= 1'b0;
						end
						pio_2usr_fifo_rd_en <= 1'b1;
					end
					else begin
						pio_2usr_fifo_rd_en <= 1'b0;
						pio_2pcie_fifo_wr_en <= 1'b0;
						usr_pio_wr_req <= 1'b0;
						usr_pio_rd_req <= 1'b0;
						pio_fifo2bus_state <= `PIO_FIFO2BUS_INIT;
					end
				end
				`PIO_FIFO2BUS_RD_WAIT:begin
					pio_2usr_fifo_rd_en <= 1'b0;
					if(usr_pio_rd_ack == 1'b1)begin
						pio_2pcie_fifo_wr_en <= 1'b1;
						pio_2pcie_fifo_din <= {1'b0, 1'b1, usr_pio_rd_data[31:0]};
						pio_fifo2bus_state <= `PIO_FIFO2BUS_INIT;
						usr_pio_wr_req <= 1'b0;
						usr_pio_rd_req <= 1'b0;
					end
					else begin
						usr_pio_wr_req <= 1'b0;
						usr_pio_rd_req <= 1'b1;
						pio_2pcie_fifo_wr_en <= 1'b0;
						pio_fifo2bus_state <= `PIO_FIFO2BUS_RD_WAIT;
					end
				end
				`PIO_FIFO2BUS_WR_WAIT:begin
					pio_2usr_fifo_rd_en <= 1'b0;
					if(usr_pio_wr_ack == 1'b1)begin
						pio_2pcie_fifo_wr_en <= 1'b1;
						pio_2pcie_fifo_din <= {1'b1, 1'b0, 32'd0};
						pio_fifo2bus_state <= `PIO_FIFO2BUS_INIT;
						usr_pio_wr_req <= 1'b0;
						usr_pio_rd_req <= 1'b0;
					end
					else begin
						usr_pio_wr_req <= 1'b1;
						usr_pio_rd_req <= 1'b0;
						pio_2pcie_fifo_wr_en <= 1'b0;
						pio_fifo2bus_state <= `PIO_FIFO2BUS_WR_WAIT;
					end
				end
				default:begin
					pio_2usr_fifo_rd_en <= 1'b0;
					pio_2pcie_fifo_din <= 33'd0;
					pio_2pcie_fifo_wr_en <= 1'b0;
					usr_pio_wr_addr <= 15'd0;
					usr_pio_wr_data <= 32'd0;
					usr_pio_wr_req <= 1'b0;
					usr_pio_rd_addr <= 15'd0;
					usr_pio_rd_req <= 1'b0;
					pio_fifo2bus_state <= `PIO_FIFO2BUS_INIT;
				end
			endcase
		end
	end

endmodule
