`timescale 1ns / 1ps
/////////////////////////////////////
//
// PIO_CLK_SWITCH.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// PIO clock domain switch. it switchs clock domain and devides the PIO interface into 4 
// independent PIO channels.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module PIO_CLK_SWITCH(
	input trn_clk,
	input usr_clk,
	input reset,
	// PIO from PCIe system, in trn_clk domain
	input  [16:0] usr_pio_wr_addr,
	input  [31:0] usr_pio_wr_data,
	input  usr_pio_wr_req,
	output usr_pio_wr_ack,
	input  [16:0] usr_pio_rd_addr,
	output [31:0] usr_pio_rd_data,
	input  usr_pio_rd_req,
	output usr_pio_rd_ack,
	 
	// PIO bus in user clock domain, ch0
	output [14:0] usr_pio_ch0_wr_addr,
	output [31:0] usr_pio_ch0_wr_data,
	output usr_pio_ch0_wr_req,
	input  usr_pio_ch0_wr_ack,
	output [14:0] usr_pio_ch0_rd_addr,
	input  [31:0] usr_pio_ch0_rd_data,
	output usr_pio_ch0_rd_req,
	input  usr_pio_ch0_rd_ack,	
	// PIO bus in user clock domain, ch1
	output [14:0] usr_pio_ch1_wr_addr,
	output [31:0] usr_pio_ch1_wr_data,
	output usr_pio_ch1_wr_req,
	input  usr_pio_ch1_wr_ack,
	output [14:0] usr_pio_ch1_rd_addr,
	input  [31:0] usr_pio_ch1_rd_data,
	output usr_pio_ch1_rd_req,
	input  usr_pio_ch1_rd_ack,
	// PIO bus in user clock domain, ch2
	output [14:0] usr_pio_ch2_wr_addr,
	output [31:0] usr_pio_ch2_wr_data,
	output usr_pio_ch2_wr_req,
	input  usr_pio_ch2_wr_ack,
	output [14:0] usr_pio_ch2_rd_addr,
	input  [31:0] usr_pio_ch2_rd_data,
	output usr_pio_ch2_rd_req,
	input  usr_pio_ch2_rd_ack,
	// PIO bus in user side, ch3
	output [14:0] usr_pio_ch3_wr_addr,
	output [31:0] usr_pio_ch3_wr_data,
	output usr_pio_ch3_wr_req,
	input  usr_pio_ch3_wr_ack,
	output [14:0] usr_pio_ch3_rd_addr,
	input  [31:0] usr_pio_ch3_rd_data,
	output usr_pio_ch3_rd_req,
	input  usr_pio_ch3_rd_ack
);

	// to user: channel 0; addr[16:15] = 2'b00
	wire [47:0] pio_ch0_2usr_fifo_dout; // {write_enable; addr[14:0]; write_data[31:0]}
	wire pio_ch0_2usr_fifo_empty;
	wire pio_ch0_2usr_fifo_rd_en;
	wire [33:0] pio_ch0_2pcie_fifo_din; // {write_ack; read_ack; read_data[31:0]}
	wire pio_ch0_2pcie_fifo_wr_en;
	wire pio_ch0_2pcie_fifo_full;
	
	// to user: channel 1; addr[16:15] = 2'b00
	wire [47:0] pio_ch1_2usr_fifo_dout; // {write_enable; addr[14:0]; write_data[31:0]}
	wire pio_ch1_2usr_fifo_empty;
	wire pio_ch1_2usr_fifo_rd_en;
	wire [33:0] pio_ch1_2pcie_fifo_din; // {write_ack; read_ack; read_data[31:0]}
	wire pio_ch1_2pcie_fifo_wr_en;
	wire pio_ch1_2pcie_fifo_full;
	
	// to user: channel 2; addr[16:15] = 2'b00
	wire [47:0] pio_ch2_2usr_fifo_dout; // {write_enable; addr[14:0]; write_data[31:0]}
	wire pio_ch2_2usr_fifo_empty;
	wire pio_ch2_2usr_fifo_rd_en;
	wire [33:0] pio_ch2_2pcie_fifo_din; // {write_ack; read_ack; read_data[31:0]}
	wire pio_ch2_2pcie_fifo_wr_en;
	wire pio_ch2_2pcie_fifo_full;
	
	// to user: channel 0; addr[16:15] = 2'b00
	wire [47:0] pio_ch3_2usr_fifo_dout; // {write_enable; addr[14:0]; write_data[31:0]}
	wire pio_ch3_2usr_fifo_empty;
	wire pio_ch3_2usr_fifo_rd_en;
	wire [33:0] pio_ch3_2pcie_fifo_din; // {write_ack; read_ack; read_data[31:0]}
	wire pio_ch3_2pcie_fifo_wr_en;
	wire pio_ch3_2pcie_fifo_full;
	
	PIO_BUS2FIFO PIO_BUS2FIFO(
		.trn_clk(trn_clk),
		.usr_clk(usr_clk),
		.reset(reset),
		/*PIO bus, pcie clock domain*/
		.usr_pio_wr_addr(usr_pio_wr_addr),
		.usr_pio_wr_data(usr_pio_wr_data),
		.usr_pio_wr_req(usr_pio_wr_req),
		.usr_pio_wr_ack(usr_pio_wr_ack),
		.usr_pio_rd_addr(usr_pio_rd_addr),
		.usr_pio_rd_data(usr_pio_rd_data),
		.usr_pio_rd_req(usr_pio_rd_req),
		.usr_pio_rd_ack(usr_pio_rd_ack),
		/*PIO fifo, user clock domain*/
		// channel 0, addr[16:15] = 2'b00
		.pio_ch0_2usr_fifo_dout(pio_ch0_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_ch0_2usr_fifo_empty(pio_ch0_2usr_fifo_empty),
		.pio_ch0_2usr_fifo_rd_en(pio_ch0_2usr_fifo_rd_en),
		.pio_ch0_2pcie_fifo_din(pio_ch0_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_ch0_2pcie_fifo_wr_en(pio_ch0_2pcie_fifo_wr_en),
		.pio_ch0_2pcie_fifo_full(pio_ch0_2pcie_fifo_full),
		// channel 1, addr[16:15] = 2'b01
		.pio_ch1_2usr_fifo_dout(pio_ch1_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_ch1_2usr_fifo_empty(pio_ch1_2usr_fifo_empty),
		.pio_ch1_2usr_fifo_rd_en(pio_ch1_2usr_fifo_rd_en),
		.pio_ch1_2pcie_fifo_din(pio_ch1_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_ch1_2pcie_fifo_wr_en(pio_ch1_2pcie_fifo_wr_en),
		.pio_ch1_2pcie_fifo_full(pio_ch1_2pcie_fifo_full),
		// channel 2, addr[16:15] = 2'b10
		.pio_ch2_2usr_fifo_dout(pio_ch2_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_ch2_2usr_fifo_empty(pio_ch2_2usr_fifo_empty),
		.pio_ch2_2usr_fifo_rd_en(pio_ch2_2usr_fifo_rd_en),
		.pio_ch2_2pcie_fifo_din(pio_ch2_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_ch2_2pcie_fifo_wr_en(pio_ch2_2pcie_fifo_wr_en),
		.pio_ch2_2pcie_fifo_full(pio_ch2_2pcie_fifo_full),
		// channel 3, addr[16:15] = 2'b11
		.pio_ch3_2usr_fifo_dout(pio_ch3_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_ch3_2usr_fifo_empty(pio_ch3_2usr_fifo_empty),
		.pio_ch3_2usr_fifo_rd_en(pio_ch3_2usr_fifo_rd_en),
		.pio_ch3_2pcie_fifo_din(pio_ch3_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_ch3_2pcie_fifo_wr_en(pio_ch3_2pcie_fifo_wr_en),
		.pio_ch3_2pcie_fifo_full(pio_ch3_2pcie_fifo_full)
	);

	PIO_FIFO2BUS PIO_FIFO2BUS_ch0(
		.usr_clk(usr_clk),
		.reset(reset),
		// for PIO FIFO, in user clock domain
		.pio_2usr_fifo_dout(pio_ch0_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_2usr_fifo_empty(pio_ch0_2usr_fifo_empty),
		.pio_2usr_fifo_rd_en(pio_ch0_2usr_fifo_rd_en),
		.pio_2pcie_fifo_din(pio_ch0_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_2pcie_fifo_wr_en(pio_ch0_2pcie_fifo_wr_en),
		.pio_2pcie_fifo_full(pio_ch0_2pcie_fifo_full),
		// PIO bus in user side
		.usr_pio_wr_addr(usr_pio_ch0_wr_addr),
		.usr_pio_wr_data(usr_pio_ch0_wr_data),
		.usr_pio_wr_req(usr_pio_ch0_wr_req),
		.usr_pio_wr_ack(usr_pio_ch0_wr_ack),
		.usr_pio_rd_addr(usr_pio_ch0_rd_addr),
		.usr_pio_rd_data(usr_pio_ch0_rd_data),
		.usr_pio_rd_req(usr_pio_ch0_rd_req),
		.usr_pio_rd_ack(usr_pio_ch0_rd_ack)
	);
	PIO_FIFO2BUS PIO_FIFO2BUS_ch1(
		.usr_clk(usr_clk),
		.reset(reset),
		// for PIO FIFO, in user clock domain
		.pio_2usr_fifo_dout(pio_ch1_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_2usr_fifo_empty(pio_ch1_2usr_fifo_empty),
		.pio_2usr_fifo_rd_en(pio_ch1_2usr_fifo_rd_en),
		.pio_2pcie_fifo_din(pio_ch1_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_2pcie_fifo_wr_en(pio_ch1_2pcie_fifo_wr_en),
		.pio_2pcie_fifo_full(pio_ch1_2pcie_fifo_full),
		// PIO bus in user side
		.usr_pio_wr_addr(usr_pio_ch1_wr_addr),
		.usr_pio_wr_data(usr_pio_ch1_wr_data),
		.usr_pio_wr_req(usr_pio_ch1_wr_req),
		.usr_pio_wr_ack(usr_pio_ch1_wr_ack),
		.usr_pio_rd_addr(usr_pio_ch1_rd_addr),
		.usr_pio_rd_data(usr_pio_ch1_rd_data),
		.usr_pio_rd_req(usr_pio_ch1_rd_req),
		.usr_pio_rd_ack(usr_pio_ch1_rd_ack)
	);
	PIO_FIFO2BUS PIO_FIFO2BUS_ch2(
		.usr_clk(usr_clk),
		.reset(reset),
		// for PIO FIFO, in user clock domain
		.pio_2usr_fifo_dout(pio_ch2_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_2usr_fifo_empty(pio_ch2_2usr_fifo_empty),
		.pio_2usr_fifo_rd_en(pio_ch2_2usr_fifo_rd_en),
		.pio_2pcie_fifo_din(pio_ch2_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_2pcie_fifo_wr_en(pio_ch2_2pcie_fifo_wr_en),
		.pio_2pcie_fifo_full(pio_ch2_2pcie_fifo_full),
		// PIO bus in user side
		.usr_pio_wr_addr(usr_pio_ch2_wr_addr),
		.usr_pio_wr_data(usr_pio_ch2_wr_data),
		.usr_pio_wr_req(usr_pio_ch2_wr_req),
		.usr_pio_wr_ack(usr_pio_ch2_wr_ack),
		.usr_pio_rd_addr(usr_pio_ch2_rd_addr),
		.usr_pio_rd_data(usr_pio_ch2_rd_data),
		.usr_pio_rd_req(usr_pio_ch2_rd_req),
		.usr_pio_rd_ack(usr_pio_ch2_rd_ack)
	);
	PIO_FIFO2BUS PIO_FIFO2BUS_ch3(
		.usr_clk(usr_clk),
		.reset(reset),
		// for PIO FIFO, in user clock domain
		.pio_2usr_fifo_dout(pio_ch3_2usr_fifo_dout), // {write_enable, addr[14:0], write_data[31:0]}
		.pio_2usr_fifo_empty(pio_ch3_2usr_fifo_empty),
		.pio_2usr_fifo_rd_en(pio_ch3_2usr_fifo_rd_en),
		.pio_2pcie_fifo_din(pio_ch3_2pcie_fifo_din), // {write_ack, read_ack, read_data[31:0]}
		.pio_2pcie_fifo_wr_en(pio_ch3_2pcie_fifo_wr_en),
		.pio_2pcie_fifo_full(pio_ch3_2pcie_fifo_full),
		// PIO bus in user side
		.usr_pio_wr_addr(usr_pio_ch3_wr_addr),
		.usr_pio_wr_data(usr_pio_ch3_wr_data),
		.usr_pio_wr_req(usr_pio_ch3_wr_req),
		.usr_pio_wr_ack(usr_pio_ch3_wr_ack),
		.usr_pio_rd_addr(usr_pio_ch3_rd_addr),
		.usr_pio_rd_data(usr_pio_ch3_rd_data),
		.usr_pio_rd_req(usr_pio_ch3_rd_req),
		.usr_pio_rd_ack(usr_pio_ch3_rd_ack)
	);
endmodule
