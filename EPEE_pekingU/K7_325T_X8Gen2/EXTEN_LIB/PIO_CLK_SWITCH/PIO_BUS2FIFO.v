`timescale 1ns / 1ps
/////////////////////////////////////
//
// PIO_BUS2FIFO.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// PIO bus to FIFO interface. Change the read & write operation in PIO bus into PIO 
// transactions and put those transactions into FIFO.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

`define PIO_BUS2FIFO_INIT      7'b0000001
`define PIO_BUS2FIFO_WR_REQ    7'b0000010
`define PIO_BUS2FIFO_RD_REQ    7'b0000100
`define PIO_BUS2FIFO_WR_WT_ACK 7'b0001000
`define PIO_BUS2FIFO_RD_WT_ACK 7'b0010000
`define PIO_BUS2FIFO_WR_CLR    7'b0100000
`define PIO_BUS2FIFO_RD_CLR    7'b1000000

module PIO_BUS2FIFO(
	input trn_clk,
	input usr_clk,
	input reset,
	/*PIO bus, pcie clock domain*/
	input  [16:0] usr_pio_wr_addr,
	input  [31:0] usr_pio_wr_data,
	input  usr_pio_wr_req,
	output reg usr_pio_wr_ack,
	input  [16:0] usr_pio_rd_addr,
	output reg [31:0] usr_pio_rd_data,
	input  usr_pio_rd_req,
	output reg usr_pio_rd_ack,
	/*PIO fifo, user clock domain*/
	// channel 0, addr[16:15] = 2'b00
	output [47:0] pio_ch0_2usr_fifo_dout, // {write_enable, addr[14:0], write_data[31:0]}
	output pio_ch0_2usr_fifo_empty,
	input  pio_ch0_2usr_fifo_rd_en,
	input  [33:0] pio_ch0_2pcie_fifo_din, // {write_ack, read_ack, read_data[31:0]}
	input  pio_ch0_2pcie_fifo_wr_en,
	output pio_ch0_2pcie_fifo_full,
	// channel 1, addr[16:15] = 2'b01
	output [47:0] pio_ch1_2usr_fifo_dout, // {write_enable, addr[14:0], write_data[31:0]}
	output pio_ch1_2usr_fifo_empty,
	input  pio_ch1_2usr_fifo_rd_en,
	input  [33:0] pio_ch1_2pcie_fifo_din, // {write_ack, read_ack, read_data[31:0]}
	input  pio_ch1_2pcie_fifo_wr_en,
	output pio_ch1_2pcie_fifo_full,
	// channel 2, addr[16:15] = 2'b10
	output [47:0] pio_ch2_2usr_fifo_dout, // {write_enable, addr[14:0], write_data[31:0]}
	output pio_ch2_2usr_fifo_empty,
	input  pio_ch2_2usr_fifo_rd_en,
	input  [33:0] pio_ch2_2pcie_fifo_din, // {write_ack, read_ack, read_data[31:0]}
	input  pio_ch2_2pcie_fifo_wr_en,
	output pio_ch2_2pcie_fifo_full,
	// channel 3, addr[16:15] = 2'b11
	output [47:0] pio_ch3_2usr_fifo_dout, // {write_enable, addr[14:0], write_data[31:0]}
	output pio_ch3_2usr_fifo_empty,
	input  pio_ch3_2usr_fifo_rd_en,
	input  [33:0] pio_ch3_2pcie_fifo_din, // {write_ack, read_ack, read_data[31:0]}
	input  pio_ch3_2pcie_fifo_wr_en,
	output pio_ch3_2pcie_fifo_full
);

	(*KEEP="TRUE"*)reg [6:0] PIO_BUS2FIFO_state;
	// for channel 0
	reg  [47:0] pio_ch0_2usr_fifo_din;
	reg  pio_ch0_2usr_fifo_wr_en;
	wire pio_ch0_2usr_fifo_full;
	wire [33:0] pio_ch0_2pcie_fifo_dout;
	wire pio_ch0_2pcie_fifo_empty;
	reg  pio_ch0_2pcie_fifo_rd_en;
	// for channel 1
	reg  [47:0] pio_ch1_2usr_fifo_din;
	reg  pio_ch1_2usr_fifo_wr_en;
	wire pio_ch1_2usr_fifo_full;
	wire [33:0] pio_ch1_2pcie_fifo_dout;
	wire pio_ch1_2pcie_fifo_empty;
	reg  pio_ch1_2pcie_fifo_rd_en;
	// for channel 2
	reg  [47:0] pio_ch2_2usr_fifo_din;
	reg  pio_ch2_2usr_fifo_wr_en;
	wire pio_ch2_2usr_fifo_full;
	wire [33:0] pio_ch2_2pcie_fifo_dout;
	wire pio_ch2_2pcie_fifo_empty;
	reg  pio_ch2_2pcie_fifo_rd_en;
	// for channel 3
	reg  [47:0] pio_ch3_2usr_fifo_din;
	reg  pio_ch3_2usr_fifo_wr_en;
	wire pio_ch3_2usr_fifo_full;
	wire [33:0] pio_ch3_2pcie_fifo_dout;
	wire pio_ch3_2pcie_fifo_empty;
	reg  pio_ch3_2pcie_fifo_rd_en;
	
	always @(posedge trn_clk)begin
		if(reset == 1'b1)begin
			usr_pio_wr_ack <= 1'b0;
			usr_pio_rd_data <= 1'b0;
			usr_pio_rd_ack <= 1'b0;
			
			PIO_BUS2FIFO_state <= `PIO_BUS2FIFO_INIT;
			
			pio_ch0_2usr_fifo_din <= 48'd0;
			pio_ch0_2usr_fifo_wr_en <= 1'b0;
			pio_ch0_2pcie_fifo_rd_en <= 1'b0;
			
			pio_ch1_2usr_fifo_din <= 48'd0;
			pio_ch1_2usr_fifo_wr_en <= 1'b0;
			pio_ch1_2pcie_fifo_rd_en <= 1'b0;
			
			pio_ch2_2usr_fifo_din <= 48'd0;
			pio_ch2_2usr_fifo_wr_en <= 1'b0;
			pio_ch2_2pcie_fifo_rd_en <= 1'b0;
			
			pio_ch3_2usr_fifo_din <= 48'd0;
			pio_ch3_2usr_fifo_wr_en <= 1'b0;
			pio_ch3_2pcie_fifo_rd_en <= 1'b0;
		end
		else begin
			case(PIO_BUS2FIFO_state)
				`PIO_BUS2FIFO_INIT:begin
					usr_pio_wr_ack <= 1'b0;
					usr_pio_rd_ack <= 1'b0;
					if(usr_pio_rd_req == 1'b1)begin // PIO bus read require
						case(usr_pio_rd_addr[16:15])
							2'b00:begin
								pio_ch0_2usr_fifo_din <= {1'b0, // write enable
																	usr_pio_rd_addr[14:0], //write address
																	32'd0}; // read operation, write data is not used
								pio_ch0_2usr_fifo_wr_en <= ((pio_ch0_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch0_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_RD_REQ:`PIO_BUS2FIFO_INIT);
							end
							2'b01:begin
								pio_ch1_2usr_fifo_din <= {1'b0, // write enable
																	usr_pio_rd_addr[14:0], //write address
																	32'd0}; // read operation, write data is not used
								pio_ch1_2usr_fifo_wr_en <= ((pio_ch1_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch1_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_RD_REQ:`PIO_BUS2FIFO_INIT);
							end
							2'b10:begin
								pio_ch2_2usr_fifo_din <= {1'b0, // write enable
																	usr_pio_rd_addr[14:0], //write address
																	32'd0}; // read operation, write data is not used
								pio_ch2_2usr_fifo_wr_en <= ((pio_ch2_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch2_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_RD_REQ:`PIO_BUS2FIFO_INIT);
							end
							2'b11:begin
								pio_ch3_2usr_fifo_din <= {1'b0, // write enable
																	usr_pio_rd_addr[14:0], //write address
																	32'd0}; // read operation, write data is not used
								pio_ch3_2usr_fifo_wr_en <= ((pio_ch3_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch3_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_RD_REQ:`PIO_BUS2FIFO_INIT);
							end
						endcase
					end
					else if(usr_pio_wr_req == 1'b1)begin // PIO bus write require
						case(usr_pio_wr_addr[16:15])
							2'b00:begin
								pio_ch0_2usr_fifo_din <= {1'b1, // write enable
																	usr_pio_wr_addr[14:0], //write address
																	usr_pio_wr_data[31:0]}; // write data
								pio_ch0_2usr_fifo_wr_en <= ((pio_ch0_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch0_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_WR_REQ:`PIO_BUS2FIFO_INIT);
							end
							2'b01:begin
								pio_ch1_2usr_fifo_din <= {1'b1, // write enable
																	usr_pio_wr_addr[14:0], //write address
																	usr_pio_wr_data[31:0]}; // write data
								pio_ch1_2usr_fifo_wr_en <= ((pio_ch1_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch1_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_WR_REQ:`PIO_BUS2FIFO_INIT);
							end
							2'b10:begin
								pio_ch2_2usr_fifo_din <= {1'b1, // write enable
																	usr_pio_wr_addr[14:0], //write address
																	usr_pio_wr_data[31:0]}; // write data
								pio_ch2_2usr_fifo_wr_en <= ((pio_ch2_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch2_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_WR_REQ:`PIO_BUS2FIFO_INIT);
							end
							2'b11:begin
								pio_ch3_2usr_fifo_din <= {1'b1, // write enable
																	usr_pio_wr_addr[14:0], //write address
																	usr_pio_wr_data[31:0]}; // write data
								pio_ch3_2usr_fifo_wr_en <= ((pio_ch3_2usr_fifo_full == 1'b0)?1'b1:1'b0);
								PIO_BUS2FIFO_state <= ((pio_ch3_2usr_fifo_full == 1'b0)?`PIO_BUS2FIFO_WR_REQ:`PIO_BUS2FIFO_INIT);
							end
						endcase
					end
					else begin
						usr_pio_rd_data <= 1'b0;
						
						PIO_BUS2FIFO_state <= `PIO_BUS2FIFO_INIT;
						
						pio_ch0_2usr_fifo_din <= 48'd0;
						pio_ch0_2usr_fifo_wr_en <= 1'b0;
						pio_ch0_2pcie_fifo_rd_en <= 1'b0;
						
						pio_ch1_2usr_fifo_din <= 48'd0;
						pio_ch1_2usr_fifo_wr_en <= 1'b0;
						pio_ch1_2pcie_fifo_rd_en <= 1'b0;
						
						pio_ch2_2usr_fifo_din <= 48'd0;
						pio_ch2_2usr_fifo_wr_en <= 1'b0;
						pio_ch2_2pcie_fifo_rd_en <= 1'b0;
						
						pio_ch3_2usr_fifo_din <= 48'd0;
						pio_ch3_2usr_fifo_wr_en <= 1'b0;
						pio_ch3_2pcie_fifo_rd_en <= 1'b0;
					end
				end
				`PIO_BUS2FIFO_WR_REQ:begin
					pio_ch0_2usr_fifo_wr_en <= 1'b0;
					pio_ch1_2usr_fifo_wr_en <= 1'b0;
					pio_ch2_2usr_fifo_wr_en <= 1'b0;
					pio_ch3_2usr_fifo_wr_en <= 1'b0;
					case(usr_pio_wr_addr[16:15])
						2'b00:begin
							pio_ch0_2pcie_fifo_rd_en <= ((pio_ch0_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch0_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_WR_WT_ACK:`PIO_BUS2FIFO_WR_REQ);
						end
						2'b01:begin
							pio_ch1_2pcie_fifo_rd_en <= ((pio_ch1_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch1_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_WR_WT_ACK:`PIO_BUS2FIFO_WR_REQ);
						end
						2'b10:begin
							pio_ch2_2pcie_fifo_rd_en <= ((pio_ch2_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch2_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_WR_WT_ACK:`PIO_BUS2FIFO_WR_REQ);
						end
						2'b11:begin
							pio_ch3_2pcie_fifo_rd_en <= ((pio_ch3_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch3_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_WR_WT_ACK:`PIO_BUS2FIFO_WR_REQ);
						end
					endcase
				end
				`PIO_BUS2FIFO_RD_REQ:begin
					pio_ch0_2usr_fifo_wr_en <= 1'b0;
					pio_ch1_2usr_fifo_wr_en <= 1'b0;
					pio_ch2_2usr_fifo_wr_en <= 1'b0;
					pio_ch3_2usr_fifo_wr_en <= 1'b0;
					case(usr_pio_rd_addr[16:15])
						2'b00:begin
							usr_pio_rd_data <= ((pio_ch0_2pcie_fifo_empty == 1'b0)? pio_ch0_2pcie_fifo_dout[31:0]: 32'd0);
							pio_ch0_2pcie_fifo_rd_en <= ((pio_ch0_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch0_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_RD_WT_ACK:`PIO_BUS2FIFO_RD_REQ);
						end
						2'b01:begin
							usr_pio_rd_data <= ((pio_ch1_2pcie_fifo_empty == 1'b0)? pio_ch1_2pcie_fifo_dout[31:0]: 32'd0);
							pio_ch1_2pcie_fifo_rd_en <= ((pio_ch1_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch1_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_RD_WT_ACK:`PIO_BUS2FIFO_RD_REQ);
						end
						2'b10:begin
							usr_pio_rd_data <= ((pio_ch2_2pcie_fifo_empty == 1'b0)? pio_ch2_2pcie_fifo_dout[31:0]: 32'd0);
							pio_ch2_2pcie_fifo_rd_en <= ((pio_ch2_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch2_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_RD_WT_ACK:`PIO_BUS2FIFO_RD_REQ);
						end
						2'b11:begin
							usr_pio_rd_data <= ((pio_ch3_2pcie_fifo_empty == 1'b0)? pio_ch3_2pcie_fifo_dout[31:0]: 32'd0);
							pio_ch3_2pcie_fifo_rd_en <= ((pio_ch3_2pcie_fifo_empty == 1'b0)?1'b1:1'b0);
							PIO_BUS2FIFO_state <= ((pio_ch3_2pcie_fifo_empty == 1'b0)?`PIO_BUS2FIFO_RD_WT_ACK:`PIO_BUS2FIFO_RD_REQ);
						end
					endcase
				end
				`PIO_BUS2FIFO_WR_WT_ACK:begin
					pio_ch0_2usr_fifo_din <= 48'd0;
					pio_ch0_2usr_fifo_wr_en <= 1'b0;
					pio_ch0_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch1_2usr_fifo_din <= 48'd0;
					pio_ch1_2usr_fifo_wr_en <= 1'b0;
					pio_ch1_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch2_2usr_fifo_din <= 48'd0;
					pio_ch2_2usr_fifo_wr_en <= 1'b0;
					pio_ch2_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch3_2usr_fifo_din <= 48'd0;
					pio_ch3_2usr_fifo_wr_en <= 1'b0;
					pio_ch3_2pcie_fifo_rd_en <= 1'b0;
					PIO_BUS2FIFO_state <= `PIO_BUS2FIFO_WR_CLR;
					
					usr_pio_wr_ack <= 1'b1;
					usr_pio_rd_data <= 1'b0;
					usr_pio_rd_ack <= 1'b0;
				end
				`PIO_BUS2FIFO_RD_WT_ACK:begin
					pio_ch0_2usr_fifo_din <= 48'd0;
					pio_ch0_2usr_fifo_wr_en <= 1'b0;
					pio_ch0_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch1_2usr_fifo_din <= 48'd0;
					pio_ch1_2usr_fifo_wr_en <= 1'b0;
					pio_ch1_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch2_2usr_fifo_din <= 48'd0;
					pio_ch2_2usr_fifo_wr_en <= 1'b0;
					pio_ch2_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch3_2usr_fifo_din <= 48'd0;
					pio_ch3_2usr_fifo_wr_en <= 1'b0;
					pio_ch3_2pcie_fifo_rd_en <= 1'b0;
					PIO_BUS2FIFO_state <= `PIO_BUS2FIFO_RD_CLR;
					
					usr_pio_wr_ack <= 1'b0;
					usr_pio_rd_data <= usr_pio_rd_data;
					usr_pio_rd_ack <= 1'b1;
				end
				`PIO_BUS2FIFO_WR_CLR:begin // wait one cycle for req signal to reset (to 0)
					usr_pio_wr_ack <= 1'b0;
					usr_pio_rd_ack <= 1'b0;
					if(usr_pio_wr_req == 1'b0)begin
						PIO_BUS2FIFO_state <= `PIO_BUS2FIFO_INIT;
					end
					else begin
						PIO_BUS2FIFO_state <= PIO_BUS2FIFO_state;
					end
				end
				`PIO_BUS2FIFO_RD_CLR:begin // wait one cycle for req signal to reset (to 0)
					usr_pio_wr_ack <= 1'b0;
					usr_pio_rd_ack <= 1'b0;
					if(usr_pio_rd_req == 1'b0)begin
						PIO_BUS2FIFO_state <= `PIO_BUS2FIFO_INIT;
					end
					else begin
						PIO_BUS2FIFO_state <= PIO_BUS2FIFO_state;
					end
				end
				default:begin
					usr_pio_wr_ack <= 1'b0;
					usr_pio_rd_data <= 1'b0;
					usr_pio_rd_ack <= 1'b0;
					
					PIO_BUS2FIFO_state <= `PIO_BUS2FIFO_INIT;
					
					pio_ch0_2usr_fifo_wr_en <= 1'b0;
					pio_ch0_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch1_2usr_fifo_wr_en <= 1'b0;
					pio_ch1_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch2_2usr_fifo_wr_en <= 1'b0;
					pio_ch2_2pcie_fifo_rd_en <= 1'b0;
					
					pio_ch3_2usr_fifo_wr_en <= 1'b0;
					pio_ch3_2pcie_fifo_rd_en <= 1'b0;
				end
			endcase
		end
	end
	
	FIFO_asyn_width48_depth16_FWFT pio_ch0_2usr_fifo(
		.rst(reset),
		.wr_clk(trn_clk),
		.rd_clk(usr_clk),
		.din(pio_ch0_2usr_fifo_din),
		.wr_en(pio_ch0_2usr_fifo_wr_en),
		.rd_en(pio_ch0_2usr_fifo_rd_en),
		.dout(pio_ch0_2usr_fifo_dout),
		.full(pio_ch0_2usr_fifo_full),
		.empty(pio_ch0_2usr_fifo_empty)
	);
	FIFO_asyn_width34_depth16_FWFT pio_ch0_2pcie_fifo(
		.rst(reset),
		.wr_clk(usr_clk),
		.rd_clk(trn_clk),
		.din(pio_ch0_2pcie_fifo_din),
		.wr_en(pio_ch0_2pcie_fifo_wr_en),
		.rd_en(pio_ch0_2pcie_fifo_rd_en),
		.dout(pio_ch0_2pcie_fifo_dout),
		.full(pio_ch0_2pcie_fifo_full),
		.empty(pio_ch0_2pcie_fifo_empty)
	);
	
	FIFO_asyn_width48_depth16_FWFT pio_ch1_2usr_fifo(
		.rst(reset),
		.wr_clk(trn_clk),
		.rd_clk(usr_clk),
		.din(pio_ch1_2usr_fifo_din),
		.wr_en(pio_ch1_2usr_fifo_wr_en),
		.rd_en(pio_ch1_2usr_fifo_rd_en),
		.dout(pio_ch1_2usr_fifo_dout),
		.full(pio_ch1_2usr_fifo_full),
		.empty(pio_ch1_2usr_fifo_empty)
	);
	FIFO_asyn_width34_depth16_FWFT pio_ch1_2pcie_fifo(
		.rst(reset),
		.wr_clk(usr_clk),
		.rd_clk(trn_clk),
		.din(pio_ch1_2pcie_fifo_din),
		.wr_en(pio_ch1_2pcie_fifo_wr_en),
		.rd_en(pio_ch1_2pcie_fifo_rd_en),
		.dout(pio_ch1_2pcie_fifo_dout),
		.full(pio_ch1_2pcie_fifo_full),
		.empty(pio_ch1_2pcie_fifo_empty)
	);
	
	FIFO_asyn_width48_depth16_FWFT pio_ch2_2usr_fifo(
		.rst(reset),
		.wr_clk(trn_clk),
		.rd_clk(usr_clk),
		.din(pio_ch2_2usr_fifo_din),
		.wr_en(pio_ch2_2usr_fifo_wr_en),
		.rd_en(pio_ch2_2usr_fifo_rd_en),
		.dout(pio_ch2_2usr_fifo_dout),
		.full(pio_ch2_2usr_fifo_full),
		.empty(pio_ch2_2usr_fifo_empty)
	);
	FIFO_asyn_width34_depth16_FWFT pio_ch2_2pcie_fifo(
		.rst(reset),
		.wr_clk(usr_clk),
		.rd_clk(trn_clk),
		.din(pio_ch2_2pcie_fifo_din),
		.wr_en(pio_ch2_2pcie_fifo_wr_en),
		.rd_en(pio_ch2_2pcie_fifo_rd_en),
		.dout(pio_ch2_2pcie_fifo_dout),
		.full(pio_ch2_2pcie_fifo_full),
		.empty(pio_ch2_2pcie_fifo_empty)
	);
	
	FIFO_asyn_width48_depth16_FWFT pio_ch3_2usr_fifo(
		.rst(reset),
		.wr_clk(trn_clk),
		.rd_clk(usr_clk),
		.din(pio_ch3_2usr_fifo_din),
		.wr_en(pio_ch3_2usr_fifo_wr_en),
		.rd_en(pio_ch3_2usr_fifo_rd_en),
		.dout(pio_ch3_2usr_fifo_dout),
		.full(pio_ch3_2usr_fifo_full),
		.empty(pio_ch3_2usr_fifo_empty)
	);
	FIFO_asyn_width34_depth16_FWFT pio_ch3_2pcie_fifo(
		.rst(reset),
		.wr_clk(usr_clk),
		.rd_clk(trn_clk),
		.din(pio_ch3_2pcie_fifo_din),
		.wr_en(pio_ch3_2pcie_fifo_wr_en),
		.rd_en(pio_ch3_2pcie_fifo_rd_en),
		.dout(pio_ch3_2pcie_fifo_dout),
		.full(pio_ch3_2pcie_fifo_full),
		.empty(pio_ch3_2pcie_fifo_empty)
	);

endmodule
