`timescale 1ns / 1ps
/////////////////////////////////////
//
// EXTEN_LIB.v
//
// This file is part of EPEE project (version 2.0)
// http://cecaraw.pku.edu.cn
// 
// Description
// The top module of the demo's extension library. It includes UCR (PIO), DMA and UDI
// clock domain switch.
//
// Author(s):
//   - Jian Gong, jian.gong@pku.edu.cn
//
// History:
//
//////////////////////////////////////
//
// Copyright (c) 2013, Center for Energy-Efficient Computing and Applications,
// Peking University. All rights reserved.
//
// The FreeBSD license
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials
//    provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE EPEE PROJECT ``AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// EPEE PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation
// are those of the authors and should not be interpreted as representing
// official policies, either expressed or implied, of the EPEE Project.
//
//////////////////////////////////////

module EXTEN_LIB(
	// clock
	input trn_clk, 
	// reset
	input usr_rst_clk,
	input trn_reset_n,
	input RST_USR, 
	output RST_USR_done,
	output usr_rst,
	// PIO
	input usr_pio_clk,
		// from PCIe system
	input  [16:0] usr_pio_wr_addr,
	input  [31:0] usr_pio_wr_data,
	input  usr_pio_wr_req,
	output usr_pio_wr_ack,
	input  [16:0] usr_pio_rd_addr,
	output [31:0] usr_pio_rd_data,
	input  usr_pio_rd_req,
	output usr_pio_rd_ack,
		// to user: channel 0, addr[16:15] = 2'b00
	output [14:0] usr_pio_ch0_wr_addr,
	output [31:0] usr_pio_ch0_wr_data,
	output usr_pio_ch0_wr_req,
	input  usr_pio_ch0_wr_ack,
	output [14:0] usr_pio_ch0_rd_addr,
	input  [31:0] usr_pio_ch0_rd_data,
	output usr_pio_ch0_rd_req,
	input  usr_pio_ch0_rd_ack,	
	
	// DMA
		// DMA host2board
	input usr_host2board_clk,
	output [129:0] usr_host2board_dout,
	output usr_host2board_empty,
	output usr_host2board_almost_empty,
	input  usr_host2board_rd_en,
	input  RST_HOST2BOARD,
	input  [129:0] FIFO_host2board_dout,
	input  FIFO_host2board_empty,
	output FIFO_host2board_rd_en,
		// DMA board2host
	input  usr_board2host_clk,
	input  [129:0] usr_board2host_din,
	output usr_board2host_prog_full,
	input  usr_board2host_wr_en,
	input  RST_BOARD2HOST,
	output [129:0] FIFO_board2host_din,
	input  FIFO_board2host_prog_full,
	output FIFO_board2host_wr_en,
	// user interrupt
	input usr_interrupt_clk,
		// from PCIe system
	output usr_int_req,
	output [2:0] usr_int_vector,
	input  [7:0] usr_int_sw_waiting,
	input  usr_int_clr,
	input  usr_int_enable,
		// user interrupt 0 (vector = 0)
	input  usr0_int_req,
	output usr0_int_clr,
	output usr0_int_enable,
		// user interrupt 1 (vector = 1)
	input  usr1_int_req,
	output usr1_int_clr,
	output usr1_int_enable
);

	RESET_GEN RESET_GEN(
		.trn_clk(trn_clk),
		.usr_rst_clk(usr_rst_clk),
		.trn_reset_n(trn_reset_n),
		.RST_USR(RST_USR),
		.RST_USR_done(RST_USR_done),
		.usr_rst(usr_rst)
	);

	PIO_CLK_SWITCH PIO_CLK_SWITCH(
		.trn_clk(trn_clk),
		.usr_clk(usr_pio_clk),
		.reset(~trn_reset_n || RST_USR),
		/*PIO bus, pcie clock domain*/
		.usr_pio_wr_addr(usr_pio_wr_addr),
		.usr_pio_wr_data(usr_pio_wr_data),
		.usr_pio_wr_req(usr_pio_wr_req),
		.usr_pio_wr_ack(usr_pio_wr_ack),
		.usr_pio_rd_addr(usr_pio_rd_addr),
		.usr_pio_rd_data(usr_pio_rd_data),
		.usr_pio_rd_req(usr_pio_rd_req),
		.usr_pio_rd_ack(usr_pio_rd_ack),
		/*PIO fifo, user clock domain*/
		// channel 0, addr[16:15] = 2'b00
		.usr_pio_ch0_wr_addr(usr_pio_ch0_wr_addr), 
		.usr_pio_ch0_wr_data(usr_pio_ch0_wr_data), 
		.usr_pio_ch0_wr_req(usr_pio_ch0_wr_req), 
		.usr_pio_ch0_wr_ack(usr_pio_ch0_wr_ack), 
		.usr_pio_ch0_rd_addr(usr_pio_ch0_rd_addr), 
		.usr_pio_ch0_rd_data(usr_pio_ch0_rd_data), 
		.usr_pio_ch0_rd_req(usr_pio_ch0_rd_req), 
		.usr_pio_ch0_rd_ack(usr_pio_ch0_rd_ack), 
		// channel 1, addr[16:15] = 2'b01
		.usr_pio_ch1_wr_addr(), 
		.usr_pio_ch1_wr_data(), 
		.usr_pio_ch1_wr_req(), 
		.usr_pio_ch1_wr_ack(1'b1), 
		.usr_pio_ch1_rd_addr(), 
		.usr_pio_ch1_rd_data(32'hFFFFFFFF), 
		.usr_pio_ch1_rd_req(), 
		.usr_pio_ch1_rd_ack(1'b1), 
		// channel 2, addr[16:15] = 2'b10
		.usr_pio_ch2_wr_addr(), 
		.usr_pio_ch2_wr_data(), 
		.usr_pio_ch2_wr_req(), 
		.usr_pio_ch2_wr_ack(1'b1), 
		.usr_pio_ch2_rd_addr(), 
		.usr_pio_ch2_rd_data(32'hFFFFFFFF), 
		.usr_pio_ch2_rd_req(), 
		.usr_pio_ch2_rd_ack(1'b1), 
		// channel 3, addr[16:15] = 2'b11
		.usr_pio_ch3_wr_addr(), 
		.usr_pio_ch3_wr_data(), 
		.usr_pio_ch3_wr_req(), 
		.usr_pio_ch3_wr_ack(1'b1), 
		.usr_pio_ch3_rd_addr(), 
		.usr_pio_ch3_rd_data(32'hFFFFFFFF), 
		.usr_pio_ch3_rd_req(), 
		.usr_pio_ch3_rd_ack(1'b1)
	);
	
	DMA_HOST2BOARD_CLK_SWITCH DMA_HOST2BOARD_CLK_SWITCH(
		.trn_clk(trn_clk), 
		.usr_host2board_clk(usr_host2board_clk),
		.reset(~trn_reset_n || RST_HOST2BOARD),
		.usr_host2board_dout(usr_host2board_dout),
		.usr_host2board_empty(usr_host2board_empty),
		.usr_host2board_almost_empty(usr_host2board_almost_empty),
		.usr_host2board_rd_en(usr_host2board_rd_en),
		.FIFO_host2board_dout(FIFO_host2board_dout), 
		.FIFO_host2board_empty(FIFO_host2board_empty), 
		.FIFO_host2board_rd_en(FIFO_host2board_rd_en)
	);
	
	DMA_BOARD2HOST_CLK_SWITCH DMA_BOARD2HOST_CLK_SWITCH(
		.trn_clk(trn_clk), 
		.usr_board2host_clk(usr_board2host_clk),
		.reset(~trn_reset_n || RST_BOARD2HOST),
		.usr_board2host_din(usr_board2host_din),
		.usr_board2host_prog_full(usr_board2host_prog_full),
		.usr_board2host_wr_en(usr_board2host_wr_en),
		.FIFO_board2host_din(FIFO_board2host_din), 
		.FIFO_board2host_prog_full(FIFO_board2host_prog_full), 
		.FIFO_board2host_wr_en(FIFO_board2host_wr_en)
	);
	
	USR_INT_MANAGER USR_INT_MANAGER(
		// TO PCIe communication lib, in PCIe clock domain
		.sys_clk(trn_clk),
		.reset(~trn_reset_n || RST_USR),
		.usr_int_req(usr_int_req),
		.usr_int_vector(usr_int_vector),
		.usr_int_sw_waiting(usr_int_sw_waiting),
		.usr_int_clr(usr_int_clr),
		.usr_int_enable(usr_int_enable),
		// To user(), in user clock domain
		.usr_clk(usr_interrupt_clk),
		// user interrupt 0 (vector = 0)
		.usr0_int_req(usr0_int_req),
		.usr0_int_clr(usr0_int_clr),
		.usr0_int_enable(usr0_int_enable),
		// user interrupt 1 (vector = 1)
		.usr1_int_req(usr1_int_req),
		.usr1_int_clr(usr1_int_clr),
		.usr1_int_enable(usr1_int_enable)
	);
	
endmodule
